/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package DB;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import java.util.Date;

/**
 *
 * @author axioo
 */
@DatabaseTable(tableName = "person", daoClass = PersonDao.class)
//@DatabaseTable(tableName = "person")
public class Person {
    @DatabaseField(id = true)
    public String person_id;
    @DatabaseField
    public String id;
    @DatabaseField
    public int father;
    @DatabaseField
    public int mother;
    @DatabaseField
    public int sex;
    @DatabaseField
    public int age;
    @DatabaseField
    public int ageProx;
    @DatabaseField
    public int ageBreast;
    @DatabaseField
    public int ageBreastProx;
    @DatabaseField
    public int ageOvary;
    @DatabaseField
    public int ageOvaryProx;
    @DatabaseField
    public int ageColorectal;
    @DatabaseField
    public int ageColorectalProx;
    @DatabaseField
    public int ageOnset;
    @DatabaseField
    public int ageOnsetProx;
    @DatabaseField
    public int age_menopause;
    @DatabaseField
    public int age_menopauseProx;
    @DatabaseField
    public int ageDeath;
    @DatabaseField
    public int ageDeathProx;
    @DatabaseField
    public int agefirst;
    @DatabaseField
    public int agefirstProx;
    @DatabaseField
    public int affection;
    @DatabaseField
    public int dead;
    @DatabaseField
    public int proband;
    @DatabaseField
    public int isBreast;
    @DatabaseField
    public int isOvary;
    @DatabaseField
    public int isColorectal;
    @DatabaseField
    public String name;
    @DatabaseField
    public int geneticTest;
    @DatabaseField
    public int degree;
    @DatabaseField
    public int bilateralitas;
    @DatabaseField
    public int age_bilateral;
    @DatabaseField
    public int testResult;
    @DatabaseField
    public int laktasi;
    @DatabaseField
    public int menarche;
    @DatabaseField
    public int menscycle;
    @DatabaseField
    public int menopause;
    @DatabaseField
    public float weight;
    @DatabaseField
    public float height;
    @DatabaseField
    public int parity;
    @DatabaseField
    public int birthcontroltype;
    @DatabaseField
    public int birthcontrollenght;
    @DatabaseField
    public int smoking;
    @DatabaseField
    public int alcohol;
    @DatabaseField
    public int hrt;
    @DatabaseField
    public int stadium;
    @DatabaseField
    public String stadiumT;
    @DatabaseField
    public String stadiumN;
    @DatabaseField
    public String stadiumM;
    @DatabaseField
    public int mamografi;
    @DatabaseField
    public int maleCancer;
    @DatabaseField
    public int otherCancer;
    @DatabaseField
    public String descOtherCancer;
    @DatabaseField
    public String brca1txt;
    @DatabaseField
    public String brca2txt;
    @DatabaseField
    public String p53txt;
    @DatabaseField
    public String ptentxt;
    @DatabaseField
    public String chemotherapyNote;
    @DatabaseField
    public String targetedtherapyNote;
    @DatabaseField
    public int typeHistopathology;
    @DatabaseField
    public String typeOtherHistopathology;
    @DatabaseField
    public int radiotherapy;
    @DatabaseField
    public int hormonaltherapy;
    @DatabaseField
    public int biopsi;
    @DatabaseField
    public int numBiopsi;
    @DatabaseField
    public int adh;
    @DatabaseField
    public int brca1;
    @DatabaseField
    public int brca2;
    @DatabaseField
    public int p53;
    @DatabaseField
    public int pten;
    @DatabaseField
    public int CHEK2;
    @DatabaseField
    public int ATM;
    @DatabaseField
    public int er;
    @DatabaseField
    public int pr;
    @DatabaseField
    public int her2;
    @DatabaseField
    public int ck56;
    @DatabaseField
    public int ck14;
    @DatabaseField
    public int bct;
    @DatabaseField
    public int TestOrder;
    @DatabaseField
    public int Mastectomy;
    @DatabaseField
    public int AgeMastectomy;
    @DatabaseField
    public int Oophorectomy;
    @DatabaseField
    public int AgeOophorectomy;
    @DatabaseField
    public int hormonaltherapyYes;
    @DatabaseField
    public int dna;
    @DatabaseField
    public int laidOut;
    @DatabaseField
    public int root;
    @DatabaseField
    public int generation;
    @DatabaseField
    public int mamodensgrade;
    @DatabaseField
    public int targetedtherapy;
    @DatabaseField
    public int multiCancerSyndrome;
    @DatabaseField
    public int numberAffectedFamily;
    @DatabaseField
    public int gradeHistopathology;
    @DatabaseField
    public int prophylacticBilateralMactectomy;
    @DatabaseField
    public int prophylacticBilateralSalphingoOophorectomy;
    @DatabaseField
    public int chemotherapy;
    @DatabaseField
    public Date Birth_Date;
    @DatabaseField
    public int hrtYears;
    @DatabaseField
    public int erly_onset;
    @DatabaseField
    public int usg;
    @DatabaseField
    public String birads;
    @DatabaseField
    public String etnik;
    @DatabaseField
    public String EtnikOther;
//    @DatabaseField(foreign = true, columnName = "id_genotype")
//    public Genotype geno;
//    @DatabaseField(foreign = true, columnName = "id_listped")
//    public ListPed listPed;
    @DatabaseField
    public String id_listped;
    public Person() {
        this.person_id = java.util.UUID.randomUUID().toString();
    }
    public Person(ListPed list) {
        this.person_id = java.util.UUID.randomUUID().toString();
        this.id_listped = list.id;
    }
}
