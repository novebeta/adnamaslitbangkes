/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package DB;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import java.util.Date;

/**
 *
 * @author axioo
 */
@DatabaseTable(tableName = "Customers", daoClass = CustomersDao.class)
//@DatabaseTable(tableName = "Customers")
public class Customers {
    public static String ID_CUSTOMERS = "Id_Customers";
    @DatabaseField(id = true)
    public String Id_Customers;
    @DatabaseField
    public String No_Rekam_Medik;
    @DatabaseField(unique = true)
    public String Name;
    @DatabaseField
    public String AcronymName;
    @DatabaseField
    public String Home_Phone;
    @DatabaseField
    public String Mobile_Phone;
    @DatabaseField
    public String Address;
    @DatabaseField
    public String City;
    @DatabaseField
    public String Province;
    @DatabaseField
    public String Zip_Postal;
    @DatabaseField
    public String Country;
    @DatabaseField
    public String Notes;
    @DatabaseField
    public String Create_Date;
    @DatabaseField
    public Date Birth_Date;
    @DatabaseField
    public String Directory;
    @DatabaseField
    public String ID_Number;
    @DatabaseField
    public String Etnik;
    @DatabaseField
    public String EtnikOther;
    @DatabaseField
    public String prov1;
    @DatabaseField
    public String prov2;
    @DatabaseField
    public String kab1;
    @DatabaseField
    public String kab2;
    @DatabaseField
    public String kec1;
    @DatabaseField
    public String kec2;
    @DatabaseField
    public String kec3;
    @DatabaseField
    public String desa1;
    @DatabaseField
    public String desa2;
    @DatabaseField
    public String desa3;
    @DatabaseField
    public String dk;
    @DatabaseField
    public String bloksensus1;
    @DatabaseField
    public String bloksensus2;
    @DatabaseField
    public String bloksensus3;
    @DatabaseField
    public String bloksensus4;
    @DatabaseField
    public String kodesample1;
    @DatabaseField
    public String kodesample2;
    @DatabaseField
    public String kodesample3;
    @DatabaseField
    public String kodesample4;
    @DatabaseField
    public String sensus1;
    @DatabaseField
    public String sensus2;
    @DatabaseField
    public String sensus3;
    @DatabaseField
    public String rt1;
    @DatabaseField
    public String rt2;
    public Customers() {
        this.Id_Customers = java.util.UUID.randomUUID().toString();
    }
}
