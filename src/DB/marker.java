/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package DB;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 *
 * @author axioo
 */
@DatabaseTable(tableName = "marker", daoClass = markerDao.class)
public class marker {

    @DatabaseField(generatedId = true)
    public int id;
    @DatabaseField
    public String nama_;
    @DatabaseField
    public float value_;

    public marker() {
    }

    public marker(String nama, float val) {
        nama_ = nama;
        value_ = val;
    }
}
