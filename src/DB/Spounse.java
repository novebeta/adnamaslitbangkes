/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package DB;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 *
 * @author axioo
 */
@DatabaseTable(tableName = "spounse", daoClass = SpounseDao.class)
//@DatabaseTable(tableName = "spounse")
public class Spounse {
    @DatabaseField(generatedId = true)
    public int id;
    @DatabaseField
    public int id_spounses;
//    @DatabaseField(foreign = true, columnName = "id_person")
//    public Person pedigree;
    @DatabaseField
    public String id_person;
    public Spounse() {
    }
    public Spounse(Person person,int id) {
        id_person = person.person_id;
        id_spounses = id;
    }
}
