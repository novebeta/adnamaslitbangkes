/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package DB;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pedigree.Utils;

/**
 *
 * @author axioo
 */
public class ListPedDao extends BaseDaoImpl<ListPed, Integer> {
    private PersonDao pedigreeDao;
//    public ListPedDao(ConnectionSource cs) throws SQLException {
//        super(cs, ListPed.class);
//        TableUtils.createTableIfNotExists(connectionSource, ListPed.class);
//        pedigreeDao = Utils.personDao;
//    }
    public ListPedDao(ConnectionSource cs, PersonDao pd) throws SQLException {
        super(cs, ListPed.class);
        TableUtils.createTableIfNotExists(connectionSource, ListPed.class);
//        this.pedigreeDao = clientDao;
        pedigreeDao = pd;
    }
    public void performDBOperations(ConnectionSource connectionSource)
            throws SQLException {
    }
    @Override
    public int delete(ListPed listPed) {
        try {
//            PersonDao pedigreeDao = new PersonDao(connectionSource);
//            DeleteBuilder<Person, Integer> deleteBuilder = pedigreeDao.deleteBuilder();
//            deleteBuilder.where().eq("id_listped", listPed.id);
//            deleteBuilder.delete();
            QueryBuilder<Person, Integer> queryBuilder = pedigreeDao.queryBuilder();
            queryBuilder.where().eq("id_listped", listPed.id);
            List<Person> persons = queryBuilder.query();
//            pedigreeDao.delete(persons);
            for (Person next : persons) {
                pedigreeDao.delete(next);
            }
            return super.delete(listPed);
        } catch (SQLException ex) {
            Logger.getLogger(ListPedDao.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }
}
