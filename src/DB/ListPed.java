/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package DB;


import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
/**
 *
 * @author axioo
 */
@DatabaseTable(tableName = "listped", daoClass = ListPedDao.class)
//@DatabaseTable(tableName = "listped")
public class ListPed {
    @DatabaseField(id = true)
    public String id;
    @DatabaseField            
    public String pedname;
    @DatabaseField            
    public String Id_Customers;
    
//    @DatabaseField(foreign = true, columnName = "id_pedigree")
//    public Pedigree pedigree;
    public ListPed(){
        this.id = java.util.UUID.randomUUID().toString();
    }
    
    public ListPed(String name){
        this.id = java.util.UUID.randomUUID().toString();
        pedname = name;        
    //    pedigree = ped;
    }
}
