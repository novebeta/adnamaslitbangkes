/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package DB;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import java.sql.SQLException;

public class FormulaDao extends BaseDaoImpl<Formula, Integer> {

    public FormulaDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, Formula.class);
        TableUtils.createTableIfNotExists(connectionSource, Formula.class);
    }
}
