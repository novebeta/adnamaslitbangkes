/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package DB;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableInfo;
import com.j256.ormlite.table.TableUtils;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pedigree.Utils;

/**
 *
 * @author axioo
 */
public class CustomersDao extends BaseDaoImpl<Customers, Integer> {
    private ListPedDao ListPedDao;
//    public CustomersDao(ConnectionSource connectionSource) throws SQLException {
//        super(connectionSource, Customers.class);
//        TableUtils.createTableIfNotExists(connectionSource, Customers.class);
//        ListPedDao = Utils.listpedDao;
//    }
    public CustomersDao(ConnectionSource connectionSource, ListPedDao lp) throws SQLException {
        super(connectionSource, Customers.class);
        TableUtils.createTableIfNotExists(connectionSource, Customers.class);
        TableInfo ti;
        ti = new TableInfo(connectionSource, this, Customers.class);
        Dao<Customers, String> dao
                = DaoManager.createDao(connectionSource, Customers.class);
//        if (!this.hasColumn("AcronymName")) {
//            dao.executeRaw("ALTER TABLE `customer` ADD COLUMN AcronymName VARCHAR;");
//        }
        ListPedDao = lp;
    }
    public void performDBOperations(ConnectionSource connectionSource)
            throws SQLException {
    }
    public boolean hasColumn(String columnName) {
        GenericRawResults<String[]> results;
        try {
            results = this.queryRaw("SELECT " + columnName + " FROM customer");
        } catch (SQLException ex) {
            return false;
        }
        return true;
    }
    @Override
    public int delete(Customers cust) {
        try {
//            PersonDao personDao;
//            GenotypeDao genotypeDao;
//            SpounseDao spounseDao;
//            genotypeDao = new GenotypeDao(connectionSource);
//            spounseDao = new SpounseDao(connectionSource);
//            personDao = new PersonDao(connectionSource);
//            ListPedDao = new ListPedDao(connectionSource);
//            DeleteBuilder<ListPed, Integer> deleteBuilder = ListPedDao.deleteBuilder();
//            deleteBuilder.where().eq(Customers.ID_CUSTOMERS, cust.Id_Customers);
//            deleteBuilder.delete();
            QueryBuilder<ListPed, Integer> queryBuilder = ListPedDao.queryBuilder();
            queryBuilder.where().eq(Customers.ID_CUSTOMERS, cust.Id_Customers);
            List<ListPed> listPeds = queryBuilder.query();
//            ListPedDao.delete(listPeds);
            for (ListPed next : listPeds) {
                //                QueryBuilder<Person, Integer> queryBuilder1 = personDao.queryBuilder();
//                queryBuilder1.where().eq("id_listped", next.id);
//                List<Person> persons = queryBuilder1.query();
//                for (Iterator<Person> iterator1 = persons.iterator(); iterator1.hasNext();) {
//                    Person next1 = iterator1.next();
//                    QueryBuilder<Genotype, Integer> queryBuilder2 = genotypeDao.queryBuilder();
//                    queryBuilder2.where().eq("id_person", next1.person_id);
//                    List<Genotype> genotypes = queryBuilder2.query();
//                    genotypeDao.delete(genotypes);
//                    QueryBuilder<Spounse, Integer> queryBuilder3 = spounseDao.queryBuilder();
//                    queryBuilder3.where().eq("id_person", next1.person_id);
//                    List<Spounse> spounses = queryBuilder3.query();
//                    spounseDao.delete(spounses);
//                    personDao.delete(next1);
//                }
                ListPedDao.delete(next);
            }
            return super.delete(cust);
        } catch (SQLException ex) {
            Logger.getLogger(ListPedDao.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }
}
