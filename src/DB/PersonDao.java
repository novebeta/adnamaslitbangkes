/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package DB;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableInfo;
import com.j256.ormlite.table.TableUtils;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pedigree.Utils;

/**
 *
 * @author axioo
 */
public class PersonDao extends BaseDaoImpl<Person, Integer> {
    private GenotypeDao genotypeDao;
    private SpounseDao spounsesDao;
//    public PersonDao(ConnectionSource cs) throws SQLException {
//        super(cs, Person.class);
//        TableUtils.createTableIfNotExists(connectionSource, Person.class);
//        genotypeDao = Utils.genotypeDao;
//        spounsesDao = Utils.spounsesDao;
//    }
    public PersonDao(ConnectionSource cs, GenotypeDao gd, SpounseDao sd) throws SQLException {
        super(cs, Person.class);
        TableUtils.createTableIfNotExists(connectionSource, Person.class);
        TableInfo ti;
        ti = new TableInfo(cs, this, Person.class);
        Dao<Person, String> dao
                = DaoManager.createDao(cs, Person.class);
//        if (!this.hasColumn("brca1")) {
//            dao.executeRaw("ALTER TABLE `person` ADD COLUMN brca1 INTEGER;");
//        }
//        if (!this.hasColumn("brca2")) {
//            dao.executeRaw("ALTER TABLE `person` ADD COLUMN brca2 INTEGER;");
//        }
//        if (!this.hasColumn("p53")) {
//            dao.executeRaw("ALTER TABLE `person` ADD COLUMN p53 INTEGER;");
//        }
//        if (!this.hasColumn("er")) {
//            dao.executeRaw("ALTER TABLE `person` ADD COLUMN er INTEGER;");
//        }
//        if (!this.hasColumn("pr")) {
//            dao.executeRaw("ALTER TABLE `person` ADD COLUMN pr INTEGER;");
//        }
//        if (!this.hasColumn("her2")) {
//            dao.executeRaw("ALTER TABLE `person` ADD COLUMN her2 INTEGER;");
//        }
//        if (!this.hasColumn("ck56")) {
//            dao.executeRaw("ALTER TABLE `person` ADD COLUMN ck56 INTEGER;");
//        }
//        if (!this.hasColumn("ck14")) {
//            dao.executeRaw("ALTER TABLE `person` ADD COLUMN ck14 INTEGER;");
//        }
//        if (!this.hasColumn("Birth_Date")) {
//            dao.executeRaw("ALTER TABLE `person` ADD COLUMN Birth_Date INTEGER;");
//        }
//        if (!this.hasColumn("hrtYears")) {
//            dao.executeRaw("ALTER TABLE `person` ADD COLUMN hrtYears INTEGER;");
//        }
        this.genotypeDao = gd;
        this.spounsesDao = sd;
    }
    public void performDBOperations(ConnectionSource connectionSource)
            throws SQLException {
    }
    @Override
    public int delete(Person pedigree) {
        try {
//            GenotypeDao genotypeDao = new GenotypeDao(connectionSource);
//            SpounseDao spounsesDao = new SpounseDao(connectionSource);
//            DeleteBuilder<Genotype, Integer> deleteBuilder = genotypeDao.deleteBuilder();
//            deleteBuilder.where().eq("id_person", pedigree.person_id);
//            deleteBuilder.delete();
            QueryBuilder<Genotype, Integer> queryBuilder = genotypeDao.queryBuilder();
            queryBuilder.where().eq("id_person", pedigree.person_id);
            List<Genotype> genotypes = queryBuilder.query();
            genotypeDao.delete(genotypes);
//            for (Iterator<Genotype> iterator = genotypes.iterator(); iterator.hasNext();) {
//                Genotype next = iterator.next();
//                genotypeDao.delete(next);
//            }
//            DeleteBuilder<Spounse, Integer> spodeleteBuilder = spounsesDao.deleteBuilder();
//            spodeleteBuilder.where().eq("id_person", pedigree.person_id);
//            spodeleteBuilder.delete();//            
            QueryBuilder<Spounse, Integer> queryBuilder1 = spounsesDao.queryBuilder();
            queryBuilder1.where().eq("id_person", pedigree.person_id);
            List<Spounse> spounses = queryBuilder1.query();
            spounsesDao.delete(spounses);
//            for (Iterator<Spounse> iterator = spounses.iterator(); iterator.hasNext();) {
//                Spounse next = iterator.next();
//                spounsesDao.delete(next);
//            }
            return super.delete(pedigree);
        } catch (SQLException ex) {
            Logger.getLogger(PersonDao.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }
    public boolean hasColumn(String columnName) {
        GenericRawResults<String[]> results;
        try {
            results = this.queryRaw("SELECT " + columnName + " FROM person");
        } catch (SQLException ex) {
            return false;
        }
        return true;
    }
}
