/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package DB;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 *
 * @author axioo
 */
@DatabaseTable(tableName = "general", daoClass = GeneralDao.class)
public class General {

    @DatabaseField(generatedId = true)
    public int id;
    @DatabaseField
    public String nama_;
    @DatabaseField
    public String value_;

    public General() {
    }

    public General(String nama, String val) {
        nama_ = nama;
        value_ = val;
    }
}
