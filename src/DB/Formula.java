/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package DB;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "formula", daoClass = FormulaDao.class)
public class Formula {

    @DatabaseField(generatedId = true)
    public int id;
    @DatabaseField
    public String nama_;
    @DatabaseField
    public String value_;

    public Formula() {
    }

    public Formula(String nama, String val) {
        nama_ = nama;
        value_ = val;
    }
}
