/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package DB;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import java.sql.SQLException;

/**
 *
 * @author axioo
 */
public class markerDao extends BaseDaoImpl<marker, Integer> {

    public markerDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, marker.class);
        TableUtils.createTableIfNotExists(connectionSource, marker.class);
    }
}
