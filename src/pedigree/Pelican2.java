/**
 * ******************************************************************
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Library General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * @author Copyright (C) Frank Dudbridge
 *
 *******************************************************************
 */
//package uk.ac.mrc.rfcgr;
package pedigree;

import DB.Customers;
import com.tomtessier.scrollabledesktop.JScrollableDesktopPane;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.awt.print.Book;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterJob;
import java.io.*;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import samanda.properties.LanguagesController;

/**
 * @author axioo
 *
 */
public class Pelican2 extends JPanel implements ActionListener {
    private static String versionMessage = "SAMANDA Versi 4.0B";
    public static JFrame frame; // public so applets can access
    public static JMenuBar menuBar;
    public static double MAJOR_VERSION = 5;
    public static double MINOR_VERSION = 0;
    private JPopupMenu popup;
    private JMenu changeMenu;
    private JMenu addMenu;
    private JMenuItem changeGeno;
    private JMenuItem addGenoMenu;
    private JMenuItem delGenoMenu;
    private JMenuItem showLinkageMenu;
    private JMenu affectionGeno;
    private JMenu changeAff;
    private JMenu changeSex;
    private JMenu changeLife;
    private JMenu changeProband;
    private JMenuItem changeName;
    private JMenuItem changeAge;
    private JMenuItem editData;
    private PelicanPerson currentPerson;
    private int currentId;
    private boolean pedHasChanged;
    private String currentDirectory;
    private JPopupMenu popLayer;
    public String probandId;
    public boolean issubmit = false;
    public String filename, probandName;
    public boolean isExit = false;
    public boolean isError = false;
    public boolean isGeneral = false;
    public JMenuItem openMenu; // public so applets can disable
    public JMenuItem saveAsMenu;
    public JMenuItem saveMenu;
    public JMenuItem openCohortMenu;
    public JMenuItem imageMenu;
    public JMenuItem printMenu;
    public JMenuItem fileMenuExit;
    private JMenuItem undoMenu;
    private JMenuItem redoMenu;
    private JToolBar toolbar;
    private JCheckBoxMenuItem slinkFormat;
    private JCheckBoxMenuItem autoLayout;
    private JCheckBoxMenuItem showId;
    private JCheckBoxMenuItem showName;
    private JCheckBoxMenuItem showMarkerNumbers;
    private JMenuItem Parents;
    private JMenuItem Spouse;
    private boolean mergeEnabled;
    private Vector history;
    private int historyPosition;
    private PrinterJob printerJob;
    private String imageFormat;
    private JCheckBox askFormat;
    private Vector displayGeno;
    private int fontAscent;
    private JMenuItem moveLeft; // these will not be displayed
    private JMenuItem moveRight;
    private JMenuItem moveUp;
    private JMenuItem moveDown;
    private HashSet matingList;
    private String CustDir;
    public LanguagesController lc;
    public Customers customer;
    public String cohortTgl;
    public String getCustDir() {
        return CustDir;
    }
    public void setCustDir(String CustDir) {
        this.CustDir = CustDir;
    }
    public static JScrollableDesktopPane parentPane;
    public List<PelicanPerson> firstDegree, secondDegree, secondDegreePaternal, secondDegreeMaternal, thirdDegree, thirdDegreePaternal, thirdDegreeMaternal;
    /*
     * {{{ constructor (popup menu)
     */
    public Pelican2(LanguagesController l) {
        super(new BorderLayout());
//        JToolBar toolBar = new JToolBar("Still draggable");
//        addButtons(toolBar);
//        add(toolBar, BorderLayout.PAGE_START);
//        super(null);
        lc = l;
        setBackground(Color.white);
        filename = "e:/testaja.tmp";
        // set up the popup menu layer
//        JMenuBar b = new JMenuBar();
//        JMenu menu = new JMenu("Menu");
//        b.add(menu);
//        JMenuItem item = new JMenuItem("Menu Item");
//        menu.add(item);
//        setLayout(new BorderLayout());
//        add(b, BorderLayout.PAGE_START);
        // set up the popup menu layer
        ButtonGroup directionGroup = new ButtonGroup();
        popLayer = new JPopupMenu();
        final JRadioButtonMenuItem generalInfo = new JRadioButtonMenuItem(
                "General Info");
        directionGroup.add(generalInfo);
        generalInfo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                isGeneral = generalInfo.isSelected();
                refreshTitle();
                // paint(getGraphics());
                updateDisplay();
            }
        });
        JRadioButtonMenuItem MarkerInfo = new JRadioButtonMenuItem(
                "Marker Info");
        directionGroup.add(MarkerInfo);
        MarkerInfo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                isGeneral = generalInfo.isSelected();
                refreshTitle();
                // paint(getGraphics());
                updateDisplay();
            }
        });
        // popLayer.add(MarkerInfo);
        generalInfo.setSelected(true);
        isGeneral = true;
        JMenu modeMenu = new JMenu("Mode");
        modeMenu.add(generalInfo);
        modeMenu.add(MarkerInfo);
//        popLayer.add(modeMenu);
        // set up the popup menu
        popup = new JPopupMenu();
        addMenu = new JMenu(lc.getWord("general.add"));
        Spouse = new JMenuItem(lc.getWord("general.spouse"));
        addMenu.add(Spouse);
        JMenuItem Sis1 = new JMenuItem("1 " + lc.getWord("general.daughter"));
        addMenu.add(Sis1);
        Sis1.addActionListener(this);
        JMenuItem Sis2 = new JMenuItem("2 " + lc.getWord("general.daughters"));
        popup.add(addMenu);
        addMenu.add(Sis2);
        Sis2.addActionListener(this);
        JMenuItem Bro1 = new JMenuItem("1 " + lc.getWord("general.son"));
        addMenu.add(Bro1);
        Bro1.addActionListener(this);
        JMenuItem Bro2 = new JMenuItem("2 " + lc.getWord("general.sons"));
        addMenu.add(Bro2);
        Bro2.addActionListener(this);
//        JMenuItem Bro3 = new JMenuItem("3 " + lc.getWord("general.sons"));
//        addMenu.add(Bro3);
//        Bro3.addActionListener(this);
//        JMenuItem Sis3 = new JMenuItem("3 " + lc.getWord("general.daughters"));
//        addMenu.add(Sis3);
//        Sis3.addActionListener(this);
//        JMenuItem SpouseBro = new JMenuItem(lc.getWord("general.spouse") + "+" + lc.getWord("general.sons"));
//        addMenu.add(SpouseBro);
//        SpouseBro.addActionListener(this);
//        JMenuItem SpouseSis = new JMenuItem(lc.getWord("general.spouse") + "+" + lc.getWord("general.daughters"));
//        addMenu.add(SpouseSis);
//        SpouseSis.addActionListener(this);
        Spouse.addActionListener(this);
        Parents = new JMenuItem(lc.getWord("general.parents"));
        addMenu.add(Parents);
        Parents.addActionListener(this);
        popup.add(addMenu);
        changeMenu = new JMenu(lc.getWord("general.change"));
        // JMenu changeAff=new JMenu("affection");
        changeAff = new JMenu(lc.getWord("general.affected"));
        JMenuItem AffectedB = new JMenuItem(lc.getWord("general.affectedBreast"));
        changeAff.add(AffectedB);
        AffectedB.addActionListener(this);
        JMenuItem AffectedO = new JMenuItem(lc.getWord("general.affectedOvary"));
        changeAff.add(AffectedO);
        AffectedO.addActionListener(this);
        popup.add(changeAff);
        affectionGeno = new JMenu(lc.getWord("general.affection"));
        JMenuItem Affected = new JMenuItem(lc.getWord("general.affected"));
        affectionGeno.add(Affected);
        Affected.addActionListener(this);
        JMenuItem Unaffected = new JMenuItem(lc.getWord("general.unaffected"));
        affectionGeno.add(Unaffected);
        Unaffected.addActionListener(this);
        JMenuItem Carrier = new JMenuItem(lc.getWord("general.carrier"));
        // affectionGeno.add(Carrier);
        Carrier.addActionListener(this);
        JMenuItem Unknown = new JMenuItem(lc.getWord("general.unknown"));
        affectionGeno.add(Unknown);
        Unknown.addActionListener(this);
        popup.add(affectionGeno);
        // JMenu changeAvail=new JMenu("availability");
        // JMenuItem With = new JMenuItem("With DNA");
        // changeAvail.add(With);
        // With.addActionListener(this);
        // JMenuItem Without = new JMenuItem("Without DNA");
        // changeAvail.add(Without);
        // Without.addActionListener(this);
        // changeMenu.add(changeAvail);
        changeSex = new JMenu(lc.getWord("general.sex"));
        JMenuItem Male = new JMenuItem(lc.getWord("general.male"));
        changeSex.add(Male);
        Male.addActionListener(this);
        JMenuItem Female = new JMenuItem(lc.getWord("general.female"));
        changeSex.add(Female);
        Female.addActionListener(this);
        JMenuItem SexUnknown = new JMenuItem(lc.getWord("general.unknown"));
        // changeSex.add(SexUnknown);
        // as this has the same label as changing affection, write the
        // actionListener here
        SexUnknown.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                currentPerson.sex = PelicanPerson.unknown;
                updateDisplay();
            }
        });
        popup.add(changeSex);
        changeLife = new JMenu(lc.getWord("general.stateAlive"));
        JMenuItem Alive = new JMenuItem(lc.getWord("general.alive"));
        changeLife.add(Alive);
        Alive.addActionListener(this);
        JMenuItem Dead = new JMenuItem(lc.getWord("general.death"));
        changeLife.add(Dead);
        Dead.addActionListener(this);
        popup.add(changeLife);
        changeProband = new JMenu(lc.getWord("general.proband"));
        JMenuItem probandYes = new JMenuItem(lc.getWord("general.yes"));
        changeProband.add(probandYes);
        probandYes.addActionListener(this);
        JMenuItem probandNo = new JMenuItem(lc.getWord("general.no"));
        changeProband.add(probandNo);
        probandNo.addActionListener(this);
        popup.add(changeProband);
        JMenuItem changeId = new JMenuItem(lc.getWord("general.id"));
        // changeMenu.add(changeId);
        changeId.addActionListener(this);
        editData = new JMenuItem(lc.getWord("general.editData"));
        popup.add(editData);
        editData.addActionListener(this);
        changeName = new JMenuItem(lc.getWord("general.changeName"));
        popup.add(changeName);
        changeName.addActionListener(this);
        changeAge = new JMenuItem(lc.getWord("general.changeAge"));
        popup.add(changeAge);
        changeAge.addActionListener(this);
        changeGeno = new JMenuItem(lc.getWord("general.changeGenotype"));
        popup.add(changeGeno);
        changeGeno.addActionListener(this);
        addGenoMenu = new JMenuItem(lc.getWord("general.addGenotype"));
        // addGenoMenu.setMnemonic(KeyEvent.VK_A);
        addGenoMenu.addActionListener(this);
        popup.add(addGenoMenu);
        delGenoMenu = new JMenuItem(lc.getWord("general.deleteGenotype"));
        // delGenoMenu.setMnemonic(KeyEvent.VK_D);
        delGenoMenu.addActionListener(this);
        popup.add(delGenoMenu);
        showLinkageMenu = new JMenuItem(lc.getWord("general.showLinkageFormat"));
        // showLinkageMenu.setMnemonic(KeyEvent.VK_L);
        showLinkageMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showLinkage();
            }
        });
        popup.add(showLinkageMenu);
        // popup.add(changeMenu);
        // JMenuItem merge=new JMenuItem("Merge with...");
        // popup.add(merge);
        // merge.addActionListener(this);
        JMenuItem delete = new JMenuItem(lc.getWord("general.delete"));
        popup.add(delete);
        delete.addActionListener(this);
        addMouseListener(new PopupListener());
        addMouseMotionListener(new dragListener());
        // registered with Swing'x ToolTipManager
        ToolTipManager.sharedInstance().registerComponent(this);
        mergeEnabled = false;
        history = new Vector();
        historyPosition = 0;
        // slinkInput=false;
        // slinkOutput=false;
        // imageFormat="PNG";
        slinkFormat = new JCheckBoxMenuItem("SLINK file format");
        slinkFormat.setSelected(false);
        // slinkFormat.setMnemonic(KeyEvent.VK_S);
        autoLayout = new JCheckBoxMenuItem("Auto layout");
        // autoLayout.setMnemonic(KeyEvent.VK_A);
        autoLayout.setSelected(true);
        autoLayout.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (autoLayout.isSelected()) {
                    pedHasChanged = true;
                    paint(getGraphics());
                }
            }
        });
        showId = new JCheckBoxMenuItem("Display IDs");
        // showId.setMnemonic(KeyEvent.VK_I);
        showId.setSelected(false);
        showId.addActionListener(this);
        showName = new JCheckBoxMenuItem("Display Name");
        showName.setMnemonic(KeyEvent.VK_N);
        showName.setSelected(true);
        showName.addActionListener(this);
        showMarkerNumbers = new JCheckBoxMenuItem("Show marker numbers");
        // showMarkerNumbers.setMnemonic(KeyEvent.VK_M);
        showMarkerNumbers.setSelected(false);
        showMarkerNumbers.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (autoLayout.isSelected()) {
                    pedHasChanged = true;
                    paint(getGraphics());
                }
            }
        });
        imageFormat = "PNG";
        askFormat = null;
        matingList = new HashSet();
        displayGeno = new Vector();
        newPedigree();
        refreshTitle();
    }
    private void SettingPopup() {
        if (showMarkerNumbers.isSelected()) {
            changeMenu.setVisible(false);
            // addMenu.setVisible(false);
            changeGeno.setVisible(true);
            addGenoMenu.setVisible(true);
            delGenoMenu.setVisible(true);
            showLinkageMenu.setVisible(true);
            affectionGeno.setVisible(true);
            changeAff.setVisible(false);
        } else {
            changeMenu.setVisible(true);
            // addMenu.setVisible(true);
            changeGeno.setVisible(false);
            addGenoMenu.setVisible(false);
            delGenoMenu.setVisible(false);
            showLinkageMenu.setVisible(false);
            affectionGeno.setVisible(false);
            changeAff.setVisible(true);
        }
    }
    public JToolBar createToolBar() {
        toolbar = new JToolBar();
        toolbar.setFloatable(false);
        JButton openCohort = new JButton(lc.getWord("general.menuOpenCohort"));
        JButton updateCohort = new JButton(lc.getWord("general.menuUpdateCohort"));
        JButton saveImage = new JButton(lc.getWord("general.menuSaveImage"));
        JButton renumberAll = new JButton(lc.getWord("general.menuRenumberAll"));
        JButton newPedigree = new JButton(lc.getWord("general.menuNewPedigree"));
        JButton newLingkage = new JButton(lc.getWord("general.menuNewLinkage"));
        openCohort.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                openFileHistory();
            }
        });
        updateCohort.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveFileHistory();
            }
        });
        saveImage.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveImage();
            }
        });
        renumberAll.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                renumberAll();
            }
        });
        newPedigree.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                newPedigree();
                paint(getGraphics());
            }
        });
        newLingkage.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                newLinkage();
            }
        });
        toolbar.add(openCohort);
        toolbar.add(new JToolBar.Separator());
        toolbar.add(updateCohort);
        toolbar.add(new JToolBar.Separator());
        toolbar.add(saveImage);
        toolbar.add(new JToolBar.Separator());
        toolbar.add(renumberAll);
        toolbar.add(new JToolBar.Separator());
        toolbar.add(newPedigree);
        toolbar.add(new JToolBar.Separator());
        toolbar.add(newLingkage);
        return toolbar;
    }
    private void refreshTitle() {
        if (isGeneral) {
            // frame.setTitle("Pedigree - General Info");
            showMarkerNumbers.setSelected(false);
        } else {
            // frame.setTitle("Pedigree - Marker Info");
            showMarkerNumbers.setSelected(true);
            while (displayGeno.isEmpty()) {
                addGenotypes();
            }
        }
        if (autoLayout.isSelected()) {
            pedHasChanged = true;
            paint(getGraphics());
        }
    }

    /*
     * }}}
     */

 /*
     * {{{ main menu bar
     */
    /**
     *
     * Create the main menu bar for Pelican2
     *
     */
    public void createMenuBar() {
        JMenuBar menuBar = new JMenuBar();
        JMenu fileMenu = new JMenu(lc.getWord("general.file"));
        // fileMenu.setMnemonic(KeyEvent.VK_F);
        // menuBar.add(fileMenu);
        JMenuItem newMenu = new JMenuItem(lc.getWord("general.newPedigree"));
        // newMenu.setMnemonic(KeyEvent.VK_N);
        newMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                newPedigree();
                paint(getGraphics());
            }
        });
        fileMenu.add(newMenu);
        JMenuItem newLinkage = new JMenuItem(lc.getWord("general.newLinkageFormat"));
        newLinkage.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // showLinkage();
                newLinkage();
            }
        });
        fileMenu.add(newLinkage);
        openMenu = new JMenuItem(lc.getWord("general.mnOpen"));
        // openMenu.setMnemonic(KeyEvent.VK_O);
        openMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                openFile(null);
            }
        });
//        fileMenu.add(openMenu);
        openCohortMenu = new JMenuItem(lc.getWord("general.menuOpenCohort"));
        // saveAsMenu.setMnemonic(KeyEvent.VK_S);
        openCohortMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                openFileHistory();
            }
        });
//        fileMenu.add(openCohortMenu);
        saveAsMenu = new JMenuItem(lc.getWord("general.mnSaveAs"));
        // saveAsMenu.setMnemonic(KeyEvent.VK_S);
        saveAsMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
//                saveFile();
                saveFileSamanda3();
            }
        });
//        fileMenu.add(saveAsMenu);
        saveMenu = new JMenuItem(lc.getWord("general.menuUpdateCohort"));
        // saveAsMenu.setMnemonic(KeyEvent.VK_S);
        saveMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveFileHistory();
            }
        });
//        fileMenu.add(saveMenu);
        // JMenuItem showLinkageMenu = new JMenuItem("Show linkage data...");
        // // showLinkageMenu.setMnemonic(KeyEvent.VK_L);
        // showLinkageMenu.addActionListener(new ActionListener() {
        // @Override
        // public void actionPerformed(ActionEvent e) {
        // showLinkage();
        // }
        // });
        // fileMenu.add(showLinkageMenu);
        printMenu = new JMenu("Print");
        // printMenu.setMnemonic(KeyEvent.VK_P);
        imageMenu = new JMenuItem("file (PNG/JPEG)...");
        printMenu.add(imageMenu);
        // imageMenu.setMnemonic(KeyEvent.VK_F);
        imageMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveImage();
            }
        });
        JMenuItem printerMenu = new JMenuItem("printer (PostScript)...");
        printMenu.add(printerMenu);
        // printerMenu.setMnemonic(KeyEvent.VK_P);
        printerMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                printImage();
            }
        });
        fileMenu.add(printMenu);
//        fileMenu.add(new JSeparator());
        fileMenuExit = new JMenuItem("Exit");
        // fileMenuExit.setMnemonic(KeyEvent.VK_X);
        fileMenuExit.setAccelerator(KeyStroke.getKeyStroke('q'));
        fileMenuExit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        // fileMenu.add(fileMenuExit);
        popLayer.add(fileMenu);
        // Edit menu
        JMenu editMenu = new JMenu("Edit");
        // editMenu.setMnemonic(KeyEvent.VK_E);
        // menuBar.add(editMenu);
        undoMenu = new JMenuItem("Undo");
        // undoMenu.setMnemonic(KeyEvent.VK_U);
        undoMenu.setAccelerator(KeyStroke.getKeyStroke('u'));
        undoMenu.addActionListener(this);
        editMenu.add(undoMenu);
        redoMenu = new JMenuItem("Redo");
        // redoMenu.setMnemonic(KeyEvent.VK_R);
        redoMenu.setAccelerator(KeyStroke.getKeyStroke('r'));
        redoMenu.addActionListener(this);
        editMenu.add(redoMenu);
        JMenuItem clearMenu = new JMenuItem("Clear history");
        // clearMenu.setMnemonic(KeyEvent.VK_C);
        clearMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                history.clear();
                historyPosition = 0;
                savePedigree();
                pedHasChanged = true;
            }
        });
        editMenu.add(clearMenu);
        // editMenu.add(new JSeparator());
        JMenuItem renumberMenu = new JMenuItem("Renumber");
        // renumberMenu.setMnemonic(KeyEvent.VK_N);
        renumberMenu.addActionListener(this);
        editMenu.add(renumberMenu);
        // editMenu.add(new JSeparator());
        popLayer.add(editMenu);
        // View menu for different size symbols
        JMenu viewMenu = new JMenu("View");
        // viewMenu.setMnemonic(KeyEvent.VK_V);
        // menuBar.add(viewMenu);
        JMenuItem view75 = new JMenuItem("75%");
        view75.addActionListener(this);
        viewMenu.add(view75);
        JMenuItem view100 = new JMenuItem("100%");
        view100.addActionListener(this);
        viewMenu.add(view100);
        JMenuItem view150 = new JMenuItem("150%");
        view150.addActionListener(this);
        viewMenu.add(view150);
        JMenuItem zoomIn = new JMenuItem("Zoom in");
        // zoomIn.setMnemonic(KeyEvent.VK_I);
        zoomIn.setAccelerator(KeyStroke.getKeyStroke('Z'));
        zoomIn.addActionListener(this);
        viewMenu.add(zoomIn);
        JMenuItem zoomOut = new JMenuItem("Zoom out");
        // zoomOut.setMnemonic(KeyEvent.VK_O);
        zoomOut.setAccelerator(KeyStroke.getKeyStroke('z'));
        zoomOut.addActionListener(this);
        viewMenu.add(zoomOut);
        JMenuItem vertUp = new JMenuItem("Vertical spacing +");
        vertUp = new JMenuItem("Vertical spacing +");
        // vertUp.setMnemonic(KeyEvent.VK_V);
        vertUp.setAccelerator(KeyStroke.getKeyStroke('v'));
        vertUp.addActionListener(this);
        viewMenu.add(vertUp);
        JMenuItem vertDown = new JMenuItem("Vertical spacing -");
        // vertDown.setMnemonic(KeyEvent.VK_E);
        vertDown.setAccelerator(KeyStroke.getKeyStroke('V'));
        vertDown.addActionListener(this);
        viewMenu.add(vertDown);
        JMenuItem horizUp = new JMenuItem("Horizontal spacing +");
        // horizUp.setMnemonic(KeyEvent.VK_H);
        horizUp.setAccelerator(KeyStroke.getKeyStroke('h'));
        horizUp.addActionListener(this);
        viewMenu.add(horizUp);
        JMenuItem horizDown = new JMenuItem("Horizontal spacing -");
        // horizDown.setMnemonic(KeyEvent.VK_O);
        horizDown.setAccelerator(KeyStroke.getKeyStroke('H'));
        horizDown.addActionListener(this);
        viewMenu.add(horizDown);
        viewMenu.add(new JSeparator());
        JMenuItem refresh = new JMenuItem("Refresh");
        // refresh.setMnemonic(KeyEvent.VK_R);
        refresh.addActionListener(this);
        viewMenu.add(refresh);
        popLayer.add(viewMenu);
        // Options menu
        JMenu optionsMenu = new JMenu("Options");
        // optionsMenu.setMnemonic(KeyEvent.VK_O);
        menuBar.add(optionsMenu);
        optionsMenu.add(autoLayout);
        // optionsMenu.add(showId);
        optionsMenu.add(showName);
        // optionsMenu.add(slinkFormat);
        // optionsMenu.add(showMarkerNumbers);
        JMenuItem genoMenu = new JMenuItem("Show genotypes...");
        // genoMenu.setMnemonic(KeyEvent.VK_G);
        genoMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                selectGenotypes();
            }
        });
        // optionsMenu.add(genoMenu);
        popLayer.add(optionsMenu);
        // Done menu
        JMenu doneMenu = new JMenu("Done");
        // doneMenu.setMnemonic(KeyEvent.VK_D);
        menuBar.add(doneMenu);
        JMenuItem submitMenu = new JMenuItem("Submit");
        submitMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                isError = false;
                //saveDoneFile();
                if (isError) {
                    return;
                }
                issubmit = true;
                isExit = true;
            }
        });
        doneMenu.add(submitMenu);
        // Help menu
        JMenu helpMenu = new JMenu("Help");
        // menuBar.add(helpMenu);
        // helpMenu.setMnemonic(KeyEvent.VK_H);
        JMenuItem helpPelican = new JMenuItem("Pelican help");
        helpMenu.add(helpPelican);
        // helpPelican.setMnemonic(KeyEvent.VK_P);
        helpPelican.addActionListener(this);
        JMenuItem helpAbout = new JMenuItem("About...");
        helpMenu.add(helpAbout);
        // helpAbout.setMnemonic(KeyEvent.VK_A);
        helpAbout.addActionListener(this);
        String javaVersion = System.getProperty("java.vm.version");
        if (javaVersion.startsWith("1.") && javaVersion.charAt(2) < '4') {
            imageMenu.setEnabled(false);
        }
        // return menuBar;
    }

    /*
     * }}}
     */

 /*
     * {{{ (tooltips)
     */
    // Return Cell Label as a Tooltip
    // public String getToolTipText(MouseEvent e)
    // {
    // int x = e.getX();
    // int y = e.getY();
    // Component c = getComponentAt(x,y);
    // if(c != null)
    // {
    // if(c instanceof PelicanMale)
    // return "Display properties of this Male";
    // else if(c instanceof PelicanFemale)
    // return "Display properties of this Female";
    // }
    // return null;
    // }

    /*
     * }}}
     */

 /*
     * {{{ newPedigree
     */
    public void newPedigree() {
        // start out with a single male
//        PelicanData curData = new PelicanData();
//        int a = JOptionPane.showConfirmDialog(this, curData, "Entry data for current", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
//        if (a == JOptionPane.CANCEL_OPTION) {
//            return;
//        }
        removeAll();
        displayGeno.clear();
        currentId = 1;
        PelicanPerson pp = new PelicanPerson(currentId++, PelicanPerson.female, 0, 0);
//        addAtrribute(currentPerson.father, curData.getTxtName(), curData.getAge(),
//                curData.getCekBreast(), curData.getCekOvary(), curData.getAgeBreast(),
//                curData.getAgeOvary());
        add(pp);
        savePedigree();
        pedHasChanged = true;
    }

    /*
     * }}}
     */

 /*
     * {{{ savePedigree
     */
    // save current pedigree in the history, and clear the future
    private void savePedigree() {
        Vector savedPed = new Vector();
        HashMap idMap = new HashMap();
        for (int i = 0; i < getComponentCount(); i++) {
            if (getComponent(i) instanceof PelicanPerson) {
                PelicanPerson p = (PelicanPerson) getComponent(i);
                savedPed.add(new PelicanPerson(p));
                // gww idMap.put(new Integer(p.id),savedPed.size() - 1);
                idMap.put(new String(p.id), savedPed.lastElement());
            }
        }
        for (int i = 0; i < savedPed.size(); i++) {
            PelicanPerson p = (PelicanPerson) savedPed.elementAt(i);
            if (p.father != null) // gww p.father=(PelicanPerson)idMap.get(new
            // Integer(p.father.id));
            {
                p.father = (PelicanPerson) idMap.get(new String(p.father.id));
            }
            if (p.mother != null) // gww p.mother=(PelicanPerson)idMap.get(new
            // Integer(p.mother.id));
            {
                p.mother = (PelicanPerson) idMap.get(new String(p.mother.id));
            }
        }
        savedPed.add(new Vector(displayGeno));
        while (history.size() > historyPosition) {
            history.remove(history.lastElement());
        }
        history.add(savedPed);
        historyPosition++;
    }

    /*
     * }}}
     */

 /*
     * {{{ loadPedigree
     */
    // load pedigree from the history
    private void loadPedigree(Vector savedPed) {
        removeAll();
        HashMap idMap = new HashMap();
        for (int i = 0; i < savedPed.size() - 1; i++) {
            PelicanPerson p = (PelicanPerson) savedPed.get(i);
            PelicanPerson person = new PelicanPerson(p);
            add(person);
            // gww idMap.put(new Integer(p.id),person);
            idMap.put(new String(p.id), person);
        }
        for (int i = 0; i < getComponentCount(); i++) {
            PelicanPerson p = (PelicanPerson) getComponent(i);
            if (p.father != null) // gww p.father=(PelicanPerson)idMap.get(new
            // Integer(p.father.id));
            {
                p.father = (PelicanPerson) idMap.get(new String(p.father.id));
            }
            if (p.mother != null) // gww p.mother=(PelicanPerson)idMap.get(new
            // Integer(p.mother.id));
            {
                p.mother = (PelicanPerson) idMap.get(new String(p.mother.id));
            }
        }
        displayGeno = new Vector((Vector) savedPed.lastElement());
    }

    /*
     * }}}
     */

 /*
     * {{{ updateDisplay
     */
    private void updateDisplay() {
        savePedigree();
        pedHasChanged = true;
        paint(getGraphics());
    }

    /*
     * }}}
     */

 /*
     * {{{ areSpouses
     */
    private boolean areSpouses(PelicanPerson person1, PelicanPerson person2) {
        if (person1 == null || person2 == null) {
            return (false);
        }
        // gww if (matingList.contains(new Dimension(person1.id,person2.id)) ||
        // gww matingList.contains(new Dimension(person2.id,person1.id)))
        // construct unique set entry from the id strings concatenated with a
        // space
        if (matingList.contains(person1.id + " " + person2.id)
                || matingList.contains(person2.id + " " + person1.id)) {
            return (true);
        }
        return (false);
    }
    public PelicanPerson getPelicanPersonByID(String idPerson) {
        for (int i = 0; i < getComponentCount(); i++) {
            if (getComponent(i) instanceof PelicanPerson) {
                PelicanPerson person = (PelicanPerson) getComponent(i);
                if (person.id.equals(idPerson)) {
                    return person;
                }
            }
        }
        return null;
    }
    public String getProbandID() {
        for (int i = 0; i < getComponentCount(); i++) {
            if (getComponent(i) instanceof PelicanPerson) {
                PelicanPerson person = (PelicanPerson) getComponent(i);
                if (person.proband) {
                    return person.id;
                }
            }
        }
        return "";
    }
    public List<PelicanPerson> getFamilySecondDegree(PelicanPerson pp, JProgressBar pb) {
        pb.setPreferredSize(new Dimension(400, 20));
        pb.setString("Working...");
        pb.setStringPainted(true);
        pb.setValue(0);
        pb.setMaximum(getComponentCount());
        List<PelicanPerson> secondDegree = new ArrayList<>();
        for (int i = 0; i < getComponentCount(); i++) {
            try {
                Thread.sleep(10);
                pb.setValue(i);
//                dialog.toFront();
                if (getComponent(i) instanceof PelicanPerson) {
                    PelicanPerson person = (PelicanPerson) getComponent(i);
                    DecimalFormat forD = new DecimalFormat("#.#");
                    pb.setString("Scanning Second Degree Family : " + Double.valueOf(forD.format(pb.getPercentComplete() * 100)) + "%");
                    if (person == pp) {
                        continue;
                    }
                    if (PedFunc.areUncleOrAunt(pp, person)) {
                        secondDegree.add(person);
                        continue;
                    }
                    if (PedFunc.areNephewOrNiece(pp, person)) {
                        secondDegree.add(person);
                        continue;
                    }
                    //FIXME : kalo cucu apakan ada maternal / paternal???
                    if (PedFunc.areGrandParent(pp, person)) {
                        secondDegree.add(person);
                        continue;
                    }
                    if (PedFunc.areGrandChild(pp, person)) {
                        secondDegree.add(person);
                        continue;
                    }
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(Pelican2.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        pb.setValue(pb.getMaximum());
        pb.setString("Done");
        return secondDegree;
    }
    public List<PelicanPerson> getFamilyPateralSecondDegree(PelicanPerson pp, JProgressBar pb) {
        pb.setPreferredSize(new Dimension(400, 20));
        pb.setString("Working...");
        pb.setStringPainted(true);
        pb.setValue(0);
        pb.setMaximum(getComponentCount());
        List<PelicanPerson> secondDegree = new Vector<>();
        for (int i = 0; i < getComponentCount(); i++) {
            try {
                Thread.sleep(10);
                pb.setValue(i);
//                dialog.toFront();
                if (getComponent(i) instanceof PelicanPerson) {
                    PelicanPerson person = (PelicanPerson) getComponent(i);
                    DecimalFormat forD = new DecimalFormat("#.#");
                    pb.setString("Scanning Second Degree Family : " + Double.valueOf(forD.format(pb.getPercentComplete() * 100)) + "%");
                    if (person == pp) {
                        continue;
                    }
                    if (PedFunc.areUncleOrAuntPateral(pp, person)) {
                        secondDegree.add(person);
                        continue;
                    }
                    if (PedFunc.areGrandParentPaternal(pp, person)) {
                        secondDegree.add(person);
                        continue;
                    }
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(Pelican2.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        pb.setValue(pb.getMaximum());
        pb.setString("Done");
        return secondDegree;
    }
    public List<PelicanPerson> getFamilyMateralSecondDegree(PelicanPerson pp, JProgressBar pb) {
        pb.setPreferredSize(new Dimension(400, 20));
        pb.setString("Working...");
        pb.setStringPainted(true);
        pb.setValue(0);
        pb.setMaximum(getComponentCount());
        List<PelicanPerson> secondDegree = new Vector<>();
        for (int i = 0; i < getComponentCount(); i++) {
            try {
                Thread.sleep(10);
                pb.setValue(i);
//                dialog.toFront();
                if (getComponent(i) instanceof PelicanPerson) {
                    PelicanPerson person = (PelicanPerson) getComponent(i);
                    DecimalFormat forD = new DecimalFormat("#.#");
                    pb.setString("Scanning Second Degree Family : " + Double.valueOf(forD.format(pb.getPercentComplete() * 100)) + "%");
                    if (person == pp) {
                        continue;
                    }
                    if (PedFunc.areUncleOrAuntMateral(pp, person)) {
                        secondDegree.add(person);
                        continue;
                    }
                    if (PedFunc.areGrandParentMaternal(pp, person)) {
                        secondDegree.add(person);
                        continue;
                    }
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(Pelican2.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
//        pb.setVisible(false);
//        dialog.dispose();
        pb.setValue(pb.getMaximum());
        pb.setString("Done");
        return secondDegree;
    }
    public List<PelicanPerson> getFamilyMaternalThirdDegree(PelicanPerson pp, JProgressBar pb) {
        pb.setPreferredSize(new Dimension(400, 20));
        pb.setString("Working...");
        pb.setStringPainted(true);
        pb.setValue(0);
        pb.setMaximum(getComponentCount());
        List<PelicanPerson> thirdDegree = new Vector<>();
        for (int i = 0; i < getComponentCount(); i++) {
            try {
                Thread.sleep(10);
                pb.setValue(i);
//                dialog.toFront();
                if (getComponent(i) instanceof PelicanPerson) {
                    PelicanPerson person = (PelicanPerson) getComponent(i);
                    DecimalFormat forD = new DecimalFormat("#.#");
                    pb.setString("Scanning Third Degree Family : " + Double.valueOf(forD.format(pb.getPercentComplete() * 100)) + "%");
                    if (person == pp) {
                        continue;
                    }
                    //TODO: saudara sepupu
                    if (PedFunc.areCousinMaternal(pp, person)) {
                        thirdDegree.add(person);
                        continue;
                    }
                    if (PedFunc.areGreatGrantParentMaternal(pp, person)) {
                        thirdDegree.add(person);
                        continue;
                    }
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(Pelican2.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        pb.setValue(pb.getMaximum());
        pb.setString("Done");
        return thirdDegree;
    }
    public List<PelicanPerson> getFamilyPaternalThirdDegree(PelicanPerson pp, JProgressBar pb) {
        pb.setPreferredSize(new Dimension(400, 20));
        pb.setString("Working...");
        pb.setStringPainted(true);
        pb.setValue(0);
        pb.setMaximum(getComponentCount());
        List<PelicanPerson> thirdDegree = new Vector<>();
        for (int i = 0; i < getComponentCount(); i++) {
            try {
                Thread.sleep(10);
                pb.setValue(i);
//                dialog.toFront();
                if (getComponent(i) instanceof PelicanPerson) {
                    PelicanPerson person = (PelicanPerson) getComponent(i);
                    DecimalFormat forD = new DecimalFormat("#.#");
                    pb.setString("Scanning Third Degree Family : " + Double.valueOf(forD.format(pb.getPercentComplete() * 100)) + "%");
                    if (person == pp) {
                        continue;
                    }
                    //TODO: saudara sepupu
                    if (PedFunc.areCousinPaternal(pp, person)) {
                        thirdDegree.add(person);
                        continue;
                    }
                    if (PedFunc.areGreatGrantParentPaternal(pp, person)) {
                        thirdDegree.add(person);
                        //System.out.println("kakek buyut");
                        continue;
                    }
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(Pelican2.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        pb.setValue(pb.getMaximum());
        pb.setString("Done");
        return thirdDegree;
    }
    public List<PelicanPerson> getFamilyThirdDegree(PelicanPerson pp, JProgressBar pb) {
        pb.setPreferredSize(new Dimension(400, 20));
        pb.setString("Working...");
        pb.setStringPainted(true);
        pb.setValue(0);
        pb.setMaximum(getComponentCount());
        List<PelicanPerson> thirdDegree = new Vector<>();
        for (int i = 0; i < getComponentCount(); i++) {
            try {
                Thread.sleep(10);
                pb.setValue(i);
//                dialog.toFront();
                if (getComponent(i) instanceof PelicanPerson) {
                    PelicanPerson person = (PelicanPerson) getComponent(i);
                    DecimalFormat forD = new DecimalFormat("#.#");
                    pb.setString("Scanning Third Degree Family : " + Double.valueOf(forD.format(pb.getPercentComplete() * 100)) + "%");
                    if (person == pp) {
                        continue;
                    }
                    //TODO: saudara sepupu
                    if (PedFunc.areGreatGrandson(pp, person)) {
                        thirdDegree.add(person);
                        continue;
                    }
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(Pelican2.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        pb.setValue(pb.getMaximum());
        pb.setString("Done");
        return thirdDegree;
    }
    public void AutoDetectionDegree(PelicanPerson pp) {
        if (pp == null) {
            return;
        }
        firstDegree = new Vector<>();
        secondDegree = new Vector<>();
        secondDegreeMaternal = new Vector<>();
        secondDegreePaternal = new Vector<>();
        thirdDegree = new Vector<>();
        thirdDegreeMaternal = new Vector<>();
        thirdDegreePaternal = new Vector<>();
        for (int i = 0; i < getComponentCount(); i++) {
            try {
                if (getComponent(i) instanceof PelicanPerson) {
                    PelicanPerson person = (PelicanPerson) getComponent(i);
                    if (PedFunc.isFirstDegree(pp, person)) {
                        firstDegree.add(person);
                        continue;
                    }
                    if (PedFunc.isSecondDegree(pp, person)) {
                        secondDegree.add(person);
                        continue;
                    }
                    if (PedFunc.isSecondDegreeMaternal(pp, person)) {
                        secondDegreeMaternal.add(person);
                        continue;
                    }
                    if (PedFunc.isSecondDegreePaternal(pp, person)) {
                        secondDegreePaternal.add(person);
                        continue;
                    }
                    if (PedFunc.isThirdDegree(pp, person)) {
                        thirdDegree.add(person);
                        continue;
                    }
                    if (PedFunc.isThirdDegreeMaternal(pp, person)) {
                        thirdDegreeMaternal.add(person);
                        continue;
                    }
                    if (PedFunc.isThirdDegreePaternal(pp, person)) {
                        thirdDegreePaternal.add(person);
                        continue;
                    }
                }
            } catch (Exception ex) {
                Logger.getLogger(Pelican2.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    public List<PelicanPerson> getFamilyFirstDegree(PelicanPerson pp, JProgressBar pb) {
        pb.setPreferredSize(new Dimension(400, 20));
        pb.setString("Working...");
        pb.setStringPainted(true);
        pb.setValue(0);
        pb.setMaximum(getComponentCount());
        List<PelicanPerson> firstdegree = new ArrayList<>();
        for (int i = 0; i < getComponentCount(); i++) {
            try {
                Thread.sleep(10);
                pb.setValue(i);
                //        dialog.toFront();
                if (getComponent(i) instanceof PelicanPerson) {
                    PelicanPerson person = (PelicanPerson) getComponent(i);
                    DecimalFormat forD = new DecimalFormat("#.#");
                    pb.setString("Scanning Second Degree Family : " + Double.valueOf(forD.format(pb.getPercentComplete() * 100)) + "%");
                    if (person == pp) {
                        continue;
                    }
                    if (PedFunc.areParent(pp, person)) {
                        firstdegree.add(person);
                        continue;
                    }
                    if (PedFunc.areSibs(pp, person)) {
                        firstdegree.add(person);
                        continue;
                    }
                    if (PedFunc.areChild(pp, person)) {
                        firstdegree.add(person);
                        continue;
                    }
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(Pelican2.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        pb.setValue(pb.getMaximum());
        pb.setString("Done");
        return firstdegree;
    }
    public int getAffectedFamily(JProgressBar pb) {
        int val = 0;
        pb.setPreferredSize(new Dimension(400, 20));
        pb.setString("Working...");
        pb.setStringPainted(true);
        pb.setValue(0);
        pb.setMaximum(getComponentCount());
        for (int i = 0; i < getComponentCount(); i++) {
            try {
                Thread.sleep(10);
                pb.setValue(i);
                if (getComponent(i) instanceof PelicanPerson) {
                    PelicanPerson person = (PelicanPerson) getComponent(i);
                    if (person.isAffectedBreast == 1 || person.isAffectedOvary == 1 || person.isColorectal == 1) {
                        val++;
                    }
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(Pelican2.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        pb.setValue(pb.getMaximum());
        pb.setString("Done");
        return val;
    }
    /*
     * }}}
     */

 /*
     * {{{ isAncestor
     */
    private boolean isAncestor(PelicanPerson parent, PelicanPerson child) {
        if (child.father == parent || child.mother == parent) {
            return (true);
        }
        if (child.father != null) {
            if (isAncestor(parent, child.father)) {
                return (true);
            }
        }
        if (child.mother != null) {
            if (isAncestor(parent, child.mother)) {
                return (true);
            }
        }
        return (false);
    }

    /*
     * }}}
     */

 /*
     * {{{ addParents
     */
    private void addParents() {
        // Nice idea to move connections to the edge of the sibships, but
        // lots of potential complications. For now, will live with
        // messy node connections...
        // // find the spouse of currentPerson (who will be an orphan)
        // PelicanPerson spouse=null;
        // int spouseIndex=0;
        // for(int i=0;i<getComponentCount();i++)
        // if (getComponent(i) instanceof PelicanPerson) {
        // if (areSpouses((PelicanPerson)getComponent(i),currentPerson)) {
        // spouseIndex=i;
        // spouse=(PelicanPerson)getComponent(i);
        // }
        // }
        // if (spouse!=null) {
        // // find out if sibs of the spouse have non-orphaned spouses
        // int nbranch=0; // number of non-orphaned spouses of sibs
        // int firstsib=-1; // index of first sib of the spouse
        // int lastsib=0; // index of last sib of the spouse
        // for(int i=0;i<getComponentCount();i++)
        // if (getComponent(i) instanceof PelicanPerson) {
        // PelicanPerson person=(PelicanPerson)getComponent(i);
        // if (person.pid==spouse.pid && person.mid==spouse.mid) {
        // // we have a sib, so check its spouses
        // if (firstsib==-1) firstsib=i;
        // lastsib=i;
        // for(int j=0;j<getComponentCount();j++)
        // if (getComponent(j) instanceof PelicanPerson) {
        // PelicanPerson sibSpouse=(PelicanPerson)getComponent(j);
        // if (areSpouses(sibSpouse,person) &&
        // sibSpouse.pid!=PelicanPerson.unknown &&
        // sibSpouse.mid!=PelicanPerson.unknown)
        // nbranch++;
        // }
        // }
        // }
        // System.out.println("nbranch "+String.valueOf(nbranch));
        // // if no other sibs, move sib to the left
        // if (nbranch==0) {
        // remove(spouseIndex);
        // add(spouse,firstsib);
        // }
        // // if one sib, move sib to the right
        // if (nbranch==1) {
        // remove(spouseIndex);
        // add(spouse,lastsib);
        // }
        // }
        // new father
        PelicanData fatData = new PelicanData(lc);
        PelicanData motData = new PelicanData(lc);
        int a;
        a = JOptionPane.showConfirmDialog(this, fatData, lc.getWord("general.entryDataForFather"), JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        if (a == JOptionPane.CANCEL_OPTION) {
            return;
        }
        a = JOptionPane.showConfirmDialog(this, motData, lc.getWord("general.entryDataForMother"), JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        if (a == JOptionPane.CANCEL_OPTION) {
            return;
        }
        currentPerson.father = new PelicanPerson(currentId++,
                PelicanPerson.male, currentPerson.generation - 1,
                countGenotypes());
        //jop.setMessage("Entry data for father");
        //jop.showConfirmDialog(null, new PelicanData());
        PedFunc.addAtrribute(currentPerson.father, fatData.getTxtName(), fatData.getAge(), fatData.getChkAge() ? 1 : 0,
                fatData.getCekBreast(), fatData.getCekOvary(), fatData.getAgeBreast(), fatData.getChkAgeBreast() ? 1 : 0,
                fatData.getAgeOvary(), fatData.getChkAgeOvary() ? 1 : 0);
        add(currentPerson.father);
        //offspring
        currentPerson.father.offsping.add(currentPerson);
        // new mother
        currentPerson.mother = new PelicanPerson(currentId++,
                PelicanPerson.female, currentPerson.generation - 1,
                countGenotypes());
        PedFunc.addAtrribute(currentPerson.mother, motData.getTxtName(), motData.getAge(), motData.getChkAge() ? 1 : 0,
                motData.getCekBreast(), motData.getCekOvary(), motData.getAgeBreast(), motData.getChkAgeBreast() ? 1 : 0,
                motData.getAgeOvary(), motData.getChkAgeOvary() ? 1 : 0);
        add(currentPerson.mother);
        //offspring
        currentPerson.mother.offsping.add(currentPerson);
        updateDisplay();
        renumberAll();
    }

    /*
     * }}}
     */

 /*
     * {{{ addChildren
     */
    private void addChildren(String request) {
        // find spouse for this subject
        PelicanData spoData = new PelicanData(lc);
        PelicanPerson pp;
        int jumlah_anak = Integer.parseInt(request.substring(0, 1));
        PelicanData[] childData = new PelicanData[jumlah_anak];
        PelicanPerson spouse = null;
        boolean haveChild = false;
        for (int i = 0; i < getComponentCount() && spouse == null; i++) {
            if (getComponent(i) instanceof PelicanPerson) {
                PelicanPerson person = (PelicanPerson) getComponent(i);
                if (areSpouses(person, currentPerson)) {
                    spouse = person;
                }
                if (PedFunc.areChild(currentPerson, person)) {
                    haveChild = true;
                }
            }
        }
        if (spouse != null && !haveChild) {
            for (int s = 0; s < currentPerson.spounse.size(); s++) {
                PelicanPerson p = (PelicanPerson) currentPerson.spounse.elementAt(s);
                if (p == spouse) {
                    currentPerson.spounse.remove(s);
                }
            }
            for (int x = 0; x < spouse.spounse.size(); x++) {
                PelicanPerson p = (PelicanPerson) spouse.spounse.elementAt(x);
                if (p == currentPerson) {
                    spouse.spounse.remove(x);
                }
            }
        }
        if (spouse == null) {
            int a = JOptionPane.showConfirmDialog(this, spoData, lc.getWord("general.entryDataForSpouse"), JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
            if (a == JOptionPane.CANCEL_OPTION) {
                return;
            }
        }
        for (int i = 0; i < jumlah_anak; i++) {
            childData[i] = new PelicanData(lc);
            int b = JOptionPane.showConfirmDialog(this, childData[i], lc.getWord("general.entryDataForChild"), JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
            if (b == JOptionPane.CANCEL_OPTION) {
                return;
            }
        }
        // create a spouse
        if (spouse == null) {
            if (currentPerson.sex == PelicanPerson.female) {
                spouse = new PelicanPerson(currentId++, PelicanPerson.male,
                        currentPerson.generation, countGenotypes());
            } else {
                spouse = new PelicanPerson(currentId++, PelicanPerson.female,
                        currentPerson.generation, countGenotypes());
            }
            PedFunc.addAtrribute(spouse, spoData.getTxtName(), spoData.getAge(), spoData.getChkAge() ? 1 : 0,
                    spoData.getCekBreast(), spoData.getCekOvary(), spoData.getAgeBreast(), spoData.getChkAgeBreast() ? 1 : 0,
                    spoData.getAgeOvary(), spoData.getChkAgeOvary() ? 1 : 0);
            add(spouse);
        }
        // add in the children
        int generation = Math.max(currentPerson.generation, spouse.generation);
        for (int i = 0; i < jumlah_anak; i++) {
            if (currentPerson.sex == PelicanPerson.female) {
//                if (request.charAt(2) == 's') {
                if (request.contains("Son") || request.contains("Putra")) {
                    pp = new PelicanPerson(currentId++, spouse, currentPerson,
                            PelicanPerson.male, generation + 1,
                            countGenotypes());
                    add(pp);
                } else {
                    pp = new PelicanPerson(currentId++, spouse, currentPerson,
                            PelicanPerson.female, generation + 1,
                            countGenotypes());
                    add(pp);
                }
            } else //                if (request.charAt(2) == 's') {
            {
                if (request.contains("Son") || request.contains("Putra")) {
                    pp = new PelicanPerson(currentId++, currentPerson, spouse,
                            PelicanPerson.male, generation + 1,
                            countGenotypes());
                    add(pp);
                } else {
                    pp = new PelicanPerson(currentId++, currentPerson, spouse,
                            PelicanPerson.female, generation + 1,
                            countGenotypes());
                    add(pp);
                }
            }
            PedFunc.addAtrribute(pp, childData[i].getTxtName(), childData[i].getAge(), childData[i].getChkAge() ? 1 : 0,
                    childData[i].getCekBreast(), childData[i].getCekOvary(), childData[i].getAgeBreast(), childData[i].getChkAgeBreast() ? 1 : 0,
                    childData[i].getAgeOvary(), childData[i].getChkAgeOvary() ? 1 : 0);
            currentPerson.offsping.add(pp);
            spouse.offsping.add(pp);
        }
        updateDisplay();
        renumberAll();
    }
//    private String[] getFirstDegree()
//    {
//
//    }
    /*
     * }}}
     */

 /*
     * {{{ addSpouse
     */
    private void addJustSpouse() {
        // create a spouse
        PelicanData spoData = new PelicanData(lc);
        int a = JOptionPane.showConfirmDialog(this, spoData, lc.getWord("general.entryDataForSpouse"), JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        if (a == JOptionPane.CANCEL_OPTION) {
            return;
        }
        int spouseId = currentId++;
        PelicanPerson spouse;
        if (currentPerson.sex == PelicanPerson.female) {
            spouse = new PelicanPerson(spouseId, PelicanPerson.male,
                    currentPerson.generation, countGenotypes());
            add(spouse);
            currentPerson.spounse.add(spouse);
        } else {
            spouse = new PelicanPerson(spouseId, PelicanPerson.female,
                    currentPerson.generation, countGenotypes());
            add(spouse);
            currentPerson.spounse.add(spouse);
        }
        PedFunc.addAtrribute(spouse, spoData.getTxtName(), spoData.getAge(), spoData.getChkAge() ? 1 : 0,
                spoData.getCekBreast(), spoData.getCekOvary(), spoData.getAgeBreast(), spoData.getChkAgeBreast() ? 1 : 0,
                spoData.getAgeOvary(), spoData.getChkAgeOvary() ? 1 : 0);
        updateDisplay();
        renumberAll();
    }
    private void addSpouse(String request) {
        // create a spouse
        PelicanData spoData = new PelicanData(lc);
        int a = JOptionPane.showConfirmDialog(this, spoData, lc.getWord("general.entryDataForSpouse"), JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        if (a == JOptionPane.CANCEL_OPTION) {
            return;
        }
        PelicanData childData = new PelicanData(lc);
        int b = JOptionPane.showConfirmDialog(this, childData, lc.getWord("general.entryDataForChild"), JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        if (b == JOptionPane.CANCEL_OPTION) {
            return;
        }
        int spouseId = currentId++;
        PelicanPerson spouse;
        if (currentPerson.sex == PelicanPerson.female) {
            spouse = new PelicanPerson(spouseId, PelicanPerson.male,
                    currentPerson.generation, countGenotypes());
            add(spouse);
        } else {
            spouse = new PelicanPerson(spouseId, PelicanPerson.female,
                    currentPerson.generation, countGenotypes());
            add(spouse);
        }
        PedFunc.addAtrribute(spouse, spoData.getTxtName(), spoData.getAge(), spoData.getChkAge() ? 1 : 0,
                spoData.getCekBreast(), spoData.getCekOvary(), spoData.getAgeBreast(), spoData.getChkAgeBreast() ? 1 : 0,
                spoData.getAgeOvary(), spoData.getChkAgeOvary() ? 1 : 0);
        // add in the child
        PelicanPerson pp;
        if (currentPerson.sex == PelicanPerson.female) {
            if (request.charAt(7) == 's') {
                pp = new PelicanPerson(currentId++, spouse, currentPerson,
                        PelicanPerson.male, currentPerson.generation + 1,
                        countGenotypes());
                add(pp);
            } else {
                pp = new PelicanPerson(currentId++, spouse, currentPerson,
                        PelicanPerson.female, currentPerson.generation + 1,
                        countGenotypes());
                add(pp);
            }
        } else if (request.charAt(7) == 's') {
            pp = new PelicanPerson(currentId++, currentPerson, spouse,
                    PelicanPerson.male, currentPerson.generation + 1,
                    countGenotypes());
            add(pp);
        } else {
            pp = new PelicanPerson(currentId++, currentPerson, spouse,
                    PelicanPerson.female, currentPerson.generation + 1,
                    countGenotypes());
            add(pp);
        }
        spouse.offsping.add(pp);
        currentPerson.offsping.add(pp);
        PedFunc.addAtrribute(pp, childData.getTxtName(), childData.getAge(), childData.getChkAge() ? 1 : 0,
                childData.getCekBreast(), childData.getCekOvary(), childData.getAgeBreast(), childData.getChkAgeBreast() ? 1 : 0,
                childData.getAgeOvary(), childData.getChkAgeOvary() ? 1 : 0);
        updateDisplay();
        renumberAll();
    }

    /*
     * }}}
     */

 /*
     * {{{ changeId
     */
    // swaps the Id'x for two subjects
    // gww private void changeId(int oldId,int newId) {
    private void changeId(String oldId, String newId) {
        for (int i = 0; i < getComponentCount(); i++) {
            if (getComponent(i) instanceof PelicanPerson) {
                PelicanPerson person = (PelicanPerson) getComponent(i);
                // gww if (person.id==oldId) person.id=newId;
                if (person.id.equals(oldId)) {
                    person.id = newId;
                } // gww else if (person.id==newId) person.id=oldId;
                else if (person.id.equals(newId)) {
                    person.id = oldId;
                }
            }
        }
    }
    private void inputChangeId() {
        // gww String
        // newIdString=JOptionPane.showInputDialog(this,"Enter new id for subject "+String.valueOf(currentPerson.id));
        String newIdString = JOptionPane.showInputDialog(this,
                "Enter new id for subject " + currentPerson.id);
        newIdString = newIdString.trim();
        // gww if (newIdString!=null && newIdString.trim().length()>0) {
        if (newIdString != null && newIdString.length() > 0) {
            try {
                // gww int newId=Integer.parseInt(newIdString);
                // gww if (newId<=0)
                // gww throw(new Error("ID must be greater than 0"));
                // more tests for valid string id here
                // not "0" (PelicanPerson.unknownID)
                // no spaces in id
                // no non-alphanum characters
                if (newIdString.equals(PelicanPerson.unknownID)) {
                    throw (new Error("ID must not be '"
                            + PelicanPerson.unknownID + "'"));
                }
                for (int i = 0; i < newIdString.length(); i++) {
                    char c = newIdString.charAt(i);
                    if (Character.isWhitespace(c)) {
                        throw (new Error("ID must not contain a space"));
                    }
                    if (!Character.isLetterOrDigit(c)) {
                        throw (new Error(
                                "ID must only contain letter or digit characters"));
                    }
                }
                // gww if (currentPerson.id!=newId) {
                if (!currentPerson.id.equals(newIdString)) {
                    changeId(currentPerson.id, newIdString);
                    updateDisplay();
                }
            } catch (Throwable t) {
                String message = t.getMessage();
                // gww if (t instanceof NumberFormatException)
                // gww message="Identifier must be a number";
                JOptionPane.showMessageDialog(this, message, "Pedigree error",
                        JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    // renumber the subjects, top-down, left-right
    private void renumberAll() {
        boolean someChange = false;
        for (int i = 0; i < getComponentCount(); i++) {
            if (getComponent(i) instanceof PelicanPerson) {
                PelicanPerson person = (PelicanPerson) getComponent(i);
                int thisId = 0;
                for (int j = 0; j < getComponentCount(); j++) {
                    if (getComponent(j) instanceof PelicanPerson) {
                        PelicanPerson person2 = (PelicanPerson) getComponent(j);
                        if (person2.generation < person.generation
                                || person2.generation == person.generation
                                && person2.getX() <= person.getX()) {
                            thisId++;
                        }
                    }
                }
                // gww if (person.id!=thisId) {
                if (!person.id.equals(String.valueOf(thisId))) {
                    // gww changeId(person.id,thisId);
                    changeId(person.id, String.valueOf(thisId));
                    someChange = true;
                }
            }
        }
        if (someChange) {
            updateDisplay();
        }
    }

    /*
     * }}}
     */

 /*
     * {{{ changeName
     */
    // change a subject'x name
    private void inputChangeName() {
        // gww String
        // newName=JOptionPane.showInputDialog(this,"Enter new name for subject "+String.valueOf(currentPerson.id));
        String newName = JOptionPane.showInputDialog(this,
                lc.getWord("general.inputNewName") + currentPerson.id);
        if (newName != null && newName.trim().length() > 0) {
            try {
                if (!currentPerson.name.equals(newName)) {
                    currentPerson.name = newName;
                    updateDisplay();
                }
            } catch (Throwable t) {
                String message = t.getMessage();
                JOptionPane.showMessageDialog(this, message, "Pedigree error",
                        JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    private void inputChangeAffectedBreast() {
        // gww String
        // newName=JOptionPane.showInputDialog(this,"Enter new name for subject "+String.valueOf(currentPerson.id));
        ChangeAffection ca = new ChangeAffection(lc.getWord("general.breast"), lc);
        ca.setCekBreast(currentPerson.isAffectedBreast == 1);
        ca.setAgeBreast(currentPerson.isAffectedBreast == 1 ? currentPerson.affectedBreast : 0);
        ca.setChkAgeBreast(currentPerson.BreastProx == 1);
        int a = JOptionPane.showConfirmDialog(this, ca,
                "Change affection breast for subject" + currentPerson.id,
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        if (a == JOptionPane.OK_OPTION) {
            if (ca.getCekBreast()) {
                currentPerson.isAffectedBreast = 1;
                currentPerson.affectedBreast = ca.getAgeBreast();
                currentPerson.BreastProx = ca.getChkAgeBreast() ? 1 : 0;
            } else {
                currentPerson.isAffectedBreast = 0;
                currentPerson.affectedBreast = currentPerson.age;
                currentPerson.BreastProx = 0;
            }
        }
//        String newAge = JOptionPane.showInputDialog(this,
//                "Enter age affected breast for subject " + currentPerson.id);
//        try {
//            if (newAge != null && newAge.trim().length() > 0) {
//
//                currentPerson.isAffectedBreast = 1;
//                currentPerson.affectedBreast = Integer.parseInt(newAge);
//                // currentPerson.affection = PelicanPerson.affected;
//            } else {
//                currentPerson.isAffectedBreast = 0;
//                currentPerson.affectedBreast = currentPerson.age;
//            }
        updateDisplay();
//        } catch (Throwable t) {
//            String message = t.getMessage();
//            JOptionPane.showMessageDialog(this, message, "Pedigree error",
//                    JOptionPane.ERROR_MESSAGE);
//        }
    }
    private void inputChangeAffectedOvary() {
        // gww String
        // newName=JOptionPane.showInputDialog(this,"Enter new name for subject "+String.valueOf(currentPerson.id));
        ChangeAffection ca = new ChangeAffection(lc.getWord("general.ovary"), lc);
        ca.setCekBreast(currentPerson.isAffectedOvary == 1);
        ca.setAgeBreast(currentPerson.isAffectedOvary == 1 ? currentPerson.affectedOvary : 0);
        ca.setChkAgeBreast(currentPerson.OvaryProx == 1);
        int a = JOptionPane.showConfirmDialog(this, ca,
                "Change affection ovary for subject" + currentPerson.id,
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        if (a == JOptionPane.OK_OPTION) {
            if (ca.getCekBreast()) {
                currentPerson.isAffectedOvary = 1;
                currentPerson.affectedOvary = ca.getAgeBreast();
                currentPerson.OvaryProx = ca.getChkAgeBreast() ? 1 : 0;
            } else {
                currentPerson.isAffectedOvary = 0;
                currentPerson.affectedOvary = currentPerson.age;
                currentPerson.OvaryProx = 0;
            }
        }
//        String newAge = JOptionPane.showInputDialog(this,
//                "Enter age affected ovary for subject " + currentPerson.id);
//        try {
//            if (newAge != null && newAge.trim().length() > 0) {
//
//                currentPerson.isAffectedOvary = 1;
//                currentPerson.affectedOvary = Integer.parseInt(newAge);
//
//            } else {
//                currentPerson.isAffectedOvary = 0;
//                currentPerson.affectedOvary = currentPerson.age;
//            }
        updateDisplay();
//        } catch (Throwable t) {
//            String message = t.getMessage();
//            JOptionPane.showMessageDialog(this, message, "Pedigree error",
//                    JOptionPane.ERROR_MESSAGE);
//        }
    }
    private void inputChangeAge() {
        // gww String
        // newName=JOptionPane.showInputDialog(this,"Enter new name for subject "+String.valueOf(currentPerson.id));
        String newAge = JOptionPane.showInputDialog(this,
                lc.getWord("general.inputNewAge") + currentPerson.id);
//        if (currentPerson.hasFather() || currentPerson.hasMother()) {
//            if (currentPerson.father.age <= Integer.parseInt(newAge)
//                    || currentPerson.mother.age <= Integer.parseInt(newAge)) {
//                JOptionPane.showMessageDialog(this, "Imposible age...",
//                        "Pedigree error", JOptionPane.ERROR_MESSAGE);
//                return;
//            }
//        }
        if (newAge != null && newAge.trim().length() > 0) {
            try {
                currentPerson.age = Integer.parseInt(newAge);
                if (currentPerson.isAffectedBreast == 0) {
                    currentPerson.affectedBreast = currentPerson.age;
                }
                if (currentPerson.isAffectedOvary == 0) {
                    currentPerson.affectedOvary = currentPerson.age;
                }
                updateDisplay();
            } catch (Throwable t) {
                String message = t.getMessage();
                JOptionPane.showMessageDialog(this, message, "Pedigree error",
                        JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    /*
     * }}}
     */

 /*
     * {{{ countGenotypes
     */
    private int countGenotypes() {
        boolean found = false;
        int ngeno = 0;
        for (int i = 0; i < getComponentCount() && !found; i++) {
            if (getComponent(i) instanceof PelicanPerson) {
                PelicanPerson person = (PelicanPerson) getComponent(i);
                ngeno = person.genotype.size();
                found = true;
            }
        }
        return (ngeno);
    }

    /*
     * }}}
     */

 /*
     * {{{ changeGenotypes
     */
    private void changeGenotypes() {
        if (displayGeno.isEmpty()) {
            JOptionPane.showMessageDialog(this,
                    "You must first display the genotypes to edit",
                    "Pedigree error", JOptionPane.ERROR_MESSAGE);
            return;
        }
        int[] indices = new int[displayGeno.size()];
        for (int i = 0; i < displayGeno.size(); i++) {
            indices[i] = ((Integer) displayGeno.elementAt(i)).intValue() - 1;
        }
        String genotypes = "";
        for (int i = 0; i < displayGeno.size(); i++) {
            if (i > 0) {
                genotypes += "\n";
            }
            Vector geno = (Vector) currentPerson.genotype.elementAt(indices[i]);
            genotypes += geno.elementAt(0).toString() + "/"
                    + geno.elementAt(1).toString();
        }
        JTextArea editArea = new JTextArea(genotypes, 5, 5);
        JPanel jp = new JPanel();
        jp.setLayout(new BoxLayout(jp, BoxLayout.PAGE_AXIS));
        jp.add(new JLabel("Format : FirstAllele/SecondAllele"));
        jp.add(new JScrollPane(editArea));
        if (JOptionPane.showConfirmDialog(this, jp, "Edit genotypes",
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE) == JOptionPane.OK_OPTION) {
            genotypes = editArea.getText();
            int posn = 0;
            int ngeno = 0;
            while (posn < genotypes.length() && ngeno < displayGeno.size()) {
                // first allele is delimited by slash or newline
                int nextSlash = genotypes.indexOf('/', posn);
                if (nextSlash < 0) {
                    nextSlash = genotypes.length();
                }
                int nextNewline = genotypes.indexOf('\n', posn);
                if (nextNewline < 0) {
                    nextNewline = genotypes.length();
                }
                int newposn = Math.min(nextSlash, nextNewline);
                String allele1 = genotypes.substring(posn, newposn);
                if (allele1.equals("")) {
                    allele1 = "0";
                }
                String allele2 = "";
                posn = newposn + 1;
                // pick up second allele if present
                if (posn < genotypes.length()
                        && genotypes.charAt(newposn) != '\n') {
                    nextSlash = genotypes.indexOf('/', posn);
                    if (nextSlash < 0) {
                        nextSlash = genotypes.length();
                    }
                    nextNewline = genotypes.indexOf('\n', posn);
                    if (nextNewline < 0) {
                        nextNewline = genotypes.length();
                    }
                    newposn = Math.min(nextSlash, nextNewline);
                    allele2 = genotypes.substring(posn, newposn);
                    if (allele2.equals("")) {
                        allele2 = "0";
                    }
                    // skip over remainder of the line
                    while (posn < genotypes.length()
                            && genotypes.charAt(posn) != '\n') {
                        posn++;
                    }
                    // skip over extra newlines
                    while (posn < genotypes.length()
                            && genotypes.charAt(posn) == '\n') {
                        posn++;
                    }
                } else {
                    allele2 = "0";
                }
                Vector newgeno = new Vector();
                newgeno.add(allele1);
                newgeno.add(allele2);
                currentPerson.genotype.setElementAt(newgeno, indices[ngeno]);
                ngeno++;
            }
            updateDisplay();
        }
    }

    /*
     * }}}
     */

 /*
     * {{{ addGenotypes
     */
    private void addGenotypes() {
        int ngeno = countGenotypes();
        Vector numbers = new Vector();
        for (int i = 1; i <= ngeno; i++) {
            numbers.add(new Integer(i));
        }
        numbers.add(new String("end"));
        JComboBox numberBox = new JComboBox(numbers);
        JPanel selectPanel = new JPanel();
        selectPanel.add(new JLabel("Insert genotypes before marker "));
        selectPanel.add(numberBox);
        if (JOptionPane.showConfirmDialog(this, selectPanel, "Add genotypes",
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE) == JOptionPane.OK_OPTION) {
            String selection = numberBox.getSelectedItem().toString();
            int index = 0;
            if (selection.equals("end")) {
                index = ngeno;
            } else {
                index = Integer.parseInt(selection) - 1;
            }
            for (int i = 0; i < getComponentCount(); i++) {
                if (getComponent(i) instanceof PelicanPerson) {
                    PelicanPerson person = (PelicanPerson) getComponent(i);
                    Vector geno = new Vector();
                    geno.add("0");
                    geno.add("0");
                    person.genotype.add(index, geno);
                }
            }
            int i = 0;
            while (i < displayGeno.size()
                    && ((Integer) displayGeno.get(i)).intValue() < index + 1) {
                i++;
            }
            displayGeno.add(i, new Integer(index + 1));
            for (i++; i < displayGeno.size(); i++) {
                displayGeno.setElementAt(
                        new Integer(((Integer) displayGeno.get(i)).intValue() + 1), i);
            }
            updateDisplay();
        }
    }
    private void addGenotypesDummy() {
        int ngeno = countGenotypes();
        Vector numbers = new Vector();
        int index = ngeno;
        int i = 0;
        while (i < displayGeno.size()
                && ((Integer) displayGeno.get(i)).intValue() < index + 1) {
            i++;
        }
        displayGeno.add(i, new Integer(index + 1));
        for (i++; i < displayGeno.size(); i++) {
            displayGeno.setElementAt(
                    new Integer(((Integer) displayGeno.get(i)).intValue() + 1), i);
        }
        updateDisplay();
    }

    /*
     * }}}
     */

 /*
     * {{{ deleteGenotypes
     */
    private void deleteGenotypes() {
        int ngeno = countGenotypes();
        Vector numbers = new Vector();
        for (int i = 1; i <= ngeno; i++) {
            numbers.add(new Integer(i));
        }
        JComboBox numberBox = new JComboBox(numbers);
        JPanel selectPanel = new JPanel();
        selectPanel.add(new JLabel("Delete genotypes for marker "));
        selectPanel.add(numberBox);
        if (JOptionPane.showConfirmDialog(this, selectPanel,
                "Delete genotypes", JOptionPane.OK_CANCEL_OPTION,
                JOptionPane.PLAIN_MESSAGE) == JOptionPane.OK_OPTION) {
            int selection = Integer.parseInt(numberBox.getSelectedItem().toString());
            for (int i = 0; i < getComponentCount(); i++) {
                if (getComponent(i) instanceof PelicanPerson) {
                    PelicanPerson person = (PelicanPerson) getComponent(i);
                    person.genotype.removeElementAt(selection - 1);
                }
            }
            boolean found = false;
            for (int i = 0; i < displayGeno.size() && !found; i++) {
                if (displayGeno.get(i).toString().equals(String.valueOf(selection))) {
                    displayGeno.removeElementAt(i);
                    found = true;
                }
            }
            for (int i = 0; i < displayGeno.size(); i++) {
                int index = Integer.parseInt(displayGeno.get(i).toString());
                if (index > selection) {
                    displayGeno.setElementAt(new Integer(index - 1), i);
                }
            }
            updateDisplay();
        }
    }

    /*
     * }}}
     */

 /*
     * {{{ selectGenotypes
     */
    private void selectGenotypes() {
        int ngeno = countGenotypes();
        if (ngeno == 0) {
            JOptionPane.showMessageDialog(this,
                    "There are no genotypes to display", "Pedigree error",
                    JOptionPane.ERROR_MESSAGE);
            return;
        }
        Vector numbers = new Vector();
        for (int i = 1; i <= ngeno; i++) {
            numbers.add(new Integer(i));
        }
        JList genoList = new JList(numbers);
        if (ngeno == 1) {
            genoList.setSelectedIndex(0);
        } else {
            int[] indices = new int[displayGeno.size()];
            for (int i = 0; i < displayGeno.size(); i++) {
                indices[i] = ((Integer) displayGeno.get(i)).intValue() - 1;
            }
            genoList.setSelectedIndices(indices);
        }
        JPanel selectPanel = new JPanel();
        selectPanel.add(new JLabel("Choose markers to display:"));
        selectPanel.add(new JScrollPane(genoList));
        if (JOptionPane.showConfirmDialog(this, selectPanel, "Select markers",
                JOptionPane.OK_CANCEL_OPTION) == JOptionPane.OK_OPTION) {
            Object[] values = genoList.getSelectedValues();
            displayGeno.clear();
            for (int i = 0; i < values.length; i++) {
                displayGeno.add(values[i]);
            }
            updateDisplay();
        }
    }

    /*
     * }}}
     */

 /*
     * {{{ mergePerson
     */
    private void mergePerson(PelicanPerson person) {
        if (mergeEnabled) {
            try {
                if (person != currentPerson) {
                    if (person.sex != currentPerson.sex) {
                        throw (new Error(
                                "Persons to be merged must have the same sex"));
                    }
                    if (!person.isOrphan() && !currentPerson.isOrphan()) {
                        throw (new Error(
                                "One person to be merged must be an orphan"));
                    }
                    if (isAncestor(person, currentPerson)
                            || isAncestor(currentPerson, person)) {
                        throw (new Error("Cannot merge with a direct ancestor"));
                    }
                    // remove the orphan, or the second-selected person
                    PelicanPerson person1 = person;
                    PelicanPerson person2 = currentPerson;
                    if (person1.isOrphan() && !person2.isOrphan()) {
                        person1 = currentPerson;
                        person2 = person;
                    }
                    if (person2.genotype.size() == 0
                            || // gww
                            // JOptionPane.showConfirmDialog(this,"Genotypes for subject "+String.valueOf(person2.id)+" will be lost","Confirm merge",JOptionPane.OK_CANCEL_OPTION,JOptionPane.WARNING_MESSAGE)==JOptionPane.OK_OPTION)
                            // {
                            JOptionPane.showConfirmDialog(this,
                                    "Genotypes for subject " + person2.id
                                    + " will be lost", "Confirm merge",
                                    JOptionPane.OK_CANCEL_OPTION,
                                    JOptionPane.WARNING_MESSAGE) == JOptionPane.OK_OPTION) {
                        // change pointers to old parents
                        for (int i = 0; i < getComponentCount(); i++) {
                            if (getComponent(i) instanceof PelicanPerson) {
                                PelicanPerson p = (PelicanPerson) getComponent(i);
                                if (p.father == person2) {
                                    p.father = person1;
                                }
                                if (p.mother == person2) {
                                    p.mother = person1;
                                }
                            }
                        }
                        remove(person2);
                    }
                }
            } catch (Throwable t) {
                JOptionPane.showMessageDialog(this, t.getMessage(),
                        "Pedigree error", JOptionPane.ERROR_MESSAGE);
            }
            // setCursor(Cursor.getDefaultCursor());
        } else {
            setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
        }
        mergeEnabled = !mergeEnabled;
    }

    /*
     * }}}
     */

 /*
     * {{{ makeProband
     */
    private void makeProband(PelicanPerson person) {
        for (int i = 0; i < getComponentCount(); i++) {
            if (getComponent(i) instanceof PelicanPerson) {
                ((PelicanPerson) getComponent(i)).proband = false;
            }
        }
        person.proband = true;
    }

    /*
     * }}}
     */

 /*
     * {{{ deletePerson
     */
    // recursively mark a subject and its descendents for deletion
    private void markForDeletion(PelicanPerson person) {
        // remove all children
        for (int i = 0; i < getComponentCount(); i++) {
            if (getComponent(i) instanceof PelicanPerson) {
                PelicanPerson p = (PelicanPerson) getComponent(i);
                if (p.father == person || p.mother == person) {
                    markForDeletion(p);
                }
            }
        }
        // remove orphan spouses, who have no other spouses of their own
        for (int i = 0; i < getComponentCount(); i++) {
            if (getComponent(i) instanceof PelicanPerson) {
                PelicanPerson p = (PelicanPerson) getComponent(i);
                if (areSpouses(p, person) && p.isOrphan()) {
                    boolean soleMate = true;
                    for (int j = 0; j < getComponentCount(); j++) {
                        if (getComponent(j) instanceof PelicanPerson) {
                            PelicanPerson pp = (PelicanPerson) getComponent(j);
                            if (areSpouses(p, pp) && pp != person) {
                                soleMate = false;
                            }
                        }
                    }
                    if (soleMate) {
                        p.laidOut = true;
                    }
                }
            }
        }
        // remove this subject
        person.laidOut = true;
    }
    // main routine to delete a person
    private void deletePerson(PelicanPerson person) {
        // check if the person has children
        boolean hasChild = false;
        boolean hasPaternalSibs = false;
        boolean hasMaternalSibs = false;
        //boolean hasSpounse = false;
        Vector spounses = new Vector();
        for (int i = 0; i < getComponentCount(); i++) {
            if (getComponent(i) instanceof PelicanPerson) {
                PelicanPerson p = (PelicanPerson) getComponent(i);
                if (p.father == person || p.mother == person) {
                    hasChild = true;
                }
                if (p != person && p.father == person.father) {
                    hasPaternalSibs = true;
                }
                if (p != person && p.mother == person.mother) {
                    hasMaternalSibs = true;
                }
                for (int s = 0; s < p.spounse.size(); s++) {
                    if (p.spounse.elementAt(s) == person) {
                        spounses.add(p);
                    }
                }
            }
        }
        if (!hasChild && hasPaternalSibs && hasMaternalSibs) {
            if (person.hasMother()) {
                int idx = getIndexOffspring(person, person.mother);
                person.mother.offsping.remove(idx);
                //    System.out.println(idx);
            }
            if (person.hasFather()) {
                int idx = getIndexOffspring(person, person.father);
                person.father.offsping.remove(idx);
                //  System.out.println(idx);
            }
            if (!spounses.isEmpty()) {
                for (int s = 0; s < spounses.size(); s++) {
                    PelicanPerson p = (PelicanPerson) spounses.elementAt(s);
                    for (int x = 0; x < p.spounse.size(); x++) {
                        if (p.spounse.elementAt(x) == person) {
                            p.spounse.remove(x);
                        }
                    }
                }
            }
            remove(person);
            updateDisplay();
            renumberAll();
            return;
        }
        // verify multiple deletes for internal subjects
        int choice = JOptionPane.OK_OPTION;
        Vector clause = new Vector();
        if (person.hasMother()) // if (getMother(person).isOrphan() && !hasMaternalSibs)
        {
            if (person.mother.isOrphan() && !hasMaternalSibs) {
                clause.add("mother");
            }
        }
        if (person.hasFather()) // if (getFather(person).isOrphan() && !hasPaternalSibs)
        {
            if (person.father.isOrphan() && !hasPaternalSibs) {
                clause.add("father");
            }
        }
        if (hasChild) {
            clause.add("descendents");
        }
        String message = "The ";
        if (clause.size() == 3) {
            message += "mother, father and descendents";
        }
        if (clause.size() == 2) {
            message += (String) clause.get(0) + " and "
                    + (String) clause.get(1);
        }
        if (clause.size() == 1) {
            message += (String) clause.get(0);
        }
        // gww
        // message+=" of subject "+String.valueOf(person.id)+" will also be deleted";
        message += " of subject " + person.id + " will also be deleted";
        if (clause.size() > 0) {
            choice = JOptionPane.showConfirmDialog(this, message,
                    "Confirm delete", JOptionPane.OK_CANCEL_OPTION);
        }
        if (choice != JOptionPane.OK_OPTION) {
            return;
        }
        // here, laidOut indicates whether a subject is marked for deletion
        for (int i = 0; i < getComponentCount(); i++) {
            if (getComponent(i) instanceof PelicanPerson) {
                ((PelicanPerson) getComponent(i)).laidOut = false;
            }
        }
        markForDeletion(person);
        if (person.hasMother()) {
            if (person.mother.isOrphan() && !hasMaternalSibs) {
                person.mother.laidOut = true;
            }
            int idx = getIndexOffspring(person, person.mother);
            person.mother.offsping.remove(idx);
            //System.out.println(idx);
        }
        if (person.hasFather()) {
            if (person.father.isOrphan() && !hasPaternalSibs) {
                person.father.laidOut = true;
            }
            int idx = getIndexOffspring(person, person.father);
            person.father.offsping.remove(idx);
            //System.out.println(idx);
        }
        boolean empty = true;
        for (int i = 0; i < getComponentCount(); i++) {
            if (getComponent(i) instanceof PelicanPerson) {
                if (((PelicanPerson) getComponent(i)).laidOut) {
                    remove(i);
                    i--;
                } else {
                    empty = false;
                }
            }
        }
        if (empty) {
            newPedigree();
        }
        updateDisplay();
        renumberAll();
    }

    /*
     * }}}
     */

 /*
     * {{{ ActionListener(this)
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        JMenuItem source = (JMenuItem) (e.getSource());
        String request = source.getText();
        // Popup menu actions
        // Add menu actions
        if (request.equals(lc.getWord("general.editData"))) {
            //addParents();
            int a;
//            if (currentPerson.proband) {
//                
//            } else {
//                PelicanFamilyHistory pfh = new PelicanFamilyHistory(currentPerson, lc);
//                a = JOptionPane.showConfirmDialog(this, pfh, "Family History",
//                        JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
//            }
            PelicanPersonHistory pph = new PelicanPersonHistory(currentPerson, this, lc);
            a = JOptionPane.showConfirmDialog(this, pph, "Personal History",
                    JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
            updateDisplay();
        }
        if (request.equals(lc.getWord("general.parents"))) {
            addParents();
        }
        if (request.charAt(0) >= '1' && request.charAt(0) <= '3'
                && !request.endsWith("%")) {
            addChildren(request);
        }
        if (request.contains(lc.getWord("general.spouse")) && !request.equals(lc.getWord("general.spouse"))) {
            addSpouse(request);
        }
        if (request.equals(lc.getWord("general.spouse"))) {
            addJustSpouse();
        }
        // Change menu actions
        if (request.equals(lc.getWord("general.affected"))) {
            currentPerson.affection = PelicanPerson.affected;
            updateDisplay();
        }
        if (request.equals(lc.getWord("general.unaffected"))) {
            currentPerson.affection = PelicanPerson.unaffected;
            updateDisplay();
        }
        if (request.equals(lc.getWord("general.affectedBreast"))) {
            inputChangeAffectedBreast();
            updateDisplay();
        }
        if (request.equals(lc.getWord("general.affectedOvary"))) {
            inputChangeAffectedOvary();
            updateDisplay();
        }
        if (request.equals(lc.getWord("general.carrier"))) {
            currentPerson.affection = PelicanPerson.carrier;
            updateDisplay();
        }
        if (request.equals(lc.getWord("general.unknown"))) {
            currentPerson.affection = PelicanPerson.unknown;
            updateDisplay();
        }
        if (request.equals("With DNA")) {
            currentPerson.dna = PelicanPerson.with_dna;
            updateDisplay();
        }
        if (request.equals("Without DNA")) {
            currentPerson.dna = PelicanPerson.without_dna;
            updateDisplay();
        }
        if (request.equals(lc.getWord("general.male"))) {
            currentPerson.sex = PelicanPerson.male;
            updateDisplay();
        }
        if (request.equals(lc.getWord("general.female"))) {
            currentPerson.sex = PelicanPerson.female;
            updateDisplay();
        }
        if (request.equals(lc.getWord("general.alive"))) {
            currentPerson.dead = false;
            updateDisplay();
        }
        if (request.equals(lc.getWord("general.death"))) {
            currentPerson.dead = true;
            updateDisplay();
        }
        if (request.equals(lc.getWord("general.yes"))) {
            makeProband(currentPerson);
            updateDisplay();
        }
        if (request.equals(lc.getWord("general.no"))) {
            currentPerson.proband = false;
            updateDisplay();
        }
        if (request.equals(lc.getWord("general.id"))) {
            inputChangeId();
        }
        if (request.equals(lc.getWord("general.changeName"))) {
            inputChangeName();
        }
        if (request.equals(lc.getWord("general.changeAge"))) {
            inputChangeAge();
        }
        if (request.equals(lc.getWord("general.changeGenotype"))) {
            changeGenotypes();
        }
        if (request.equals(lc.getWord("general.addGenotype"))) {
            addGenotypes();
        }
        if (request.equals(lc.getWord("general.deleteGenotype"))) {
            deleteGenotypes();
        }
        if (request.equals("Merge with...")) {
            mergePerson(currentPerson);
        }
        if (request.equals(lc.getWord("general.delete"))) {
            int a = JOptionPane.showConfirmDialog(this, "Are sure want delete?", "Confirmation", JOptionPane.OK_CANCEL_OPTION);
            if (a == JOptionPane.CANCEL_OPTION) {
                return;
            }
            deletePerson(currentPerson);
        }
        // Edit menu actions
        if (request.startsWith("Undo") && historyPosition > 1
                || request.startsWith("Redo")
                && historyPosition < history.size()) {
            if (request.startsWith("Undo")) {
                historyPosition--;
            }
            if (request.startsWith("Redo")) {
                historyPosition++;
            }
            Vector savedPed = (Vector) history.elementAt(historyPosition - 1);
            loadPedigree(savedPed);
            pedHasChanged = true;
            paint(getGraphics());
        }
        if (request.equals("Renumber")) {
            renumberAll();
            updateDisplay();
        }
        // Done menu action
        // View menu actions
        if (request.equals("Zoom in")) {
            PelicanPerson.changeScale(1.1111111);
            pedHasChanged = true;
            paint(getGraphics());
        }
        if (request.equals("Zoom out")) {
            PelicanPerson.changeScale(0.9);
            pedHasChanged = true;
            paint(getGraphics());
        }
        if (request.endsWith("%")) {
            double scale = Double.parseDouble(request.substring(0,
                    request.length() - 1)) / 100.0;
            PelicanPerson.setScale(scale);
            pedHasChanged = true;
            paint(getGraphics());
        }
        if (request.startsWith("Vertical")) {
            PelicanPerson.changeVspace(request.endsWith("+") ? 5 : -5);
            pedHasChanged = true;
            paint(getGraphics());
        }
        if (request.startsWith("Horizontal")) {
            PelicanPerson.changeHspace(request.endsWith("+") ? 5 : -5);
            pedHasChanged = true;
            paint(getGraphics());
        }
        if (request.equals("Refresh")) {
            if (autoLayout.isSelected()) {
                pedHasChanged = true;
            }
            paint(getGraphics());
        }
        if (request.equals("Display IDs") || request.equals("Display names")) {
            setVisible(false);
            for (int i = 0; i < getComponentCount(); i++) {
                if (getComponent(i) instanceof PelicanLines) {
                    remove(i);
                }
            }
            add(new PelicanLines(this, showId.isSelected(),
                    showName.isSelected(), showMarkerNumbers.isSelected(),
                    displayGeno), -1);
            pedHasChanged = true;
            paint(getGraphics());
            setVisible(true);
        }
        if (request.equals("Samanda help")) {
            showHelp();
        }
        if (request.equals("About...")) {
            JOptionPane.showMessageDialog(this, versionMessage,
                    "About Samanda", JOptionPane.INFORMATION_MESSAGE);
        }
        if (request.equals("left")) {
            System.out.println("here");
        }
    }

    /*
     * }}}
     */

 /*
     * {{{ reorderSelected
     */
    // this is called after a mouse drag
    // if appropriate, move the selected person to a different position
    // in the list
    private void reorderSelected() {
        if (currentPerson == null) {
            return;
        }
        boolean haveMoved = false;
        // if currentPerson is a child, change its location among
        // its full sibs
        if (!currentPerson.isOrphan()) {
            boolean atCurrentPerson = false;
            boolean moveRight = false;
            for (int i = 0; i < getComponentCount() && !haveMoved; i++) {
                if (getComponent(i) instanceof PelicanPerson) {
                    PelicanPerson sib = (PelicanPerson) getComponent(i);
                    // find a sib to the right of currentPerson
                    if (sib != currentPerson && PedFunc.areSibs(sib, currentPerson)
                            && currentPerson.getX() < sib.getX()) {
                        // insert currentPerson into this position
                        if (!atCurrentPerson) {
                            if (moveRight) {
                                add(currentPerson, i - 1);
                            } else {
                                add(currentPerson, i);
                            }
                            savePedigree();
                        }
                        haveMoved = true;
                    }
                    if (PedFunc.areSibs(sib, currentPerson)) {
                        atCurrentPerson = (sib == currentPerson);
                        if (atCurrentPerson) {
                            moveRight = true;
                        }
                    }
                }
            }
            // if no full sibs, move currentPerson to the end
            if (!haveMoved && !atCurrentPerson) {
                add(currentPerson);
                savePedigree();
            }
        } // if it is a root subject, change location among other roots
        else if (currentPerson.isRoot()) {
            // start by making a shorter list of root subjects
            Vector rootSubjects = new Vector();
            for (int i = 0; i < getComponentCount(); i++) {
                if (getComponent(i) instanceof PelicanPerson) {
                    PelicanPerson root = (PelicanPerson) getComponent(i);
                    if (root.isRoot()) {
                        rootSubjects.add(root);
                    }
                }
            }
            // find the root subjects to the left and right of currentPerson
            PelicanPerson currentLeft = null;
            boolean haveLeft = false;
            for (int i = rootSubjects.size() - 1; i >= 0 && !haveLeft; i--) {
                PelicanPerson root = (PelicanPerson) (rootSubjects.get(i));
                if (root != currentPerson && !areSpouses(root, currentPerson)
                        && currentPerson.getX() > root.getX()) {
                    currentLeft = root;
                    haveLeft = true;
                }
            }
            PelicanPerson currentRight = null;
            boolean haveRight = false;
            for (int i = 0; i < rootSubjects.size() && !haveRight; i++) {
                PelicanPerson root = (PelicanPerson) (rootSubjects.get(i));
                if (root != currentPerson && !areSpouses(root, currentPerson)
                        && currentPerson.getX() < root.getX()) {
                    currentRight = root;
                    haveRight = true;
                }
            }
            // find the root subject to the right of the first spouse
            boolean diffRight = false;
            haveRight = false;
            for (int i = 0; i < rootSubjects.size() && !haveRight; i++) {
                PelicanPerson spouse = (PelicanPerson) (rootSubjects.get(i));
                if (spouse != currentPerson
                        && areSpouses(spouse, currentPerson)) {
                    for (int j = 0; j < rootSubjects.size() && !haveRight; j++) {
                        PelicanPerson root = (PelicanPerson) (rootSubjects.get(j));
                        if (root != spouse && root != currentPerson
                                && !areSpouses(root, currentPerson)
                                && spouse.getX() < root.getX()) {
                            if (root != currentRight) {
                                diffRight = true;
                            }
                            haveRight = true;
                        }
                    }
                    if (!haveRight && currentRight != null) {
                        diffRight = true;
                    }
                    haveRight = true;
                }
            }
            // don't allow movements between spouses
            if (areSpouses(currentLeft, currentRight)) {
                return;
            }
            // if movement is outside the current mating,
            // move the whole mating
            if (diffRight) {
                // first remove and save currentPerson and its spouses
                Vector currentMating = new Vector();
                int limit = rootSubjects.size();
                for (int i = 0; i < limit; i++) {
                    PelicanPerson spouse = (PelicanPerson) (rootSubjects.get(i));
                    if (spouse == currentPerson
                            || areSpouses(spouse, currentPerson)) {
                        rootSubjects.remove(spouse);
                        currentMating.add(spouse);
                        i--;
                        limit--;
                    }
                }
                // then reinsert them in before currentRight
                for (int i = 0; i < currentMating.size(); i++) {
                    PelicanPerson spouse = (PelicanPerson) (currentMating.get(i));
                    if (currentRight == null) {
                        rootSubjects.add(spouse);
                    } else {
                        rootSubjects.insertElementAt(spouse,
                                rootSubjects.indexOf(currentRight));
                    }
                }
                // then put all the root subjects back into this container
                for (int i = 0; i < rootSubjects.size(); i++) {
                    add((PelicanPerson) rootSubjects.get(i));
                }
                savePedigree();
            } // otherwise rearrange this mating
            else {
                boolean atCurrentPerson = false;
                boolean moveRight = false;
                for (int i = 0; i < getComponentCount() && !haveMoved; i++) {
                    if (getComponent(i) instanceof PelicanPerson) {
                        PelicanPerson spouse = (PelicanPerson) getComponent(i);
                        // find a spouse to the right of currentPerson
                        if (spouse != currentPerson
                                && areSpouses(spouse, currentPerson)
                                && currentPerson.getX() < spouse.getX()) {
                            // insert currentPerson into this position
                            if (!atCurrentPerson) {
                                if (moveRight) {
                                    add(currentPerson, i - 1);
                                } else {
                                    add(currentPerson, i);
                                }
                                savePedigree();
                            }
                            haveMoved = true;
                        }
                        if (spouse == currentPerson
                                || areSpouses(spouse, currentPerson)) {
                            atCurrentPerson = (spouse == currentPerson);
                            if (atCurrentPerson) {
                                moveRight = true;
                            }
                        }
                    }
                }
                // if no spouses, move currentPerson to the end
                if (!haveMoved && !atCurrentPerson) {
                    add(currentPerson);
                    savePedigree();
                }
            }
        }
        renumberAll();
    }

    /*
     * }}}
     */

 /*
     * {{{ layoutPerson
     */
    // recursively lay out a person
    // returns: number of horizontal units taken up by its nuclear family
    private double layoutPerson(PelicanPerson person, int across) {
        int offspringSpace = 0;
        int spouseSpace = 0;
        PelicanPerson spouse = null;
        //TODO: ini harus diganti juga kalo mo nambah  keterangan
        int verticalOffset = ((PelicanLines.showMarkerNumbers ? displayGeno.size() : 1) + (showName.isSelected() ? 1 : 0)) * fontAscent;
        // lay out with the first spouse
        boolean haveSpouse = false;
        boolean hasSibs = false;
        for (int i = 0; i < getComponentCount() && !haveSpouse; i++) {
            if (getComponent(i) instanceof PelicanPerson) {
                spouse = (PelicanPerson) getComponent(i);
                PelicanPerson lastChild = null;
                if (areSpouses(spouse, person) /*
                         * && spouse.isOrphan()
                         *//*
                         * && !spouse . laidOut
                         */) {
                    for (int j = 0; j < getComponentCount(); j++) {
                        if (getComponent(j) instanceof PelicanPerson) {
                            PelicanPerson child = (PelicanPerson) getComponent(j);
                            if (PedFunc.isChild(child, person, spouse)) {
                                offspringSpace += layoutPerson(child, across
                                        + offspringSpace);
                                lastChild = child;
                            }
                        }
                    }
                    haveSpouse = true;
                }
                if (PedFunc.areSibs(spouse, person) && spouse != person) {
                    hasSibs = true;
                }
            }
        }
        // place to the left of the spouse
        if (!person.laidOut) {
            if (offspringSpace > 1) {
                person.setLocation(PelicanPerson.xSpace
                        * (2 * across + offspringSpace - 2) / 2,
                        (PelicanPerson.ySpace + verticalOffset)
                        * person.generation);
            } else {
                person.setLocation(PelicanPerson.xSpace * across,
                        (PelicanPerson.ySpace + verticalOffset)
                        * person.generation);
            }
            // move a singleton child across by half a space
            if (!hasSibs && !haveSpouse) {
                if ((person.father == null || !person.father.laidOut)
                        && (person.mother == null || !person.mother.laidOut)) {
                    person.setLocation(
                            person.getX() + PelicanPerson.xSpace / 2,
                            person.getY());
                }
            }
            person.laidOut = true;
            spouseSpace++;
        }
        // place the spouse
        if (haveSpouse && !spouse.laidOut
                && spouse.generation == person.generation) {
            if (offspringSpace > 1) {
                spouse.setLocation(PelicanPerson.xSpace
                        * (2 * across + offspringSpace - 2 + spouseSpace * 2)
                        / 2, (PelicanPerson.ySpace + verticalOffset)
                        * person.generation);
            } else {
                spouse.setLocation(PelicanPerson.xSpace
                        * (across + spouseSpace),
                        (PelicanPerson.ySpace + verticalOffset)
                        * person.generation);
            }
            spouse.laidOut = true;
            spouseSpace++;
        }
        int maxSpace = (offspringSpace > spouseSpace) ? offspringSpace
                : spouseSpace;
        // lay out with other spouses
        for (int i = 0; i < getComponentCount(); i++) {
            if (getComponent(i) instanceof PelicanPerson) {
                spouse = (PelicanPerson) getComponent(i);
                spouseSpace = 0;
                offspringSpace = 0;
                if (areSpouses(spouse, person)
                        && (spouse.isOrphan() || !spouse.laidOut)) {
                    for (int j = 0; j < getComponentCount(); j++) {
                        if (getComponent(j) instanceof PelicanPerson) {
                            PelicanPerson child = (PelicanPerson) getComponent(j);
                            if (PedFunc.isChild(child, person, spouse)) {
                                offspringSpace += layoutPerson(child, across
                                        + maxSpace + offspringSpace);
                            }
                        }
                    }
                    // place the spouse
                    if (!spouse.laidOut
                            && spouse.generation == person.generation) {
                        spouse.setLocation(
                                PelicanPerson.xSpace
                                * (2 * (across + maxSpace)
                                + offspringSpace - 1) / 2,
                                (PelicanPerson.ySpace + verticalOffset)
                                * person.generation);
                        spouse.laidOut = true;
                        spouseSpace++;
                    }
                    maxSpace += (offspringSpace > spouseSpace) ? offspringSpace
                            : spouseSpace;
                }
            }
        }
        // System.out.println("ID "+person.id+" max "+String.valueOf(maxSpace));
        return (maxSpace);
    }

    /*
     * }}}
     */

 /*
     * {{{ paintComponent: draw the pedigree
     */
    // draw the pedigree
    @Override
    public void paintComponent(Graphics g) {
        int width = getWidth();             // width of window in pixels
        int height = getHeight();           // height of window in pixels
        super.paintComponent(g);
        Font f = g.getFont();
        g.setFont(new Font("Dialog", Font.PLAIN, 12));
        g.drawString(customer.AcronymName == null ? "" : customer.AcronymName, 20, 25);
        g.drawString(versionMessage, width - 120, 25);
        g.drawString(cohortTgl == null ? "" : cohortTgl, width - 70, height - 15);
        g.setFont(f);
//        JButton openCohortBtn = new JButton(lc.getWord("general.mnOpenCohort"));
//        openCohortBtn.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                openFileHistory();
//            }
//        });
//        add(openCohortBtn);
        if (!pedHasChanged) {
            return;
        }
        setVisible(false);
        // undoMenu.setEnabled(historyPosition > 1);
        // redoMenu.setEnabled(historyPosition != history.size());
        Graphics2D g2 = (Graphics2D) g;
        fontAscent = g.getFontMetrics().getAscent();
        // Initialise: nobody laid out, orphans are root subjects
        for (int i = 0; i < getComponentCount(); i++) {
            if (getComponent(i) instanceof PelicanPerson) {
                PelicanPerson person = (PelicanPerson) getComponent(i);
                person.laidOut = false;
                person.root = person.isOrphan();
            }
        }
        // Make list of matings and ensure roots have orphan spouses
        matingList.clear();
        for (int i = 0; i < getComponentCount(); i++) {
            if (getComponent(i) instanceof PelicanPerson) {
                PelicanPerson person = (PelicanPerson) getComponent(i);
                // khusus samanda
                if (!person.spounse.isEmpty()) {
                    for (int s = 0; s < person.spounse.size(); s++) {
                        matingList.add(person.id + " " + ((PelicanPerson) person.spounse.elementAt(s)).id);
                    }
                }
                if (person.father != null && person.mother != null) // gww matingList.add(new
                // Dimension(person.father.id,person.mother.id));
                {
                    matingList.add(person.father.id + " " + person.mother.id);
                }
                if (person.father != null && !person.father.isOrphan()) {
                    if (person.mother != null
                            && person.mother.generation >= person.father.generation) {
                        person.mother.root = false;
                    }
                }
                if (person.mother != null && !person.mother.isOrphan()) {
                    if (person.father != null
                            && person.father.generation >= person.mother.generation) {
                        person.father.root = false;
                    }
                }
            }
        }
        // lay out the root subjects
        // person is a root if it has spouses which are all orphans
        int rootSpace = 0;
        for (int i = 0; i < getComponentCount(); i++) {
            if (getComponent(i) instanceof PelicanPerson) {
                PelicanPerson person = (PelicanPerson) getComponent(i);
                if (!person.laidOut && person.isOrphan() && person.isRoot()) {
                    rootSpace += layoutPerson(person, rootSpace);
                }
            }
        }
        // normalise x-y locations
        for (int i = 0; i < getComponentCount(); i++) {
            if (getComponent(i) instanceof PelicanLines) {
                remove(i);
            }
        }
        int minx = 0;
        int miny = 0;
        int maxx = 0;
        int maxy = 0;
        for (int i = 0; i < getComponentCount(); i++) {
            Component c = getComponent(i);
            if (i == 0 || c.getX() < minx) {
                minx = c.getX();
            }
            if (i == 0 || c.getY() < miny) {
                miny = c.getY();
            }
            if (i == 0 || c.getX() > maxx) {
                maxx = c.getX();
            }
            if (i == 0 || c.getY() > maxy) {
                maxy = c.getY();
            }
        }
        for (int i = 0; i < getComponentCount(); i++) {
            Component c = getComponent(i);
            // if pedigree fits in the frame, then centre it
            // otherwise start it at (0,0)
            if (maxx - minx + PelicanPerson.xSpace < this.getWidth()) {
                c.setLocation(
                        c.getX()
                        - (maxx + minx - this.getWidth() + PelicanPerson.symbolSize)
                        / 2, c.getY() - miny + PelicanPerson.symbolSize
                        / 2);
            } else {
                c.setLocation(c.getX() - minx + PelicanPerson.symbolSize / 2,
                        c.getY() - miny + PelicanPerson.symbolSize / 2);
            }
        }
        // draw the lines on the graph
        add(new PelicanLines(this, showId.isSelected(), showName.isSelected(),
                showMarkerNumbers.isSelected(), displayGeno), -1);
        setPreferredSize(new Dimension(maxx - minx + PelicanPerson.xSpace
                + PelicanPerson.symbolSize / 2, maxy - miny
                + PelicanPerson.ySpace + PelicanPerson.symbolSize / 2
                + displayGeno.size() * fontAscent));
        setVisible(true);
        currentId = 0;
        // need to check if this is a valid integer ID
        for (int i = 0; i < getComponentCount(); i++) {
            if (getComponent(i) instanceof PelicanPerson) {
                // if 'id' is a valid integer
                String id = ((PelicanPerson) getComponent(i)).id;
                int idnum = checkInt(id);
                if (idnum > 0) // gww
                // currentId=Math.max(currentId,((PelicanPerson)getComponent(i)).id);
                {
                    currentId = Math.max(currentId, idnum);
                }
            }
        }
        currentId++;
        currentPerson = null;
        pedHasChanged = false;
    }

    /*
     * }}}
     */
    // returns the integer value if this string can be a valid positive integer
    // else it returns 0
    private int checkInt(String str) {
        int result = 0;
        try {
            result = Integer.parseInt(str);
            if (result <= 0) {
                result = 0;
            }
        } catch (Throwable t) {
            if (t instanceof NumberFormatException) {
                result = 0;
            }
        }
        return result;
    }

    /*
     * {{{ readPedNames
     */
    private Vector readPedNames(File file) {
        Vector pedNames = new Vector();
        if (!file.exists()) {
            return (pedNames);
        }
        try {
            BufferedReader infile = new BufferedReader(new InputStreamReader(
                    new FileInputStream(file)));
            String line;
            while ((line = infile.readLine()) != null) {
                StringTokenizer words = new StringTokenizer(line);
                if (words.hasMoreTokens()) {
                    String name = words.nextToken();
                    if (!pedNames.contains(name)) {
                        pedNames.add(name);
                    }
                }
            }
            infile.close();
        } catch (Throwable t) {
            JOptionPane.showMessageDialog(this, t.getMessage(),
                    "Pedigree error reading pedigree names",
                    JOptionPane.ERROR_MESSAGE);
        }
        return (pedNames);
    }

    /*
     * }}}
     */

 /*
     * {{{ showLinkage
     */
    private void newLinkage() {
        JTextArea area = new JTextArea(null, 20, 25);
        area.setFont(new Font("Monospaced", Font.PLAIN, 12));
        JScrollPane s = new JScrollPane(area);
        JPanel jp = new JPanel();
        jp.setLayout(new BoxLayout(jp, BoxLayout.PAGE_AXIS));
        JLabel format = new JLabel("Format : FamillyNumber Individual'sID FatherID MotherID Gender Proband Affection FirstAllele SecondAllele");
        format.setHorizontalAlignment(JLabel.LEFT);
        jp.add(format);
        JLabel ket1 = new JLabel("Gender : 0 = Unknown 1 = Male 2 = Female");
        ket1.setHorizontalAlignment(JLabel.LEFT);
        jp.add(ket1);
        JLabel ket2 = new JLabel("Affection : 0 = Unknown 1 = Unaffected 2 = Affected");
        ket2.setHorizontalAlignment(JLabel.LEFT);
        jp.add(ket2);
//        jp.add(new JLabel(""));
        jp.add(s);
        JOptionPane.showMessageDialog(this, jp, "New Pedigree Linkage Format",
                JOptionPane.PLAIN_MESSAGE);
//        try {
//            FileWriter outfile = new FileWriter("e:/testlinkage.txt", true);
//            outfile.write(area.getText());
//            outfile.close();
//        } catch (IOException ex) {
//            Logger.getLogger(Pelican2.class.getName()).log(Level.SEVERE, null, ex);
//        }
        try {
            // read in this pedigree from the file
            Vector newPedigree = new Vector();
            int lenghtGeno = 0;
            int pedSize = 0;
            boolean anyProban = false;
            Vector pidList = new Vector();
            Vector midList = new Vector();
            HashMap idMap = new HashMap();
            String[] lines = area.getText().split("\\n");
            String line;
            for (int i = 0; i < lines.length; i++) {
                line = lines[i];
//            while ((line = infile.readLine()) != null) {
//                String lineNoComments = line;
//                if (line.indexOf("<<") >= 0) {
//                    lineNoComments = line.substring(0, line.indexOf("<<"));
//                }
                StringTokenizer words = new StringTokenizer(line);
                if (words.hasMoreTokens()) {
                    String name = words.nextToken();
                    //int name = Integer.parseInt( name_string );;
                    //if (name.equals(pedName)) {
                    //if (true) {
                    if (!words.hasMoreTokens()) {
                        throw (new Error(
                                "Linkage format error: missing individual's ID"));
                    }
                    // gww int id=Integer.parseInt(words.nextToken());
                    String id = words.nextToken();
                    if (!words.hasMoreTokens()) {
                        throw (new Error(
                                "Linkage format error: missing father ID"));
                    }
                    // gww int pid=Integer.parseInt(words.nextToken());
                    String pid = words.nextToken();
                    if (!words.hasMoreTokens()) {
                        throw (new Error(
                                "Linkage format error: missing mother ID"));
                    }
                    // gww int mid=Integer.parseInt(words.nextToken());
                    String mid = words.nextToken();
//                    if (!words.hasMoreTokens()) {
//                        throw (new Error(
//                                "Linkage format error: missing first offspring ID"));
//                    }
//
//                    String idOffspring = words.nextToken();
//
//                    if (!words.hasMoreTokens()) {
//                        throw (new Error(
//                                "Linkage format error: missing next paternal sibling ID"));
//                    }
//
//                    String npsid = words.nextToken();
//
//                    if (!words.hasMoreTokens()) {
//                        throw (new Error(
//                                "Linkage format error: missing next maternal sibling ID"));
//                    }
//                    String nmsid = words.nextToken();
                    if (!words.hasMoreTokens()) {
                        throw (new Error(
                                "Linkage format error: missing gender"));
                    }
                    int sex = Integer.parseInt(words.nextToken());
                    if (sex != PelicanPerson.unknown
                            && sex != PelicanPerson.male
                            && sex != PelicanPerson.female) {
                        throw (new Error("Value out of range: sex"));
                    }
                    if (!words.hasMoreTokens()) {
                        throw (new Error(
                                "Linkage format error: missing proband"));
                    }
                    int proband = Integer.parseInt(words.nextToken());
//------------------------------------------------------------ SAMANDA -----------------------------------
//                        if (!words.hasMoreTokens()) {
//                            throw (new Error(
//                                    "Linkage format error: missing age"));
//                        }
//                        int age = Integer.parseInt(words.nextToken());
//                        if (!words.hasMoreTokens()) {
//                            throw (new Error(
//                                    "Linkage format error: missing age breast"));
//                        }
//                        int ageBreast = Integer.parseInt(words.nextToken());
//                        if (!words.hasMoreTokens()) {
//                            throw (new Error(
//                                    "Linkage format error: missing age ovary"));
//                        }
//                        int ageOvary = Integer.parseInt(words.nextToken());
////------------------------------------------------------------ SAMANDA -----------------------------------
//                        if (!words.hasMoreTokens()) {
//                            throw (new Error(
//                                    "Linkage format error: missing affection status"));
//                        }
                    if (!words.hasMoreTokens()) {
                        throw (new Error(
                                "Linkage format error: missing affection"));
                    }
                    int affection = Integer.parseInt(words.nextToken());
                    if (affection != PelicanPerson.unknown
                            && affection != PelicanPerson.affected
                            && affection != PelicanPerson.unaffected) {
                        throw (new Error("Value out of range: affection"));
                    }
                    int dna = PelicanPerson.without_dna;
                    boolean haveDna = false;
                    Vector genotype = new Vector();
                    while (words.hasMoreTokens()) {
                        Vector allele = new Vector();
                        allele.add(words.nextToken());
                        if (!words.hasMoreTokens()) {
//                                if (slinkFormat.isSelected()) {
//                                    dna = Integer.parseInt((String) allele.firstElement());
//                                    if (dna != PelicanPerson.with_dna
//                                            && dna != PelicanPerson.without_dna) {
//                                        throw (new Error(
//                                                "Value out of range: DNA availability"));
//                                    }
//                                    haveDna = true;
//                                } else {
                            throw (new Error(
                                    "Linkage format error: missing second allele of last marker "
                                    + words));
//                                }
                        } else {
                            allele.add(words.nextToken());
                            genotype.add(allele);
                        }
                    }
//                        if (slinkFormat.isSelected() && !haveDna) {
//                            throw (new Error(
//                                    "Linkage format error: missing DNA availability"));
//                        }
//                        int dataIndex = line.indexOf("<PelicanData>");
//                        boolean dead = false;
//                        boolean proband = false;
//                        int isAffB = 0;
//                        int isAffO = 0;
//                        String personName = "";
//                        if (dataIndex >= 0) {
//                            dead = (line.charAt(dataIndex + 13) == '1');
//                            proband = (line.charAt(dataIndex + 14) == '1');
//                            affection = Integer.parseInt(line.substring(
//                                    dataIndex + 15, dataIndex + 16));
//                            isAffB = Integer.parseInt(line.substring(
//                                    dataIndex + 16, dataIndex + 17));
//                            isAffO = Integer.parseInt(line.substring(
//                                    dataIndex + 17, dataIndex + 18));
//                            personName = line.substring(dataIndex + 18,
//                                    line.indexOf("</PelicanData>"));
//
//                        }
                    if (!anyProban && proband == 1) {
                        anyProban = true;
                    } else if (anyProban && proband == 1) {//proband hanya boleh satu
                        proband = 0;
                    }
                    PelicanPerson pp = new PelicanPerson(id, null, null, sex,
                            affection, dna, false, proband == 1, "", 0,
                            genotype);
                    pp.age = 0;
                    lenghtGeno = genotype.size();
//                        if (genotype.size() > 0) {
//                    for (int j = 0; j < genotype.size(); j++) {
//                        Vector ve = (Vector) genotype.elementAt(j);
//                        System.out.println(" " + ve.elementAt(0) + " "
//                                + ve.elementAt(1));
//                    }
//                    // String.valueOf(person.dna);
//                }
//                        pp.affectedBreast = ageBreast;
//                        pp.affectedOvary = ageOvary;
//                        pp.isAffectedBreast = isAffB;
//                        pp.isAffectedOvary = isAffO;
                    newPedigree.add(pp);
                    // gww pidList.add(new Integer(pid));
                    // gww midList.add(new Integer(mid));
                    // gww idMap.put(new Integer(id),new
                    // Integer(pedSize++));
                    pidList.add(new String(pid));
                    midList.add(new String(mid));
                    idMap.put(new String(id), new Integer(pedSize++));
                }
            }
            for (int i = 0; i < newPedigree.size(); i++) {
                PelicanPerson person = (PelicanPerson) newPedigree.get(i);
                // gww if
                // (((Integer)pidList.get(i)).intValue()!=PelicanPerson.unknown)
                // {
                if (!((String) pidList.get(i)).equals(PelicanPerson.unknownID)) {
                    if (!idMap.containsKey(pidList.get(i))) // gww throw(new
                    // Error("Father of subject "+String.valueOf(person.id)+" is missing"));
                    {
                        throw (new Error("Father of subject " + person.id
                                + " is missing"));
                    }
                    person.father = (PelicanPerson) newPedigree.get(((Integer) idMap.get(pidList.get(i))).intValue());
                    person.father.offsping.add(person);
                }
                // gww if
                // (((Integer)midList.get(i)).intValue()!=PelicanPerson.unknown)
                // {
                if (!((String) midList.get(i)).equals(PelicanPerson.unknownID)) {
                    if (!idMap.containsKey(midList.get(i))) // gww throw(new
                    // Error("Mother of subject "+String.valueOf(person.id)+" is missing"));
                    {
                        throw (new Error("Mother of subject " + person.id
                                + " is missing"));
                    }
                    person.mother = (PelicanPerson) newPedigree.get(((Integer) idMap.get(midList.get(i))).intValue());
                    person.mother.offsping.add(person);
                }
            }
            // figure out the generations
            ((PelicanPerson) newPedigree.get(0)).laidOut = true;
            boolean someChange = true;
            int nperson = newPedigree.size();
            // repeatedly pass through the pedigree all subjects laid out
            while (someChange) {
                someChange = false;
                for (int i = 0; i < nperson; i++) {
                    PelicanPerson p = (PelicanPerson) newPedigree.get(i);
                    if (!p.laidOut) {
                        // try to get it from the parents
                        for (int j = 0; j < nperson; j++) {
                            PelicanPerson parent = (PelicanPerson) newPedigree.get(j);
                            if (parent == p.father && parent.laidOut) {
                                p.generation = parent.generation + 1;
                                p.laidOut = true;
                                someChange = true;
                            }
                            if (parent == p.mother && parent.laidOut) {
                                p.generation = parent.generation + 1;
                                p.laidOut = true;
                                someChange = true;
                            }
                        }
                    }
                    if (p.laidOut) {
                        // assign parents generation
                        for (int j = 0; j < nperson; j++) {
                            PelicanPerson parent = (PelicanPerson) newPedigree.elementAt(j);
                            if (parent == p.father && !parent.laidOut) {
                                parent.generation = p.generation - 1;
                                parent.laidOut = true;
                                someChange = true;
                            }
                            if (parent == p.mother && !parent.laidOut) {
                                parent.generation = p.generation - 1;
                                parent.laidOut = true;
                                someChange = true;
                            }
                        }
                    }
                }
            }
            if (checkIntegrity(newPedigree) == JOptionPane.CANCEL_OPTION) {
                return;
            }
            // end
            removeAll();
            displayGeno.clear();
            for (int g = 0; g < lenghtGeno; g++) {
                addGenotypesDummy();
            }
            currentId = 0;
            boolean haveNames = false;
            for (int i = 0; i < nperson; i++) {
                PelicanPerson p = (PelicanPerson) newPedigree.get(i);
                add(p);
                if (!p.name.equals("")) {
                    haveNames = true;
                }
                // gww need to add in check for valid integer ID else ignore
                // updating of currentId
                int idnum = checkInt(p.id);
                if (idnum > 0) {
                    // gww if (i==0 || p.id>currentId) currentId=p.id;
                    if (i == 0 || idnum > currentId) {
                        currentId = idnum;
                    }
                }
            }
            currentId++;
            newPedigree.clear();
            showName.setSelected(haveNames);
            updateDisplay();
            // currentDirectory = file.getParent();
        } catch (Throwable t) {
            JOptionPane.showMessageDialog(this, t.getMessage(),
                    "Pedigree error", JOptionPane.ERROR_MESSAGE);
        }
    }
    private int getIndexOffspring(PelicanPerson offspring, PelicanPerson parent) {
        if (!PedFunc.areChild(parent, offspring)) {
            return -1;
        }
        int result = -1;
        for (int i = 0; i < parent.offsping.size(); i++) {
            if (parent.offsping.elementAt(i) == offspring) {
                return i;
            }
        }
        return result;
    }
    private String getIdFirstOffspring(PelicanPerson pp) {
        if (pp.offsping.isEmpty()) {
            return "0 ";
        }
        return ((PelicanPerson) pp.offsping.firstElement()).id + " ";
    }
    private String getIdSiblingNextPateral(PelicanPerson person) {
        if (person.father == null) {
            return "0 ";
        }
        if (person.father.offsping.lastElement() == person) {
            return "0 ";
        }
        if (person.father.offsping.isEmpty()) {
            return "0 ";
        }
        int index = getIndexOffspring(person, person.father);
        return ((PelicanPerson) person.father.offsping.elementAt(index + 1)).id + " ";   //sib next pateral
    }
    private String getIdSiblingNextMateral(PelicanPerson person) {
        if (person.mother == null) {
            return "0 ";
        }
        if (person.mother.offsping.lastElement() == person) {
            return "0 ";
        }
        if (person.mother.offsping.isEmpty()) {
            return "0 ";
        }
        int index = getIndexOffspring(person, person.mother);
        return ((PelicanPerson) person.mother.offsping.elementAt(index + 1)).id + " ";   //sib next pateral
    }
    private void showLinkage() {
        String pedName = JOptionPane.showInputDialog(this,
                "Enter pedigree name", "Pedigree Input",
                JOptionPane.QUESTION_MESSAGE);
        if (pedName == null) {
            return;
        }
        if (pedName.trim().length() == 0) {
            pedName = "0";
        }
        String linkageText = "";
        int maxLength = 40; // minimum number of columns
        for (int i = 0; i < getComponentCount(); i++) {
            if (getComponent(i) instanceof PelicanPerson) {
                PelicanPerson person = (PelicanPerson) getComponent(i);
                String thisText = pedName + " "
                        + // gww String.valueOf(person.id)+" ";//+
                        person.id + " ";// +
                if (person.father == null) {
                    thisText += "0 ";
                } // gww else thisText+=String.valueOf(person.father.id)+" ";//+
                else {
                    thisText += person.father.id + " ";// +
                }
                if (person.mother == null) {
                    thisText += "0 ";
                } // gww else thisText+=String.valueOf(person.mother.id)+" ";//+
                else {
                    thisText += person.mother.id + " ";// +
                }
//                thisText += getIdFirstOffspring(person);
//
//                thisText += getIdSiblingNextPateral(person);
//
//                thisText += getIdSiblingNextMateral(person);
                //thisText += "0 ";
                // if(person.isAffectedBreast == 1 || person.isAffectedOvary ==
                // 1)
                // person.affection = person.affected;
                // else {
                // person.affection = person.unaffected;
                // }
                thisText += String.valueOf(person.sex) + " ";
                thisText += person.proband ? "1 " : "0 ";
                // affection is mod 3 to make carriers unaffected
                // String.valueOf(person.affection%3);
                thisText += String.valueOf(person.affection);
                // person.genotype.get(1)linkageText
                // if (slinkFormat.isSelected())
                // {
                // thisText+=" "+"0 0 "+
                if (person.genotype.size() > 0) {
                    for (int j = 0; j < person.genotype.size(); j++) {
                        Vector ve = (Vector) person.genotype.elementAt(j);
                        thisText += " " + ve.elementAt(0) + " "
                                + ve.elementAt(1);
                    } // String.valueOf(person.dna);
                }
                // }
                if (thisText.length() > maxLength) {
                    maxLength = thisText.length();
                }
                linkageText += thisText + "\n";
            }
        }
        JTextArea area = new JTextArea(linkageText, Math.min(
                getComponentCount(), 24), maxLength);
        area.setFont(new Font("Monospaced", Font.PLAIN, 12));
        JScrollPane s = new JScrollPane(area);
        JOptionPane.showMessageDialog(this, s, "Pedigree Linkage Format",
                JOptionPane.PLAIN_MESSAGE);
    }
    // this old function
    private void showLinkage1() {
        String pedName = JOptionPane.showInputDialog(this,
                "Enter pedigree name", "Pelican: input",
                JOptionPane.QUESTION_MESSAGE);
        if (pedName == null) {
            return;
        }
        if (pedName.trim().length() == 0) {
            pedName = "0";
        }
        String linkageText = "";
        int maxLength = 40; // minimum number of columns
        for (int i = 0; i < getComponentCount(); i++) {
            if (getComponent(i) instanceof PelicanPerson) {
                PelicanPerson person = (PelicanPerson) getComponent(i);
                String thisText = pedName + " "
                        + // gww String.valueOf(person.id)+" ";//+
                        person.id + " ";// +
                if (person.father == null) {
                    thisText += "0 ";
                } // gww else thisText+=String.valueOf(person.father.id)+" ";//+
                else {
                    thisText += person.father.id + " ";// +
                }
                if (person.mother == null) {
                    thisText += "0 ";
                } // gww else thisText+=String.valueOf(person.mother.id)+" ";//+
                else {
                    thisText += person.mother.id + " ";// +
                }
                thisText += String.valueOf(person.sex) + " "
                        + // affection is mod 3 to make carriers unaffected
                        String.valueOf(person.affection % 3);
                if (slinkFormat.isSelected()) {
                    thisText += " " + "0 0 " + String.valueOf(person.dna);
                }
                if (thisText.length() > maxLength) {
                    maxLength = thisText.length();
                }
                linkageText += thisText + "\n";
            }
        }
        JTextArea area = new JTextArea(linkageText, Math.min(
                getComponentCount(), 24), maxLength);
        area.setFont(new Font("Monospaced", Font.PLAIN, 12));
        JScrollPane s = new JScrollPane(area);
        JOptionPane.showMessageDialog(this, s, "Pelican: linkage data",
                JOptionPane.PLAIN_MESSAGE);
    }

    /*
     * }}}
     */

 /*
     * {{{ checkIntegrity
     */
    // check that fathers are male, etc
    private int checkIntegrity(Vector pedigree) {
        for (int i = 0; i < pedigree.size(); i++) {
            PelicanPerson person = (PelicanPerson) pedigree.get(i);
            boolean fatherError = false;
            boolean motherError = false;
            for (int j = 0; j < pedigree.size(); j++) {
                PelicanPerson parent = (PelicanPerson) pedigree.get(j);
                if (parent == person.father
                        && parent.sex == PelicanPerson.female) {
                    fatherError = true;
                }
                if (parent == person.mother && parent.sex == PelicanPerson.male) {
                    motherError = true;
                }
            }
            // perhaps the pid/mid fields are swapped?
            if (fatherError && motherError) {
                // gww int choice =
                // JOptionPane.showConfirmDialog(this,"Subject "+String.valueOf(person.id)+" has a male mother and female father.  Choose YES to exchange the parental IDs.","Pelican2: pedigree structure error",JOptionPane.YES_NO_CANCEL_OPTION,JOptionPane.QUESTION_MESSAGE);
                int choice = JOptionPane.showConfirmDialog(
                        this,
                        "Subject "
                        + person.id
                        + " has a male mother and female father.  Choose YES to exchange the parental IDs.",
                        "Pelican: pedigree structure error",
                        JOptionPane.YES_NO_CANCEL_OPTION,
                        JOptionPane.QUESTION_MESSAGE);
                if (choice == JOptionPane.CANCEL_OPTION) {
                    return (choice);
                }
                if (choice == JOptionPane.YES_OPTION) {
                    PelicanPerson temp = person.father;
                    person.father = person.mother;
                    person.mother = temp;
                }
            } // male mother...
            else {
                if (fatherError) {
                    // gww int choice =
                    // JOptionPane.showConfirmDialog(this,"Subject "+String.valueOf(person.id)+" has a female father.","Pelican2: pedigree structure error",JOptionPane.OK_CANCEL_OPTION,JOptionPane.INFORMATION_MESSAGE);
                    int choice = JOptionPane.showConfirmDialog(this, "Subject "
                            + person.id + " has a female father.",
                            "Pelican: pedigree structure error",
                            JOptionPane.OK_CANCEL_OPTION,
                            JOptionPane.INFORMATION_MESSAGE);
                    if (choice == JOptionPane.CANCEL_OPTION) {
                        return (choice);
                    }
                }
                if (motherError) {
                    // gww int choice =
                    // JOptionPane.showConfirmDialog(this,"Subject "+String.valueOf(person.id)+" has a male mother.","Pelican2: pedigree structure error",JOptionPane.OK_CANCEL_OPTION,JOptionPane.INFORMATION_MESSAGE);
                    int choice = JOptionPane.showConfirmDialog(this, "Subject "
                            + person.id + " has a male mother.",
                            "Pelican: pedigree structure error",
                            JOptionPane.OK_CANCEL_OPTION,
                            JOptionPane.INFORMATION_MESSAGE);
                    if (choice == JOptionPane.CANCEL_OPTION) {
                        return (choice);
                    }
                }
            }
        }
        return (JOptionPane.OK_OPTION);
    }

    /*
     * }}}
     */

 /*
     * {{{ openFile
     */
    private void openFile() {
        openFileSamanda2(null, null);
    }
    private void openFile(String fileName) {
//        openFile(fileName, null);
//        openFileSamanda2(fileName, "0");
    }
    public void openSamanda2(Customers customers) {
        String pedName = Utils.getMaxListPedName(Utils.DATA_DIR + "settings.db3", customers.Id_Customers);
        if (pedName == null) {
            this.newPedigree();
            this.customer = customers;
            return;
        }
        this.openFileSamanda2(customers, pedName);
    }
    public void openFileSamanda2(Customers customers, String pedName) {
        try {
            this.customer = customers;
            DateFormat dfYYYY = new SimpleDateFormat("yyyy-MM-dd");
            Date pedname_new = dfYYYY.parse(pedName);
            DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
            this.cohortTgl = df.format(pedname_new);
//            File file;
//            if (fileName == null) {
//                JFileChooser dialog = new JFileChooser(currentDirectory);
//                dialog.setDialogTitle("Open pedigree file");
//                GeneralFilter gf = new GeneralFilter("sam");
//                //dialog.addChoosableFileFilter(new GeneralFilter("ped"));
//                dialog.addChoosableFileFilter(gf);
//                dialog.setFileFilter(gf);
//                dialog.setAcceptAllFileFilterUsed(false);
//                if (dialog.showOpenDialog(this) != JFileChooser.APPROVE_OPTION) {
//                    return;
//                }
//                file = dialog.getSelectedFile();
//            } else {
//                file = new File(fileName);
//            }
//            String filename = file.getCanonicalPath();
//            if (!file.canRead()) {
//                throw (new Error("Cannot read file " + filename));
//            }
            // first read in all the pedigree names
//            Vector pedNames = Utils.getListPedName(file.getCanonicalPath());
            // select one pedigree to display
//            if (pedNames.size() == 0) {
//                throw (new Error("File contains no pedigrees"));
//            }
//            if (pedName == null) {
//                pedName = (String) (pedNames.firstElement());
//                if (pedNames.size() > 1) {
//                    JPanel choosePed = new JPanel();
//                    choosePed.add(new JLabel(
//                            "Choose which pedigree to display:"));
//                    JComboBox pedChoice = new JComboBox(pedNames);
//                    choosePed.add(pedChoice);
//                    JOptionPane.showMessageDialog(parentPane, choosePed,
//                            "Choose pedigree", JOptionPane.QUESTION_MESSAGE);
//                    pedName = (String) (pedChoice.getSelectedItem());
//                }
//            }
            // read in this pedigree from the file
            Vector newPedigree = new Vector();
//            BufferedReader infile = new BufferedReader(new InputStreamReader(
//                    new FileInputStream(filename)));
            int pedSize = 0;
            Vector pidList = new Vector();
            Vector midList = new Vector();
            HashMap idMap = new HashMap();
            String line;
            //Utils.ReadPedigree(file.getCanonicalPath(), pedName, newPedigree, pidList, midList, idMap);
            Utils.ReadPedigreeORM(Utils.DATA_DIR + "settings.db3", this.customer, pedName, newPedigree, pidList, midList, idMap);
            for (int i = 0; i < newPedigree.size(); i++) {
                PelicanPerson person = (PelicanPerson) newPedigree.get(i);
                // gww if
                // (((Integer)pidList.get(i)).intValue()!=PelicanPerson.unknown)
                // {
                if (!((String) pidList.get(i)).equals(PelicanPerson.unknownID)) {
                    if (!idMap.containsKey(pidList.get(i))) // gww throw(new
                    // Error("Father of subject "+String.valueOf(person.id)+" is missing"));
                    {
                        throw (new Error("Father of subject " + person.id
                                + " is missing"));
                    }
                    person.father = (PelicanPerson) newPedigree.elementAt(((Integer) idMap.get(pidList.elementAt(i))).intValue());
                    person.father.offsping.add(person);
                }
                // gww if
                // (((Integer)midList.get(i)).intValue()!=PelicanPerson.unknown)
                // {
                if (!((String) midList.elementAt(i)).equals(PelicanPerson.unknownID)) {
                    if (!idMap.containsKey(midList.elementAt(i))) // gww throw(new
                    // Error("Mother of subject "+String.valueOf(person.id)+" is missing"));
                    {
                        throw (new Error("Mother of subject " + person.id
                                + " is missing"));
                    }
                    person.mother = (PelicanPerson) newPedigree.elementAt(((Integer) idMap.get(midList.elementAt(i))).intValue());
                    person.mother.offsping.add(person);
                }
            }
            // figure out the generations
            ((PelicanPerson) newPedigree.elementAt(0)).laidOut = true;
            boolean someChange = true;
            int nperson = newPedigree.size();
            // repeatedly pass through the pedigree all subjects laid out
            while (someChange) {
                someChange = false;
                for (int i = 0; i < nperson; i++) {
                    PelicanPerson p = (PelicanPerson) newPedigree.elementAt(i);
                    if (!p.laidOut) {
                        // try to get it from the parents
                        for (int j = 0; j < nperson; j++) {
                            PelicanPerson parent = (PelicanPerson) newPedigree.elementAt(j);
                            if (parent == p.father && parent.laidOut) {
                                p.generation = parent.generation + 1;
                                p.laidOut = true;
                                someChange = true;
                            }
                            if (parent == p.mother && parent.laidOut) {
                                p.generation = parent.generation + 1;
                                p.laidOut = true;
                                someChange = true;
                            }
                        }
                    }
                    if (p.laidOut) {
                        // assign parents generation
                        for (int j = 0; j < nperson; j++) {
                            PelicanPerson parent = (PelicanPerson) newPedigree.elementAt(j);
                            if (parent == p.father && !parent.laidOut) {
                                parent.generation = p.generation - 1;
                                parent.laidOut = true;
                                someChange = true;
                            }
                            if (parent == p.mother && !parent.laidOut) {
                                parent.generation = p.generation - 1;
                                parent.laidOut = true;
                                someChange = true;
                            }
                        }
                    }
                    for (int s = 0; s < p.spounse.size(); s++) {
                        PelicanPerson person = (PelicanPerson) p.spounse.elementAt(s);
                        person.generation = p.generation;
                    }
                }
            }
            if (checkIntegrity(newPedigree) == JOptionPane.CANCEL_OPTION) {
                return;
            }
            // end
            removeAll();
            displayGeno.clear();
            currentId = 0;
            boolean haveNames = false;
            for (int i = 0; i < nperson; i++) {
                PelicanPerson p = (PelicanPerson) newPedigree.get(i);
                add(p);
                if (!p.name.equals("")) {
                    haveNames = true;
                }
                // gww need to add in check for valid integer ID else ignore
                // updating of currentId
                int idnum = checkInt(p.id);
                if (idnum > 0) {
                    // gww if (i==0 || p.id>currentId) currentId=p.id;
                    if (i == 0 || idnum > currentId) {
                        currentId = idnum;
                    }
                }
            }
            currentId++;
            newPedigree.clear();
            showName.setSelected(haveNames);
            updateDisplay();
            currentDirectory = Utils.DATA_DIR;
            renumberAll();
        } catch (Throwable t) {
            JOptionPane.showMessageDialog(this, "File corrupted. " + t.getMessage(),
                    "Pedigree error", JOptionPane.ERROR_MESSAGE);
        }
    }

    /*
     * }}}
     */

 /*
     * {{{ saveFile
     */
    public String getPedImage() {
        try {
            File file;
            BufferedImage image = (BufferedImage) createImage(getWidth(),
                    getHeight());
            Graphics graphics = image.getGraphics();
            if (graphics != null) {
                paintAll(graphics);
            }
            file = File.createTempFile("img", "");
            String sss = file.getAbsolutePath();
            if (file.exists() && !file.canWrite()) {
                throw (new Error("Cannot write file " + sss));
            }
            ImageIO.write(image, imageFormat, file);
            //currentDirectory = file.getParent();
            return file.getAbsolutePath().replace("\\", "/");
        } catch (IOException ex) {
            Logger.getLogger(Pelican2.class.getName()).log(Level.SEVERE, null, ex);
            return "";
        }
    }
    public String getFilename() {
        renumberAll();
        probandId = "";
        // String
        // pedName=JOptionPane.showInputDialog(this,"Enter pedigree name","Pelican2: input",JOptionPane.QUESTION_MESSAGE);
        // String pedName="samanda";
        // if (pedName==null) return;
        // if (pedName.trim().length()==0) pedName="0";
        // JFileChooser dialog=new JFileChooser(currentDirectory);
        // dialog.setDialogTitle("Save pedigree file");
        // dialog.addChoosableFileFilter(new GeneralFilter("pre"));
        // dialog.addChoosableFileFilter(new GeneralFilter("ped"));
        // if (dialog.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
        File temp;
        try {
            // File file=dialog.getSelectedFile();
            // String filename=file.getAbsolutePath();
            // if (file.exists() && !file.canWrite())
            // throw(new Error("Cannot write file "+filename));
            // if pedigree already exists, remove it from existing file
            // Vector pedNames=readPedNames(file);
            // if (pedNames.contains(pedName)) {
            // if
            // (JOptionPane.showConfirmDialog(this,"Replace existing pedigree "+pedName+"?","Confirm replacement",JOptionPane.OK_CANCEL_OPTION,JOptionPane.QUESTION_MESSAGE)
            // != JOptionPane.OK_OPTION) return;
            // BufferedReader infile=new BufferedReader(new
            // InputStreamReader(new FileInputStream(filename)));
            // String line;
            // Vector storedFile=new Vector();
            // while((line=infile.readLine()) !=null) {
            // StringTokenizer words=new StringTokenizer(line);
            // if (words.hasMoreTokens()) {
            // String name=words.nextToken();
            // if (!name.equals(pedName)) {
            // storedFile.add(name+" ");
            // while (words.hasMoreTokens())
            // storedFile.add(words.nextToken()+" ");
            // storedFile.add("\n");
            // }
            // }
            // }
            // FileWriter outfile=new FileWriter(filename,false);
            // for(int i=0;i<storedFile.size();i++)
            // outfile.write((String)storedFile.get(i));
            // outfile.close();
            // }
            // Create temp file.
            temp = File.createTempFile("sam", "");
            // Delete temp file when program exits.
            temp.deleteOnExit();
            // Write to temp file
            BufferedWriter outfile = new BufferedWriter(new FileWriter(temp));
//                out.write("aString");
//                out.close();
            //FileWriter outfile = new FileWriter(filename, true);
            outfile.write("ID,Gender,FatherID,MotherID,AffectedBreast,AffectedOvary,AgeBreast,AgeOvary,AgeBreastContralateral\n");
            for (int i = 0; i < getComponentCount(); i++) {
                if (getComponent(i) instanceof PelicanPerson) {
                    PelicanPerson person = (PelicanPerson) getComponent(i);
                    // outfile.write(pedName+" "+
                    outfile.write(
                            // gww String.valueOf(person.id)+" ");//+
                            person.id + ",");// +
                    outfile.write(String.valueOf(person.sex == PelicanPerson.male ? 1 : 0) + ",");
                    if (person.father == null) {
                        outfile.write("0,");
                    } // gww else
                    // outfile.write(String.valueOf(person.father.id)+" ");//+
                    else {
                        outfile.write(person.father.id + ",");// +
                    }
                    if (person.mother == null) {
                        outfile.write("0,");
                    } // gww else
                    // outfile.write(String.valueOf(person.mother.id)+" ");//+
                    else {
                        outfile.write(person.mother.id + ",");// +
                    }					// outfile.write(String.valueOf(person.sex)+","+
                    // affection is mod 3 to make
                    // carriers unaffected
                    // String.valueOf(person.affection%3));
                    // write out genotype data
                    outfile.write(person.isAffectedBreast + ","
                            + person.isAffectedOvary + ","
                            + person.affectedBreast + ","
                            + person.affectedOvary + ",0\n");
                    //
                    if (person.proband) {
                        probandId = person.id;
                        probandName = person.name;
//                        System.out.println(probandId);
                    }
                    //JOptionPane.showMessageDialog(this,"age must set.");
                    if (person.age == 0) {
                        isError = true;
                        JOptionPane.showMessageDialog(this, "Person id "
                                + person.id + " : Age not valid",
                                "Pedigree error", JOptionPane.ERROR_MESSAGE);
                        return "";
                    }
//                     outfile.write((person.dead?"1":"0")+(person.proband?"1":"0")+String.valueOf(person.affection)+person.name);
                }
            }
            outfile.close();
            return temp.getAbsolutePath().replace("\\", "/");
            // currentDirectory=file.getParent();
        } catch (IOException | NumberFormatException t) {
            JOptionPane.showMessageDialog(this, t.getMessage(),
                    "Pedigree error", JOptionPane.ERROR_MESSAGE);
            return "";
        }
    }
    public String getMatrix() {
        renumberAll();
        probandId = "";
        try {
            String fam = "brcafam=NULL\n";
            String marker = "marker.testing=NULL\n";
            String germline = "germline.testing=NULL\n";
            for (int i = 0; i < getComponentCount(); i++) {
                if (getComponent(i) instanceof PelicanPerson) {
                    PelicanPerson person = (PelicanPerson) getComponent(i);
                    String man = "brcafam <- rbind(brcafam,c(" + person.id + ",";
                    germline
                            += "germline.testing<-rbind(germline.testing,c(" + person.brca1
                            + "," + person.brca2 + "," + person.TestOrder + "))\n";
                    marker
                            += "marker.testing<-rbind(marker.testing,c(" + person.er + "," + person.ck14 + ","
                            + person.ck56 + "," + person.pr + "," + person.her2 + "))\n";
                    man += String.valueOf(person.sex == PelicanPerson.male ? 1 : 0) + ",";
                    if (person.father == null) {
                        man += "0,";
                    } else {
                        man += person.father.id + ",";
                    }
                    if (person.mother == null) {
                        man += "0,";
                    } else {
                        man += person.mother.id + ",";
                    }
                    man += person.isAffectedBreast + ","
                            + person.isAffectedOvary + ","
                            + person.affectedBreast + ","
                            + person.affectedOvary + ",0))\n";
                    fam += man;
                    if (person.proband) {
                        probandId = person.id;
                        probandName = person.name;
                    }
                    if (person.age == 0) {
                        isError = true;
                        JOptionPane.showMessageDialog(this, "Person id "
                                + person.id + " : Age not valid",
                                "Pedigree error", JOptionPane.ERROR_MESSAGE);
                        return "";
                    }
                }
            }
            fam += "colnames(brcafam) <- c('ID','Gender','FatherID','MotherID','AffectedBreast','AffectedOvary','AgeBreast','AgeOvary','AgeBreastContralateral')\n";
            germline += "colnames(germline.testing) <- c('BRCA1','BRCA2','TestOrder')\n";
            marker += "colnames(marker.testing) <- c('ER','CK14','CK5.6','PR','HER2')\n";
            fam += "brcafam<-data.frame(brcafam)\n";
            germline += "germline.testing<-data.frame(germline.testing)\n";
            marker += "marker.testing<-data.frame(marker.testing)\n";
            return fam + germline + marker;
        } catch (NumberFormatException t) {
            JOptionPane.showMessageDialog(this, t.getMessage(),
                    "Pedigree error", JOptionPane.ERROR_MESSAGE);
            return "";
        }
    }
    public void openFileHistory() {
        try {
            String pedName;
            JPanel choosePed = new JPanel();
            choosePed.add(new JLabel(
                    "Choose which cohort to display:"));
            Vector pedNames = Utils.getListPedName(Utils.DATA_DIR + "settings.db3", this.customer);
            JComboBox pedChoice = new JComboBox(pedNames);
            choosePed.add(pedChoice);
            JOptionPane.showMessageDialog(this, choosePed,
                    "Choose cohort", JOptionPane.QUESTION_MESSAGE);
            pedName = (String) (pedChoice.getSelectedItem());
            DateFormat dfYYYY = new SimpleDateFormat("dd.MM.yyyy");
            Date pedname_new = dfYYYY.parse(pedName);
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            this.cohortTgl = pedName;
            openFileSamanda2(this.customer, df.format(pedname_new));
        } catch (ParseException ex) {
            Logger.getLogger(Pelican2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void saveFileHistory() {
        try {
            String pedName = JOptionPane.showInputDialog(this,
                    "Date (dd.mm.yyyy): ", "Save Cohort",
                    JOptionPane.QUESTION_MESSAGE);
            if (pedName == null) {
                return;
            }
            DateFormat dfYYYY = new SimpleDateFormat("dd.MM.yyyy");
            Date pedname_new = dfYYYY.parse(pedName);
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            Utils.InsertDataORM(Utils.DATA_DIR + "settings.db3", df.format(pedname_new), this);
        } catch (ParseException ex) {
            Logger.getLogger(Pelican2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void saveFileSamanda2() {
        String pedName = JOptionPane.showInputDialog(this,
                "Enter pedigree name (if exist will replace)", "Pedigree",
                JOptionPane.QUESTION_MESSAGE);
        if (pedName == null) {
            return;
        }
        if (pedName.trim().length() == 0) {
            pedName = "0";
        }
        JFileChooser dialog = new JFileChooser(currentDirectory);
        dialog.setDialogTitle("Save pedigree file");
        //dialog.addChoosableFileFilter(new GeneralFilter("pre"));
        GeneralFilter gf = new GeneralFilter("sam");
        dialog.addChoosableFileFilter(gf);
        dialog.setAcceptAllFileFilterUsed(false);
        dialog.setFileFilter(gf);
        if (dialog.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
            try {
                File file = dialog.getSelectedFile();
                String file_Stringname = file.getAbsolutePath();
                file_Stringname += file_Stringname.contains(".sam") ? "" : ".sam";
                if (file.exists() && !file.canWrite()) {
                    throw (new Error("Cannot write file " + file_Stringname));
                }
                Utils.insertData(file_Stringname, pedName, this);
            } catch (Error | ClassNotFoundException | SQLException t) {
                JOptionPane.showMessageDialog(this, t.getMessage(),
                        "Pedigree error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    private void saveFileSamanda3() {
        String pedName = "0";
//                JOptionPane.showInputDialog(this,
//                "Enter pedigree name (if exist will replace)", "Pedigree",
//                JOptionPane.QUESTION_MESSAGE);
        if (pedName == null) {
            return;
        }
        if (pedName.trim().length() == 0) {
            pedName = "0";
        }
        JFileChooser dialog = new JFileChooser(currentDirectory);
        dialog.setDialogTitle("Save pedigree file");
        //dialog.addChoosableFileFilter(new GeneralFilter("pre"));
        GeneralFilter gf = new GeneralFilter("sam");
        dialog.addChoosableFileFilter(gf);
        dialog.setAcceptAllFileFilterUsed(false);
        dialog.setFileFilter(gf);
        if (dialog.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
            try {
                File file = dialog.getSelectedFile();
                String file_Stringname = file.getAbsolutePath();
                file_Stringname += file_Stringname.contains(".sam") ? "" : ".sam";
                if (file.exists() && !file.canWrite()) {
                    throw (new Error("Cannot write file " + file_Stringname));
                }
                if (file.exists()) {
                    if (JOptionPane.showConfirmDialog(this, "Are you sure to overwrite this file?", "Confirmation", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE) == JOptionPane.CANCEL_OPTION) {
                        return;
                    }
                    Utils.DeleteFile(file_Stringname);
                }
                Utils.InsertDataORM(file_Stringname, pedName, this);
            } catch (Error t) {
                JOptionPane.showMessageDialog(this, t.getMessage(),
                        "Pedigree error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    /*
     * }}}
     */

 /*
     * {{{ saveImage
     */
    // save as a PNG or JPEG image
    private void saveImage() {
        // this only works under java1.4+
        String javaVersion = System.getProperty("java.vm.version");
        if (javaVersion.startsWith("1.") && javaVersion.charAt(2) < '4') {
            return;
        }
        BufferedImage image = (BufferedImage) createImage(getWidth(),
                getHeight());
        Graphics graphics = image.getGraphics();
        if (graphics != null) {
            paintAll(graphics);
        }
        if (askFormat == null || !askFormat.isSelected()) {
            if (askFormat == null) {
                askFormat = new JCheckBox("Do not ask again", true);
            }
            GridBagLayout grid = new GridBagLayout();
            GridBagConstraints c = new GridBagConstraints();
            JPanel chooseFormat = new JPanel(grid);
            c.weightx = 1.0;
            JLabel label = new JLabel("Choose image format: ");
            grid.setConstraints(label, c);
            chooseFormat.add(label);
            String[] formats = {"PNG", "JPEG"};
            JComboBox formatChoice = new JComboBox(formats);
            c.gridwidth = GridBagConstraints.REMAINDER;
            grid.setConstraints(formatChoice, c);
            chooseFormat.add(formatChoice);
            grid.setConstraints(askFormat, c);
            chooseFormat.add(askFormat);
            if (JOptionPane.showConfirmDialog(this, chooseFormat,
                    "Pedigree: image format", JOptionPane.OK_CANCEL_OPTION) == JOptionPane.CANCEL_OPTION) {
                askFormat = null;
                return;
            }
            imageFormat = (String) (formatChoice.getSelectedItem());
        }
        JFileChooser dialog = new JFileChooser(currentDirectory);
        dialog.setDialogTitle("Print image file");
        if (imageFormat.equals("PNG")) {
            dialog.addChoosableFileFilter(new GeneralFilter("png"));
        }
        if (imageFormat.equals("JPEG")) {
            dialog.addChoosableFileFilter(new GeneralFilter("jpeg"));
            dialog.addChoosableFileFilter(new GeneralFilter("jpg"));
        }
        if (dialog.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
            try {
                String filename = dialog.getSelectedFile().getAbsolutePath();
                if (!filename.toLowerCase().endsWith("." + imageFormat)) {
                    filename += "." + imageFormat;
                }
                File file = new File(filename);
                if (file.exists() && !file.canWrite()) {
                    throw (new Error("Cannot write file " + filename));
                }
                ImageIO.write(image, imageFormat, file);
                currentDirectory = file.getParent();
            } catch (Throwable t) {
                JOptionPane.showMessageDialog(this, t.getMessage(),
                        "Pedigree error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    /**
     * @return the parentPane
     */
    public JScrollableDesktopPane getParentPane() {
        return parentPane;
    }
    /**
     * @param parentPane the parentPane to set
     */
    public void setParentPane(JScrollableDesktopPane parentPane) {
        this.parentPane = parentPane;
    }

    /*
     * }}}
     */

 /*
     * {{{ printImage
     */
    // Thanks to Hugh Morgan for sharing his code for this
    private class MyWriter implements Printable {
        private BufferedImage image = null;
        public MyWriter() {
        }
        public void setImage(BufferedImage newImage) {
            image = newImage;
        }
        @Override
        public int print(Graphics g, PageFormat format, int pageIndex) {
            Graphics2D g2d = (Graphics2D) g;
            g2d.translate(format.getImageableX(), format.getImageableY());
            double scale = 1.0;
            if (format.getImageableWidth() / image.getWidth() < scale) {
                scale = format.getImageableWidth() / image.getWidth();
            }
            if (format.getImageableHeight() / image.getHeight() < scale) {
                scale = format.getImageableHeight() / image.getHeight();
            }
            g2d.scale(scale, scale);
            g2d.drawImage(image, null, null);
            return Printable.PAGE_EXISTS;
        }
    }
    private void printImage() {
        if (printerJob == null) {
            printerJob = PrinterJob.getPrinterJob();
        }
        if (printerJob.printDialog()) {
            try {
                BufferedImage image = (BufferedImage) createImage(getWidth(),
                        getHeight());
                Graphics graphics = image.getGraphics();
                if (graphics != null) {
                    paintAll(graphics);
                }
                MyWriter writer = new MyWriter();
                writer.setImage(image);
                Book book = new Book();
                book.append(writer, new PageFormat());
                printerJob.setPageable(book);
                printerJob.print();
            } catch (Throwable t) {
                JOptionPane.showMessageDialog(this, t.getMessage(),
                        "Printing error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    /*
     * }}}
     */

 /*
     * {{{ showHelp
     */
    private void showHelp() {
        // try {
        // final JFrame helpFrame = new JFrame("Pelican2: Help");
        // final JTextPane helpPane = new JTextPane();
        //
        // JMenuBar helpMenuBar = new JMenuBar();
        //
        // JMenu helpFileMenu = new JMenu("File");
        // // helpFileMenu.setMnemonic(KeyEvent.VK_F);
        // helpMenuBar.add(helpFileMenu);
        // JMenuItem helpFileMenuHome = new JMenuItem("Home");
        // // helpFileMenuHome.setMnemonic(KeyEvent.VK_H);
        // helpFileMenuHome.addActionListener(new ActionListener() {
        // @Override
        // public void actionPerformed(ActionEvent e) {
        // try {
        // helpPane.setPage(this.getClass().getClassLoader()
        // .getResource("pelicanHelp.html"));
        // } catch (Throwable t) {
        // }
        // }
        // });
        // helpFileMenu.add(helpFileMenuHome);
        // JMenuItem helpFileMenuClose = new JMenuItem("Close");
        // // helpFileMenuClose.setMnemonic(KeyEvent.VK_C);
        // helpFileMenuClose.addActionListener(new ActionListener() {
        // @Override
        // public void actionPerformed(ActionEvent e) {
        // helpFrame.dispatchEvent(new WindowEvent(frame,
        // WindowEvent.WINDOW_CLOSING));
        // }
        // });
        // helpFileMenu.add(helpFileMenuClose);
        // helpFrame.setJMenuBar(helpMenuBar);
        //
        // helpPane.setEditable(false);
        // helpPane.addHyperlinkListener(new hyperListener());
        // helpPane.setPage(this.getClass().getClassLoader()
        // .getResource("pelicanHelp.html"));
        // JScrollPane scroll = new JScrollPane(helpPane);
        // helpFrame.getContentPane().add(scroll);
        // helpPane.setPreferredSize(new Dimension(600, 400));
        // helpFrame.pack();
        // scroll.setViewportView(helpPane);
        // helpFrame.setLocation(frame.getX() + 300, frame.getY());
        // helpFrame.setVisible(true);
        // } catch (Throwable t) {
        // JOptionPane.showMessageDialog(this, t.getMessage(),
        // "Pedigree error", JOptionPane.ERROR_MESSAGE);
        // }
    }
    class PopupListener extends MouseAdapter {
        @Override
        public void mousePressed(MouseEvent e) {
            maybeShowPopup(e);
        }
        @Override
        public void mouseReleased(MouseEvent e) {
            if (!popup.isVisible() && autoLayout.isSelected()) {
                reorderSelected();
                pedHasChanged = true;
                paint(getGraphics());
            }
            maybeShowPopup(e);
        }
        private void maybeShowPopup(MouseEvent e) {
            int x = e.getX();
            int y = e.getY();
            final Component c = getComponentAt(x, y);
            if (c instanceof PelicanPerson) {
                PelicanPerson p = (PelicanPerson) c;
                if (e.isPopupTrigger()) {
                    if (p.isOrphan()) {
                        Parents.setEnabled(true);
                    } else {
                        Parents.setEnabled(false);
                    }
                    //Spouse.setEnabled(p.spounse.isEmpty());
                    popup.show(e.getComponent(), x, y);
                    currentPerson = (PelicanPerson) c;
                } else if (mergeEnabled) {
                    mergePerson((PelicanPerson) c);
                    updateDisplay();
                } else {
                    currentPerson = (PelicanPerson) c;
                }
            } else if (e.isPopupTrigger()) {
                popLayer.show(e.getComponent(), x, y);
            }
            setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            mergeEnabled = false;
            SettingPopup();
        }
    }

    /*
     * }}}
     */

 /*
     * {{{ mouse drag listener
     */
    class dragListener extends MouseMotionAdapter {
        @Override
        public void mouseDragged(MouseEvent e) {
            if (!popup.isVisible() && currentPerson != null) {
                int x = e.getX();
                int y = e.getY();
                setVisible(false);
                currentPerson.setLocation(x, y);
                paint(getGraphics());
                setVisible(true);
            }
        }
    }

    /*
     * }}}
     */

 /*
     * {{{ hyperlink listener
     */
    class hyperListener implements HyperlinkListener {
        @Override
        public void hyperlinkUpdate(HyperlinkEvent e) {
            if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
                JEditorPane pane = (JEditorPane) e.getSource();
                try {
                    pane.setPage(e.getURL());
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }
        }
    }

    /*
     * }}}
     */

 /*
     * {{{ GeneralFilter
     */
    class GeneralFilter extends javax.swing.filechooser.FileFilter {
        private String suffix;
        GeneralFilter(String s) {
            suffix = s;
        }
        @Override
        public boolean accept(File f) {
            if (f.isDirectory()) {
                return (true);
            }
            if (f.getName().endsWith(suffix)) {
                return (true);
            }
            return (false);
        }
        @Override
        public String getDescription() {
            return "*." + suffix;
        }
    }

    /*
     * }}}
     */
}
