/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pedigree;

import java.awt.Dimension;
import java.awt.Point;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JProgressBar;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.tree.DefaultMutableTreeNode;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.Highlighter;
import org.jdesktop.swingx.decorator.HighlighterFactory;
import org.jdesktop.swingx.renderer.StringValue;
import samanda.TableRowData;
import samanda.TreeModelClaus;
import samanda.TreeTableCellRenderer;
import samanda.properties.LanguagesController;

/**
 *
 * @author axioo
 */
public class PelicanPersonHistory extends javax.swing.JPanel {
    private PelicanPerson person;
    private Pelican2 pelican;
    private List<PelicanPerson> firstDegree, secondDegree, secondDegreeMateral, secondDegreePateral;
    private DefaultMutableTreeNode first;
    private DefaultMutableTreeNode second;
    private DefaultMutableTreeNode secondMateral = new DefaultMutableTreeNode(new TableRowData("Second Degree Materal",
            "", "", "", true));
    private DefaultMutableTreeNode secondPateral = new DefaultMutableTreeNode(new TableRowData("Second Degree Pateral",
            "", "", "", true));
    private DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode(new TableRowData("CF", "", "", "", true));
    public LanguagesController lc;
    /**
     * Creates new form PelicanPersonHistory
     */
    public PelicanPersonHistory(PelicanPerson pp, Pelican2 pe,
            LanguagesController l) {
        lc = l;
        first = new DefaultMutableTreeNode(new TableRowData(lc.getWord("general.firstDegree"), "", "", "", true));
        second = new DefaultMutableTreeNode(new TableRowData(lc.getWord("general.secondDegree"), "", "", "", true));
//        secondMateral = new DefaultMutableTreeNode(new TableRowData(lc.getWord("general.secondDegreeMaternal"),
//                "", "", "", true));
//        secondPateral = new DefaultMutableTreeNode(new TableRowData(lc.getWord("general.secondDegreePaternal"),
//                "", "", "", true));
        rootNode.add(first);
        rootNode.add(new DefaultMutableTreeNode());
        rootNode.add(second);
//        rootNode.add(new DefaultMutableTreeNode());
//        rootNode.add(secondMateral);
//        rootNode.add(new DefaultMutableTreeNode());
//        rootNode.add(secondPateral);
        initComponents();
        Highlighter highligher = HighlighterFactory.createSimpleStriping(HighlighterFactory.BEIGE);
        treeTable.setHighlighters(highligher);
        treeTable.setShowGrid(false);
        treeTable.setShowsRootHandles(true);
        configureCommonTableProperties(treeTable);
        treeTable.setTreeCellRenderer(new TreeTableCellRenderer());
        setPerson(pp);
        pelican = pe;
        initValue();
        runCode();
        Utils.setSpinnerHandle(age);
        Utils.setSpinnerHandle(ageBreast);
        Utils.setSpinnerHandle(ageOvary);
        Utils.setSpinnerHandle(ageColorectal);
        Utils.setSpinnerHandle(ageFirstLiveBornChild);
        Utils.setSpinnerHandle(birthControlLenght);
        Utils.setSpinnerHandle(menarche);
        Utils.setSpinnerHandle(menopause);
        Utils.setSpinnerHandle(numberAffected);
        Utils.setSpinnerHandle(numberAffected1);
        Utils.setSpinnerHandle(parity);
//        Utils.setSpinnerHandle(ageOnset);
//        Utils.setSpinnerHandle(weight);
//        Utils.setSpinnerHandle(height);
        Utils.setSpinnerHandle(numAgeMastectomy);
        Utils.setSpinnerHandle(numAgeOophorectomy);
        Utils.HideArrowSpinner(age);
        Utils.HideArrowSpinner(ageBreast);
        Utils.HideArrowSpinner(ageOvary);
        Utils.HideArrowSpinner(ageColorectal);
        Utils.HideArrowSpinner(ageFirstLiveBornChild);
        Utils.HideArrowSpinner(birthControlLenght);
        Utils.HideArrowSpinner(menarche);
        Utils.HideArrowSpinner(menopause);
        Utils.HideArrowSpinner(numberAffected);
        Utils.HideArrowSpinner(numberAffected1);
        Utils.HideArrowSpinner(parity);
//        Utils.HideArrowSpinner(ageOnset);
//        Utils.HideArrowSpinner(weight);
//        Utils.HideArrowSpinner(height);
        Utils.HideArrowSpinner(age);
        Utils.HideArrowSpinner(numAgeMastectomy);
        Utils.HideArrowSpinner(numAgeOophorectomy);
        Utils.HideArrowSpinner(hrtYears);
    }
    private void configureCommonTableProperties(JXTable table) {
        table.setColumnControlVisible(true);
        StringValue toString = new StringValue() {
            public String getString(Object value) {
                if (value instanceof Point) {
                    Point p = (Point) value;
                    return createString(p.x, p.y);
                } else if (value instanceof Dimension) {
                    Dimension dim = (Dimension) value;
                    return createString(dim.width, dim.height);
                }
                return "";
            }
            private String createString(int width, int height) {
                return "(" + width + ", " + height + ")";
            }
        };
//        TableCellRenderer renderer = new DefaultTableRenderer(toString);
//        table.setDefaultRenderer(Point.class, renderer);
//        table.setDefaultRenderer(Dimension.class, renderer);
    }
    private void initValue() {
//        lblID.setText(person.id);
        jLabel53.setVisible(false);
        degree.setVisible(false);
        jLabel42.setVisible(false);
        jLabel43.setVisible(false);
        jLabel44.setVisible(false);
        jLabel45.setVisible(false);
        jLabel63.setVisible(false);
        cmbOophorectomy.setVisible(false);
        numAgeOophorectomy.setVisible(false);
        cmbMastectomy.setVisible(false);
        numAgeMastectomy.setVisible(false);
        numAgeHistopathologi.setVisible(false);
        txtName.setText(person.name);
        age.setValue(person.age);
        ageDeath.setValue(person.ageDead);
        cekProband.setSelected(person.proband);
//        person.Birth_Date = new Date();
        if (person.Birth_Date != null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
            txtBirthDate.setText(dateFormat.format(person.Birth_Date));
        }
        cekBreast.setSelected(person.isAffectedBreast == 1);
        cekOvary.setSelected(person.isAffectedOvary == 1);
        ageBreast.setValue(person.affectedBreast);
        ageOvary.setValue(person.affectedOvary);
        cekBreast1.setSelected(person.isAffectedBreast == 1);
        cekOvary1.setSelected(person.isAffectedOvary == 1);
        ageBreast1.setValue(person.affectedBreast);
        ageOvary1.setValue(person.affectedOvary);
//        ageOnset.setValue(person.ageOnset);
        numWeight.setValue(person.weight);
        numHeight.setValue(person.height);
        menarche.setValue(person.menarche);
        cmbMensCycle.setSelectedIndex(person.menscycle);
        cmbMenopause.setSelectedIndex(person.menopause);
        menopause.setValue(person.age_menopause);
        parity.setValue(person.parity);
        ageFirstLiveBornChild.setValue(person.agefirst);
        Laktasi.setSelectedIndex(person.laktasi);
        birthControlType.setSelectedIndex(person.birthcontroltype);
        birthControlLenght.setValue(person.birthcontrollenght);
        smoking.setSelectedIndex(person.smooking);
        alcohol.setSelectedIndex(person.alcohol);
        hrt.setSelectedIndex(person.HRT);
        cekColorectal.setSelected(person.isColorectal == 1);
        ageColorectal.setValue(person.colorectal);
        cekColorectal1.setSelected(person.isColorectal == 1);
        ageColorectal1.setValue(person.colorectal);
        cekOther1.setSelected(person.otherCancer == 1);
        txtDescOther1.setText(person.descOtherCancer);
        chkAge.setSelected(person.ageProx == 1);
        chkAgeBreast1.setSelected(person.BreastProx == 1);
        chkAgeOvary1.setSelected(person.OvaryProx == 1);
        chkAgeColorectal1.setSelected(person.colorectalProx == 1);
        txtDescOther.setText(person.descOtherCancer);
        cekOtherCancer.setSelected(person.otherCancer == 1);
        chkAgeFirst.setSelected(person.agefirstProx == 1);
        stadium.setSelectedIndex(person.stadium);
        bilateralitas.setSelectedIndex(person.bilateralitas);
//        cmbErlyOnset.setSelectedIndex(person.early_onset);
        mamografi.setSelectedIndex(person.mamografi);
        numberAffected.setValue(person.numberAffectedFamily);
//        maleCancer.setSelectedIndex(person.maleCancer);
//        maleCancer.setVisible(false);
//        jLabel27.setVisible(false);
        maleCancer1.setSelectedIndex(person.biopsi);
        numberAffected1.setValue(person.numBiopsi);
        cmbADH.setSelectedIndex(person.adh);
        chkAgeMeno.setSelected(person.age_menopauseProx == 1);
//        chkAgeOnset.setSelected(person.ageOnsetProx == 1);
        cmbGeneticsTest.setSelectedIndex(person.geneticTest);
        if (person.geneticTest == 0) {
            cmbBRCA1.setSelectedIndex(0);
            cmbBRCA2.setSelectedIndex(0);
            cmbTestOrder.setSelectedIndex(0);
            cmbP53.setSelectedIndex(0);
        } else {
            cmbBRCA1.setSelectedIndex(person.brca1);
            cmbBRCA2.setSelectedIndex(person.brca2);
            cmbTestOrder.setSelectedIndex(person.TestOrder);
            cmbP53.setSelectedIndex(person.p53);
        }
        cmbER.setSelectedIndex(person.er);
        cmbHER2.setSelectedIndex(person.her2);
        cmbPR.setSelectedIndex(person.pr);
//        cmbCK56.setSelectedIndex(person.ck56);
        txtCK56.setText(person.ck56String);
        cmbCK14.setSelectedIndex(person.ck14);
        cmbCK14.setVisible(false);
        jLabel40.setVisible(false);
        cmbOophorectomy.setSelectedIndex(person.Oophorectomy);
        numAgeOophorectomy.setValue(person.Oophorectomy);
        cmbMastectomy.setSelectedIndex(person.Mastectomy);
        numAgeMastectomy.setValue(person.AgeMastectomy);
        cekBreastActionPerformed(null);
        cekOvaryActionPerformed(null);
        cekColorectalActionPerformed(null);
        cekOtherCancerActionPerformed(null);
        cmb_mamoDens.setEnabled(person.mamografi == 0);
        numBMI.setValue(person.count_bmi());
        cmbUSG.setSelectedIndex(person.usg);
        cmbBirads.setSelectedItem(person.birads);
        cmbEtnik.setSelectedItem(person.etnik);
        txtOtherEtnik.setText(person.EtnikOther);
    }
    private void modeAffected() {
        if (person.isAffectedBreast == 0 && person.isAffectedOvary == 0
                && person.isColorectal == 0 && person.otherCancer == 0) {
            if (this.jTabbedPane1.getTabCount() > 5) {
                this.jTabbedPane1.removeTabAt(9);
                this.jTabbedPane1.removeTabAt(7);
                this.jTabbedPane1.removeTabAt(6);
                this.jTabbedPane1.removeTabAt(5);
//            this.jTabbedPane1.removeTabAt(8);
                this.jTabbedPane1.removeTabAt(4);
            }
        }
    }
    private void runCode() {
        Thread t = new Thread() {
            @Override
            public void run() {
                JProgressBar progressBar = new JProgressBar();
//                ArrayList firstList = new ArrayList();
//                ArrayList secondList = new ArrayList();
//                ArrayList secondMateralList = new ArrayList();
//                ArrayList secondPateralList = new ArrayList();
                Iterator it;
                first.removeAllChildren();
                second.removeAllChildren();
//                secondMateral.removeAllChildren();
//                secondPateral.removeAllChildren();
                try {
                    firstDegree = pelican.getFamilyFirstDegree(person, progressBar);
//                    secondDegreePateral = pelican.getFamilyPateralSecondDegree(person, progressBar);
//                    secondDegreeMateral = pelican.getFamilyMateralSecondDegree(person, progressBar);
                    secondDegree = pelican.getFamilySecondDegree(person, progressBar);
                    it = firstDegree.iterator();
                    while (it.hasNext()) {
                        PelicanPerson tmp = (PelicanPerson) it.next();
                        boolean tmpaff = (tmp.isAffectedBreast == 1) || (tmp.isAffectedOvary == 1);
                        String organ = "";
                        String age = "";
                        if (tmp.isAffectedBreast == 1 && tmp.isAffectedOvary == 1) {
                            organ = "Breast and Ovary";
                            age = Integer.toString(tmp.affectedBreast) + " and " + Integer.toString(tmp.affectedOvary);
                        } else if (tmp.isAffectedOvary == 1) {
                            organ = "Ovary";
                            age = Integer.toString(tmp.affectedOvary);
                        } else if (tmp.isAffectedBreast == 1) {
                            organ = "Breast";
                            age = Integer.toString(tmp.affectedBreast);
                        }
                        if (tmpaff) {
                            first.add(new DefaultMutableTreeNode(new TableRowData(
                                    "", tmp.id, organ, age, false)));
                        }
//                        if (tmpaff) {
//                            firstList.add(tmp.age);
//                        }
                    }
                    it = secondDegree.iterator();
                    while (it.hasNext()) {
                        PelicanPerson tmp = (PelicanPerson) it.next();
                        boolean tmpaff = (tmp.isAffectedBreast == 1) || (tmp.isAffectedOvary == 1);
                        String organ = "";
                        String age = "";
                        if (tmp.isAffectedBreast == 1 && tmp.isAffectedOvary == 1) {
                            organ = "Breast and Ovary";
                            age = Integer.toString(tmp.affectedBreast) + " and " + Integer.toString(tmp.affectedOvary);
                        } else if (tmp.isAffectedOvary == 1) {
                            organ = "Ovary";
                            age = Integer.toString(tmp.affectedOvary);
                        } else if (tmp.isAffectedBreast == 1) {
                            organ = "Breast";
                            age = Integer.toString(tmp.affectedBreast);
                        }
                        if (tmpaff) {
                            second.add(new DefaultMutableTreeNode(new TableRowData(
                                    "", tmp.id, organ, age, false)));
                        }
//                        if (tmpaff) {
//                            secondList.add(tmp.age);
//                        }
                    }
//                    it = secondDegreeMateral.iterator();
//                    while (it.hasNext()) {
//                        PelicanPerson tmp = (PelicanPerson) it.next();
//                        boolean tmpaff = tmp.isAffectedBreast == 1;
//                        secondMateral.add(new DefaultMutableTreeNode(new TableRowData(
//                                tmp.id, tmp.name, tmpaff ? "1" : "0",
//                                tmpaff ? "" + tmp.affectedBreast : "", false)));
//                    }
//                    it = secondDegreePateral.iterator();
//                    while (it.hasNext()) {
//                        PelicanPerson tmp = (PelicanPerson) it.next();
//                        boolean tmpaff = tmp.isAffectedBreast == 1;
//                        secondPateral.add(new DefaultMutableTreeNode(new TableRowData(
//                                tmp.id, tmp.name, tmpaff ? "1" : "0",
//                                tmpaff ? "" + tmp.affectedBreast : "", false)));
//                    }
                    int num = pelican.getAffectedFamily(progressBar);
                    numberAffected.setValue(num);
                } catch (Exception e) {
                    System.out.println(e.toString());
                }
            }
        };
        t.start();
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel21 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        age = new javax.swing.JSpinner();
        txtName = new javax.swing.JTextField();
        chkAge = new javax.swing.JCheckBox();
        jLabel46 = new javax.swing.JLabel();
        cekBreast1 = new javax.swing.JCheckBox();
        cekOvary1 = new javax.swing.JCheckBox();
        jLabel48 = new javax.swing.JLabel();
        lblOvary1 = new javax.swing.JLabel();
        chkAgeDead = new javax.swing.JCheckBox();
        jLabel49 = new javax.swing.JLabel();
        chkAgeBreast1 = new javax.swing.JCheckBox();
        chkAgeOvary1 = new javax.swing.JCheckBox();
        ageBreast1 = new javax.swing.JSpinner();
        chkAgeColorectal1 = new javax.swing.JCheckBox();
        ageOvary1 = new javax.swing.JSpinner();
        ageDeath = new javax.swing.JSpinner();
        ageColorectal1 = new javax.swing.JSpinner();
        jLabel50 = new javax.swing.JLabel();
        jLabel51 = new javax.swing.JLabel();
        lblBreast1 = new javax.swing.JLabel();
        lblColorectal1 = new javax.swing.JLabel();
        death = new javax.swing.JCheckBox();
        jLabel52 = new javax.swing.JLabel();
        cekColorectal1 = new javax.swing.JCheckBox();
        degree = new javax.swing.JSpinner();
        jLabel53 = new javax.swing.JLabel();
        txtBirthDate = new javax.swing.JTextField();
        jLabel34 = new javax.swing.JLabel();
        txtDescOther1 = new javax.swing.JTextArea();
        cekProband = new javax.swing.JCheckBox();
        jLabel67 = new javax.swing.JLabel();
        cekOther1 = new javax.swing.JCheckBox();
        jLabel84 = new javax.swing.JLabel();
        cmbEtnik = new javax.swing.JComboBox<>();
        jLabel85 = new javax.swing.JLabel();
        txtOtherEtnik = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        menarche = new javax.swing.JSpinner();
        cmbMenopause = new javax.swing.JComboBox();
        jLabel8 = new javax.swing.JLabel();
        cmbMensCycle = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        birthControlType = new javax.swing.JComboBox();
        jLabel7 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        Laktasi = new javax.swing.JComboBox();
        jLabel5 = new javax.swing.JLabel();
        ageFirstLiveBornChild = new javax.swing.JSpinner();
        jLabel4 = new javax.swing.JLabel();
        parity = new javax.swing.JSpinner();
        birthControlLenght = new javax.swing.JSpinner();
        menopause = new javax.swing.JSpinner();
        chkAgeMeno = new javax.swing.JCheckBox();
        chkAgeFirst = new javax.swing.JCheckBox();
        jLabel35 = new javax.swing.JLabel();
        jXLabel1 = new org.jdesktop.swingx.JXLabel();
        jLabel27 = new javax.swing.JLabel();
        numHeight = new javax.swing.JSpinner();
        jLabel80 = new javax.swing.JLabel();
        numWeight = new javax.swing.JSpinner();
        numBMI = new javax.swing.JSpinner();
        jLabel81 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        smoking = new javax.swing.JComboBox();
        jLabel14 = new javax.swing.JLabel();
        alcohol = new javax.swing.JComboBox();
        jLabel15 = new javax.swing.JLabel();
        hrt = new javax.swing.JComboBox();
        jLabel47 = new javax.swing.JLabel();
        hrtYears = new javax.swing.JSpinner();
        jPanel5 = new javax.swing.JPanel();
        jLabel26 = new javax.swing.JLabel();
        numberAffected = new javax.swing.JSpinner();
        btnUpdate = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        treeTable = new org.jdesktop.swingx.JXTreeTable(new TreeModelClaus(rootNode));
        lblHSBOC = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        cekBreast = new javax.swing.JCheckBox();
        cekOvary = new javax.swing.JCheckBox();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        ageOvary = new javax.swing.JSpinner();
        lblBreast = new javax.swing.JLabel();
        ageBreast = new javax.swing.JSpinner();
        lblOvary = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        cekColorectal = new javax.swing.JCheckBox();
        ageColorectal = new javax.swing.JSpinner();
        lblColorectal = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        cekOtherCancer = new javax.swing.JCheckBox();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtDescOther = new javax.swing.JTextArea();
        chkAgeBreast = new javax.swing.JCheckBox();
        chkAgeOvary = new javax.swing.JCheckBox();
        chkAgeColorectal = new javax.swing.JCheckBox();
        jLabel33 = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        jLabel19 = new javax.swing.JLabel();
        stadium = new javax.swing.JComboBox();
        jLabel23 = new javax.swing.JLabel();
        bilateralitas = new javax.swing.JComboBox();
        jLabel24 = new javax.swing.JLabel();
        mamografi = new javax.swing.JComboBox();
        jLabel28 = new javax.swing.JLabel();
        jLabel55 = new javax.swing.JLabel();
        tnmT = new javax.swing.JTextField();
        jLabel56 = new javax.swing.JLabel();
        tnmN = new javax.swing.JTextField();
        jLabel57 = new javax.swing.JLabel();
        tnmM = new javax.swing.JTextField();
        ageBilateralitas = new javax.swing.JSpinner();
        jLabel58 = new javax.swing.JLabel();
        cmb_mamoDens = new javax.swing.JComboBox();
        jLabel59 = new javax.swing.JLabel();
        multiCancer = new javax.swing.JComboBox();
        cmbUSG = new javax.swing.JComboBox();
        jLabel82 = new javax.swing.JLabel();
        cmbBirads = new javax.swing.JComboBox();
        jLabel83 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jLabel29 = new javax.swing.JLabel();
        maleCancer1 = new javax.swing.JComboBox();
        jLabel31 = new javax.swing.JLabel();
        numberAffected1 = new javax.swing.JSpinner();
        jLabel32 = new javax.swing.JLabel();
        cmbADH = new javax.swing.JComboBox();
        jLabel10 = new javax.swing.JLabel();
        cmbGeneticsTest = new javax.swing.JComboBox();
        jLabel42 = new javax.swing.JLabel();
        jLabel43 = new javax.swing.JLabel();
        numAgeOophorectomy = new javax.swing.JSpinner();
        cmbOophorectomy = new javax.swing.JComboBox();
        jLabel44 = new javax.swing.JLabel();
        jLabel45 = new javax.swing.JLabel();
        numAgeMastectomy = new javax.swing.JSpinner();
        cmbMastectomy = new javax.swing.JComboBox();
        jLabel63 = new javax.swing.JLabel();
        numAgeHistopathologi = new javax.swing.JSpinner();
        jLabel64 = new javax.swing.JLabel();
        cmbtypeHistopathology = new javax.swing.JComboBox();
        jLabel65 = new javax.swing.JLabel();
        cmbgradeHistopathology = new javax.swing.JComboBox();
        jLabel66 = new javax.swing.JLabel();
        txtOthertypeHis = new javax.swing.JTextArea();
        jPanel7 = new javax.swing.JPanel();
        cmbER = new javax.swing.JComboBox();
        jLabel37 = new javax.swing.JLabel();
        jLabel38 = new javax.swing.JLabel();
        cmbHER2 = new javax.swing.JComboBox();
        cmbPR = new javax.swing.JComboBox();
        jLabel39 = new javax.swing.JLabel();
        jLabel40 = new javax.swing.JLabel();
        jLabel41 = new javax.swing.JLabel();
        cmbCK14 = new javax.swing.JComboBox();
        jLabel60 = new javax.swing.JLabel();
        numki76 = new javax.swing.JSpinner();
        jLabel61 = new javax.swing.JLabel();
        txtCK56 = new javax.swing.JTextField();
        jLabel62 = new javax.swing.JLabel();
        cmbSubtype = new javax.swing.JComboBox();
        jPanel9 = new javax.swing.JPanel();
        jLabel36 = new javax.swing.JLabel();
        cmbP53 = new javax.swing.JComboBox();
        cmbTestOrder = new javax.swing.JComboBox();
        jLabel25 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        cmbBRCA2 = new javax.swing.JComboBox();
        jLabel12 = new javax.swing.JLabel();
        cmbBRCA1 = new javax.swing.JComboBox();
        txtbrca1 = new javax.swing.JTextField();
        txtbrca2 = new javax.swing.JTextField();
        jLabel54 = new javax.swing.JLabel();
        cmbPTEN = new javax.swing.JComboBox();
        jLabel68 = new javax.swing.JLabel();
        cmbATM = new javax.swing.JComboBox();
        cmbCHEK2 = new javax.swing.JComboBox();
        jLabel69 = new javax.swing.JLabel();
        jPanel10 = new javax.swing.JPanel();
        jLabel70 = new javax.swing.JLabel();
        cmbBCT = new javax.swing.JComboBox();
        jLabel71 = new javax.swing.JLabel();
        jLabel72 = new javax.swing.JLabel();
        cmdMastectomySur = new javax.swing.JComboBox();
        jLabel73 = new javax.swing.JLabel();
        cmbOophorectomySur = new javax.swing.JComboBox();
        jLabel74 = new javax.swing.JLabel();
        cmbPropBilMas = new javax.swing.JComboBox();
        jLabel75 = new javax.swing.JLabel();
        cmbPropBilSal = new javax.swing.JComboBox();
        jLabel76 = new javax.swing.JLabel();
        cmbChemo = new javax.swing.JComboBox();
        jLabel77 = new javax.swing.JLabel();
        cmbRadiotherapy = new javax.swing.JComboBox();
        cmbHormonalTherapy = new javax.swing.JComboBox();
        jLabel78 = new javax.swing.JLabel();
        jLabel79 = new javax.swing.JLabel();
        cmbTargetedTherapy = new javax.swing.JComboBox();
        txtTargetedTheraphy = new javax.swing.JTextField();
        txtChemotherapy = new javax.swing.JTextField();
        cmbHormonalTherapyYes = new javax.swing.JComboBox();

        addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                formFocusLost(evt);
            }
        });
        addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentHidden(java.awt.event.ComponentEvent evt) {
                formComponentHidden(evt);
            }
        });

        jTabbedPane1.setTabLayoutPolicy(javax.swing.JTabbedPane.SCROLL_TAB_LAYOUT);
        jTabbedPane1.setPreferredSize(new java.awt.Dimension(0, 0));
        jTabbedPane1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTabbedPane1FocusLost(evt);
            }
        });

        jLabel21.setText(lc.getWord("general.statusproband"));

        jLabel1.setText(lc.getWord("general.name"));

        jLabel22.setText(lc.getWord("general.currentAge"));

        age.setModel(new javax.swing.SpinnerNumberModel(0, 0, null, 1));
        age.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                ageStateChanged(evt);
            }
        });

        txtName.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtNameFocusLost(evt);
            }
        });
        txtName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNameActionPerformed(evt);
            }
        });

        chkAge.setText(lc.getWord("general.estimation"));
        chkAge.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkAgeActionPerformed(evt);
            }
        });

        jLabel46.setText(lc.getWord("general.birthDate"));

        cekBreast1.setText(lc.getWord("general.breast"));
        cekBreast1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cekBreast1ActionPerformed(evt);
            }
        });

        cekOvary1.setText(lc.getWord("general.ovary"));
        cekOvary1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cekOvary1ActionPerformed(evt);
            }
        });

        jLabel48.setText(lc.getWord("general.affected"));

        lblOvary1.setText(lc.getWord("general.age"));
        lblOvary1.setEnabled(false);

        chkAgeDead.setText(lc.getWord("general.estimation"));
        chkAgeDead.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkAgeDeadActionPerformed(evt);
            }
        });

        jLabel49.setText(lc.getWord("general.affected"));

        chkAgeBreast1.setText(lc.getWord("general.estimation"));
        chkAgeBreast1.setEnabled(false);
        chkAgeBreast1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkAgeBreast1ActionPerformed(evt);
            }
        });

        chkAgeOvary1.setText(lc.getWord("general.estimation"));
        chkAgeOvary1.setEnabled(false);
        chkAgeOvary1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkAgeOvary1ActionPerformed(evt);
            }
        });

        ageBreast1.setEnabled(false);
        ageBreast1.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                ageBreast1StateChanged(evt);
            }
        });

        chkAgeColorectal1.setText(lc.getWord("general.estimation"));
        chkAgeColorectal1.setEnabled(false);
        chkAgeColorectal1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkAgeColorectal1ActionPerformed(evt);
            }
        });

        ageOvary1.setEnabled(false);
        ageOvary1.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                ageOvary1StateChanged(evt);
            }
        });

        ageDeath.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                ageDeathStateChanged(evt);
            }
        });

        ageColorectal1.setEnabled(false);
        ageColorectal1.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                ageColorectal1StateChanged(evt);
            }
        });

        jLabel50.setText(lc.getWord("general.affected"));

        jLabel51.setText(lc.getWord("general.ageOfDeath"));

        lblBreast1.setText(lc.getWord("general.age"));
        lblBreast1.setEnabled(false);

        lblColorectal1.setText(lc.getWord("general.age"));
        lblColorectal1.setEnabled(false);

        death.setText(lc.getWord("general.death"));
        death.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deathActionPerformed(evt);
            }
        });

        jLabel52.setText(lc.getWord("general.stateAlive"));

        cekColorectal1.setText(lc.getWord("general.colorectal"));
        cekColorectal1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cekColorectal1ActionPerformed(evt);
            }
        });

        degree.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                degreeStateChanged(evt);
            }
        });

        jLabel53.setText(lc.getWord("general.degree"));

        txtBirthDate.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtBirthDateFocusLost(evt);
            }
        });
        txtBirthDate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtBirthDateActionPerformed(evt);
            }
        });

        jLabel34.setText(lc.getWord("general.notify"));

        txtDescOther1.setColumns(20);
        txtDescOther1.setRows(5);
        txtDescOther1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtDescOther1FocusLost(evt);
            }
        });

        cekProband.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cekProbandActionPerformed(evt);
            }
        });

        jLabel67.setText(lc.getWord("general.affected"));

        cekOther1.setText(lc.getWord("general.othercancer"));
        cekOther1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cekOther1ActionPerformed(evt);
            }
        });

        jLabel84.setText(lc.getWord("general.etnik"));

        cmbEtnik.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "ACEH", "AMBON", "ARAB", "BALI", "BATAK", "BANJAR", "BETAWI", "BIMA", "BUGIS", "DAYAK", "FLORES", "INDIA", "JAWA", "MADURA", "MELAYU", "MINANG", "PALEMBANG", "PAPUA", "SUNDA", "TIONGHOA", "LAIN-LAIN" }));
        cmbEtnik.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbEtnikActionPerformed(evt);
            }
        });

        jLabel85.setText("Notify");

        txtOtherEtnik.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtOtherEtnikFocusLost(evt);
            }
        });
        txtOtherEtnik.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtOtherEtnikActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel34)
                    .addComponent(jLabel22)
                    .addComponent(jLabel46)
                    .addComponent(jLabel67)
                    .addComponent(jLabel51)
                    .addComponent(jLabel21)
                    .addComponent(jLabel52)
                    .addComponent(jLabel50)
                    .addComponent(jLabel49)
                    .addComponent(jLabel48)
                    .addComponent(jLabel84)
                    .addComponent(jLabel85))
                .addGap(135, 135, 135)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(chkAge))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(cekBreast1, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addGap(159, 159, 159))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cekOther1)
                            .addComponent(txtBirthDate, javax.swing.GroupLayout.PREFERRED_SIZE, 239, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(age, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, 239, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(ageDeath, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(158, 158, 158)
                                .addComponent(chkAgeDead))
                            .addComponent(death)
                            .addComponent(cekProband)
                            .addComponent(cmbEtnik, javax.swing.GroupLayout.PREFERRED_SIZE, 313, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cekColorectal1, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(lblColorectal1)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(ageColorectal1, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(chkAgeColorectal1))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(cekOvary1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGap(60, 60, 60)
                                        .addComponent(lblOvary1)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(ageOvary1, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(chkAgeOvary1))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(lblBreast1)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(ageBreast1, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(chkAgeBreast1))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(txtDescOther1, javax.swing.GroupLayout.PREFERRED_SIZE, 396, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(37, 37, 37)
                                        .addComponent(jLabel53)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(degree, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(txtOtherEtnik, javax.swing.GroupLayout.PREFERRED_SIZE, 313, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel22)
                    .addComponent(age, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(chkAge))
                .addGap(2, 2, 2)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel46)
                    .addComponent(txtBirthDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel49)
                            .addComponent(cekBreast1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel48)
                            .addComponent(cekOvary1))
                        .addGap(3, 3, 3)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel50)
                            .addComponent(cekColorectal1)))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(lblBreast1)
                                .addComponent(ageBreast1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(chkAgeBreast1))
                            .addGap(49, 49, 49))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(lblOvary1)
                                .addComponent(ageOvary1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(chkAgeOvary1))
                            .addGap(3, 3, 3)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(lblColorectal1)
                                .addComponent(ageColorectal1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(chkAgeColorectal1)))))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(cekOther1))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel67)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel34)
                    .addComponent(txtDescOther1, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel53)
                    .addComponent(degree, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(death)
                    .addComponent(jLabel52))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel51)
                    .addComponent(ageDeath, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(chkAgeDead))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cekProband)
                    .addComponent(jLabel21))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cmbEtnik, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel84)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel85)
                            .addComponent(txtOtherEtnik, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(45, Short.MAX_VALUE))
        );

        txtDescOther.getDocument().addDocumentListener(new DocumentListener() {

            @Override
            public void removeUpdate(DocumentEvent e) {

            }

            @Override
            public void insertUpdate(DocumentEvent e) {

            }

            @Override
            public void changedUpdate(DocumentEvent arg0) {
                person.descOtherCancer = txtDescOther.getText();
            }
        });

        jTabbedPane1.addTab(lc.getWord("general.tab_status"), jPanel1);

        menarche.setModel(new javax.swing.SpinnerNumberModel(0, 0, null, 1));
        menarche.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                menarcheStateChanged(evt);
            }
        });

        cmbMenopause.setModel(new javax.swing.DefaultComboBoxModel(new String[] { lc.getWord("general.notYet"), lc.getWord("general.yes"), lc.getWord("general.hysterectomy")}));
        cmbMenopause.setSelectedIndex(0);
        cmbMenopause.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbMenopauseActionPerformed(evt);
            }
        });

        jLabel8.setText(lc.getWord("general.mensCycle"));

        cmbMensCycle.setModel(new javax.swing.DefaultComboBoxModel(new String[] { lc.getWord("general.mensRegular"), lc.getWord("general.mensIrregular")}));
        cmbMensCycle.setSelectedIndex(0);
        cmbMensCycle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbMensCycleActionPerformed(evt);
            }
        });

        jLabel2.setText(lc.getWord("general.menarche"));

        jLabel3.setText(lc.getWord("general.ageAtMenopause"));

        jLabel9.setText(lc.getWord("general.menopause"));

        jLabel11.setText(lc.getWord("general.birthcontrolduration"));

        birthControlType.setModel(new javax.swing.DefaultComboBoxModel(new String[] { lc.getWord("general.no"), lc.getWord("general.hormonal"), lc.getWord("general.nonHormonal")}));
        birthControlType.setSelectedIndex(0);
        birthControlType.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                birthControlTypeActionPerformed(evt);
            }
        });

        jLabel7.setText(lc.getWord("general.birthControlType"));

        jLabel6.setText(lc.getWord("general.breastFeeding"));

        Laktasi.setModel(new javax.swing.DefaultComboBoxModel(new String[] { lc.getWord("general.no"), lc.getWord("general.yeslt6months"), lc.getWord("general.yesgt6months")}));
        Laktasi.setSelectedIndex(0);
        Laktasi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LaktasiActionPerformed(evt);
            }
        });

        jLabel5.setText(lc.getWord("general.ageatfirsthlivebornchild"));

        ageFirstLiveBornChild.setModel(new javax.swing.SpinnerNumberModel(0, 0, null, 1));
        ageFirstLiveBornChild.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                ageFirstLiveBornChildStateChanged(evt);
            }
        });

        jLabel4.setText(lc.getWord("general.numberOfChild"));

        parity.setModel(new javax.swing.SpinnerNumberModel(0, 0, null, 1));
        parity.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                parityStateChanged(evt);
            }
        });

        birthControlLenght.setModel(new javax.swing.SpinnerNumberModel(0, 0, null, 1));
        birthControlLenght.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                birthControlLenghtStateChanged(evt);
            }
        });

        menopause.setModel(new javax.swing.SpinnerNumberModel(0, 0, null, 1));
        menopause.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                menopauseStateChanged(evt);
            }
        });

        chkAgeMeno.setText(lc.getWord("general.estimation"));
        chkAgeMeno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkAgeMenoActionPerformed(evt);
            }
        });

        chkAgeFirst.setText(lc.getWord("general.estimation"));
        chkAgeFirst.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkAgeFirstActionPerformed(evt);
            }
        });

        jLabel35.setText(lc.getWord("general.months"));

        jXLabel1.setText(lc.getWord("msg.notekb"));
        jXLabel1.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jXLabel1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jXLabel1.setLineWrap(true);

        jLabel27.setText(lc.getWord("general.height"));

        numHeight.setModel(new javax.swing.SpinnerNumberModel(0.0d, 0.0d, null, 1.0d));
        numHeight.setEditor(new javax.swing.JSpinner.NumberEditor(numHeight, "0.00"));
        numHeight.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                numHeightStateChanged(evt);
            }
        });

        jLabel80.setText(lc.getWord("general.weight"));

        numWeight.setModel(new javax.swing.SpinnerNumberModel(0.0d, 0.0d, null, 1.0d));
        numWeight.setEditor(new javax.swing.JSpinner.NumberEditor(numWeight, "0.00"));
        numWeight.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                numWeightStateChanged(evt);
            }
        });

        numBMI.setModel(new javax.swing.SpinnerNumberModel(0.0d, 0.0d, null, 1.0d));
        numBMI.setEditor(new javax.swing.JSpinner.NumberEditor(numBMI, "0.00"));
        numBMI.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                numBMIStateChanged(evt);
            }
        });

        jLabel81.setText(lc.getWord("general.bmi"));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jXLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel8)
                            .addComponent(jLabel9)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5)
                            .addComponent(jLabel6)
                            .addComponent(jLabel7)
                            .addComponent(jLabel11)
                            .addComponent(jLabel27)
                            .addComponent(jLabel80)
                            .addComponent(jLabel81))
                        .addGap(92, 92, 92)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(menarche, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(birthControlLenght, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(parity, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(Laktasi, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(birthControlType, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel35))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(ageFirstLiveBornChild, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(chkAgeFirst))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(menopause, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(chkAgeMeno))
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(cmbMensCycle, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(cmbMenopause, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(numWeight, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(numHeight, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(numBMI, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(menarche, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbMensCycle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbMenopause, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(menopause, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(chkAgeMeno))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(parity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(ageFirstLiveBornChild, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(chkAgeFirst))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Laktasi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(birthControlType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(birthControlLenght, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel35))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(numHeight, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel27))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(numWeight, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel80))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(numBMI, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel81))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jXLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 154, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab(lc.getWord("general.reproduction"), jPanel2);

        jLabel13.setText(lc.getWord("general.smoking"));

        smoking.setModel(new javax.swing.DefaultComboBoxModel(new String[] { lc.getWord("general.unknown"),lc.getWord("general.no"), lc.getWord("general.yes")}));
        smoking.setSelectedIndex(0);
        smoking.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                smokingActionPerformed(evt);
            }
        });

        jLabel14.setText(lc.getWord("general.alcohol"));

        alcohol.setModel(new javax.swing.DefaultComboBoxModel(new String[] { lc.getWord("general.unknown"),lc.getWord("general.no"), lc.getWord("general.yes")}));
        alcohol.setSelectedIndex(0);
        alcohol.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                alcoholActionPerformed(evt);
            }
        });

        jLabel15.setText(lc.getWord("general.hrt"));

        hrt.setModel(new javax.swing.DefaultComboBoxModel(new String[] { lc.getWord("general.unknown"),lc.getWord("general.no"), lc.getWord("general.yes")}));
        hrt.setSelectedIndex(0);
        hrt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hrtActionPerformed(evt);
            }
        });

        jLabel47.setText(lc.getWord("general.hrtyears"));

        hrtYears.setModel(new javax.swing.SpinnerNumberModel(0, 0, null, 1));
        hrtYears.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                hrtYearsStateChanged(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel47)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(hrtYears, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel13)
                            .addComponent(jLabel14)
                            .addComponent(jLabel15))
                        .addGap(113, 113, 113)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(smoking, 0, 84, Short.MAX_VALUE)
                            .addComponent(alcohol, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(hrt, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap(513, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(smoking, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(alcohol, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(hrt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(hrtYears, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel47))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab(lc.getWord("general.lifeStyle"), jPanel3);

        jLabel26.setText(lc.getWord("general.numberAffectedFamily"));

        numberAffected.setModel(new javax.swing.SpinnerNumberModel(0, 0, null, 1));
        numberAffected.setEnabled(false);
        numberAffected.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                numberAffectedStateChanged(evt);
            }
        });

        btnUpdate.setText("Update");
        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });

        jScrollPane1.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        jScrollPane1.setAutoscrolls(true);

        treeTable.setAutoStartEditOnKeyStroke(false);
        treeTable.setColumnControlVisible(true);
        treeTable.setScrollsOnExpand(false);
        treeTable.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(treeTable);

        lblHSBOC.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblHSBOC.setText(lc.getWord("msg.hsboc"));
        lblHSBOC.setVerticalAlignment(javax.swing.SwingConstants.TOP);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addComponent(jLabel26)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(numberAffected, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(lblHSBOC, javax.swing.GroupLayout.PREFERRED_SIZE, 356, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 220, Short.MAX_VALUE)
                        .addComponent(btnUpdate)
                        .addGap(121, 121, 121)))
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(lblHSBOC, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel26)
                            .addComponent(numberAffected, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(60, 60, 60)
                        .addComponent(btnUpdate)))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 335, Short.MAX_VALUE)
                .addContainerGap())
        );

        btnUpdate.setVisible(false);

        jTabbedPane1.addTab(lc.getWord("general.familyHistory"), jPanel5);

        cekBreast.setText(lc.getWord("general.breast"));
        cekBreast.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cekBreastActionPerformed(evt);
            }
        });

        cekOvary.setText(lc.getWord("general.ovary"));
        cekOvary.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cekOvaryActionPerformed(evt);
            }
        });

        jLabel17.setText(lc.getWord("general.affected"));

        jLabel18.setText(lc.getWord("general.affected"));

        ageOvary.setModel(new javax.swing.SpinnerNumberModel(0, 0, null, 1));
        ageOvary.setEnabled(false);

        lblBreast.setText(lc.getWord("general.age"));
        lblBreast.setEnabled(false);

        ageBreast.setModel(new javax.swing.SpinnerNumberModel(0, 0, null, 1));
        ageBreast.setEnabled(false);

        lblOvary.setText(lc.getWord("general.age"));
        lblOvary.setEnabled(false);

        jLabel20.setText(lc.getWord("general.affected"));

        cekColorectal.setText(lc.getWord("general.colorectal"));
        cekColorectal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cekColorectalActionPerformed(evt);
            }
        });

        ageColorectal.setModel(new javax.swing.SpinnerNumberModel(0, 0, null, 1));
        ageColorectal.setEnabled(false);

        lblColorectal.setText(lc.getWord("general.age")
        );
        lblColorectal.setEnabled(false);

        jLabel30.setText(lc.getWord("general.affected"));

        cekOtherCancer.setText(lc.getWord("general.othercancer"));
        cekOtherCancer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cekOtherCancerActionPerformed(evt);
            }
        });

        txtDescOther.setColumns(20);
        txtDescOther.setRows(5);
        txtDescOther.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtDescOtherFocusLost(evt);
            }
        });
        jScrollPane2.setViewportView(txtDescOther);
        txtDescOther.getDocument().addDocumentListener(new DocumentListener() {

            @Override
            public void removeUpdate(DocumentEvent e) {

            }

            @Override
            public void insertUpdate(DocumentEvent e) {

            }

            @Override
            public void changedUpdate(DocumentEvent arg0) {
                person.descOtherCancer = txtDescOther.getText();
            }
        });

        chkAgeBreast.setText(lc.getWord("general.estimation")
        );
        chkAgeBreast.setEnabled(false);
        chkAgeBreast.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkAgeBreastActionPerformed(evt);
            }
        });

        chkAgeOvary.setText(lc.getWord("general.estimation")
        );
        chkAgeOvary.setEnabled(false);
        chkAgeOvary.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkAgeOvaryActionPerformed(evt);
            }
        });

        chkAgeColorectal.setText(lc.getWord("general.estimation")
        );
        chkAgeColorectal.setEnabled(false);
        chkAgeColorectal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkAgeColorectalActionPerformed(evt);
            }
        });

        jLabel33.setText(lc.getWord("general.notify"));

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 507, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jLabel18)
                                .addComponent(jLabel17)
                                .addComponent(jLabel20))
                            .addComponent(jLabel30)
                            .addComponent(jLabel33))
                        .addGap(26, 26, 26)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cekOtherCancer)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(cekOvary)
                                    .addComponent(cekBreast)
                                    .addComponent(cekColorectal))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblBreast)
                                    .addComponent(lblOvary)
                                    .addComponent(lblColorectal))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(ageColorectal, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(ageBreast, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(ageOvary, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(chkAgeBreast)
                                    .addComponent(chkAgeOvary)
                                    .addComponent(chkAgeColorectal))))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel18)
                    .addComponent(cekBreast)
                    .addComponent(lblBreast)
                    .addComponent(ageBreast, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(chkAgeBreast))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel17)
                    .addComponent(cekOvary)
                    .addComponent(lblOvary)
                    .addComponent(ageOvary, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(chkAgeOvary))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel20)
                    .addComponent(cekColorectal)
                    .addComponent(lblColorectal)
                    .addComponent(ageColorectal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(chkAgeColorectal))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cekOtherCancer)
                    .addComponent(jLabel30))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel33)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab(lc.getWord("general.cancer"), jPanel4);

        jLabel19.setText(lc.getWord("general.stage"));

        stadium.setModel(new javax.swing.DefaultComboBoxModel(new String[] {lc.getWord("general.unknown"),"I","II","IIIA","IIIB","IV"}));
        stadium.setSelectedIndex(0);
        stadium.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stadiumActionPerformed(evt);
            }
        });

        jLabel23.setText(lc.getWord("general.bilaterality"));

        bilateralitas.setModel(new javax.swing.DefaultComboBoxModel(new String[] {lc.getWord("general.unknown"),lc.getWord("general.yes"),lc.getWord("general.no")}));
        bilateralitas.setSelectedIndex(1);
        bilateralitas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bilateralitasActionPerformed(evt);
            }
        });

        jLabel24.setText(lc.getWord("general.mamography"));

        mamografi.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Dens", "Normal" }));
        mamografi.setSelectedIndex(1);
        mamografi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mamografiActionPerformed(evt);
            }
        });

        jLabel28.setText("TNM");

        jLabel55.setText("T");

        tnmT.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                tnmTFocusLost(evt);
            }
        });

        jLabel56.setText("N");

        tnmN.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                tnmNFocusLost(evt);
            }
        });

        jLabel57.setText("M");

        tnmM.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                tnmMFocusLost(evt);
            }
        });

        ageBilateralitas.setModel(new javax.swing.SpinnerNumberModel(0, 0, null, 1));
        ageBilateralitas.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                ageBilateralitasStateChanged(evt);
            }
        });

        jLabel58.setText(lc.getWord("general.ageBilateralitas"));

        cmb_mamoDens.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Grade 1", "Grade 2", "Grade 3" }));
        cmb_mamoDens.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmb_mamoDensActionPerformed(evt);
            }
        });

        jLabel59.setText(lc.getWord("general.multipleCancerSyndrome"));

        multiCancer.setModel(new javax.swing.DefaultComboBoxModel(new String[] {lc.getWord("general.unknown"),"Li-fraumeni Syndrome","Cowden Syndrome","HNPCC","MEN1-MEN2"}));
        multiCancer.setSelectedIndex(0);
        multiCancer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                multiCancerActionPerformed(evt);
            }
        });

        cmbUSG.setModel(new javax.swing.DefaultComboBoxModel(new String[] {lc.getWord("general.unknown"),lc.getWord("general.yes"),lc.getWord("general.no")}));
        cmbUSG.setSelectedIndex(0);
        cmbUSG.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbUSGActionPerformed(evt);
            }
        });

        jLabel82.setText(lc.getWord("general.usg"));

        cmbBirads.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "1", "2", "3", "4", "5", "6" }));
        cmbBirads.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbBiradsActionPerformed(evt);
            }
        });

        jLabel83.setText(lc.getWord("general.birads"));

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel19)
                    .addComponent(jLabel23)
                    .addComponent(jLabel24)
                    .addComponent(jLabel58)
                    .addComponent(jLabel59)
                    .addComponent(jLabel28)
                    .addComponent(jLabel82)
                    .addComponent(jLabel83))
                .addGap(18, 18, 18)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(ageBilateralitas, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(bilateralitas, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(mamografi, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(cmb_mamoDens, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(multiCancer, javax.swing.GroupLayout.Alignment.LEADING, 0, 144, Short.MAX_VALUE)
                        .addComponent(cmbUSG, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(cmbBirads, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(stadium, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addComponent(jLabel55)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tnmT)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel56)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tnmN, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel57)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tnmM, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(548, Short.MAX_VALUE))
        );

        jPanel8Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {tnmM, tnmN, tnmT});

        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(stadium, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel19))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel28)
                    .addComponent(jLabel55)
                    .addComponent(tnmT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel56)
                    .addComponent(tnmN, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel57)
                    .addComponent(tnmM, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bilateralitas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel23))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ageBilateralitas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel58))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel24)
                    .addComponent(mamografi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cmb_mamoDens, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbUSG, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel82))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbBirads, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel83))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel59)
                    .addComponent(multiCancer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(229, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab(lc.getWord("general.clinicalFeature"), jPanel8);

        jLabel29.setText(lc.getWord("general.benignBiopsies"));

        maleCancer1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { lc.getWord("general.unknown"),lc.getWord("general.no"), lc.getWord("general.yes")}));
        maleCancer1.setSelectedIndex(0);
        maleCancer1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                maleCancer1ActionPerformed(evt);
            }
        });

        jLabel31.setText(lc.getWord("general.howManyTimes"));

        numberAffected1.setModel(new javax.swing.SpinnerNumberModel(0, 0, null, 1));
        numberAffected1.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                numberAffected1StateChanged(evt);
            }
        });

        jLabel32.setText("A D H");

        cmbADH.setModel(new javax.swing.DefaultComboBoxModel(new String[] { lc.getWord("general.unknown"),lc.getWord("general.no"), lc.getWord("general.yes")}));
        cmbADH.setSelectedIndex(0);
        cmbADH.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbADHActionPerformed(evt);
            }
        });

        jLabel10.setText(lc.getWord("general.geneticTest"));

        cmbGeneticsTest.setModel(new javax.swing.DefaultComboBoxModel(new String[] { lc.getWord("general.unknown"),lc.getWord("general.no"), lc.getWord("general.yes")}));
        cmbGeneticsTest.setSelectedIndex(0);
        cmbGeneticsTest.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbGeneticsTestActionPerformed(evt);
            }
        });

        jLabel42.setText(lc.getWord("general.oophorectomy"));

        jLabel43.setText(lc.getWord("general.ageOophorectomy"));

        numAgeOophorectomy.setModel(new javax.swing.SpinnerNumberModel(0, 0, null, 1));
        numAgeOophorectomy.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                numAgeOophorectomyStateChanged(evt);
            }
        });

        cmbOophorectomy.setModel(new javax.swing.DefaultComboBoxModel(new String[] { lc.getWord("general.unknown"),lc.getWord("general.no"), lc.getWord("general.yes")}));
        cmbOophorectomy.setSelectedIndex(0);
        cmbOophorectomy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbOophorectomyActionPerformed(evt);
            }
        });

        jLabel44.setText(lc.getWord("general.mastectomy"));

        jLabel45.setText(lc.getWord("general.ageMastectomy"));

        numAgeMastectomy.setModel(new javax.swing.SpinnerNumberModel(0, 0, null, 1));
        numAgeMastectomy.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                numAgeMastectomyStateChanged(evt);
            }
        });

        cmbMastectomy.setModel(new javax.swing.DefaultComboBoxModel(new String[] { lc.getWord("general.unknown"),lc.getWord("general.no"), lc.getWord("general.yes")}));
        cmbMastectomy.setSelectedIndex(0);
        cmbMastectomy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbMastectomyActionPerformed(evt);
            }
        });

        jLabel63.setText(lc.getWord("general.ageHistopathology"));

        numAgeHistopathologi.setModel(new javax.swing.SpinnerNumberModel(0, 0, null, 1));
        numAgeHistopathologi.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                numAgeHistopathologiStateChanged(evt);
            }
        });

        jLabel64.setText(lc.getWord("general.typeHistopathology"));

        cmbtypeHistopathology.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "DCIS", "IDC", "ILC", "Medullary", "Other" }));
        cmbtypeHistopathology.setSelectedIndex(0);
        cmbtypeHistopathology.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbtypeHistopathologyActionPerformed(evt);
            }
        });

        jLabel65.setText(lc.getWord("general.gradeHistopathology"));

        cmbgradeHistopathology.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "I", "II", "III" }));
        cmbgradeHistopathology.setSelectedIndex(0);
        cmbgradeHistopathology.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbgradeHistopathologyActionPerformed(evt);
            }
        });

        jLabel66.setText(lc.getWord("general.notify"));

        txtOthertypeHis.setColumns(20);
        txtOthertypeHis.setRows(5);
        txtOthertypeHis.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtOthertypeHisFocusLost(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel63, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel64, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel65, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel66, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel31, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel32, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel10, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel29, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGap(33, 33, 33)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(cmbgradeHistopathology, 0, 131, Short.MAX_VALUE)
                        .addComponent(numAgeHistopathologi))
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(jPanel6Layout.createSequentialGroup()
                            .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(maleCancer1, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(cmbGeneticsTest, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(cmbADH, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(numberAffected1)
                                .addComponent(cmbtypeHistopathology, 0, 131, Short.MAX_VALUE))
                            .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel6Layout.createSequentialGroup()
                                    .addGap(57, 57, 57)
                                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel42)
                                        .addComponent(jLabel44))
                                    .addGap(106, 106, 106))
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabel45)
                                        .addComponent(jLabel43))
                                    .addGap(91, 91, 91)))
                            .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(cmbOophorectomy, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(numAgeOophorectomy, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(cmbMastectomy, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(numAgeMastectomy, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addComponent(txtOthertypeHis)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(maleCancer1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel29))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel31)
                            .addComponent(numberAffected1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cmbOophorectomy, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel42))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(numAgeOophorectomy, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel43))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cmbADH, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel32))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel10)
                            .addComponent(cmbGeneticsTest, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel64)
                            .addComponent(cmbtypeHistopathology, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cmbMastectomy, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel44))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(numAgeMastectomy, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel45))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel66)
                    .addComponent(txtOthertypeHis, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbgradeHistopathology, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel65))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(numAgeHistopathologi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel63))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel42.getAccessibleContext().setAccessibleName(lc.getWord("general.oophorectomy"));
        jLabel43.getAccessibleContext().setAccessibleName("");
        jLabel43.getAccessibleContext().setAccessibleDescription("");
        txtDescOther.getDocument().addDocumentListener(new DocumentListener() {

            @Override
            public void removeUpdate(DocumentEvent e) {

            }

            @Override
            public void insertUpdate(DocumentEvent e) {

            }

            @Override
            public void changedUpdate(DocumentEvent arg0) {
                person.descOtherCancer = txtDescOther.getText();
            }
        });

        jTabbedPane1.addTab(lc.getWord("general.patology"), jPanel6);

        cmbER.setModel(new javax.swing.DefaultComboBoxModel(new String[] { lc.getWord("general.notTested"), lc.getWord("general.positive"),lc.getWord("general.negative")}));
        cmbER.setSelectedIndex(0);
        cmbER.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbERActionPerformed(evt);
            }
        });

        jLabel37.setText("PR");

        jLabel38.setText("ER");

        cmbHER2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { lc.getWord("general.notTested"), lc.getWord("general.positive"),lc.getWord("general.negative")}));
        cmbHER2.setSelectedIndex(0);
        cmbHER2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbHER2ActionPerformed(evt);
            }
        });

        cmbPR.setModel(new javax.swing.DefaultComboBoxModel(new String[] { lc.getWord("general.notTested"), lc.getWord("general.positive"),lc.getWord("general.negative")}));
        cmbPR.setSelectedIndex(0);
        cmbPR.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbPRActionPerformed(evt);
            }
        });

        jLabel39.setText("HER2");

        jLabel40.setText("CK14");

        jLabel41.setText("CK5/6");

        cmbCK14.setModel(new javax.swing.DefaultComboBoxModel(new String[] { lc.getWord("general.notTested"), lc.getWord("general.positive"),lc.getWord("general.negative")}));
        cmbCK14.setSelectedIndex(0);
        cmbCK14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbCK14ActionPerformed(evt);
            }
        });

        jLabel60.setText("Ki67");

        numki76.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                numki76StateChanged(evt);
            }
        });

        jLabel61.setText("%");

        txtCK56.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtCK56FocusLost(evt);
            }
        });
        txtCK56.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCK56ActionPerformed(evt);
            }
        });

        jLabel62.setText("Subtype");

        cmbSubtype.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Triple Negative", "Luminal A", "Luminal B", "Her2 " }));
        cmbSubtype.setSelectedIndex(0);
        cmbSubtype.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbSubtypeActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel38)
                    .addComponent(jLabel37)
                    .addComponent(jLabel39)
                    .addComponent(jLabel60)
                    .addComponent(jLabel41)
                    .addComponent(jLabel62))
                .addGap(60, 60, 60)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(cmbER, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(82, 82, 82)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel7Layout.createSequentialGroup()
                                .addGap(100, 100, 100)
                                .addComponent(cmbCK14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel40)))
                    .addComponent(txtCK56, javax.swing.GroupLayout.PREFERRED_SIZE, 239, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(numki76, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel61))
                    .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(cmbHER2, javax.swing.GroupLayout.Alignment.LEADING, 0, 88, Short.MAX_VALUE)
                        .addComponent(cmbPR, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(cmbSubtype, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbER, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel38)
                    .addComponent(cmbCK14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel40))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbPR, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel37))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbHER2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel39))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel60)
                    .addComponent(numki76, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel61))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(jLabel41)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel62))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(txtCK56, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cmbSubtype, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab(lc.getWord("general.markerTesting"), jPanel7);

        jLabel36.setText("P53");

        cmbP53.setModel(new javax.swing.DefaultComboBoxModel(new String[] { lc.getWord("general.notTested"), lc.getWord("general.positive"),lc.getWord("general.negative")}));
        cmbP53.setSelectedIndex(0);
        cmbP53.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbP53ActionPerformed(evt);
            }
        });

        cmbTestOrder.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Unknown", "BRCA1", "BRCA2" }));
        cmbTestOrder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbTestOrderActionPerformed(evt);
            }
        });

        jLabel25.setText("Test Order");

        jLabel16.setText("BRCA 2");

        cmbBRCA2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { lc.getWord("general.notTested"), lc.getWord("general.positive"),lc.getWord("general.negative"),lc.getWord("general.Uninformative"),lc.getWord("general.fus")}));
        cmbBRCA2.setSelectedIndex(0);
        cmbBRCA2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbBRCA2ActionPerformed(evt);
            }
        });

        jLabel12.setText("BRCA 1");

        cmbBRCA1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { lc.getWord("general.notTested"), lc.getWord("general.positive"),lc.getWord("general.negative"),lc.getWord("general.Uninformative"),lc.getWord("general.fus")}));
        cmbBRCA1.setSelectedIndex(0);
        cmbBRCA1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbBRCA1ActionPerformed(evt);
            }
        });

        txtbrca1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtbrca1FocusLost(evt);
            }
        });
        txtbrca1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtbrca1ActionPerformed(evt);
            }
        });

        txtbrca2.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtbrca2FocusLost(evt);
            }
        });
        txtbrca2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtbrca2ActionPerformed(evt);
            }
        });

        jLabel54.setText("PTEN");

        cmbPTEN.setModel(new javax.swing.DefaultComboBoxModel(new String[] { lc.getWord("general.notTested"), lc.getWord("general.positive"),lc.getWord("general.negative")}));
        cmbPTEN.setSelectedIndex(0);
        cmbPTEN.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbPTENActionPerformed(evt);
            }
        });

        jLabel68.setText("ATM");

        cmbATM.setModel(new javax.swing.DefaultComboBoxModel(new String[] { lc.getWord("general.notTested"), lc.getWord("general.positive"),lc.getWord("general.negative")}));
        cmbATM.setSelectedIndex(0);
        cmbATM.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbATMActionPerformed(evt);
            }
        });

        cmbCHEK2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { lc.getWord("general.notTested"), lc.getWord("general.positive"),lc.getWord("general.negative")}));
        cmbCHEK2.setSelectedIndex(0);
        cmbCHEK2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbCHEK2ActionPerformed(evt);
            }
        });

        jLabel69.setText("CHEK2");

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addComponent(jLabel12)
                        .addGap(87, 87, 87)
                        .addComponent(cmbBRCA1, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtbrca1, javax.swing.GroupLayout.PREFERRED_SIZE, 354, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel16)
                            .addGroup(jPanel9Layout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addComponent(jLabel25))
                            .addComponent(jLabel36)
                            .addComponent(jLabel54)
                            .addComponent(jLabel68)
                            .addComponent(jLabel69))
                        .addGap(59, 59, 59)
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel9Layout.createSequentialGroup()
                                .addComponent(cmbBRCA2, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtbrca2, javax.swing.GroupLayout.PREFERRED_SIZE, 354, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(cmbP53, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(cmbTestOrder, 0, 94, Short.MAX_VALUE)
                                    .addComponent(cmbPTEN, javax.swing.GroupLayout.Alignment.LEADING, 0, 0, Short.MAX_VALUE)
                                    .addComponent(cmbATM, javax.swing.GroupLayout.Alignment.LEADING, 0, 0, Short.MAX_VALUE)
                                    .addComponent(cmbCHEK2, javax.swing.GroupLayout.Alignment.LEADING, 0, 0, Short.MAX_VALUE))
                                .addGap(559, 559, 559))))))
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbBRCA1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12)
                    .addComponent(txtbrca1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(3, 3, 3)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbBRCA2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel16)
                    .addComponent(txtbrca2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel25)
                    .addComponent(cmbTestOrder, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel36)
                    .addComponent(cmbP53, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel54)
                    .addComponent(cmbPTEN, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel68)
                    .addComponent(cmbATM, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel69)
                    .addComponent(cmbCHEK2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab(lc.getWord("general.geneticTest"), jPanel9);

        jLabel70.setText(lc.getWord("general.lblBCT"));

        cmbBCT.setModel(new javax.swing.DefaultComboBoxModel(new String[] { lc.getWord("general.unknown"),lc.getWord("general.no"), lc.getWord("general.yes")}));
        cmbBCT.setSelectedIndex(0);
        cmbBCT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbBCTActionPerformed(evt);
            }
        });

        jLabel71.setText("Surgery");

        jLabel72.setText(lc.getWord("general.lblMastectomy"));

        cmdMastectomySur.setModel(new javax.swing.DefaultComboBoxModel(new String[] { lc.getWord("general.unknown"),lc.getWord("general.no"), lc.getWord("general.yes")}));
        cmdMastectomySur.setSelectedIndex(0);
        cmdMastectomySur.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdMastectomySurActionPerformed(evt);
            }
        });

        jLabel73.setText(lc.getWord("general.lblOophorectomy"));

        cmbOophorectomySur.setModel(new javax.swing.DefaultComboBoxModel(new String[] { lc.getWord("general.unknown"),lc.getWord("general.no"), lc.getWord("general.yes")}));
        cmbOophorectomySur.setSelectedIndex(0);
        cmbOophorectomySur.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbOophorectomySurActionPerformed(evt);
            }
        });

        jLabel74.setText(lc.getWord("general.lblPropBilMas"));

        cmbPropBilMas.setModel(new javax.swing.DefaultComboBoxModel(new String[] { lc.getWord("general.unknown"),lc.getWord("general.no"), lc.getWord("general.yes")}));
        cmbPropBilMas.setSelectedIndex(0);
        cmbPropBilMas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbPropBilMasActionPerformed(evt);
            }
        });

        jLabel75.setText(lc.getWord("general.lblPropBilSalOopho"));

        cmbPropBilSal.setModel(new javax.swing.DefaultComboBoxModel(new String[] { lc.getWord("general.unknown"),lc.getWord("general.no"), lc.getWord("general.yes")}));
        cmbPropBilSal.setSelectedIndex(0);
        cmbPropBilSal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbPropBilSalActionPerformed(evt);
            }
        });

        jLabel76.setText(lc.getWord("general.lblChemo"));

        cmbChemo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { lc.getWord("general.unknown"),lc.getWord("general.no"), lc.getWord("general.yes")}));
        cmbChemo.setSelectedIndex(0);
        cmbChemo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbChemoActionPerformed(evt);
            }
        });

        jLabel77.setText(lc.getWord("general.lblRadiotherapy"));

        cmbRadiotherapy.setModel(new javax.swing.DefaultComboBoxModel(new String[] { lc.getWord("general.unknown"),lc.getWord("general.no"), lc.getWord("general.yes")}));
        cmbRadiotherapy.setSelectedIndex(0);
        cmbRadiotherapy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbRadiotherapyActionPerformed(evt);
            }
        });

        cmbHormonalTherapy.setModel(new javax.swing.DefaultComboBoxModel(new String[] { lc.getWord("general.unknown"),lc.getWord("general.no"), lc.getWord("general.yes")}));
        cmbHormonalTherapy.setSelectedIndex(0);
        cmbHormonalTherapy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbHormonalTherapyActionPerformed(evt);
            }
        });

        jLabel78.setText(lc.getWord("general.lblHormonalTherapy"));

        jLabel79.setText(lc.getWord("general.TargetedTherapy"));

        cmbTargetedTherapy.setModel(new javax.swing.DefaultComboBoxModel(new String[] { lc.getWord("general.unknown"),lc.getWord("general.no"), lc.getWord("general.yes")}));
        cmbTargetedTherapy.setSelectedIndex(0);
        cmbTargetedTherapy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbTargetedTherapyActionPerformed(evt);
            }
        });

        txtTargetedTheraphy.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtTargetedTheraphyFocusLost(evt);
            }
        });
        txtTargetedTheraphy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTargetedTheraphyActionPerformed(evt);
            }
        });

        txtChemotherapy.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtChemotherapyFocusLost(evt);
            }
        });
        txtChemotherapy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtChemotherapyActionPerformed(evt);
            }
        });

        cmbHormonalTherapyYes.setModel(new javax.swing.DefaultComboBoxModel(new String[] { lc.getWord("general.HormonalTax"), lc.getWord("general.HormonalAI")}));
        cmbHormonalTherapyYes.setSelectedIndex(0);
        cmbHormonalTherapyYes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbHormonalTherapyYesActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel71))
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel10Layout.createSequentialGroup()
                                .addGap(38, 38, 38)
                                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel75)
                                    .addComponent(jLabel74)
                                    .addComponent(jLabel73)
                                    .addComponent(jLabel70)
                                    .addComponent(jLabel72)))
                            .addGroup(jPanel10Layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabel76))
                            .addGroup(jPanel10Layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabel77))
                            .addGroup(jPanel10Layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabel78))
                            .addGroup(jPanel10Layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabel79)))
                        .addGap(114, 114, 114)
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(cmbTargetedTherapy, 0, 72, Short.MAX_VALUE)
                            .addComponent(cmbHormonalTherapy, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cmbRadiotherapy, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cmbChemo, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cmbPropBilSal, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cmbPropBilMas, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cmbOophorectomySur, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cmbBCT, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cmdMastectomySur, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(cmbHormonalTherapyYes, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtChemotherapy, javax.swing.GroupLayout.DEFAULT_SIZE, 225, Short.MAX_VALUE)
                            .addComponent(txtTargetedTheraphy))))
                .addContainerGap(265, Short.MAX_VALUE))
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel71)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbBCT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel70))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmdMastectomySur, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel72))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbOophorectomySur, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel73))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbPropBilMas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel74))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbPropBilSal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel75))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbChemo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel76)
                    .addComponent(txtChemotherapy, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbRadiotherapy, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel77))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbHormonalTherapy, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel78)
                    .addComponent(cmbHormonalTherapyYes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbTargetedTherapy, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel79)
                    .addComponent(txtTargetedTheraphy, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab(lc.getWord("general.threatment"), jPanel10);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 791, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 505, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents
    private void cekBreastActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cekBreastActionPerformed
        ageBreast.setEnabled(cekBreast.isSelected());
        lblBreast.setEnabled(cekBreast.isSelected());
        chkAgeBreast.setEnabled(cekBreast.isSelected());
        ageBreast1.setEnabled(cekBreast.isSelected());
        lblBreast1.setEnabled(cekBreast.isSelected());
        chkAgeBreast1.setEnabled(cekBreast.isSelected());
        person.isAffectedBreast = cekBreast.isSelected() ? 1 : 0;
        person.affectedBreast = cekBreast.isSelected() ? Integer.parseInt(ageBreast.getValue().toString()) : person.age;
        modeAffected();
    }//GEN-LAST:event_cekBreastActionPerformed
    private void cekOvaryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cekOvaryActionPerformed
        chkAgeOvary.setEnabled(cekOvary.isSelected());
        ageOvary.setEnabled(cekOvary.isSelected());
        lblOvary.setEnabled(cekOvary.isSelected());
        chkAgeOvary1.setEnabled(cekOvary.isSelected());
        ageOvary1.setEnabled(cekOvary.isSelected());
        lblColorectal.setEnabled(cekOvary.isSelected());
        person.isAffectedOvary = cekOvary.isSelected() ? 1 : 0;
        person.affectedOvary = cekOvary.isSelected() ? Integer.parseInt(ageOvary.getValue().toString()) : person.age;
        modeAffected();
    }//GEN-LAST:event_cekOvaryActionPerformed
    private void formFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_formFocusLost
        person.name = txtName.getText();
        person.age = Integer.parseInt(age.getValue().toString());
        if (txtBirthDate.getText().trim().length() > 0) {
            DateFormat dfYYYY = new SimpleDateFormat("dd.MM.yyyy");
            try {
                person.Birth_Date = dfYYYY.parse(txtBirthDate.getText());
            } catch (ParseException ex) {
                Logger.getLogger(PelicanPersonHistory.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        person.menarche = Integer.parseInt(menarche.getValue().toString());
        person.menscycle = cmbMensCycle.getSelectedIndex();
        person.menopause = cmbMenopause.getSelectedIndex();
        person.age_menopause = Integer.parseInt(menopause.getValue().toString());
        person.parity = Integer.parseInt(parity.getValue().toString());
        person.agefirst = Integer.parseInt(ageFirstLiveBornChild.getValue().toString());
        person.laktasi = Laktasi.getSelectedIndex();
        person.birthcontroltype = birthControlType.getSelectedIndex();
        person.birthcontrollenght = Integer.parseInt(birthControlLenght.getValue().toString());
        person.smooking = smoking.getSelectedIndex();
        person.alcohol = alcohol.getSelectedIndex();
        person.HRT = hrt.getSelectedIndex();
        if (person.HRT == 0) {
            person.hrtYears = 0;
        } else {
            person.hrtYears = Integer.parseInt(hrtYears.getValue().toString());
        }
        person.isAffectedBreast = cekBreast.isSelected() ? 1 : 0;
        person.isAffectedOvary = cekOvary.isSelected() ? 1 : 0;
        person.affectedBreast = Integer.parseInt(ageBreast.getValue().toString());
        person.affectedOvary = Integer.parseInt(ageOvary.getValue().toString());
        person.stadium = stadium.getSelectedIndex();
        person.bilateralitas = bilateralitas.getSelectedIndex();
//        person.early_onset = cmbErlyOnset.getSelectedIndex();
        person.age_bilateral = (int) ageBilateralitas.getValue();
//        person.age_bilateral = (int)ageBilateralitas1.getValue();
        person.mamografi = mamografi.getSelectedIndex();
        person.numberAffectedFamily = Integer.parseInt(numberAffected.getValue().toString());
//        person.maleCancer = maleCancer.getSelectedIndex();
        person.geneticTest = cmbGeneticsTest.getSelectedIndex();
//        if (person.geneticTest == 0) {
//            person.brca1 = 0;
//            person.brca2 = 0;
//            person.TestOrder = 0;
//            person.p53 = 0;
//        } else {
        person.brca1 = cmbBRCA1.getSelectedIndex();
        person.brca2 = cmbBRCA2.getSelectedIndex();
        person.brca1txt = txtbrca1.getText();
        person.brca2txt = txtbrca2.getText();
        person.TestOrder = cmbTestOrder.getSelectedIndex();
        person.p53 = cmbP53.getSelectedIndex();
        person.pten = cmbPTEN.getSelectedIndex();
        person.ATM = cmbATM.getSelectedIndex();
        person.CHEK2 = cmbCHEK2.getSelectedIndex();
//        }
        person.er = cmbER.getSelectedIndex();
        person.her2 = cmbHER2.getSelectedIndex();
        person.pr = cmbPR.getSelectedIndex();
//        person.ck56 = cmbCK56.getSelectedIndex();
        person.ck56String = txtCK56.getText();
        person.comSubtype = cmbSubtype.getSelectedIndex();
        person.ck14 = cmbCK14.getSelectedIndex();
        person.Mastectomy = cmbMastectomy.getSelectedIndex();
        person.AgeMastectomy = (int) numAgeMastectomy.getValue();
        person.Oophorectomy = cmbOophorectomy.getSelectedIndex();
        person.AgeOophorectomy = (int) numAgeOophorectomy.getValue();
        person.adh = cmbADH.getSelectedIndex();
        person.age_pathology = (int) numAgeHistopathologi.getValue();
        person.gradeHistopathology = cmbgradeHistopathology.getSelectedIndex();
        person.typeHistopathology = cmbtypeHistopathology.getSelectedIndex();
        person.typeOtherHistopathology = txtOthertypeHis.getText();
        person.prophylacticBilateralMactectomy = cmbPropBilMas.getSelectedIndex();
        person.prophylacticBilateralSalphingoOophorectomy = cmbPropBilSal.getSelectedIndex();
        person.mamodensgrade = cmb_mamoDens.getSelectedIndex();
        person.targetedtherapy = cmbTargetedTherapy.getSelectedIndex();
        person.multiCancerSyndrome = multiCancer.getSelectedIndex();
        person.bct = cmbBCT.getSelectedIndex();
        person.Mastectomy = cmbMastectomy.getSelectedIndex();
        person.Oophorectomy = cmbOophorectomy.getSelectedIndex();
        person.chemotherapy = cmbChemo.getSelectedIndex();
        person.chemotherapyNote = txtChemotherapy.getText();
        person.targetedtherapyNote = txtTargetedTheraphy.getText();
        person.radiotherapy = cmbRadiotherapy.getSelectedIndex();
        person.hormonaltherapy = cmbHormonalTherapy.getSelectedIndex();
        person.hormonaltherapyYes = cmbHormonalTherapyYes.getSelectedIndex();
        person.usg = cmbUSG.getSelectedIndex();
        person.birads = cmbBirads.getSelectedObjects().toString();
    }//GEN-LAST:event_formFocusLost
    private void formComponentHidden(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentHidden
        formFocusLost(null);
    }//GEN-LAST:event_formComponentHidden
    private void menarcheStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_menarcheStateChanged
        person.menarche = Integer.parseInt(menarche.getValue().toString());
    }//GEN-LAST:event_menarcheStateChanged
    private void cmbMensCycleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbMensCycleActionPerformed
        person.menscycle = cmbMensCycle.getSelectedIndex();
    }//GEN-LAST:event_cmbMensCycleActionPerformed
    private void cmbMenopauseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbMenopauseActionPerformed
        person.menopause = cmbMenopause.getSelectedIndex();
    }//GEN-LAST:event_cmbMenopauseActionPerformed
    private void menopauseStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_menopauseStateChanged
        person.age_menopause = Integer.parseInt(menopause.getValue().toString());
    }//GEN-LAST:event_menopauseStateChanged
    private void parityStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_parityStateChanged
        person.parity = Integer.parseInt(parity.getValue().toString());
    }//GEN-LAST:event_parityStateChanged
    private void ageFirstLiveBornChildStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_ageFirstLiveBornChildStateChanged
        person.agefirst = Integer.parseInt(ageFirstLiveBornChild.getValue().toString());
    }//GEN-LAST:event_ageFirstLiveBornChildStateChanged
    private void LaktasiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LaktasiActionPerformed
        person.laktasi = Laktasi.getSelectedIndex();
    }//GEN-LAST:event_LaktasiActionPerformed
    private void birthControlTypeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_birthControlTypeActionPerformed
        person.birthcontroltype = birthControlType.getSelectedIndex();
    }//GEN-LAST:event_birthControlTypeActionPerformed
    private void birthControlLenghtStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_birthControlLenghtStateChanged
        person.birthcontrollenght = Integer.parseInt(birthControlLenght.getValue().toString());
    }//GEN-LAST:event_birthControlLenghtStateChanged
    private void smokingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_smokingActionPerformed
        person.smooking = smoking.getSelectedIndex();
    }//GEN-LAST:event_smokingActionPerformed
    private void alcoholActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_alcoholActionPerformed
        person.alcohol = alcohol.getSelectedIndex();
    }//GEN-LAST:event_alcoholActionPerformed
    private void hrtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_hrtActionPerformed
        person.HRT = hrt.getSelectedIndex();
        hrtYears.setEnabled(person.HRT == 1);
    }//GEN-LAST:event_hrtActionPerformed
    private void stadiumActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_stadiumActionPerformed
        person.stadium = stadium.getSelectedIndex();
    }//GEN-LAST:event_stadiumActionPerformed
    private void bilateralitasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bilateralitasActionPerformed
        person.bilateralitas = bilateralitas.getSelectedIndex();
//        bilateralitas1.setSelectedIndex(person.bilateralitas);
    }//GEN-LAST:event_bilateralitasActionPerformed
    private void mamografiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mamografiActionPerformed
        person.mamografi = mamografi.getSelectedIndex();
        cmb_mamoDens.setEnabled(person.mamografi == 0);
    }//GEN-LAST:event_mamografiActionPerformed
    private void numberAffectedStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_numberAffectedStateChanged
        person.numberAffectedFamily = Integer.parseInt(numberAffected.getValue().toString());
    }//GEN-LAST:event_numberAffectedStateChanged
    private void maleCancer1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_maleCancer1ActionPerformed
        person.biopsi = maleCancer1.getSelectedIndex();
    }//GEN-LAST:event_maleCancer1ActionPerformed
    private void cekColorectalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cekColorectalActionPerformed
        ageColorectal.setEnabled(cekColorectal.isSelected());
        lblColorectal.setEnabled(cekColorectal.isSelected());
        chkAgeColorectal.setEnabled(cekColorectal.isSelected());
        ageColorectal1.setEnabled(cekColorectal.isSelected());
        lblColorectal1.setEnabled(cekColorectal.isSelected());
        chkAgeColorectal1.setEnabled(cekColorectal.isSelected());
        person.isColorectal = cekColorectal.isSelected() ? 1 : 0;
        person.colorectal = cekColorectal.isSelected() ? Integer.parseInt(ageColorectal.getValue().toString()) : person.age;
        modeAffected();
    }//GEN-LAST:event_cekColorectalActionPerformed
    private void cekOtherCancerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cekOtherCancerActionPerformed
        txtDescOther.setEnabled(cekOtherCancer.isSelected());
        txtDescOther1.setEnabled(cekOtherCancer.isSelected());
        person.otherCancer = cekOtherCancer.isSelected() ? 1 : 0;
        modeAffected();
    }//GEN-LAST:event_cekOtherCancerActionPerformed
    private void numberAffected1StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_numberAffected1StateChanged
        person.numBiopsi = Integer.parseInt(numberAffected1.getValue().toString());
    }//GEN-LAST:event_numberAffected1StateChanged
    private void cmbADHActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbADHActionPerformed
        person.adh = cmbADH.getSelectedIndex();
    }//GEN-LAST:event_cmbADHActionPerformed
    private void txtDescOtherFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtDescOtherFocusLost
        txtDescOther1.setText(txtDescOther.getText());
        person.descOtherCancer = txtDescOther.getText();
    }//GEN-LAST:event_txtDescOtherFocusLost
    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
        runCode();
    }//GEN-LAST:event_btnUpdateActionPerformed
    private void chkAgeBreastActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkAgeBreastActionPerformed
        person.BreastProx = chkAgeBreast.isSelected() ? 1 : 0;
        chkAgeBreast1.setSelected(chkAgeBreast.isSelected());
        
    }//GEN-LAST:event_chkAgeBreastActionPerformed
    private void chkAgeOvaryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkAgeOvaryActionPerformed
        person.OvaryProx = chkAgeOvary.isSelected() ? 1 : 0;
        chkAgeOvary1.setSelected(chkAgeOvary.isSelected());
    }//GEN-LAST:event_chkAgeOvaryActionPerformed
    private void chkAgeColorectalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkAgeColorectalActionPerformed
        person.colorectalProx = chkAgeColorectal.isSelected() ? 1 : 0;
        chkAgeColorectal1.setSelected(chkAgeColorectal.isSelected());
    }//GEN-LAST:event_chkAgeColorectalActionPerformed
    private void chkAgeMenoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkAgeMenoActionPerformed
        person.age_menopauseProx = chkAgeMeno.isSelected() ? 1 : 0;
    }//GEN-LAST:event_chkAgeMenoActionPerformed
    private void chkAgeFirstActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkAgeFirstActionPerformed
        person.agefirstProx = chkAgeFirst.isSelected() ? 1 : 0;
    }//GEN-LAST:event_chkAgeFirstActionPerformed
    private void cmbGeneticsTestActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbGeneticsTestActionPerformed
        person.geneticTest = cmbGeneticsTest.getSelectedIndex();
//        cmbBRCA1.setEnabled(person.geneticTest == 1);
//        cmbBRCA2.setEnabled(person.geneticTest == 1);
//        cmbTestOrder.setEnabled(person.geneticTest == 1);
//        cmbP53.setEnabled(person.geneticTest == 1);
    }//GEN-LAST:event_cmbGeneticsTestActionPerformed
    private void cmbBRCA1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbBRCA1ActionPerformed
        person.brca1 = cmbBRCA1.getSelectedIndex();
        testOrderSelect();
    }//GEN-LAST:event_cmbBRCA1ActionPerformed
    private void cmbBRCA2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbBRCA2ActionPerformed
        person.brca2 = cmbBRCA2.getSelectedIndex();
        testOrderSelect();
    }//GEN-LAST:event_cmbBRCA2ActionPerformed
    private void testOrderSelect() {
        cmbTestOrder.setEnabled(false);
        if (cmbBRCA2.getSelectedIndex() == 0 && cmbBRCA1.getSelectedIndex() == 0) {
            cmbTestOrder.setSelectedIndex(0);
        }
        if (cmbBRCA2.getSelectedIndex() > 0 && cmbBRCA1.getSelectedIndex() == 0) {
            cmbTestOrder.setSelectedIndex(2);
        }
        if (cmbBRCA2.getSelectedIndex() == 0 && cmbBRCA1.getSelectedIndex() > 0) {
            cmbTestOrder.setSelectedIndex(1);
        }
        if (cmbBRCA2.getSelectedIndex() > 0 && cmbBRCA1.getSelectedIndex() > 0) {
            cmbTestOrder.setEnabled(true);
        }
    }
    private void cmbP53ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbP53ActionPerformed
        person.p53 = cmbP53.getSelectedIndex();
    }//GEN-LAST:event_cmbP53ActionPerformed
    private void cmbERActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbERActionPerformed
        person.er = cmbER.getSelectedIndex();
    }//GEN-LAST:event_cmbERActionPerformed
    private void cmbHER2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbHER2ActionPerformed
        person.her2 = cmbHER2.getSelectedIndex();
    }//GEN-LAST:event_cmbHER2ActionPerformed
    private void cmbPRActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbPRActionPerformed
        person.pr = cmbPR.getSelectedIndex();
    }//GEN-LAST:event_cmbPRActionPerformed
    private void cmbCK14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbCK14ActionPerformed
        person.ck14 = cmbCK14.getSelectedIndex();
    }//GEN-LAST:event_cmbCK14ActionPerformed
    private void chkAgeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkAgeActionPerformed
        person.ageProx = chkAge.isSelected() ? 1 : 0;
    }//GEN-LAST:event_chkAgeActionPerformed
    private void txtNameFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtNameFocusLost
        person.name = txtName.getText();
    }//GEN-LAST:event_txtNameFocusLost
    private void txtNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNameActionPerformed
        person.name = txtName.getText();
    }//GEN-LAST:event_txtNameActionPerformed
    private void ageStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_ageStateChanged
        person.age = Integer.parseInt(age.getValue().toString());
    }//GEN-LAST:event_ageStateChanged
    private void cmbTestOrderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbTestOrderActionPerformed
        person.TestOrder = cmbTestOrder.getSelectedIndex();
    }//GEN-LAST:event_cmbTestOrderActionPerformed
    private void numAgeOophorectomyStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_numAgeOophorectomyStateChanged
        person.AgeOophorectomy = (int) numAgeOophorectomy.getValue();
    }//GEN-LAST:event_numAgeOophorectomyStateChanged
    private void cmbOophorectomyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbOophorectomyActionPerformed
        person.Oophorectomy = cmbOophorectomy.getSelectedIndex();
        numAgeOophorectomy.setEnabled(cmbOophorectomy.getSelectedIndex() == 1);
        if (cmbOophorectomy.getSelectedIndex() == 0) {
            numAgeOophorectomy.setValue(0);
        }
    }//GEN-LAST:event_cmbOophorectomyActionPerformed
    private void numAgeMastectomyStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_numAgeMastectomyStateChanged
        person.AgeMastectomy = (int) numAgeMastectomy.getValue();
    }//GEN-LAST:event_numAgeMastectomyStateChanged
    private void cmbMastectomyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbMastectomyActionPerformed
        person.Mastectomy = cmbMastectomy.getSelectedIndex();
        numAgeMastectomy.setEnabled(cmbMastectomy.getSelectedIndex() == 1);
        if (cmbMastectomy.getSelectedIndex() == 0) {
            numAgeMastectomy.setValue(0);
        }
    }//GEN-LAST:event_cmbMastectomyActionPerformed

    private void hrtYearsStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_hrtYearsStateChanged
        person.hrtYears = Integer.parseInt(hrtYears.getValue().toString());
    }//GEN-LAST:event_hrtYearsStateChanged

    private void cekBreast1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cekBreast1ActionPerformed
        ageBreast.setEnabled(cekBreast1.isSelected());
        lblBreast.setEnabled(cekBreast1.isSelected());
        chkAgeBreast.setEnabled(cekBreast1.isSelected());
        ageBreast1.setEnabled(cekBreast1.isSelected());
        lblBreast1.setEnabled(cekBreast1.isSelected());
        chkAgeBreast1.setEnabled(cekBreast1.isSelected());
        person.isAffectedBreast = cekBreast1.isSelected() ? 1 : 0;
        person.affectedBreast = cekBreast1.isSelected() ? (int) ageBreast1.getValue() : person.age;
    }//GEN-LAST:event_cekBreast1ActionPerformed

    private void cekOvary1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cekOvary1ActionPerformed
        ageOvary.setEnabled(cekOvary1.isSelected());
        lblOvary.setEnabled(cekOvary1.isSelected());
        chkAgeOvary.setEnabled(cekOvary1.isSelected());
        ageOvary1.setEnabled(cekOvary1.isSelected());
        lblOvary1.setEnabled(cekOvary1.isSelected());
        chkAgeOvary1.setEnabled(cekOvary1.isSelected());
        person.isAffectedOvary = cekOvary1.isSelected() ? 1 : 0;
        person.affectedOvary = cekOvary1.isSelected() ? (int) ageOvary1.getValue() : person.age;
    }//GEN-LAST:event_cekOvary1ActionPerformed

    private void chkAgeDeadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkAgeDeadActionPerformed
        person.ageDeadProx = chkAgeDead.isSelected() ? 1 : 0;
    }//GEN-LAST:event_chkAgeDeadActionPerformed

    private void chkAgeBreast1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkAgeBreast1ActionPerformed
        person.BreastProx = chkAgeBreast1.isSelected() ? 1 : 0;
        chkAgeBreast.setSelected(chkAgeBreast1.isSelected());
    }//GEN-LAST:event_chkAgeBreast1ActionPerformed

    private void chkAgeOvary1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkAgeOvary1ActionPerformed
        person.OvaryProx = chkAgeOvary1.isSelected() ? 1 : 0;
        chkAgeOvary.setSelected(chkAgeOvary1.isSelected());
    }//GEN-LAST:event_chkAgeOvary1ActionPerformed

    private void ageBreast1StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_ageBreast1StateChanged
        person.affectedBreast = (int) ageBreast.getValue();
    }//GEN-LAST:event_ageBreast1StateChanged

    private void chkAgeColorectal1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkAgeColorectal1ActionPerformed
        person.colorectalProx = chkAgeColorectal1.isSelected() ? 1 : 0;
        chkAgeColorectal.setSelected(chkAgeColorectal1.isSelected());
    }//GEN-LAST:event_chkAgeColorectal1ActionPerformed

    private void ageOvary1StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_ageOvary1StateChanged
        person.affectedOvary = (int) ageOvary.getValue();
    }//GEN-LAST:event_ageOvary1StateChanged

    private void ageDeathStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_ageDeathStateChanged
        person.ageDead = (int) ageDeath.getValue();
        age.setValue(person.ageDead);
    }//GEN-LAST:event_ageDeathStateChanged

    private void ageColorectal1StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_ageColorectal1StateChanged
        person.colorectal = (int) ageColorectal.getValue();
    }//GEN-LAST:event_ageColorectal1StateChanged

    private void deathActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deathActionPerformed
        person.dead = death.isSelected();
    }//GEN-LAST:event_deathActionPerformed

    private void cekColorectal1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cekColorectal1ActionPerformed
        ageColorectal.setEnabled(cekColorectal1.isSelected());
        lblColorectal.setEnabled(cekColorectal1.isSelected());
        chkAgeColorectal.setEnabled(cekColorectal1.isSelected());
        ageColorectal1.setEnabled(cekColorectal1.isSelected());
        lblColorectal1.setEnabled(cekColorectal1.isSelected());
        chkAgeColorectal1.setEnabled(cekColorectal1.isSelected());
        person.isColorectal = cekColorectal1.isSelected() ? 1 : 0;
        person.colorectal = cekColorectal1.isSelected() ? (int) ageColorectal.getValue() : person.age;
    }//GEN-LAST:event_cekColorectal1ActionPerformed

    private void degreeStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_degreeStateChanged
        person.degree = (int) degree.getValue();
    }//GEN-LAST:event_degreeStateChanged

    private void txtBirthDateFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtBirthDateFocusLost
        DateFormat dfYYYY = new SimpleDateFormat("dd.MM.yyyy");
        try {
            if (txtBirthDate.getText().trim().length() > 0) {
                person.Birth_Date = dfYYYY.parse(txtBirthDate.getText().trim());
            }
        } catch (ParseException ex) {
            Logger.getLogger(PelicanPersonHistory.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_txtBirthDateFocusLost

    private void txtBirthDateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtBirthDateActionPerformed
        DateFormat dfYYYY = new SimpleDateFormat("dd.MM.yyyy");
        try {
            if (txtBirthDate.getText().trim().length() > 0) {
                person.Birth_Date = dfYYYY.parse(txtBirthDate.getText().trim());
            }
        } catch (ParseException ex) {
            Logger.getLogger(PelicanPersonHistory.class.getName()).log(Level.SEVERE, null, ex);
        }
//        System.out.println(person.Birth_Date);
    }//GEN-LAST:event_txtBirthDateActionPerformed

    private void jTabbedPane1FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTabbedPane1FocusLost
        formFocusLost(evt);
    }//GEN-LAST:event_jTabbedPane1FocusLost

    private void txtDescOther1FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtDescOther1FocusLost
        person.descOtherCancer = txtDescOther1.getText();
    }//GEN-LAST:event_txtDescOther1FocusLost

    private void txtbrca1FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtbrca1FocusLost
        person.brca1txt = txtbrca1.getText();
    }//GEN-LAST:event_txtbrca1FocusLost

    private void txtbrca1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtbrca1ActionPerformed

    }//GEN-LAST:event_txtbrca1ActionPerformed

    private void txtbrca2FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtbrca2FocusLost
        person.brca2txt = txtbrca2.getText();
    }//GEN-LAST:event_txtbrca2FocusLost

    private void txtbrca2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtbrca2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtbrca2ActionPerformed

    private void cmbPTENActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbPTENActionPerformed
        person.pten = cmbPTEN.getSelectedIndex();
    }//GEN-LAST:event_cmbPTENActionPerformed

    private void cekProbandActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cekProbandActionPerformed
        person.proband = cekProband.isSelected();
    }//GEN-LAST:event_cekProbandActionPerformed

    private void ageBilateralitasStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_ageBilateralitasStateChanged
        person.age_bilateral = (int) ageBilateralitas.getValue();
//        ageBilateralitas1.setValue(person.age_bilateral);
    }//GEN-LAST:event_ageBilateralitasStateChanged

    private void cmb_mamoDensActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmb_mamoDensActionPerformed
        person.mamodensgrade = cmb_mamoDens.getSelectedIndex();
    }//GEN-LAST:event_cmb_mamoDensActionPerformed

    private void multiCancerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_multiCancerActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_multiCancerActionPerformed

    private void numki76StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_numki76StateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_numki76StateChanged

    private void txtCK56FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtCK56FocusLost
        person.ck56String = txtCK56.getText();
    }//GEN-LAST:event_txtCK56FocusLost

    private void txtCK56ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCK56ActionPerformed

    }//GEN-LAST:event_txtCK56ActionPerformed

    private void cmbSubtypeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbSubtypeActionPerformed
        person.her2 = cmbSubtype.getSelectedIndex();
    }//GEN-LAST:event_cmbSubtypeActionPerformed

    private void numAgeHistopathologiStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_numAgeHistopathologiStateChanged
        person.AgeMastectomy = (int) numAgeHistopathologi.getValue();
    }//GEN-LAST:event_numAgeHistopathologiStateChanged

    private void cmbtypeHistopathologyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbtypeHistopathologyActionPerformed
        person.geneticTest = cmbtypeHistopathology.getSelectedIndex();
    }//GEN-LAST:event_cmbtypeHistopathologyActionPerformed

    private void cmbgradeHistopathologyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbgradeHistopathologyActionPerformed
        person.gradeHistopathology = cmbgradeHistopathology.getSelectedIndex();
    }//GEN-LAST:event_cmbgradeHistopathologyActionPerformed

    private void txtOthertypeHisFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtOthertypeHisFocusLost
        person.descOtherCancer = txtOthertypeHis.getText();
    }//GEN-LAST:event_txtOthertypeHisFocusLost

    private void cekOther1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cekOther1ActionPerformed
        person.otherCancer = cekOther1.isSelected() ? 1 : 0;
        txtDescOther1.setEnabled(cekOther1.isSelected());
        txtDescOther.setEnabled(cekOther1.isSelected());
    }//GEN-LAST:event_cekOther1ActionPerformed

    private void cmbATMActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbATMActionPerformed
        person.ATM = cmbATM.getSelectedIndex();
    }//GEN-LAST:event_cmbATMActionPerformed

    private void cmbCHEK2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbCHEK2ActionPerformed
        person.CHEK2 = cmbCHEK2.getSelectedIndex();
    }//GEN-LAST:event_cmbCHEK2ActionPerformed

    private void cmbBCTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbBCTActionPerformed
        person.bct = cmbBCT.getSelectedIndex();
    }//GEN-LAST:event_cmbBCTActionPerformed

    private void cmdMastectomySurActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdMastectomySurActionPerformed
        person.Mastectomy = cmdMastectomySur.getSelectedIndex();
    }//GEN-LAST:event_cmdMastectomySurActionPerformed

    private void cmbOophorectomySurActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbOophorectomySurActionPerformed
        person.Oophorectomy = cmbOophorectomySur.getSelectedIndex();
    }//GEN-LAST:event_cmbOophorectomySurActionPerformed

    private void cmbPropBilMasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbPropBilMasActionPerformed
        person.prophylacticBilateralMactectomy = cmbPropBilMas.getSelectedIndex();
    }//GEN-LAST:event_cmbPropBilMasActionPerformed

    private void cmbPropBilSalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbPropBilSalActionPerformed
        person.prophylacticBilateralSalphingoOophorectomy = cmbPropBilSal.getSelectedIndex();
    }//GEN-LAST:event_cmbPropBilSalActionPerformed

    private void cmbChemoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbChemoActionPerformed
        person.chemotherapy = cmbChemo.getSelectedIndex();
    }//GEN-LAST:event_cmbChemoActionPerformed

    private void cmbRadiotherapyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbRadiotherapyActionPerformed
        person.radiotherapy = cmbRadiotherapy.getSelectedIndex();
    }//GEN-LAST:event_cmbRadiotherapyActionPerformed

    private void cmbHormonalTherapyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbHormonalTherapyActionPerformed
        person.hormonaltherapy = cmbHormonalTherapy.getSelectedIndex();
    }//GEN-LAST:event_cmbHormonalTherapyActionPerformed

    private void cmbTargetedTherapyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbTargetedTherapyActionPerformed
        person.targetedtherapy = cmbTargetedTherapy.getSelectedIndex();
    }//GEN-LAST:event_cmbTargetedTherapyActionPerformed

    private void txtTargetedTheraphyFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtTargetedTheraphyFocusLost
        person.targetedtherapyNote = txtTargetedTheraphy.getText();
    }//GEN-LAST:event_txtTargetedTheraphyFocusLost

    private void txtTargetedTheraphyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTargetedTheraphyActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTargetedTheraphyActionPerformed

    private void txtChemotherapyFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtChemotherapyFocusLost
        person.chemotherapyNote = txtChemotherapy.getText();
    }//GEN-LAST:event_txtChemotherapyFocusLost

    private void txtChemotherapyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtChemotherapyActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtChemotherapyActionPerformed

    private void cmbHormonalTherapyYesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbHormonalTherapyYesActionPerformed
        person.hormonaltherapyYes = cmbHormonalTherapy.getSelectedIndex();
    }//GEN-LAST:event_cmbHormonalTherapyYesActionPerformed

    private void tnmTFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tnmTFocusLost
        person.stadiumT = tnmT.getText();
    }//GEN-LAST:event_tnmTFocusLost

    private void tnmNFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tnmNFocusLost
        person.stadiumN = tnmN.getText();
    }//GEN-LAST:event_tnmNFocusLost

    private void tnmMFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tnmMFocusLost
        person.stadiumM = tnmM.getText();
    }//GEN-LAST:event_tnmMFocusLost

    private void numHeightStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_numHeightStateChanged
        person.height = (double) numHeight.getValue();
        numBMI.setValue(person.count_bmi());
    }//GEN-LAST:event_numHeightStateChanged

    private void numWeightStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_numWeightStateChanged
        person.weight = (double) numWeight.getValue();
        numBMI.setValue(person.count_bmi());
    }//GEN-LAST:event_numWeightStateChanged

    private void numBMIStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_numBMIStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_numBMIStateChanged

    private void cmbUSGActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbUSGActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbUSGActionPerformed

    private void cmbBiradsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbBiradsActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbBiradsActionPerformed

    private void cmbEtnikActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbEtnikActionPerformed
        person.etnik = cmbEtnik.getSelectedItem().toString();
    }//GEN-LAST:event_cmbEtnikActionPerformed

    private void txtOtherEtnikFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtOtherEtnikFocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_txtOtherEtnikFocusLost

    private void txtOtherEtnikActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtOtherEtnikActionPerformed
        person.EtnikOther = txtOtherEtnik.getText();
    }//GEN-LAST:event_txtOtherEtnikActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox Laktasi;
    private javax.swing.JSpinner age;
    private javax.swing.JSpinner ageBilateralitas;
    private javax.swing.JSpinner ageBreast;
    private javax.swing.JSpinner ageBreast1;
    private javax.swing.JSpinner ageColorectal;
    private javax.swing.JSpinner ageColorectal1;
    private javax.swing.JSpinner ageDeath;
    private javax.swing.JSpinner ageFirstLiveBornChild;
    private javax.swing.JSpinner ageOvary;
    private javax.swing.JSpinner ageOvary1;
    private javax.swing.JComboBox alcohol;
    private javax.swing.JComboBox bilateralitas;
    private javax.swing.JSpinner birthControlLenght;
    private javax.swing.JComboBox birthControlType;
    private javax.swing.JButton btnUpdate;
    private javax.swing.JCheckBox cekBreast;
    private javax.swing.JCheckBox cekBreast1;
    private javax.swing.JCheckBox cekColorectal;
    private javax.swing.JCheckBox cekColorectal1;
    private javax.swing.JCheckBox cekOther1;
    private javax.swing.JCheckBox cekOtherCancer;
    private javax.swing.JCheckBox cekOvary;
    private javax.swing.JCheckBox cekOvary1;
    private javax.swing.JCheckBox cekProband;
    private javax.swing.JCheckBox chkAge;
    private javax.swing.JCheckBox chkAgeBreast;
    private javax.swing.JCheckBox chkAgeBreast1;
    private javax.swing.JCheckBox chkAgeColorectal;
    private javax.swing.JCheckBox chkAgeColorectal1;
    private javax.swing.JCheckBox chkAgeDead;
    private javax.swing.JCheckBox chkAgeFirst;
    private javax.swing.JCheckBox chkAgeMeno;
    private javax.swing.JCheckBox chkAgeOvary;
    private javax.swing.JCheckBox chkAgeOvary1;
    private javax.swing.JComboBox cmbADH;
    private javax.swing.JComboBox cmbATM;
    private javax.swing.JComboBox cmbBCT;
    private javax.swing.JComboBox cmbBRCA1;
    private javax.swing.JComboBox cmbBRCA2;
    private javax.swing.JComboBox cmbBirads;
    private javax.swing.JComboBox cmbCHEK2;
    private javax.swing.JComboBox cmbCK14;
    private javax.swing.JComboBox cmbChemo;
    private javax.swing.JComboBox cmbER;
    public javax.swing.JComboBox<String> cmbEtnik;
    private javax.swing.JComboBox cmbGeneticsTest;
    private javax.swing.JComboBox cmbHER2;
    private javax.swing.JComboBox cmbHormonalTherapy;
    private javax.swing.JComboBox cmbHormonalTherapyYes;
    private javax.swing.JComboBox cmbMastectomy;
    private javax.swing.JComboBox cmbMenopause;
    private javax.swing.JComboBox cmbMensCycle;
    private javax.swing.JComboBox cmbOophorectomy;
    private javax.swing.JComboBox cmbOophorectomySur;
    private javax.swing.JComboBox cmbP53;
    private javax.swing.JComboBox cmbPR;
    private javax.swing.JComboBox cmbPTEN;
    private javax.swing.JComboBox cmbPropBilMas;
    private javax.swing.JComboBox cmbPropBilSal;
    private javax.swing.JComboBox cmbRadiotherapy;
    private javax.swing.JComboBox cmbSubtype;
    private javax.swing.JComboBox cmbTargetedTherapy;
    private javax.swing.JComboBox cmbTestOrder;
    private javax.swing.JComboBox cmbUSG;
    private javax.swing.JComboBox cmb_mamoDens;
    private javax.swing.JComboBox cmbgradeHistopathology;
    private javax.swing.JComboBox cmbtypeHistopathology;
    private javax.swing.JComboBox cmdMastectomySur;
    private javax.swing.JCheckBox death;
    private javax.swing.JSpinner degree;
    private javax.swing.JComboBox hrt;
    private javax.swing.JSpinner hrtYears;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JLabel jLabel52;
    private javax.swing.JLabel jLabel53;
    private javax.swing.JLabel jLabel54;
    private javax.swing.JLabel jLabel55;
    private javax.swing.JLabel jLabel56;
    private javax.swing.JLabel jLabel57;
    private javax.swing.JLabel jLabel58;
    private javax.swing.JLabel jLabel59;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel60;
    private javax.swing.JLabel jLabel61;
    private javax.swing.JLabel jLabel62;
    private javax.swing.JLabel jLabel63;
    private javax.swing.JLabel jLabel64;
    private javax.swing.JLabel jLabel65;
    private javax.swing.JLabel jLabel66;
    private javax.swing.JLabel jLabel67;
    private javax.swing.JLabel jLabel68;
    private javax.swing.JLabel jLabel69;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel70;
    private javax.swing.JLabel jLabel71;
    private javax.swing.JLabel jLabel72;
    private javax.swing.JLabel jLabel73;
    private javax.swing.JLabel jLabel74;
    private javax.swing.JLabel jLabel75;
    private javax.swing.JLabel jLabel76;
    private javax.swing.JLabel jLabel77;
    private javax.swing.JLabel jLabel78;
    private javax.swing.JLabel jLabel79;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel80;
    private javax.swing.JLabel jLabel81;
    private javax.swing.JLabel jLabel82;
    private javax.swing.JLabel jLabel83;
    private javax.swing.JLabel jLabel84;
    private javax.swing.JLabel jLabel85;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTabbedPane jTabbedPane1;
    private org.jdesktop.swingx.JXLabel jXLabel1;
    private javax.swing.JLabel lblBreast;
    private javax.swing.JLabel lblBreast1;
    private javax.swing.JLabel lblColorectal;
    private javax.swing.JLabel lblColorectal1;
    private javax.swing.JLabel lblHSBOC;
    private javax.swing.JLabel lblOvary;
    private javax.swing.JLabel lblOvary1;
    private javax.swing.JComboBox maleCancer1;
    private javax.swing.JComboBox mamografi;
    private javax.swing.JSpinner menarche;
    private javax.swing.JSpinner menopause;
    private javax.swing.JComboBox multiCancer;
    private javax.swing.JSpinner numAgeHistopathologi;
    private javax.swing.JSpinner numAgeMastectomy;
    private javax.swing.JSpinner numAgeOophorectomy;
    private javax.swing.JSpinner numBMI;
    private javax.swing.JSpinner numHeight;
    private javax.swing.JSpinner numWeight;
    private javax.swing.JSpinner numberAffected;
    private javax.swing.JSpinner numberAffected1;
    private javax.swing.JSpinner numki76;
    private javax.swing.JSpinner parity;
    private javax.swing.JComboBox smoking;
    private javax.swing.JComboBox stadium;
    private javax.swing.JTextField tnmM;
    private javax.swing.JTextField tnmN;
    private javax.swing.JTextField tnmT;
    private org.jdesktop.swingx.JXTreeTable treeTable;
    private javax.swing.JTextField txtBirthDate;
    private javax.swing.JTextField txtCK56;
    private javax.swing.JTextField txtChemotherapy;
    private javax.swing.JTextArea txtDescOther;
    private javax.swing.JTextArea txtDescOther1;
    private javax.swing.JTextField txtName;
    public javax.swing.JTextField txtOtherEtnik;
    private javax.swing.JTextArea txtOthertypeHis;
    private javax.swing.JTextField txtTargetedTheraphy;
    private javax.swing.JTextField txtbrca1;
    private javax.swing.JTextField txtbrca2;
    // End of variables declaration//GEN-END:variables
    /**
     * @return the person
     */
    public PelicanPerson getPerson() {
        return person;
    }
    /**
     * @param person the person to set
     */
    public void setPerson(PelicanPerson person) {
        this.person = person;
    }
}
