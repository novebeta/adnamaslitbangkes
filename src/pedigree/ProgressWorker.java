/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pedigree;
import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;
/**
 *
 * @author axioo
 */
public class ProgressWorker extends SwingWorker<Object, Object> {
    @Override
    protected Object doInBackground() throws Exception {
        JProgressBar pb = new JProgressBar();
        pb.setIndeterminate(true);
        pb.setPreferredSize(new Dimension(175, 20));
        pb.setString("Working");
        pb.setStringPainted(true);
        pb.setValue(0);
        JLabel label = new JLabel("Progress: ");
        JPanel center_panel = new JPanel();
        center_panel.add(label);
        center_panel.add(pb);
        JFrame dialog = new JFrame("Working ...");
//                dialog.setModal(true);
        dialog.getContentPane().add(center_panel, BorderLayout.CENTER);
        dialog.pack();
        dialog.setLocationRelativeTo(null); // center on screen
        dialog.toFront();
        dialog.setVisible(true);
        return null;
    }
}
