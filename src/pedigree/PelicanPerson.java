/**
 * ******************************************************************
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Library General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * @author Copyright (C) Frank Dudbridge
 *
 *******************************************************************
 */
//package uk.ac.mrc.rfcgr;
package pedigree;

import DB.ListPed;
import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.*;

public class PelicanPerson extends JPanel {
    public static final int male = 1;
    public static final int female = 2;
    public static final int affected = 2;
    public static final int unaffected = 1;
    public static final int carrier = 4;
    public static final int unknown = 0;
    public static final String unknownID = "0";
    public static final int with_dna = 2;
    public static final int without_dna = 0;
    // size of clickable area
    private static final int xSizeDefault = 30;
    private static final int ySizeDefault = 30;
    // total size of a symbol
    private static final int xSpaceDefault = 60;
    private static final int ySpaceDefault = 60;
    // size of a pedigree symbol
    private static final int symbolSizeDefault = 28;
    // working values
    public static int xSize = xSizeDefault;
    public static int ySize = ySizeDefault;
    public static int xSpace = xSpaceDefault;
    public static int ySpace = ySpaceDefault;
    public static int symbolSize = symbolSizeDefault;
    public int affectedBreast = 0;
    public int isAffectedBreast = 0;
    public int BreastProx = 0;
    public int affectedOvary = 0;
    public int isAffectedOvary = 0;
    public int OvaryProx = 0;
    public int sex = female;
    public int age = 0;
    public int ageProx = 0;
    public int affection = unaffected;
    public int dna = without_dna;
    public String id = "";
    public String person_id = "";
    public ListPed listped;
    public PelicanPerson father = null;
    public PelicanPerson mother = null;
    public boolean dead = false;
    public boolean proband = false;
    public String name = "";
    public Vector genotype = new Vector();
    public boolean laidOut = false;
    public boolean root = false;
    public int generation = 0;
    public Vector offsping = new Vector();
    public Vector spounse = new Vector();
    //Samanda start
    public int isColorectal = 0;
    public int colorectal = 0;
    public int colorectalProx = 0;
    public int otherCancer = 0;
    public String descOtherCancer = "";
    public int geneticTest = 0;
    public int degree = 0;
    public int bilateralitas = 1;
    public int early_onset = 0;
    public int testResult = 0;
    public int ageOnset = 0;
    public int ageOnsetProx = 0;
    public int ageDead = 0;
    public int ageDeadProx = 0;
    public int biopsi = 0;
    public int numBiopsi = 0;
    public int adh = 0;
    public int laktasi = 0;
    public int menarche = 0;
    public int menscycle = 0;
    public int menopause = 0;
    public int age_menopause = 0;
    public int age_bilateral = 0;
    public int age_menopauseProx = 0;
    public double weight = 0;
    public double height = 0;
    public int parity = 0;
    public int agefirst = 0;
    public int agefirstProx = 0;
    public int birthcontroltype = 0;
    public int birthcontrollenght = 0;
    public int smooking = 0;
    public int alcohol = 0;
    public int HRT = 0;
    public int stadium = 0;
    public String stadiumT;
    public String stadiumN;
    public String stadiumM;
    public int mamografi = 1;
    public int multiCancerSyndrome = 0;
    public int numberAffectedFamily = 0;
    public int maleCancer = 0;
    public int brca1 = 0;
    public int brca2 = 0;
    public int TestOrder = 0;
    public int p53 = 0;
    public int pten = 0;
    public int CHEK2 = 0;
    public int ATM = 0;
    public String brca1txt = "";
    public String brca2txt = "";
    public String p53txt = "";
    public String ptentxt = "";
    public int er = 0;
    public int pr = 0;
    public int her2 = 0;
    public int ck56 = 0;
    public String ck56String = "";
    public int comSubtype = 0;
    public int ck14 = 0;
    public int Oophorectomy = 0;
    public int AgeOophorectomy = 0;
    public int Mastectomy = 0;
    public int AgeMastectomy = 0;
    public Date Birth_Date;
    public int hrtYears = 0;
    public int bct = 0;
    public int prophylacticBilateralMactectomy = 0;
    public int prophylacticBilateralSalphingoOophorectomy = 0;
    public int chemotherapy = 0;
    public String chemotherapyNote;
    public int radiotherapy = 0;
    public int hormonaltherapy = 0;
    public int hormonaltherapyYes = 0;
    public int targetedtherapy = 0;
    public String targetedtherapyNote;
    public int gradeHistopathology = 0;
    public int typeHistopathology = 0;
    public String typeOtherHistopathology;
    public int mamodensgrade = 0;
    public int age_pathology = 0;
    public int usg = 0;
    public String birads = "";
    public String etnik = "";
    public String EtnikOther = "";
    //Samanda end

    /*
     * {{{ constructors
     */
    public PelicanPerson() {
        super();
        setBackground(Color.white);
        setOpaque(false);
        setPreferredSize(new Dimension(xSize, ySize));
        setSize(xSize, ySize);
        this.sex = female;
//        JOptionPane jop = new JOptionPane();
//        jop.showConfirmDialog(null, new PelicanData());
    }
    public PelicanPerson(int id, int sex, int generation, int ngeno) {
        this();
        this.id = String.valueOf(id);
        this.sex = sex;
        this.generation = generation;
        for (int i = 0; i < ngeno; i++) {
            Vector v = new Vector();
            v.add("0");
            v.add("0");
            genotype.add(v);
        }
    }
    public PelicanPerson(String id, int sex, int generation, int ngeno) {
        this();
        this.id = id;
        this.sex = sex;
        this.generation = generation;
        for (int i = 0; i < ngeno; i++) {
            Vector v = new Vector();
            v.add("0");
            v.add("0");
            genotype.add(v);
        }
    }
    public PelicanPerson(int id, PelicanPerson father, PelicanPerson mother,
            int sex, int generation, int ngeno) {
        this();
        this.id = String.valueOf(id);
        this.father = father;
        this.mother = mother;
        this.sex = sex;
        this.generation = generation;
        for (int i = 0; i < ngeno; i++) {
            Vector v = new Vector();
            v.add("0");
            v.add("0");
            genotype.add(v);
        }
    }
    public PelicanPerson(String id, PelicanPerson father, PelicanPerson mother,
            int sex, int generation, int ngeno) {
        this();
        this.id = id;
        this.father = father;
        this.mother = mother;
        this.sex = sex;
        this.generation = generation;
        for (int i = 0; i < ngeno; i++) {
            Vector v = new Vector();
            v.add("0");
            v.add("0");
            genotype.add(v);
        }
    }
    public PelicanPerson(int id, PelicanPerson father, PelicanPerson mother,
            int sex, int affection, int dna, boolean dead, boolean proband,
            String name, int generation, Vector genotype) {
        this();
        this.id = String.valueOf(id);
        this.father = father;
        this.mother = mother;
        this.sex = sex;
        this.affection = affection;
        this.dna = dna;
        this.dead = dead;
        this.proband = proband;
        this.name = name;
        this.generation = generation;
        this.genotype = genotype;
    }
    public PelicanPerson(String id, PelicanPerson father, PelicanPerson mother,
            int sex, int affection, int dna, boolean dead, boolean proband,
            String name, int generation, Vector genotype) {
        this();
        this.id = id;
        this.father = father;
        this.mother = mother;
        this.sex = sex;
        this.affection = affection;
        this.dna = dna;
        this.dead = dead;
        this.proband = proband;
        this.name = name;
        this.generation = generation;
        this.genotype = genotype;
    }
    public PelicanPerson(PelicanPerson p) {
        this();
        this.id = p.id;
        this.father = p.father;
        this.mother = p.mother;
        this.sex = p.sex;
        this.affection = p.affection;
        this.dna = p.dna;
        this.dead = p.dead;
        this.proband = p.proband;
        this.name = p.name;
        this.generation = p.generation;
        this.genotype = new Vector(p.genotype);
    }
    public double count_bmi() {
        if ((height * height) == 0) {
            return 0.00;
        }
        return weight / (height * height);
    }
    /*
     * }}}
     */
    public boolean isOrphan() {
        return (father == null && mother == null);
    }
    public boolean hasMother() {
        return (mother != null);
    }
    public boolean hasFather() {
        return (father != null);
    }
    public boolean isRoot() {
        return root;
    }
    public void paintComponent(Graphics g) {
        setSize(xSize, ySize);
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        BufferedImage s;
        BufferedImage h;
        BufferedImage dot;
        TexturePaint slatetp;
        TexturePaint hlatetp;
        TexturePaint dotPaint;
        try {
            s = ImageIO.read(this.getClass().getResource("stripe.png"));
            slatetp = new TexturePaint(s, new Rectangle(0, 0, 2, 2));
            h = ImageIO.read(this.getClass().getResource("hstripe.png"));
            hlatetp = new TexturePaint(h, new Rectangle(0, 0, 14, 14));
            dot = ImageIO.read(this.getClass().getResource("dot.png"));
            dotPaint = new TexturePaint(dot, new Rectangle(0, 0, 14, 14));
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        g2.setRenderingHint // smooth antialising on circles, GWW 210205
                (RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setColor(Color.black);
        // g2.setStroke(new BasicStroke(2)); // line thickness of 2, - looks
        // horrible, GWW 2005-01-13
        // JOptionPane.showMessageDialog(this, id);
        if (!PelicanLines.showMarkerNumbers) {
            if (isAffectedBreast == 1 || isAffectedOvary == 1) {
                affection = affected;
            } else if (isAffectedBreast == 0 && isAffectedOvary == 0) {
                affection = unaffected;
            }
        } else if (affection == affected) {
            isAffectedBreast = 1;
        } else if (affection == unaffected) {
            isAffectedBreast = 0;
            isAffectedOvary = 0;
        }
        if (sex == male) {
            g2.drawRect(0, 0, symbolSize, symbolSize);
            // g2.drawRect(1,1,symbolSize-2,symbolSize-2); // try thicker lines,
            // GWW 2005-01-13
            if (!PelicanLines.showMarkerNumbers) {
                if (isAffectedBreast == 1 && isAffectedOvary == 0) {
                    g2.fillRect(0, 0, symbolSize / 2, symbolSize);
                } else if (isAffectedOvary == 1 && isAffectedBreast == 0) {
                    // g2.setPaint(slatetp);
                    g2.fillRect(0, symbolSize / 2, symbolSize, symbolSize / 2);
                } else if (isAffectedOvary == 1 && isAffectedBreast == 1) {
                    // g2.setPaint(slatetp);
                    g2.fillRect(0, 0, symbolSize, symbolSize);
                } else if (isColorectal == 1) {
                    g2.setPaint(hlatetp);
                    g2.fillRect(0, 0, symbolSize, symbolSize);
                } else if (otherCancer == 1) {
                    g2.setPaint(dotPaint);
                    g2.fillRect(0, 0, symbolSize, symbolSize);
                }
            } else if (affection == affected) {
                g2.fillRect(0, 0, symbolSize, symbolSize);
            }
        }
        if (sex == female) {
            g2.drawArc(0, 0, symbolSize, symbolSize, 0, 360);
            // g2.drawArc(1,1,symbolSize-2,symbolSize-2,0,360); // try thicker
            // lines, GWW 2005-01-13
            if (!PelicanLines.showMarkerNumbers) {
                if (isAffectedBreast == 1 && isAffectedOvary == 0) {
                    g2.fillArc(0, 0, symbolSize, symbolSize, 90, 180);
                } else if (isAffectedOvary == 1 && isAffectedBreast == 0) {
                    // g2.setPaint(slatetp);
                    g2.fillArc(0, 0, symbolSize, symbolSize, 180, 180);
                } else if (isAffectedOvary == 1 && isAffectedBreast == 1) {
                    // g2.setPaint(slatetp);
                    g2.fillArc(0, 0, symbolSize, symbolSize, 0, 360);
                } else if (isColorectal == 1) {
                    g2.setPaint(hlatetp);
                    g2.fillArc(0, 0, symbolSize, symbolSize, 0, 360);
                } else if (otherCancer == 1) {
                    g2.setPaint(dotPaint);
                    g2.fillArc(0, 0, symbolSize, symbolSize, 0, 360);
                }
            } else if (affection == affected) {
                g2.fillArc(0, 0, symbolSize, symbolSize, 0, 360);
            }
        }
        if (sex == unknown) {
            int diamondX[] = {symbolSize / 2, symbolSize, symbolSize / 2, 0};
            int diamondY[] = {0, symbolSize / 2, symbolSize, symbolSize / 2};
            Polygon diamond = new Polygon(diamondX, diamondY, 4);
            g2.drawPolygon(diamond);
            if (isAffectedBreast == 1) {
                int diamondX1[] = {symbolSize / 2, symbolSize / 2, 0};
                int diamondY1[] = {symbolSize, 0, symbolSize / 2};
                Polygon diaBre = new Polygon(diamondX1, diamondY1, 3);
                g2.fillPolygon(diaBre);
            }
            if (isAffectedOvary == 1) {
                Polygon diaOva = new Polygon(diamondX, diamondY, 3);
                // g2.setPaint(slatetp);
                g2.fillPolygon(diaOva);
            }
            if (isColorectal == 1) {
                Polygon diaOva = new Polygon(diamondX, diamondY, 3);
//                hlatetp = new TexturePaint(h, new Rectangle(0, 0, 2, 2));
                g2.setPaint(hlatetp);
                g2.fillPolygon(diaOva);
            }
            if (otherCancer == 1) {
                Polygon diaOva = new Polygon(diamondX, diamondY, 3);
                g2.setPaint(dotPaint);
                g2.fillPolygon(diaOva);
            }
        }
        if (PelicanLines.showMarkerNumbers) {
            if (affection == unknown) {
                g2.drawString("?", (symbolSize - g2.getFontMetrics().stringWidth("?")) / 2, (symbolSize + g2.getFontMetrics().getAscent()) / 2);
            }
            if (affection == carrier) {
                g2.drawArc(symbolSize / 3, symbolSize / 3, symbolSize / 3,
                        symbolSize / 3, 0, 360);
                g2.fillArc(symbolSize / 3, symbolSize / 3, symbolSize / 3,
                        symbolSize / 3, 0, 360);
            }
        }
        if (!PelicanLines.showMarkerNumbers) {
            g2.setColor(Color.ORANGE);
            g2.drawString(id,
                    (symbolSize - g2.getFontMetrics().stringWidth(id)) / 2,
                    (symbolSize + g2.getFontMetrics().getAscent()) / 2);
        }
        g2.setColor(Color.black);
    }
    public static void setScale(double s) {
        xSize = (int) (s * xSizeDefault + 0.5);
        ySize = (int) (s * ySizeDefault + 0.5);
        xSpace = (int) (s * xSpaceDefault + 0.5);
        ySpace = (int) (s * ySpaceDefault + 0.5);
        symbolSize = (int) (s * symbolSizeDefault + 0.5);
    }
    public static void changeScale(double s) {
        xSize = (int) (s * xSize + 0.5);
        ySize = (int) (s * ySize + 0.5);
        xSpace = (int) (s * xSpace + 0.5);
        ySpace = (int) (s * ySpace + 0.5);
        symbolSize = (int) (s * symbolSize + 0.5);
    }
    public static void changeVspace(int s) {
        if (ySpace + s > ySize) {
            ySpace += s;
        }
    }
    public static void changeHspace(int s) {
        if (xSpace + s > xSize) {
            xSpace += s;
        }
    }
}
