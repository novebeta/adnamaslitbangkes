/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pedigree;

import DB.*;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.SelectArg;
import com.j256.ormlite.support.DatabaseConnection;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.CodeSource;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.*;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import org.apache.commons.lang3.RandomStringUtils;

/**
 *
 * @author axioo
 */
public class Utils {
    public static final String SAMANDA_FORMULA = "samanda";
    public static final String SAMANDA2_FORMULA = "samanda2";
    public static final String RUN_FORMULA = "run";
    public static final String COMPRISK_DEATH = "comprisk_death";
    public static final String CLAUS_NEW = "clausNew";
    public static final String R_SCRIPT_EXE = "R\\bin\\Rscript.exe";
//    public static final String R_SCRIPT_EXE = "Rscript";
    public static final String R_EXE = "R\\bin\\R.exe";
//    public static final String R_EXE = "R";
    public static final String BRCA_KEY = "/data/brca.key";
    public static final String PARAM_DB = "/data/param.db";
    public static final String fFX3 = "/data/fFX3";
    public static final String fFY3 = "/data/fFY3";
    public static final String fMX3 = "/data/fMX3";
    public static final String fMY3 = "/data/fMY3";
    public static final String COMPRISK_SURV = "/data/compriskSurv3";
    public static final String DEATH_OTHER_CAUSES = "/data/death.othercauses.samuel2011";
    public static final String FDR = "/data/claus_fdr";
    public static final String SDR = "/data/claus_sdr";
    public static final String FDR2 = "/data/claus_2fdr";
    public static final String MO_MAUNT = "/data/claus_mo_maunt";
    public static final String MO_PAUNT = "/data/claus_mo_paunt";
    public static final String M1P1SDR = "/data/claus_1m1psdr";
    public static final String MP2SDR = "/data/claus_2mpsdr";
    public static Dimension dimension = new Dimension();
    public static DecimalFormat forD = new DecimalFormat("#.###");
    public static String DATA_DIR = "";
    public static String SETTINGS_URL = "jdbc:sqlite:settings.db3";
    public static JdbcConnectionSource connectionSource = null;
//    public static Dao<Customers, Integer> customersDao;
//    public static Dao<Customers, Integer> customersDao;
//    public static Dao<ListPed, Integer> listpedDao;
//    public static Dao<Genotype, Integer> genotypeDao;
//    public static Dao<Person, Integer> personDao;
//    public static Dao<Spounse, Integer> spounsesDao;
    public static CustomersDao customersDao;
    public static ListPedDao listpedDao;
    public static GenotypeDao genotypeDao;
    public static PersonDao personDao;
    public static SpounseDao spounsesDao;
    public static String getJarContainingFolder(Class aclass) throws Exception {
        CodeSource codeSource = aclass.getProtectionDomain().getCodeSource();
        File jarFile;
        if (codeSource.getLocation() != null) {
            jarFile = new File(codeSource.getLocation().toURI());
        } else {
            String path = aclass.getResource(aclass.getSimpleName() + ".class").getPath();
            String jarFilePath = path.substring(path.indexOf(":") + 1, path.indexOf("!"));
            jarFilePath = URLDecoder.decode(jarFilePath, "UTF-8");
            jarFile = new File(jarFilePath);
        }
        return jarFile.getParentFile().getAbsolutePath();
    }
    public static void setSpinnerHandle(final JSpinner js) {
        ((JSpinner.DefaultEditor) js.getEditor()).getTextField().addKeyListener(new KeyListener() {
            @Override
            public void keyPressed(KeyEvent e) {
                //System.out.println(js.getValue());
                if (!js.getValue().equals(0)) {
                    return;
                }
                ((JSpinner.DefaultEditor) js.getEditor()).getTextField().selectAll();
            }
            @Override
            public void keyReleased(KeyEvent e) {
                JTextField jtf = ((JSpinner.DefaultEditor) js.getEditor()).getTextField();
                String text = jtf.getText().replace(",", "");
                int oldCaretPos = jtf.getCaretPosition();
                try {
                    Integer newValue = Integer.valueOf(text);
                    js.setValue(newValue);
                    jtf.setCaretPosition(oldCaretPos);
                } catch (NumberFormatException ex) {
                    //Not a number in text field -> do nothing
                }
            }
            @Override
            public void keyTyped(KeyEvent e) {
            }
        });
    }
    public static void InsertDataORM(String pathString, String pedName, Pelican2 p) {
        JdbcConnectionSource conn = null;
        if (pedName.isEmpty()) {
            pedName = "0";
        }
        try {
            conn = new JdbcConnectionSource("jdbc:sqlite:" + pathString);
            DatabaseConnection dbConn = conn.getReadWriteConnection();
//            genotypeDao = DaoManager.createDao(conn, Genotype.class);
//            spounsesDao = DaoManager.createDao(conn, Spounse.class);
//            genotypeDao = DaoManager.createDao(conn, Genotype.class); //new GenotypeDao(conn);
//            spounsesDao = DaoManager.createDao(conn, Spounse.class);//new SpounseDao(conn);
//            personDao =DaoManager.createDao(conn, Person.class);//(conn,genotypeDao,spounsesDao);
//            listpedDao = DaoManager.createDao(conn, ListPed.class);//new ListPedDao(conn,personDao);
            genotypeDao = new GenotypeDao(conn);
            spounsesDao = new SpounseDao(conn);
            personDao = new PersonDao(conn, genotypeDao, spounsesDao);
            listpedDao = new ListPedDao(conn, personDao);
            genotypeDao.setAutoCommit(dbConn, false);
            personDao.setAutoCommit(dbConn, false);
            listpedDao.setAutoCommit(dbConn, false);
            spounsesDao.setAutoCommit(dbConn, false);
//            TableUtils.createTableIfNotExists(conn, ListPed.class);
//            TableUtils.createTableIfNotExists(conn, Genotype.class);
//            TableUtils.createTableIfNotExists(conn, Person.class);
//            TableUtils.createTableIfNotExists(conn, Spounse.class);
//            QueryBuilder<ListPed, Integer> qb = listpedDao.queryBuilder();
//            qb.where().eq("pedname", pedName).and().eq(Customers.ID_CUSTOMERS, p.customer.Id_Customers);
//            ListPed results = listpedDao.queryForFirst(qb.prepare());
            //String[] values = results.getFirstResult();
//            if (results != null) {
//                listpedDao.delete(results);
//            }
            DeleteBuilder<ListPed, Integer> deleteBuilder = listpedDao.deleteBuilder();
            deleteBuilder.where().eq("pedname", pedName).and().eq(Customers.ID_CUSTOMERS, p.customer.Id_Customers);
            deleteBuilder.delete();
            ListPed listPed = new ListPed(pedName);
            listPed.Id_Customers = p.customer.Id_Customers;
            listpedDao.create(listPed);
            for (int i = 0; i < p.getComponentCount(); i++) {
                if (p.getComponent(i) instanceof PelicanPerson) {
                    PelicanPerson person = (PelicanPerson) p.getComponent(i);
                    Person pedigree = new Person(listPed);
                    pedigree.id = person.id;
                    pedigree.father = person.hasFather() ? Integer.parseInt(person.father.id) : 0;
                    pedigree.mother = person.hasMother() ? Integer.parseInt(person.mother.id) : 0;
                    pedigree.sex = person.sex;
                    pedigree.age = person.age;
                    pedigree.Birth_Date = person.Birth_Date;
                    pedigree.ageBreast = person.affectedBreast;
                    pedigree.ageOvary = person.affectedOvary;
                    pedigree.ageColorectal = person.colorectal;
                    pedigree.ageOnset = person.ageOnset;
                    pedigree.age_menopause = person.age_menopause;
                    pedigree.ageDeath = person.ageDead;
                    pedigree.affection = person.affection;
                    pedigree.dead = person.dead ? 1 : 0;
                    pedigree.proband = person.proband ? 1 : 0;
                    pedigree.isBreast = person.isAffectedBreast;
                    pedigree.isOvary = person.isAffectedOvary;
                    pedigree.isColorectal = person.isColorectal;
                    pedigree.name = person.name;
                    pedigree.geneticTest = person.geneticTest;
                    pedigree.degree = person.degree;
                    pedigree.bilateralitas = person.bilateralitas;
                    pedigree.testResult = person.testResult;
                    pedigree.laktasi = person.laktasi;
                    pedigree.menarche = person.menarche;
                    pedigree.menscycle = person.menscycle;
                    pedigree.menopause = person.menopause;
                    pedigree.weight = (float) person.weight;
                    pedigree.height = (float) person.height;
                    pedigree.parity = person.parity;
                    pedigree.agefirst = person.agefirst;
                    pedigree.birthcontroltype = person.birthcontroltype;
                    pedigree.birthcontrollenght = person.birthcontrollenght;
                    pedigree.smoking = person.smooking;
                    pedigree.alcohol = person.alcohol;
                    pedigree.hrt = person.HRT;
                    pedigree.hrtYears = person.hrtYears;
                    pedigree.stadium = person.stadium;
                    pedigree.mamografi = person.mamografi;
                    pedigree.maleCancer = person.maleCancer;
                    pedigree.otherCancer = person.otherCancer;
                    pedigree.descOtherCancer = person.descOtherCancer;
                    pedigree.biopsi = person.biopsi;
                    pedigree.numBiopsi = person.numBiopsi;
                    pedigree.adh = person.adh;
                    pedigree.erly_onset = person.early_onset;
                    pedigree.ageProx = person.ageProx;
                    pedigree.ageBreastProx = person.BreastProx;
                    pedigree.ageOvaryProx = person.OvaryProx;
                    pedigree.ageColorectalProx = person.colorectalProx;
                    pedigree.ageDeathProx = person.ageDeadProx;
                    pedigree.ageOnsetProx = person.ageOnsetProx;
                    pedigree.age_menopauseProx = person.age_menopauseProx;
                    pedigree.agefirstProx = person.agefirstProx;
                    pedigree.brca1 = person.brca1;
                    pedigree.brca2 = person.brca2;
                    pedigree.TestOrder = person.TestOrder;
                    pedigree.p53 = person.p53;
                    pedigree.pten = person.pten;
                    pedigree.CHEK2 = person.CHEK2;
                    pedigree.ATM = person.ATM;
                    pedigree.er = person.er;
                    pedigree.pr = person.pr;
                    pedigree.her2 = person.her2;
                    pedigree.ck56 = person.ck56;
                    pedigree.ck14 = person.ck14;
                    pedigree.bct = person.bct;
                    pedigree.Mastectomy = person.Mastectomy;
                    pedigree.AgeMastectomy = person.AgeMastectomy;
                    pedigree.Oophorectomy = person.Oophorectomy;
                    pedigree.AgeOophorectomy = person.AgeOophorectomy;
                    pedigree.age_bilateral = person.age_bilateral;
                    pedigree.stadiumT = person.stadiumT;
                    pedigree.stadiumN = person.stadiumN;
                    pedigree.stadiumM = person.stadiumM;
                    pedigree.brca1txt = person.brca1txt;
                    pedigree.brca2txt = person.brca2txt;
                    pedigree.p53txt = person.p53txt;
                    pedigree.ptentxt = person.ptentxt;
                    pedigree.chemotherapy = person.chemotherapy;
                    pedigree.chemotherapyNote = person.chemotherapyNote;
                    pedigree.targetedtherapyNote = person.targetedtherapyNote;
                    pedigree.radiotherapy = person.radiotherapy;
                    pedigree.hormonaltherapy = person.hormonaltherapy;
                    pedigree.hormonaltherapyYes = person.hormonaltherapyYes;
                    pedigree.dna = person.dna;
                    pedigree.laidOut = person.laidOut ? 1 : 0;
                    pedigree.root = person.root ? 1 : 0;
                    pedigree.generation = person.generation;
                    pedigree.mamodensgrade = person.mamodensgrade;
                    pedigree.targetedtherapy = person.targetedtherapy;
                    pedigree.multiCancerSyndrome = person.multiCancerSyndrome;
                    pedigree.numberAffectedFamily = person.numberAffectedFamily;
                    pedigree.gradeHistopathology = person.gradeHistopathology;
                    pedigree.prophylacticBilateralMactectomy = person.prophylacticBilateralMactectomy;
                    pedigree.prophylacticBilateralSalphingoOophorectomy = person.prophylacticBilateralSalphingoOophorectomy;
                    personDao.create(pedigree);
                    for (int j = 0; j < person.genotype.size(); j++) {
                        Vector geno = (Vector) person.genotype.get(j);
                        Genotype genotype = new Genotype(j, Integer.parseInt(geno.firstElement() + ""), Integer.parseInt(geno.lastElement() + ""), pedigree);
                        genotypeDao.create(genotype);
                        //pedigree.spounse = genotype;
                    }
                    for (int j = 0; j < person.spounse.size(); j++) {
                        PelicanPerson spounse = (PelicanPerson) person.spounse.get(j);
                        Spounse spo = new Spounse(pedigree, Integer.parseInt(spounse.id));
                        spounsesDao.create(spo);
                        //pedigree.spounse = genotype;
                    }
                    //                    for (int x = 0; x < person.offsping.size(); x++){
                    //                        PelicanPerson anakPerson = (PelicanPerson) person.offsping.elementAt(x);
                    //                        stat.executeUpdate("insert into offspring (pedigree_id,offspring,pedname) values ("
                    //                                + person.id + "," + anakPerson.id + ",'" + pedName + "')");
                    //                    }
                }
            }
            listpedDao.commit(dbConn);
            personDao.commit(dbConn);
            genotypeDao.commit(dbConn);
        } catch (SQLException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                conn.close();
            } catch (IOException ex) {
                Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    public static void copyFileUsingFileChannels(File source, File dest) {
        FileChannel inputChannel = null;
        FileChannel outputChannel = null;
        try {
            inputChannel = new FileInputStream(source).getChannel();
            outputChannel = new FileOutputStream(dest).getChannel();
            outputChannel.transferFrom(inputChannel, 0, inputChannel.size());
        } catch (IOException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                inputChannel.close();
                outputChannel.close();
            } catch (IOException ex) {
                Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    public static void importSamanda(File source, File dest) {
        CustomersDao customersDaoDest;
        ListPedDao listpedDaoDest;
        GenotypeDao genotypeDaoDest;
        PersonDao personDaoDest;
        SpounseDao spounsesDaoDest;
        JdbcConnectionSource connSource = null;
        JdbcConnectionSource connDest = null;
        try {
            connSource = new JdbcConnectionSource("jdbc:sqlite:" + source);
            connDest = new JdbcConnectionSource("jdbc:sqlite:" + dest);
            DatabaseConnection dbConnDest = connSource.getReadWriteConnection();
            genotypeDao = new GenotypeDao(connSource);
            spounsesDao = new SpounseDao(connSource);
            personDao = new PersonDao(connSource, genotypeDao, spounsesDao);
            listpedDao = new ListPedDao(connSource, personDao);
            customersDao = new CustomersDao(connSource, listpedDao);
//            genotypeDao = DaoManager.createDao(connSource, Genotype.class); //new GenotypeDao(conn);
//            spounsesDao = DaoManager.createDao(connSource, Spounse.class);//new SpounseDao(conn);
//            personDao = DaoManager.createDao(connSource, Person.class);//(conn,genotypeDao,spounsesDao);
//            listpedDao = DaoManager.createDao(connSource, ListPed.class);//new ListPedDao(conn,personDao);
            genotypeDaoDest = new GenotypeDao(connDest);
            spounsesDaoDest = new SpounseDao(connDest);
            personDaoDest = new PersonDao(connDest, genotypeDaoDest, spounsesDaoDest);
            listpedDaoDest = new ListPedDao(connDest, personDaoDest);
            customersDaoDest = new CustomersDao(connDest, listpedDaoDest);
            customersDaoDest.setAutoCommit(dbConnDest, false);
            genotypeDaoDest.setAutoCommit(dbConnDest, false);
            personDaoDest.setAutoCommit(dbConnDest, false);
            listpedDaoDest.setAutoCommit(dbConnDest, false);
            spounsesDaoDest.setAutoCommit(dbConnDest, false);
            List<Customers> custs = customersDao.queryForAll();
            for (Iterator<Customers> iterator = custs.iterator(); iterator.hasNext();) {
                Customers next = iterator.next();
                DeleteBuilder<ListPed, Integer> deleteBuilder = listpedDaoDest.deleteBuilder();
                deleteBuilder.where().eq(Customers.ID_CUSTOMERS, next.Id_Customers);
                deleteBuilder.delete();
                customersDaoDest.create(next);
                List<ListPed> listpeds = listpedDao.queryForEq("Id_Customers", next.Id_Customers);
                for (Iterator<ListPed> iterator1 = listpeds.iterator(); iterator1.hasNext();) {
                    ListPed next1 = iterator1.next();
                    listpedDaoDest.create(next1);
                    List<Person> persons = personDao.queryForEq("id_listped", next1.id);
                    for (Iterator<Person> iterator2 = persons.iterator(); iterator2.hasNext();) {
                        Person next2 = iterator2.next();
                        personDaoDest.create(next2);
                        List<Genotype> genotypes = genotypeDao.queryForEq("id_person", next2.person_id);
                        for (Iterator<Genotype> iterator3 = genotypes.iterator(); iterator3.hasNext();) {
                            Genotype next3 = iterator3.next();
                            genotypeDao.create(next3);
                        }
                        List<Spounse> spounses = spounsesDao.queryForEq("id_person", next2.person_id);
                        for (Iterator<Spounse> iterator3 = spounses.iterator(); iterator3.hasNext();) {
                            Spounse next3 = iterator3.next();
                            spounsesDaoDest.create(next3);
                        }
                    }
                }
            }
            customersDaoDest.commit(dbConnDest);
            listpedDaoDest.commit(dbConnDest);
            personDaoDest.commit(dbConnDest);
            genotypeDaoDest.commit(dbConnDest);
            spounsesDaoDest.commit(dbConnDest);
        } catch (SQLException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                connSource.close();
                connDest.close();
            } catch (IOException ex) {
                Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    public static void insertData(String pathString, String pedName, Pelican2 p) throws ClassNotFoundException, SQLException {
        Connection conn = null;
        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:" + pathString.replace("\\", "/"));
            Statement stat = conn.createStatement();
            stat.setQueryTimeout(30);
            conn.setAutoCommit(false);
            stat.executeUpdate("Create  TABLE IF NOT EXISTS genotype([id] integer PRIMARY KEY AUTOINCREMENT UNIQUE"
                    + ",[pos] integer,[first] integer,[second] integer,[pedname] varchar(225));");
            stat.executeUpdate("Create  TABLE IF NOT EXISTS listPed([id] integer PRIMARY KEY AUTOINCREMENT UNIQUE"
                    + ",[pedname] varchar(225));");
            stat.executeUpdate("Create  TABLE if not exists pedigree([pedname] varchar(225),[id] integer,[father] integer"
                    + ",[mother] integer,[sex] integer,[age] integer,[ageBreast] integer,[ageOvary] integer"
                    + ",[ageColorectal] integer,[ageOnset] integer,[age_menopause] integer,[ageDeath] integer"
                    + ",[affection] integer,[dead] integer,[proband] integer,[isBreast] integer,[isOvary] integer"
                    + ",[isColorectal] integer,[name] varchar(50),[geneticTest] integer,[degree] integer"
                    + ",[bilateralitas] integer,[testResult] integer,[laktasi] integer,[menarche] integer"
                    + ",[menscycle] integer,[menopause] integer,[weight] float,[height] float,[parity] integer"
                    + ",[agefirst] integer,[birthcontroltype] integer,[birthcontrollenght] integer"
                    + ",[smoking] integer,[alcohol] integer,[hrt] integer,[stadium] integer,[mamografi] integer"
                    + ",[maleCancer] integer,[otherCancer] integer,[descOtherCancer] text,[biopsi] integer"
                    + ",[numBiopsi] integer,[adh] integer);");
//            stat.executeUpdate("CREATE TABLE IF NOT EXISTS offspring("
//                    + "[id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL"
//                    + ",[pedigree_id] integer,[offspring] integer,[pedname] varchar(225));");
            conn.commit();
            stat.executeUpdate("delete from listPed where pedname = '" + pedName + "'");
            stat.executeUpdate("delete from pedigree where pedname = '" + pedName + "'");
            stat.executeUpdate("delete from genotype where pedname = '" + pedName + "'");
            //stat.executeUpdate("delete from offspring where pedname = '" + pedName + "'");
            conn.commit();
            stat.executeUpdate("insert into listPed (pedname) values ('" + pedName + "')");
            PreparedStatement prepPedigree = conn.prepareStatement("insert into pedigree values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,"
                    + "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);");
            //PreparedStatement prepGenotype = conn.prepareStatement("insert into genotype values (?,?,?,?);");
            for (int i = 0; i < p.getComponentCount(); i++) {
                if (p.getComponent(i) instanceof PelicanPerson) {
                    PelicanPerson person = (PelicanPerson) p.getComponent(i);
                    prepPedigree.setString(1, pedName);
                    prepPedigree.setString(2, person.id);
                    prepPedigree.setString(3, person.hasFather() ? person.father.id : "0");
                    prepPedigree.setString(4, person.hasMother() ? person.mother.id : "0");
                    prepPedigree.setString(5, person.sex + "");
                    prepPedigree.setString(6, person.age + "");
                    prepPedigree.setString(7, person.affectedBreast + "");
                    prepPedigree.setString(8, person.affectedOvary + "");
                    prepPedigree.setString(9, person.colorectal + "");
                    prepPedigree.setString(10, person.ageOnset + "");
                    prepPedigree.setString(11, person.age_menopause + "");
                    prepPedigree.setString(12, person.ageDead + "");
                    prepPedigree.setString(13, person.affection + "");
                    prepPedigree.setString(14, person.dead ? "1" : "0");
                    prepPedigree.setString(15, person.proband ? "1" : "0");
                    prepPedigree.setString(16, person.isAffectedBreast + "");
                    prepPedigree.setString(17, person.isAffectedOvary + "");
                    prepPedigree.setString(18, person.isColorectal + "");
                    prepPedigree.setString(19, person.name);
                    prepPedigree.setString(20, person.geneticTest + "");
                    prepPedigree.setString(21, person.degree + "");
                    prepPedigree.setString(22, person.bilateralitas + "");
                    prepPedigree.setString(23, person.testResult + "");
                    prepPedigree.setString(24, person.laktasi + "");
                    prepPedigree.setString(25, person.menarche + "");
                    prepPedigree.setString(26, person.menscycle + "");
                    prepPedigree.setString(27, person.menopause + "");
                    prepPedigree.setString(28, person.weight + "");
                    prepPedigree.setString(29, person.height + "");
                    prepPedigree.setString(30, person.parity + "");
                    prepPedigree.setString(31, person.agefirst + "");
                    prepPedigree.setString(32, person.birthcontroltype + "");
                    prepPedigree.setString(33, person.birthcontrollenght + "");
                    prepPedigree.setString(34, person.smooking + "");
                    prepPedigree.setString(35, person.alcohol + "");
                    prepPedigree.setString(36, person.HRT + "");
                    prepPedigree.setString(37, person.stadium + "");
                    prepPedigree.setString(38, person.mamografi + "");
                    prepPedigree.setString(39, person.maleCancer + "");
                    prepPedigree.setString(40, person.otherCancer + "");
                    prepPedigree.setString(41, person.descOtherCancer);
                    prepPedigree.setString(42, person.biopsi + "");
                    prepPedigree.setString(43, person.numBiopsi + "");
                    prepPedigree.setString(44, person.adh + "");
                    prepPedigree.addBatch();
                    for (int j = 0; j < person.genotype.size(); j++) {
                        Vector geno = (Vector) person.genotype.get(j);
                        stat.executeUpdate("insert into genotype (id,pos,first,second,pedname) values ("
                                + person.id + "," + j + ","
                                + geno.firstElement() + "," + geno.lastElement() + ",'" + pedName + "')");
//                            outfile.write(" " + spounse.firstElement() + " "
//                                    + spounse.size() - 1);
                    }
//                    for (int x = 0; x < person.offsping.size(); x++){
//                        PelicanPerson anakPerson = (PelicanPerson) person.offsping.elementAt(x);
//                        stat.executeUpdate("insert into offspring (pedigree_id,offspring,pedname) values ("
//                                + person.id + "," + anakPerson.id + ",'" + pedName + "')");
//                    }
                }
            }
            prepPedigree.executeBatch();
            conn.commit();
//            PreparedStatement prep = conn.prepareStatement();
        } catch (SQLException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            conn.rollback();
        } finally {
            conn.close();
        }
    }
    public static Vector getListPedName(String pathString, Customers cust) {
        try {
            Vector list = new Vector();
            Class.forName("org.sqlite.JDBC");
            Connection connection = null;
            try {
                // create a database connection
                connection = DriverManager.getConnection("jdbc:sqlite:" + pathString.replace("\\", "/"));
                Statement statement = connection.createStatement();
                statement.setQueryTimeout(30);
                ResultSet rs = statement
                        .executeQuery("select strftime('%d.%m.%Y',pedname) as pedname from listped where Id_Customers = '"
                                + cust.Id_Customers + "' ORDER BY date(pedname) desc");
                while (rs.next()) {
                    list.add(rs.getString("pedname"));
                }
            } catch (SQLException e) {
                System.err.println(e.getMessage());
                return null;
            } finally {
                try {
                    if (connection != null) {
                        connection.close();
                    }
                } catch (SQLException e) {
                    System.err.println(e);
                }
            }
            return list;
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    public static String getMaxListPedName(String pathString, String Id_Customers) {
        try {
            Class.forName("org.sqlite.JDBC");
            Connection connection = null;
            try {
                // create a database connection
                connection = DriverManager.getConnection("jdbc:sqlite:" + pathString.replace("\\", "/"));
                Statement statement = connection.createStatement();
                statement.setQueryTimeout(30);
                ResultSet rs = statement
                        .executeQuery("SELECT max(date(pedname)) AS pedname FROM listped WHERE Id_Customers = '" + Id_Customers + "'");
                if (rs.next()) {
                    return rs.getString("pedname");
                }
                return null;
//                while (rs.next()) {
//                    list.add(rs.getString("pedname"));
//                }
            } catch (SQLException e) {
                System.err.println(e.getMessage());
                return null;
            } finally {
                try {
                    if (connection != null) {
                        connection.close();
                    }
                } catch (SQLException e) {
                    System.err.println(e);
                }
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    public static void compressFile(
            String infile, String outfile)
            throws Exception {
        System.out.println("Compressing "
                + infile + " to " + outfile);
        long byteCount = 0;
        FileInputStream in
                = new FileInputStream(infile);
        GZIPOutputStream out
                = new GZIPOutputStream(
                        new FileOutputStream(
                                outfile));
        byte[] buf = new byte[16000];
        int read;
        while ((read = in.read(buf)) != -1) {
            out.write(buf, 0, read);
            byteCount += read;
        }
        in.close();
        out.close();
        System.out.println("read "
                + byteCount + " bytes");
        File zipped = new File(outfile);
        System.out.println("wrote "
                + zipped.length() + " bytes");
    }
    public static void decompressFile(
            String infile, String outfile)
            throws Exception {
        System.out.println("Decompressing "
                + infile + " to " + outfile);
        long byteCount = 0;
        GZIPInputStream in
                = new GZIPInputStream(
                        new FileInputStream(infile));
        FileOutputStream out
                = new FileOutputStream(outfile);
        byte[] buf = new byte[16000];
        int read;
        while ((read = in.read(buf)) != -1) {
            out.write(buf, 0, read);
            byteCount += read;
        }
        in.close();
        out.close();
        File unzipped = new File(infile);
        System.out.println("read "
                + unzipped.length() + " bytes");
        System.out.println("wrote "
                + byteCount + " bytes");
    }
    public static void decompressFile(
            String infile, File outfile)
            throws Exception {
        System.out.println("Decompressing "
                + infile + " to " + outfile);
        long byteCount = 0;
        GZIPInputStream in
                = new GZIPInputStream(
                        new FileInputStream(infile));
        FileOutputStream out
                = new FileOutputStream(outfile);
        byte[] buf = new byte[16000];
        int read;
        while ((read = in.read(buf)) != -1) {
            out.write(buf, 0, read);
            byteCount += read;
        }
        in.close();
        out.close();
        File unzipped = new File(infile);
        System.out.println("read "
                + unzipped.length() + " bytes");
        System.out.println("wrote "
                + byteCount + " bytes");
    }
    public static void ReadPedigreeORM(String pathString, Customers cust, String pedName, Vector newPedigree, Vector pidList,
            Vector midList, HashMap idMap) {
        JdbcConnectionSource conn = null;
        try {
            int pedSize = 0;
            int sex = 0;
            int affection;
            String pid, mid;
            String id;
            boolean dead;
            boolean proband;
            String name;
            int dna = PelicanPerson.without_dna;
            conn = new JdbcConnectionSource("jdbc:sqlite:" + pathString);
            genotypeDao = new GenotypeDao(conn);
            spounsesDao = new SpounseDao(conn);
            personDao = new PersonDao(conn, genotypeDao, spounsesDao);
            listpedDao = new ListPedDao(conn, personDao);
//            genotypeDao = DaoManager.createDao(conn, Genotype.class); //new GenotypeDao(conn);
//            spounsesDao = DaoManager.createDao(conn, Spounse.class);//new SpounseDao(conn);
//            personDao =DaoManager.createDao(conn, Person.class);//(conn,genotypeDao,spounsesDao);
//            listpedDao = DaoManager.createDao(conn, ListPed.class);//new ListPedDao(conn,personDao);
            QueryBuilder<ListPed, Integer> db = listpedDao.queryBuilder();
            db.where().eq("pedname", pedName).and().eq("Id_Customers", cust.Id_Customers);
            List<ListPed> results = listpedDao.query(db.prepare());
            String listpedid;
            if (!results.isEmpty()) {
                listpedid = results.get(0).id;
            } else {
                return;
            }
            QueryBuilder<Person, Integer> dbPerson = personDao.queryBuilder();
            dbPerson.where().eq("id_listped", listpedid);
            List<Person> persons = personDao.query(dbPerson.prepare());
            for (Person person : persons) {
                Vector genotype = new Vector();
                QueryBuilder<Genotype, Integer> dbGeno = genotypeDao.queryBuilder();
                dbGeno.where().eq("id_person", person.id);
                List<Genotype> genos = genotypeDao.query(dbGeno.prepare());
                for (Genotype geno : genos) {
                    Vector allele = new Vector();
                    allele.add(geno.first);
                    allele.add(geno.second);
                    genotype.add(allele);
                }
                id = person.id + "";
                sex = person.sex;
                affection = person.affection;
                dead = person.dead == 1;
                proband = person.proband == 1;
                name = person.name;
                PelicanPerson pp = new PelicanPerson(id, null, null, sex,
                        affection, dna, dead, proband, name, 0,
                        genotype);
                pid = person.father + "";
                mid = person.mother + "";
                pp.age = person.age;
                pp.early_onset = person.erly_onset;
                pp.person_id = person.person_id;
                ListPed listPed = listpedDao.queryForEq("id", person.id_listped).get(0);
                pp.listped = listPed;
                pp.Birth_Date = person.Birth_Date;
                pp.affectedBreast = person.ageBreast;
                pp.affectedOvary = person.ageOvary;
                pp.colorectal = person.ageColorectal;
                pp.ageOnset = person.ageOnset;
                pp.age_menopause = person.age_menopause;
                pp.ageDead = person.ageDeath;
                pp.isAffectedBreast = person.isBreast;
                pp.isAffectedOvary = person.isOvary;
                pp.isColorectal = person.isColorectal;
                pp.geneticTest = person.geneticTest;
                pp.degree = person.degree;
                pp.bilateralitas = person.bilateralitas;
                pp.testResult = person.testResult;
                pp.laktasi = person.laktasi;
                pp.menarche = person.menarche;
                pp.menscycle = person.menscycle;
                pp.menopause = person.menopause;
                pp.weight = person.weight;
                pp.height = person.height;
                pp.parity = person.parity;
                pp.agefirst = person.agefirst;
                pp.birthcontroltype = person.birthcontroltype;
                pp.birthcontrollenght = person.birthcontrollenght;
                pp.smooking = person.smoking;
                pp.alcohol = person.alcohol;
                pp.HRT = person.hrt;
                pp.hrtYears = person.hrtYears;
                pp.stadium = person.stadium;
                pp.mamografi = person.mamografi;
                pp.maleCancer = person.maleCancer;
                pp.otherCancer = person.otherCancer;
                pp.descOtherCancer = person.descOtherCancer;
                pp.biopsi = person.biopsi;
                pp.numBiopsi = person.numBiopsi;
                pp.adh = person.adh;
                pp.ageProx = person.ageProx;
                pp.BreastProx = person.ageBreastProx;
                pp.OvaryProx = person.ageOvaryProx;
                pp.colorectalProx = person.ageColorectalProx;
                pp.ageDeadProx = person.ageDeathProx;
                pp.ageOnsetProx = person.ageOnsetProx;
                pp.age_menopauseProx = person.age_menopauseProx;
                pp.agefirstProx = person.agefirstProx;
                pp.brca1 = person.brca1;
                pp.brca2 = person.brca2;
                pp.TestOrder = person.TestOrder;
                pp.p53 = person.p53;
                pp.pten = person.pten;
                pp.CHEK2 = person.CHEK2;
                pp.ATM = person.ATM;
                pp.er = person.er;
                pp.pr = person.pr;
                pp.her2 = person.her2;
                pp.ck56 = person.ck56;
                pp.ck14 = person.ck14;
                pp.bct = person.bct;
                pp.Mastectomy = person.Mastectomy;
                pp.AgeMastectomy = person.AgeMastectomy;
                pp.Oophorectomy = person.Oophorectomy;
                pp.AgeOophorectomy = person.AgeOophorectomy;
                pp.age_bilateral = person.age_bilateral;
                pp.stadiumT = person.stadiumT;
                pp.stadiumN = person.stadiumN;
                pp.stadiumM = person.stadiumM;
                pp.brca1txt = person.brca1txt;
                pp.brca2txt = person.brca2txt;
                pp.p53txt = person.p53txt;
                pp.ptentxt = person.ptentxt;
                pp.chemotherapyNote = person.chemotherapyNote;
                pp.targetedtherapyNote = person.targetedtherapyNote;
                pp.radiotherapy = person.radiotherapy;
                pp.hormonaltherapy = person.hormonaltherapy;
                pp.hormonaltherapyYes = person.hormonaltherapyYes;
                pp.dna = person.dna;
                pp.laidOut = person.laidOut == 1;
                pp.root = person.root == 1;
                pp.generation = person.generation;
                pp.mamodensgrade = person.mamodensgrade;
                pp.targetedtherapy = person.targetedtherapy;
                pp.multiCancerSyndrome = person.multiCancerSyndrome;
                pp.numberAffectedFamily = person.numberAffectedFamily;
                pp.gradeHistopathology = person.gradeHistopathology;
                pp.prophylacticBilateralMactectomy = person.prophylacticBilateralMactectomy;
                pp.prophylacticBilateralSalphingoOophorectomy = person.prophylacticBilateralSalphingoOophorectomy;
                newPedigree.add(pp);
                pidList.add(pid);
                midList.add(mid);
                idMap.put(id, pedSize++);
            }
            for (int i = 0; i < newPedigree.size(); i++) {
                if (newPedigree.elementAt(i) instanceof PelicanPerson) {
                    PelicanPerson person = (PelicanPerson) newPedigree.elementAt(i);
                    QueryBuilder<Spounse, Integer> dbSpoBuilder = spounsesDao.queryBuilder();
                    dbSpoBuilder.where().eq("id_person", person.person_id);//.and().eq("id_listped", person.listped.id);
                    List<Spounse> spounses = spounsesDao.query(dbSpoBuilder.prepare());
                    for (Spounse spounse : spounses) {
                        person.spounse.add(getPelicanPersonByID(newPedigree, spounse.id_spounses + ""));
                    }
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                conn.close();
            } catch (IOException ex) {
                Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    public static PelicanPerson getPelicanPersonByID(Vector p, String idPerson) {
        for (int i = 0; i < p.size(); i++) {
            if (p.elementAt(i) instanceof PelicanPerson) {
                PelicanPerson person = (PelicanPerson) p.elementAt(i);
                if (person.id.equals(idPerson)) {
                    return person;
                }
            }
        }
        return null;
    }
    public static void ReadPedigree(String pathString, String pedName, Vector newPedigree, Vector pidList,
            Vector midList, HashMap idMap) {
        try {
            int pedSize = 0;
            int sex = 0;
            int affection;
            String pid, mid;
            String id;
            boolean dead;
            boolean proband;
            String name;
            int dna = PelicanPerson.without_dna;
            Class.forName("org.sqlite.JDBC");
            Connection connection = null;
            Connection connection2 = null;
            try {
                // create a database connection
                Properties prop = new Properties();
                prop.setProperty("shared_cache", "true");
                connection = DriverManager.getConnection("jdbc:sqlite:" + pathString.replace("\\", "/"), prop);
                Statement statement = connection.createStatement();
                statement.setQueryTimeout(30);
                connection2 = DriverManager.getConnection("jdbc:sqlite:" + pathString.replace("\\", "/"), prop);
                Statement statement2 = connection2.createStatement();
                statement2.setQueryTimeout(30);
                ResultSet rs = statement.executeQuery("select * from pedigree where pedname = '" + pedName + "'");
                while (rs.next()) {
                    //list.add(rs.getString("pedname"));
                    id = rs.getString("id");
                    sex = rs.getInt("sex");
                    affection = rs.getInt("affection");
                    dead = rs.getBoolean("dead");
                    proband = rs.getBoolean("proband");
                    name = rs.getString("name");
                    Vector genotype = new Vector();
                    ResultSet rsGeno = statement2.executeQuery("select * from genotype where pedname = '"
                            + pedName + "' and id = " + id + " order by pos");
                    while (rsGeno.next()) {
                        Vector allele = new Vector();
                        allele.add(rsGeno.getString("first"));
                        allele.add(rsGeno.getString("second"));
                        genotype.add(allele);
                    }
                    PelicanPerson pp = new PelicanPerson(id, null, null, sex,
                            affection, dna, dead, proband, name, 0,
                            genotype);
                    pid = rs.getString("father");
                    mid = rs.getString("mother");
                    pp.age = rs.getInt("age");
                    pp.affectedBreast = rs.getInt("ageBreast");
                    pp.affectedOvary = rs.getInt("ageOvary");
                    pp.colorectal = rs.getInt("ageColorectal");
                    pp.ageOnset = rs.getInt("ageOnset");
                    pp.age_menopause = rs.getInt("age_menopause");
                    pp.ageDead = rs.getInt("ageDeath");
                    pp.isAffectedBreast = rs.getInt("isBreast");
                    pp.isAffectedOvary = rs.getInt("isOvary");
                    pp.isColorectal = rs.getInt("isColorectal");
                    pp.geneticTest = rs.getInt("geneticTest");
                    pp.degree = rs.getInt("degree");
                    pp.bilateralitas = rs.getInt("bilateralitas");
                    pp.testResult = rs.getInt("testResult");
                    pp.laktasi = rs.getInt("laktasi");
                    pp.menarche = rs.getInt("menarche");
                    pp.menscycle = rs.getInt("menscycle");
                    pp.menopause = rs.getInt("menopause");
                    pp.weight = rs.getDouble("weight");
                    pp.height = rs.getDouble("height");
                    pp.parity = rs.getInt("parity");
                    pp.agefirst = rs.getInt("agefirst");
                    pp.birthcontroltype = rs.getInt("birthcontroltype");
                    pp.birthcontrollenght = rs.getInt("birthcontrollenght");
                    pp.smooking = rs.getInt("smoking");
                    pp.alcohol = rs.getInt("alcohol");
                    pp.HRT = rs.getInt("hrt");
                    pp.stadium = rs.getInt("stadium");
                    pp.mamografi = rs.getInt("mamografi");
                    pp.maleCancer = rs.getInt("maleCancer");
                    pp.otherCancer = rs.getInt("otherCancer");
                    pp.descOtherCancer = rs.getString("descOtherCancer");
                    pp.biopsi = rs.getInt("biopsi");
                    pp.numBiopsi = rs.getInt("numBiopsi");
                    pp.adh = rs.getInt("adh");
                    newPedigree.add(pp);
                    pidList.add(new String(pid));
                    midList.add(new String(mid));
                    idMap.put(new String(id), new Integer(pedSize++));
                }
            } catch (SQLException e) {
                System.err.println(e.getMessage());
            } finally {
                try {
                    if (connection != null) {
                        connection.close();
                    }
                } catch (SQLException e) {
                    System.err.println(e);
                }
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public static void HideArrowSpinner(JSpinner js) {
        Component arrowUp = js.getComponent(0);
        Component arrowDown = js.getComponent(1);
        LayoutManager layout = js.getLayout();
        layout.removeLayoutComponent(arrowUp);
        layout.removeLayoutComponent(arrowDown);
    }
    public static String generateTempFile() {
        try {
            File file = File.createTempFile(RandomStringUtils.randomAlphanumeric(8), "");
            String sss = file.getAbsolutePath();
            if (file.exists() && !file.canWrite()) {
                throw (new Error("Cannot write file " + sss));
            }
            return sss;
        } catch (IOException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            return "";
        }
    }
    public static StringBuilder ReadFile(String dir, String file) {
        StringBuilder text = new StringBuilder();
        Charset charset = Charset.forName("US-ASCII");
        String NL = System.getProperty("line.separator");
        Path path = FileSystems.getDefault().getPath(dir, file);
        try (BufferedReader reader = Files.newBufferedReader(path, charset)) {
            String line = null;
            while ((line = reader.readLine()) != null) {
                text.append(line + NL);
            }
        } catch (IOException x) {
            System.err.format("IOException: %s%n", x);
        }
        return text;
    }
    private static String convertToHex(byte[] data) {
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < data.length; i++) {
            int halfbyte = (data[i] >>> 4) & 0x0F;
            int two_halfs = 0;
            do {
                if ((0 <= halfbyte) && (halfbyte <= 9)) {
                    buf.append((char) ('0' + halfbyte));
                } else {
                    buf.append((char) ('a' + (halfbyte - 10)));
                }
                halfbyte = data[i] & 0x0F;
            } while (two_halfs++ < 1);
        }
        return buf.toString();
    }
    public static String SHA1(String text) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md;
        md = MessageDigest.getInstance("SHA-1");
        byte[] sha1hash = new byte[40];
        md.update(text.getBytes("iso-8859-1"), 0, text.length());
        sha1hash = md.digest();
        return convertToHex(sha1hash);
    }
    public static boolean DeleteDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = DeleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        // The directory is now empty so delete it
        return dir.delete();
    }
    public static void DeleteFile(String file) {
        File f1 = new File(file);
        boolean success = f1.delete();
        if (!success) {
            System.out.println("Deletion failed.");
            //System.exit(0);
        } else {
            System.out.println("File deleted.");
        }
    }
    public static boolean FileOrDirectoryExists(String dir) {
        File file = new File(dir);
        return file.exists();
    }
    public static void CreateDirectory(String dir) {
        (new File(dir)).mkdirs();
    }
    public static void OpenFileWithDefaultEditor(File file) {
        try {
            if (System.getProperty("os.name").toLowerCase().contains("windows")) {
                String cmd = "rundll32 url.dll,FileProtocolHandler " + file.getCanonicalPath();
                Runtime.getRuntime().exec(cmd);
            } else {
                Desktop.getDesktop().edit(file);
            }
        } catch (IOException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public static Formula loadFormulaByName(JdbcConnectionSource conn, String nama) {
        try {
            FormulaDao formDao = new FormulaDao(conn);
            QueryBuilder<Formula, Integer> statementBuilder = formDao.queryBuilder();
            SelectArg selectArg = new SelectArg();
            // build a query with the WHERE clause set to 'name = ?'
            statementBuilder.where().like("nama_", selectArg);
            PreparedQuery<Formula> preparedQuery = statementBuilder.prepare();
            // now we can set the select arg (?) and run the query
            selectArg.setValue(nama);
            List<Formula> results = formDao.query(preparedQuery);
            return results.get(0);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }
}
