/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package samanda;

import com.nexes.wizards.Wizard;
import com.nexes.wizards.WizardPanelDescriptor;
import com.tomtessier.scrollabledesktop.JScrollableDesktopPane;
import javax.swing.JInternalFrame;
import samanda.properties.LanguagesController;

/**
 *
 * @author axioo
 */
public class ClausWiz {
    private JScrollableDesktopPane parent;
    public Wizard wizard;
    public LanguagesController lc;
    ClausWiz(JScrollableDesktopPane parentPane, String title, LanguagesController l) {
        try {
            lc = l;
//            wizard = new Wizard(parentPane);
            wizard = new Wizard();
            wizard.getDialog().setTitle(title);
            wizard.getDialog().setResizable(true);
//            wizard.getDialog().setClosable(true);
//            wizard.getDialog().setMaximizable(true);
//            wizard.getDialog().setIconifiable(true);
            wizard.getDialog().setDefaultCloseOperation(JInternalFrame.DISPOSE_ON_CLOSE);
            WizardPanelDescriptor clausPed = new ClausPedDesc(lc);
            wizard.registerWizardPanel(ClausPedDesc.IDENTIFIER, clausPed);
            WizardPanelDescriptor claus = new ClausDesc();
            wizard.registerWizardPanel(ClausDesc.IDENTIFIER, claus);
            wizard.setCurrentPanel(ClausPedDesc.IDENTIFIER);
            wizard.getDialog().pack();
            wizard.getDialog().setVisible(true);
//            wizard.getDialog().setSelected(true);
        } catch (Throwable t) {
            System.out.println(t.toString());
        }
    }
}
