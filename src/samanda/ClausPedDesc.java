package samanda;

import com.nexes.wizards.Wizard;
import com.nexes.wizards.WizardPanelDescriptor;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;
import javax.swing.JScrollPane;
import pedigree.Pelican2;
import pedigree.Utils;
import samanda.properties.LanguagesController;

public class ClausPedDesc extends WizardPanelDescriptor {
    public static final String IDENTIFIER = "CLAUS_PEDIGREE_PANEL";
    Pelican2 fami;
    private final Object lock = new Object();
    public LanguagesController lc;
    public ClausPedDesc(LanguagesController l) // TODO Auto-generated constructor stub
    {
        lc = l;
        Pelican2.parentPane = Wizard.parent;
        fami = new Pelican2(lc);
        fami.createMenuBar();
        setPanelDescriptorIdentifier(IDENTIFIER);
        setPanelComponent(new JScrollPane(fami));
        runCode();
    }
    @Override
    public Object getNextPanelDescriptor() {
        return ClausDesc.IDENTIFIER;
    }
    @Override
    public Object getBackPanelDescriptor() {
        return AgreementDesc.IDENTIFIER;
    }
    @Override
    public void aboutToHidePanel() {
        getWizard().famPelican = fami;
    }
    private void runCode1() {
        getWizard().t1 = new Thread() {
            public void run() {
            }
        };
        getWizard().t1.start();
    }
    private void runCode() {
        Thread t = new Thread() {
            public void run() {
                File directory = new File(".");
                Properties prop = new Properties();
                try {
                    String currentPath = directory.getCanonicalPath().replace("\\", "/");
                    //load a properties file
                    prop.load(new FileInputStream(Utils.DATA_DIR + "param.properties"));
                    Enumeration em = prop.keys();
                    while (em.hasMoreElements()) {
                        String str = (String) em.nextElement();
                        //Wizard.s.eval(str + "=" + prop.get(str), false);
                        Wizard.code.addRCode(str + "=" + prop.get(str));
                    }
                    //Wizard.s.
//                    Wizard.s.eval("pathLib = '" + currentPath + "/data'", false);
                    Wizard.code.addRCode("pathLib = '" + currentPath + "/data'");
                    //Wizard.s.source(new File(currentPath + "/data/claus.jev"));
                    StringBuilder dataCode = Utils.ReadFile("data", "claus.jev");
                    Wizard.code.addRCode(dataCode.toString());
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        };
        t.start();
    }
    private void runCode2() {
        getWizard().t2 = new Thread() {
            public void run() {
//                getWizard().secondDegreePateral = fami.getFamilyPateralSecondDegree(fami.getPelicanPersonByID(fami.getProbandID()));
            }
        };
        getWizard().t2.start();
    }
    private void runCode3() {
        getWizard().t3 = new Thread() {
            public void run() {
//                getWizard().secondDegreeMateral = fami.getFamilyMateralSecondDegree(fami.getPelicanPersonByID(fami.getProbandID()));
            }
        };
        getWizard().t3.start();
    }
    @Override
    public void aboutToDisplayPanel() {
        //getWizard().setNextFinishButtonEnabled(false);
        getWizard().setBackButtonEnabled(false);
        // getWizard().setTitle("Samanda - Claus Method");
    }
}
