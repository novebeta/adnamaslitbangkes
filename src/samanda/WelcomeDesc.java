/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package samanda;

import com.nexes.wizards.Wizard;
import com.nexes.wizards.WizardPanelDescriptor;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.*;
import pedigree.Utils;
import rcaller.exception.RCallerExecutionException;
import rcaller.exception.RCallerParseException;

public class WelcomeDesc extends WizardPanelDescriptor {

    public static final String IDENTIFIER = "INTRODUCTION_PANEL";
    Welcome wel;
    JDialog dialog = new JDialog((JFrame) null, "Working ...");
    JProgressBar pb = new JProgressBar(0, 100);
    JPanel center_panel = new JPanel();
    //JLabel label = new JLabel("Please wait...");
    File directory = new File(".");
    String currentPath = directory.getCanonicalPath().replace("\\", "/");
    Thread t;

    public WelcomeDesc() throws IOException {
        System.out.println("Create Welcome Desc");
        wel = new Welcome();
        setPanelDescriptorIdentifier(IDENTIFIER);
        setPanelComponent(wel);
        runCode();      
    }

    private void runCode() {
        t = new Thread() {
            @Override
            public void run() {
                try {
//                    Wizard.code.clear();
                    Wizard.code.addRCode("ols <- R.version.string");                    
                    Wizard.code.addRCode("eCek = exists(\"fFX\")");
                    Wizard.caller.setRCode(Wizard.code);
                    //Wizard.caller.runOnly();
                    Wizard.caller.runAndReturnResultOnline("ols");
                    String[] result = Wizard.caller.getParser().getAsStringArray("ols");
                    Wizard.caller.runAndReturnResultOnline("eCek");
                    boolean[] b = Wizard.caller.getParser().getAsLogicalArray("eCek");
                    wel.setRver(result[0]);
                    Utils.dimension = wel.getSize();
                    Wizard.code.clear();
//                    System.out.println(result[0]);
//                    System.out.println(b[0]);
//                    if (!b[0]) {
//                        Properties prop = new Properties();
//                        prop.load(new FileInputStream("param.properties"));
//                        Enumeration em = prop.keys();
//                        while (em.hasMoreElements()) {
//                            String str = (String) em.nextElement();
//                            Wizard.code.addRCode(str + "=" + prop.get(str));
//                        }
//                        String fKey = currentPath + Utils.BRCA_KEY;
//                        String fParam = currentPath + Utils.PARAM_DB;
//                        String fTemp = Utils.generateTempFile();
//                        Encryption.setFileKeyPath(fKey);
//                        Encryption.decrypt(new FileInputStream(new File(fParam)), new FileOutputStream(new File(fTemp)));
//                        JdbcConnectionSource conn = new JdbcConnectionSource("jdbc:sqlite:" + fTemp);
//                        GeneralDao generalDao = new GeneralDao(conn);
//                        QueryBuilder<General, Integer> dbGeneral = generalDao.queryBuilder();
//                        List<General> generals = generalDao.query(dbGeneral.prepare());
//                        for (General general : generals) {
//                            Wizard.code.addRCode(general.nama_ + "=" + general.value_);
//                        }
//                        markerDao MarkerDao = new markerDao(conn);
//                        QueryBuilder<marker, Integer> dbMarker = MarkerDao.queryBuilder();
//                        List<marker> markers = MarkerDao.query(dbMarker.orderBy("id", true).prepare());
//                        ArrayList<Float> paramStrings = new ArrayList<Float>();;
//                        for (marker marker_item : markers) {
//                             paramStrings.add(marker_item.value_);
//                        }
//                        String paramMarker = StringUtils.join(paramStrings);
//                        paramMarker = paramMarker.replace("[", "(");
//                        paramMarker = paramMarker.replace("]", ")");
//                        Wizard.code.addRCode("marker.prob=c" + paramMarker);
//                        Formula sam = Utils.loadFormulaByName(conn, Utils.SAMANDA_FORMULA);
//                        Formula sam2 = Utils.loadFormulaByName(conn, Utils.SAMANDA2_FORMULA);
//                        Wizard.code.addRCode(sam.value_);
//                        Wizard.code.addRCode(sam2.value_);
//                        String fFXTemp = Utils.generateTempFile();
//                        Encryption.decrypt(new FileInputStream(new File(currentPath + Utils.fFX3)), new FileOutputStream(new File(fFXTemp)));
//                        Wizard.code.addRCode("fFX = read.csv(\"" + fFXTemp.replace("\\", "/") + "\")");
//                        String fFYTemp = Utils.generateTempFile();
//                        Encryption.decrypt(new FileInputStream(new File(currentPath + Utils.fFY3)), new FileOutputStream(new File(fFYTemp)));
//                        Wizard.code.addRCode("fFY = read.csv(\"" + fFYTemp.replace("\\", "/") + "\")");
//                        String fMXTemp = Utils.generateTempFile();
//                        Encryption.decrypt(new FileInputStream(new File(currentPath + Utils.fMX3)), new FileOutputStream(new File(fMXTemp)));
//                        Wizard.code.addRCode("fMX = read.csv(\"" + fMXTemp.replace("\\", "/") + "\")");
//                        String fMYTemp = Utils.generateTempFile();
//                        Encryption.decrypt(new FileInputStream(new File(currentPath + Utils.fMY3)), new FileOutputStream(new File(fMYTemp)));
//                        Wizard.code.addRCode("fMY = read.csv(\"" + fMYTemp.replace("\\", "/") + "\")");
//                        String compriskTemp = Utils.generateTempFile();
//                        Encryption.decrypt(new FileInputStream(new File(currentPath + Utils.COMPRISK_SURV)), new FileOutputStream(new File(compriskTemp)));
//                        Wizard.code.addRCode("compriskSurv.samuel=read.csv(\"" + compriskTemp.replace("\\", "/") + "\")");
//                        String deathTemp = Utils.generateTempFile();
//                        Encryption.decrypt(new FileInputStream(new File(currentPath + Utils.DEATH_OTHER_CAUSES)), new FileOutputStream(new File(deathTemp)));
//                        Wizard.code.addRCode("death.othercauses.samuel=read.csv(\"" + deathTemp.replace("\\", "/") + "\")");
//                        String fDRTemp = Utils.generateTempFile();
//                        Encryption.decrypt(new FileInputStream(new File(currentPath + Utils.FDR)), new FileOutputStream(new File(fDRTemp)));
//                        Wizard.code.addRCode("fdr = read.csv(\"" + fDRTemp.replace("\\", "/") + "\")");
//                        String sDRTemp = Utils.generateTempFile();
//                        Encryption.decrypt(new FileInputStream(new File(currentPath + Utils.SDR)), new FileOutputStream(new File(sDRTemp)));
//                        Wizard.code.addRCode("sdr = read.csv(\"" + sDRTemp.replace("\\", "/") + "\")");
//                        String fDR2Temp = Utils.generateTempFile();
//                        Encryption.decrypt(new FileInputStream(new File(currentPath + Utils.FDR2)), new FileOutputStream(new File(fDR2Temp)));
//                        Wizard.code.addRCode("fdr2 = read.csv(\"" + fDR2Temp.replace("\\", "/") + "\")");
//                        String mauntTemp = Utils.generateTempFile();
//                        Encryption.decrypt(new FileInputStream(new File(currentPath + Utils.MO_MAUNT)), new FileOutputStream(new File(mauntTemp)));
//                        Wizard.code.addRCode("mo_maunt = read.csv(\"" + mauntTemp.replace("\\", "/") + "\")");
//                        String pauntTemp = Utils.generateTempFile();
//                        Encryption.decrypt(new FileInputStream(new File(currentPath + Utils.MO_PAUNT)), new FileOutputStream(new File(pauntTemp)));
//                        Wizard.code.addRCode("mo_paunt = read.csv(\"" + pauntTemp.replace("\\", "/") + "\")");
//                        String m1p1Temp = Utils.generateTempFile();
//                        Encryption.decrypt(new FileInputStream(new File(currentPath + Utils.M1P1SDR)), new FileOutputStream(new File(m1p1Temp)));
//                        Wizard.code.addRCode("m1p1sdr = read.csv(\"" + m1p1Temp.replace("\\", "/") + "\")");
//                        String mp2Temp = Utils.generateTempFile();
//                        Encryption.decrypt(new FileInputStream(new File(currentPath + Utils.MP2SDR)), new FileOutputStream(new File(mp2Temp)));
//                        Wizard.code.addRCode("mp2sdr = read.csv(\"" + mp2Temp.replace("\\", "/") + "\")");
//                        Formula comprisk = Utils.loadFormulaByName(conn, Utils.COMPRISK_DEATH);
//                        Wizard.code.addRCode(comprisk.value_);
//                        Formula claus = Utils.loadFormulaByName(conn, Utils.CLAUS_NEW);
//                        Wizard.code.addRCode(claus.value_);
//                        Formula run = Utils.loadFormulaByName(conn, Utils.RUN_FORMULA);
//                        Wizard.RUN_COMMAND = run.value_;
//                        Wizard.caller.setRCode(Wizard.code);
////                        System.out.println(Wizard.code);                        
//                        conn.close();
//                        Utils.DeleteFile(fTemp);
//                        Utils.DeleteFile(fFXTemp);
//                        Utils.DeleteFile(fFYTemp);
//                        Utils.DeleteFile(fMXTemp);
//                        Utils.DeleteFile(fMYTemp);
//                        Utils.DeleteFile(compriskTemp);
//                        Utils.DeleteFile(deathTemp);
//                        Utils.DeleteFile(fDRTemp);
//                        Utils.DeleteFile(sDRTemp);
//                        Utils.DeleteFile(fDR2Temp);
//                        Utils.DeleteFile(mauntTemp);
//                        Utils.DeleteFile(pauntTemp);
//                        Utils.DeleteFile(m1p1Temp);
//                        Utils.DeleteFile(mp2Temp);
//                    }           
                } catch (RCallerExecutionException | RCallerParseException ex) {
                    Logger.getLogger(WelcomeDesc.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        };
        t.start();
    }

    @Override
    public Object getNextPanelDescriptor() {
        return AgreementDesc.IDENTIFIER;
    }

    @Override
    public Object getBackPanelDescriptor() {
        return null;
    }

    @Override
    public void aboutToDisplayPanel() {
        // RCaller rCaller = new RCaller();
        // rCaller.setRscriptExecutable("E:/Program Files/R/R-2.13.2/bin/i386/Rscript.exe");
        // RCode code = new RCode();
        // code.clear();
        // code.addRCode("ols <- R.version.string");
        // rCaller.setRCode(code);
        // rCaller.runAndReturnResult("ols");
        // String[] results = rCaller.getParser().getAsStringArray("ols");
        // getWizard().re.
        // double[] rand = (double[]) s.eval("rnorm(10)", false); // create java
        // // variable
        // // from R command
        //
        // s.set("c", Math.random()); // create R variable from java one
        //
        // s.save(new File("save.Rdata"), "c"); // save variables in .Rdata
        // s.rm("c"); // delete variable in R environment
        // s.load(new File("save.Rdata")); // load R variable from .Rdata
        //
        // s.set("df", new double[][] { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 },
        // { 10, 11, 12 } }, "x1", "x2", "x3"); // create data frame from
        // // given vectors
        // double value = (Double) (cast(s.eval("df$x1[3]"))); // access one
        // value
        // // in data frame
        //
        // s.toJPEG(new File("plot.jpg"), 400, 400, "plot(rnorm(10))"); //
        // create
        // jpeg
        // file
        // from
        // R
        // graphical
        // command
        // (like
        // plot)
        // String html = s.asHTML("summary(rnorm(100))"); // format in html
        // using
        // // R2HTML
        // System.out.println(html);
        //
        // String txt = s.asString("summary(rnorm(100))"); // format in text
        // System.out.println(txt);
        //
        // System.out.println(s.installPackage("sensitivity", true)); // install
        // // and load
        // // R package
        // System.out.println(s.installPackage("wavelets", true));
//        String currentDir = new File(".").getAbsolutePath();
//        System.out.println(currentDir);
//      RCaller caller = new RCaller();
//      caller.setRscriptExecutable(Utils.R_SCRIPT_EXE);
//
//      RCode code = new RCode();
//        Wizard.code.clear();
//        Wizard.code.addRCode("ols <- R.version.string");
//        try {
//            //Wizard.caller.setRCode(Wizard.code);
//            Wizard.caller.runAndReturnResultOnline("ols");
//            String[] result = Wizard.caller.getParser().getAsStringArray("ols");
//            wel.setRver(result[0]);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//        REXP res = getWizard().s.eval("ols <- R.version.string");
//        //REXP res = getWizard().s.eval("ols <- ls()");
//        String[] result = null;
//        try {
//            result = res.asStrings();
//
//        } catch (REXPMismatchException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
        //wel.setRver(result[0]);
//        for(String i : result){
//            System.out.println(i);
//        }
    }
}
