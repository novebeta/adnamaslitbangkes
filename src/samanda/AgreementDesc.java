/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package samanda;

import com.nexes.wizards.WizardPanelDescriptor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JScrollBar;
import samanda.properties.LanguagesController;

/**
 *
 * @author axioo
 */
public class AgreementDesc extends WizardPanelDescriptor implements ActionListener {

    public static final String IDENTIFIER = "AGREEMENT_PANEL";
    Agreement agreement;

    public AgreementDesc(LanguagesController lc) {
        System.out.println("Create Agrement Desc");
        agreement = new Agreement(lc);
        setPanelDescriptorIdentifier(IDENTIFIER);
        setPanelComponent(agreement);
        agreement.addCheckBoxActionListener(this);
    }

    public Object getNextPanelDescriptor() {
//        if(getWizard().getTitle().equals("Samanda 2"))
//            return RekamMedikDesc.IDENTIFIER;
//        else
//            return NonGeneticDesc.IDENTIFIER;
        return RekamMedikDesc.IDENTIFIER;
    }

    public Object getBackPanelDescriptor() {
        return WelcomeDesc.IDENTIFIER;
    }

    public void aboutToDisplayPanel() {
        agreement.setcek(false);
        //getWizard().setTitle("Samanda - Agreement");
        JScrollBar verticalScrollBar = agreement.jScrollPane1.getVerticalScrollBar();
        verticalScrollBar.setValue(verticalScrollBar.getMinimum());
        setNextButtonAccordingToCheckBox();


    }

    @Override
    public void actionPerformed(ActionEvent e) {
        setNextButtonAccordingToCheckBox();
    }

    private void setNextButtonAccordingToCheckBox() {
        if (agreement.isCheckBoxSelected()) {
            getWizard().setNextFinishButtonEnabled(true);
        } else {
            getWizard().setNextFinishButtonEnabled(false);
        }

    }
}
