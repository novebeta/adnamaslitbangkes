package samanda;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.nexes.wizards.WizardPanelDescriptor;

public class NonGeneticDesc extends WizardPanelDescriptor implements ChangeListener, ActionListener {

    public static final String IDENTIFIER = "NONGENETIC_PANEL";
    NonGenetic ng;
    Dimension size;

    public NonGeneticDesc() {
        ng = new NonGenetic();
        ng.AddChange(this);
        ng.addAction(this);
        setPanelDescriptorIdentifier(IDENTIFIER);
        setPanelComponent(ng);

    }

    private void initHR() {
        getWizard().setAgefirst(0);
        getWizard().setFPmethod(0);
        getWizard().setHeight(0);
        getWizard().setHorm_t(0);
        getWizard().setMenarche(0);
        getWizard().setMenop(0);
        getWizard().setMens_cycle(0);
        getWizard().setParity(0);
        getWizard().setSmooking(0);
        getWizard().setWeight(0);
    }

    @Override
    public Object getNextPanelDescriptor() {
        return RelativeHazardDesc.IDENTIFIER;
    }

    @Override
    public Object getBackPanelDescriptor() {
        return AgreementDesc.IDENTIFIER;
    }

    @Override
    public void aboutToDisplayPanel() {
//		size = ng.getSize();
        //ng.setPreferredSize(size);
        //ng.revalidate();
        initHR();
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        getWizard().setMenarche(ng.getValHaidPertama());
        getWizard().setParity(ng.getValJumlahAnak());
        getWizard().setAgefirst(ng.getValLahirPertama());
        getWizard().setHeight(ng.getValTinggiBadan());
        getWizard().setWeight(ng.getValBeratBadan());
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        getWizard().setMens_cycle(ng.getSiklusHaid());
        getWizard().setFPmethod(ng.getRiwayatKB());
        getWizard().setHorm_t(ng.getHRT());
        getWizard().setSmooking(ng.getMerokok());
        getWizard().setMenop(ng.getValMenapause());

    }

    @Override
    public void aboutToHidePanel() {
        getWizard().setMenarche(ng.getValHaidPertama());
        getWizard().setParity(ng.getValJumlahAnak());
        getWizard().setAgefirst(ng.getValLahirPertama());
        getWizard().setHeight(ng.getValTinggiBadan());
        getWizard().setWeight(ng.getValBeratBadan());
        getWizard().setMens_cycle(ng.getSiklusHaid());
        getWizard().setFPmethod(ng.getRiwayatKB());
        getWizard().setHorm_t(ng.getHRT());
        getWizard().setSmooking(ng.getMerokok());
        getWizard().setMenop(ng.getValMenapause());
        
    }
    
    
}
