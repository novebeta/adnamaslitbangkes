package samanda;

import com.nexes.wizards.Wizard;
import com.nexes.wizards.WizardPanelDescriptor;
import java.text.DecimalFormat;
import java.util.*;
import javax.swing.tree.DefaultMutableTreeNode;
import pedigree.PelicanPerson;

public class ClausDesc extends WizardPanelDescriptor {

    public static final String IDENTIFIER = "CLAUS_PANEL";
    Claus claus;
    public List<PelicanPerson> firstDegree, secondDegree, secondDegreeMateral, secondDegreePateral, 
            thirdDegreeMateral, thirdDegreePateral,thirdDegree;

    ClausDesc() // TODO Auto-generated constructor stub
    {
        claus = new Claus();
        setPanelDescriptorIdentifier(IDENTIFIER);
        setPanelComponent(claus);

    }

    public Object getNextPanelDescriptor() {
        return FINISH;
    }

    public Object getBackPanelDescriptor() {
        return ClausPedDesc.IDENTIFIER;
    }

    public void aboutToDisplayPanel() {
        //claus.setProgressValue(0);
        //claus.addTxtClaus("Please wait...");
        getWizard().setNextFinishButtonEnabled(false);
        getWizard().setBackButtonEnabled(false);

        runCode();



    }

    private void runCode() {
        Thread t = new Thread() {

            public void run() {
                int aw = 0;
                int af = 0;
                int af1 = 0;
                int af2 = 0;
                int as = 0;
                int amo = 0;
                int amaunt = 0;
                int apaunt = 0;
                int amsdr1 = 0;
                int apsdr1 = 0;
                int amsdr2 = 0;
                int apsdr2 = 0;

                ArrayList firstList = new ArrayList();
                ArrayList secondList = new ArrayList();
                ArrayList secondMateralList = new ArrayList();
                ArrayList secondPateralList = new ArrayList();
                ArrayList thirdList = new ArrayList();
                ArrayList thirdMateralList = new ArrayList();
                ArrayList thirdPateralList = new ArrayList();
                
                if (getWizard().famPelican.getProbandID().isEmpty()) {
                    getWizard().setNextFinishButtonEnabled(true);
                    getWizard().setBackButtonEnabled(true);
                    return;
                }


                try {
                    PelicanPerson pp = getWizard().famPelican.getPelicanPersonByID(getWizard().famPelican.getProbandID());
                    aw = pp.age;
                    Iterator it;
                    firstDegree = getWizard().famPelican.getFamilyFirstDegree(getWizard().famPelican.getPelicanPersonByID(getWizard().famPelican.getProbandID()), claus.progressBar);
                    secondDegreePateral = getWizard().famPelican.getFamilyPateralSecondDegree(getWizard().famPelican.getPelicanPersonByID(getWizard().famPelican.getProbandID()), claus.progressBar);
                    secondDegreeMateral = getWizard().famPelican.getFamilyMateralSecondDegree(getWizard().famPelican.getPelicanPersonByID(getWizard().famPelican.getProbandID()), claus.progressBar);
                    secondDegree = getWizard().famPelican.getFamilySecondDegree(getWizard().famPelican.getPelicanPersonByID(getWizard().famPelican.getProbandID()), claus.progressBar);                    
                    thirdDegreePateral = getWizard().famPelican.getFamilyPaternalThirdDegree(getWizard().famPelican.getPelicanPersonByID(getWizard().famPelican.getProbandID()), claus.progressBar);
                    thirdDegreeMateral = getWizard().famPelican.getFamilyMaternalThirdDegree(getWizard().famPelican.getPelicanPersonByID(getWizard().famPelican.getProbandID()), claus.progressBar);
                    thirdDegree = getWizard().famPelican.getFamilyThirdDegree(getWizard().famPelican.getPelicanPersonByID(getWizard().famPelican.getProbandID()), claus.progressBar);
                    claus.progressBar.setValue(0);
                    claus.progressBar.setMaximum(100);
                    claus.progressBar.setString("Print family first degree");
                    it = firstDegree.iterator();

                    while (it.hasNext()) {
                        PelicanPerson tmp = (PelicanPerson) it.next();
                        boolean tmpaff = tmp.isAffectedBreast == 1;
                        claus.first.add(new DefaultMutableTreeNode(new TableRowData(
                                tmp.id, tmp.name, tmpaff ? "true" : "false",
                                tmpaff ? "" + tmp.affectedBreast : "", false)));
                        if (tmpaff) {
                            firstList.add(tmp.age);
                        }

                    }

                    claus.progressBar.setValue(25);
                    claus.progressBar.setString("Print family second degree");
                    it = secondDegree.iterator();
                    while (it.hasNext()) {
                        PelicanPerson tmp = (PelicanPerson) it.next();
                        boolean tmpaff = tmp.isAffectedBreast == 1;
                        claus.second.add(new DefaultMutableTreeNode(new TableRowData(
                                tmp.id, tmp.name, tmp.generation+"",
                                tmpaff ? "" + tmp.affectedBreast : "", false)));
                        if (tmpaff) {
                            secondList.add(tmp.age);
                        }
                    }
                    claus.progressBar.setValue(50);
                    claus.progressBar.setString("Print family second degree materal");
                    it = secondDegreeMateral.iterator();
                    while (it.hasNext()) {
                        PelicanPerson tmp = (PelicanPerson) it.next();
                        boolean tmpaff = tmp.isAffectedBreast == 1;
                        claus.secondMateral.add(new DefaultMutableTreeNode(new TableRowData(
                                tmp.id, tmp.name, tmpaff ? "true" : "false",
                                tmpaff ? "" + tmp.affectedBreast : "", false)));
                        if (tmpaff) {
                            secondMateralList.add(tmp.age);
                        }
                    }
                    claus.progressBar.setValue(75);
                    claus.progressBar.setString("Print family second degree pateral");
                    it = secondDegreePateral.iterator();
                    while (it.hasNext()) {
                        PelicanPerson tmp = (PelicanPerson) it.next();
                        boolean tmpaff = tmp.isAffectedBreast == 1;
                        claus.secondPateral.add(new DefaultMutableTreeNode(new TableRowData(
                                tmp.id, tmp.name, tmpaff ? "true" : "false",
                                tmpaff ? "" + tmp.affectedBreast : "", false)));
                        if (tmpaff) {
                            secondPateralList.add(tmp.age);
                        }
                    }
                    claus.progressBar.setValue(100);
                    claus.progressBar.setString("Done");

                    it = thirdDegreeMateral.iterator();
                    while (it.hasNext()) {
                        PelicanPerson tmp = (PelicanPerson) it.next();
                        boolean tmpaff = tmp.isAffectedBreast == 1;
                        claus.thirdMateral.add(new DefaultMutableTreeNode(new TableRowData(
                                tmp.id, tmp.name, tmpaff ? "true" : "false",
                                tmpaff ? "" + tmp.affectedBreast : "", false)));
                        if (tmpaff) {
                            thirdPateralList.add(tmp.age);
                        }
                    }
                    claus.progressBar.setValue(100);
                    claus.progressBar.setString("Done");
                    
                    it = thirdDegreePateral.iterator();
                    while (it.hasNext()) {
                        PelicanPerson tmp = (PelicanPerson) it.next();
                        boolean tmpaff = tmp.isAffectedBreast == 1;
                        claus.thirdPateral.add(new DefaultMutableTreeNode(new TableRowData(
                                tmp.id, tmp.name, tmpaff ? "true" : "false",
                                tmpaff ? "" + tmp.affectedBreast : "", false)));
                        if (tmpaff) {
                            thirdPateralList.add(tmp.age);
                        }
                    }
                    claus.progressBar.setValue(100);
                    claus.progressBar.setString("Done");
                    
                    it = thirdDegree.iterator();
                    while (it.hasNext()) {
                        PelicanPerson tmp = (PelicanPerson) it.next();
                        boolean tmpaff = tmp.isAffectedBreast == 1;
                        claus.third.add(new DefaultMutableTreeNode(new TableRowData(
                                tmp.id, tmp.name, tmpaff ? "true" : "false",
                                tmpaff ? "" + tmp.affectedBreast : "", false)));
                        if (tmpaff) {
                            thirdList.add(tmp.age);
                        }
                    }
                    claus.progressBar.setValue(100);
                    claus.progressBar.setString("Done");
                    
                    Collections.sort(firstList);
                    Collections.sort(secondList);
                    Collections.sort(secondMateralList);
                    Collections.sort(secondPateralList);

                    if (firstList.size() == 1) {
                        af = (int) firstList.get(0);
                    } else if (secondList.size() + secondMateralList.size() + secondPateralList.size() == 1) {
                        if (secondList.size() == 1) {
                            as = (int) secondList.get(0);
                        }
                        if (secondMateralList.size() == 1) {
                            as = (int) secondMateralList.get(0);
                        }
                        if (secondPateralList.size() == 1) {
                            as = (int) secondPateralList.get(0);
                        }
                    } else if (firstList.size() >= 2) {
                        af1 = (int) firstList.get(0);
                        af2 = (int) firstList.get(1);
                    } else if (pp.hasMother() && pp.mother.isAffectedBreast == 1 && secondMateralList.size() > 0) {
                        amo = pp.mother.affectedBreast;
                        amaunt = (int) secondMateralList.get(0);
                    } else if (pp.hasMother() && pp.mother.isAffectedBreast == 1 && secondPateralList.size() > 0) {
                        amo = pp.mother.affectedBreast;
                        apaunt = (int) secondPateralList.get(0);
                    } else if (secondPateralList.size() == 1 && secondMateralList.size() == 1) {
                        amsdr1 = (int) secondMateralList.get(0);
                        apsdr1 = (int) secondPateralList.get(0);
                    } else if (secondMateralList.size() >= 2) {
                        amsdr1 = (int) secondMateralList.get(0);
                        amsdr2 = (int) secondMateralList.get(1);
                    } else if (secondPateralList.size() >= 2) {
                        apsdr1 = (int) secondPateralList.get(0);
                        apsdr2 = (int) secondPateralList.get(1);
                    }

//                    REXP res = getWizard().s.eval("hsl_claus <- claus(" + aw + "," + af + "," + as + "," +
//                            af1 + "," + af2 + "," + amo + "," + amaunt + "," + apaunt + "," + amsdr1 + "," +
//                            apsdr1 + "," + amsdr2 + "," + apsdr2 + ")",true);
                    Wizard.code.addRCode("hsl_claus <- claus(" + aw + "," + af + "," + as + "," +
                            af1 + "," + af2 + "," + amo + "," + amaunt + "," + apaunt + "," + amsdr1 + "," +
                            apsdr1 + "," + amsdr2 + "," + apsdr2 + ")");
                    Wizard.caller.setRCode(Wizard.code);
                    Wizard.caller.runAndReturnResultOnline("hsl_claus");
                   // double result_claus = res.asDouble();
                    double[] hsl_claus = Wizard.caller.getParser().getAsDoubleArray("hsl_claus");
                    DecimalFormat forD = new DecimalFormat("#.###");
                    //claus.setResult(Double.valueOf(forD.format(result_claus)).toString());
                    claus.setResult(Double.valueOf(forD.format(hsl_claus[0])).toString());
                    getWizard().setNextFinishButtonEnabled(true);
                    getWizard().setBackButtonEnabled(true);

                } catch (Exception e) {

                    claus.progressBar.setValue(0);
                    claus.addTxtClaus("An Error Has Occurred");
                    getWizard().setBackButtonEnabled(true);
                }
            }
        };
        t.start();
    }
}
