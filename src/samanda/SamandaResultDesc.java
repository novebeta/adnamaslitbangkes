package samanda;

import DB.Formula;
import DB.General;
import DB.GeneralDao;
import DB.marker;
import DB.markerDao;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.stmt.QueryBuilder;
import com.nexes.wizards.Wizard;
import com.nexes.wizards.WizardPanelDescriptor;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import org.apache.commons.lang3.StringUtils;
import pedigree.PelicanPerson;
import pedigree.Utils;
import samanda.properties.LanguagesController;

public class SamandaResultDesc extends WizardPanelDescriptor {
    public static final String IDENTIFIER = "SAMANDA_RESULT_PANEL";
    private String fKey, fParam;
    SamandaResult sr;
    public List<PelicanPerson> firstDegree, secondDegree, secondDegreeMateral, secondDegreePateral,
            thirdDegreeMateral, thirdDegreePateral, thirdDegree;
    Thread t;
    public LanguagesController lc;
    public SamandaResultDesc(LanguagesController l) {
        lc = l;
        System.out.println("Create Result Desc");
        sr = new SamandaResult(lc);
        setPanelDescriptorIdentifier(IDENTIFIER);
        setPanelComponent(sr);
    }
    @Override
    public Object getNextPanelDescriptor() {
        return FINISH;
    }
    @Override
    public Object getBackPanelDescriptor() {
//        if (getWizard().getTitle().equals("Samanda 2")) {
        return Samanda2PedDesc.IDENTIFIER;
//        } else {
//            return SamandaPedDesc.IDENTIFIER;
//        }
    }
    @Override
    public void aboutToDisplayPanel() {
        sr.clearTable();
        sr.RemoveallTab();
//        if(Wizard.Samanda_file.isEmpty()|| Wizard.proband == -1) {
        if (Wizard.Samanda_file.isEmpty() || Wizard.proband.isEmpty()) {
            //getWizard().setCurrentPanel(SamandaPedDesc.IDENTIFIER);
            return;
        }
//        if(getWizard().getWeight() == 0 && getWizard().getHeight() == 0 && getWizard().getParity() == 0 &&
//               getWizard().getAgefirst() == 0 && getWizard().getMenarche() == 0 && getWizard().getMens_cycle() == 0 &&
//                getWizard().getFPmethod() == 0 && getWizard().getSmooking() == 0 && getWizard().getHorm_t() == 0 &&
//                getWizard().getMenop() == 0){
//
//            JOptionPane.showMessageDialog(getWizard().getParent(),"Non genetic must set.");
//            return;
//        }
        JProgressBar pb = new JProgressBar(0, 100);
        pb.setPreferredSize(new Dimension(175, 20));
        pb.setString("Working");
        pb.setStringPainted(true);
        pb.setValue(0);
        JLabel label = new JLabel("Progress: ");
        JPanel center_panel = new JPanel();
        center_panel.add(label);
        center_panel.add(pb);
        JDialog dialog = new JDialog((JFrame) null, "Working ...");
        dialog.getContentPane().add(center_panel, BorderLayout.CENTER);
        dialog.pack();
        dialog.setVisible(true);
        dialog.setLocationRelativeTo(null); // center on screen
        dialog.toFront();
        File directory = new File(".");
//        Wizard.s.eval("pathLib = '" + currentPath + "/data'",false);
        //load param
        //Properties prop = new Properties();
        try {
            String currentPath = directory.getCanonicalPath().replace("\\", "/");
            //load a properties file
//            prop.load(new FileInputStream("param.properties"));
//            Enumeration em = prop.keys();
//            while (em.hasMoreElements()) {
//                String str = (String) em.nextElement();
//                Wizard.s.eval(str + "=" + prop.get(str),false);
//            }
            //input non-genetic
//            Wizard.s.eval("weight=" + getWizard().getWeight(),false);
//            Wizard.s.eval("height=" + getWizard().getHeight(),false);
//            Wizard.s.eval("parity=" + getWizard().getParity(),false);
//            Wizard.s.eval("age.first=" + getWizard().getAgefirst(),false);
//            Wizard.s.eval("menarche=" + getWizard().getMenarche(),false);
//            Wizard.s.eval("mens.cycle=" + getWizard().getMens_cycle(),false);
//            Wizard.s.eval("FP.method=" + getWizard().getFPmethod(),false);
//            Wizard.s.eval("smooking=" + getWizard().getSmooking(),false);
//            Wizard.s.eval("horm.t=" + getWizard().getHorm_t(),false);
//            Wizard.s.eval("menop=" + getWizard().getMenop(),false);
//
//            Wizard.s.eval("pathFam = \"" + Wizard.Samanda_file + "\"",false);
//            Wizard.s.eval("counseleeid = " + Wizard.proband,false);
            Wizard.code.clear();
            pb.setValue(25);
            Properties prop = new Properties();
            //load a properties file
            prop.load(new FileInputStream(Utils.DATA_DIR + "param.properties"));
            Enumeration em = prop.keys();
            while (em.hasMoreElements()) {
                String str = (String) em.nextElement();
                //Wizard.s.eval(str + "=" + prop.get(str), false);
                Wizard.code.addRCode(str + "=" + prop.get(str));
            }
            String fKey = currentPath + Utils.BRCA_KEY;
            String fParam = currentPath + Utils.PARAM_DB;
            String fTemp = Utils.generateTempFile();
            Encryption.setFileKeyPath(fKey);
            Encryption.decrypt(new FileInputStream(new File(fParam)), new FileOutputStream(new File(fTemp)));
            JdbcConnectionSource conn = new JdbcConnectionSource("jdbc:sqlite:" + fTemp);
            GeneralDao generalDao = new GeneralDao(conn);
            QueryBuilder<General, Integer> dbGeneral = generalDao.queryBuilder();
            List<General> generals = generalDao.query(dbGeneral.prepare());
            for (General general : generals) {
                Wizard.code.addRCode(general.nama_ + "=" + general.value_);
            }
            markerDao MarkerDao = new markerDao(conn);
            QueryBuilder<marker, Integer> dbMarker = MarkerDao.queryBuilder();
            List<marker> markers = MarkerDao.query(dbMarker.orderBy("id", true).prepare());
            ArrayList<Float> paramStrings = new ArrayList<Float>();;
            for (marker marker_item : markers) {
                paramStrings.add(marker_item.value_);
            }
            String paramMarker = StringUtils.join(paramStrings);
            paramMarker = paramMarker.replace("[", "(");
            paramMarker = paramMarker.replace("]", ")");
            Wizard.code.addRCode("marker.prob=c" + paramMarker);
            Formula sam = Utils.loadFormulaByName(conn, Utils.SAMANDA_FORMULA);
            Formula sam2 = Utils.loadFormulaByName(conn, Utils.SAMANDA2_FORMULA);
            Wizard.code.addRCode(sam.value_);
            Wizard.code.addRCode(sam2.value_);
            String fFXTemp = Utils.generateTempFile();
            Encryption.decrypt(new FileInputStream(new File(currentPath + Utils.fFX3)), new FileOutputStream(new File(fFXTemp)));
            Wizard.code.addRCode("fFX = read.csv(\"" + fFXTemp.replace("\\", "/") + "\")");
            String fFYTemp = Utils.generateTempFile();
            Encryption.decrypt(new FileInputStream(new File(currentPath + Utils.fFY3)), new FileOutputStream(new File(fFYTemp)));
            Wizard.code.addRCode("fFY = read.csv(\"" + fFYTemp.replace("\\", "/") + "\")");
            String fMXTemp = Utils.generateTempFile();
            Encryption.decrypt(new FileInputStream(new File(currentPath + Utils.fMX3)), new FileOutputStream(new File(fMXTemp)));
            Wizard.code.addRCode("fMX = read.csv(\"" + fMXTemp.replace("\\", "/") + "\")");
            String fMYTemp = Utils.generateTempFile();
            Encryption.decrypt(new FileInputStream(new File(currentPath + Utils.fMY3)), new FileOutputStream(new File(fMYTemp)));
            Wizard.code.addRCode("fMY = read.csv(\"" + fMYTemp.replace("\\", "/") + "\")");
            String compriskTemp = Utils.generateTempFile();
            Encryption.decrypt(new FileInputStream(new File(currentPath + Utils.COMPRISK_SURV)), new FileOutputStream(new File(compriskTemp)));
            Wizard.code.addRCode("compriskSurv.samuel=read.csv(\"" + compriskTemp.replace("\\", "/") + "\")");
            String deathTemp = Utils.generateTempFile();
            Encryption.decrypt(new FileInputStream(new File(currentPath + Utils.DEATH_OTHER_CAUSES)), new FileOutputStream(new File(deathTemp)));
            Wizard.code.addRCode("death.othercauses.samuel=read.csv(\"" + deathTemp.replace("\\", "/") + "\")");
            String fDRTemp = Utils.generateTempFile();
            Encryption.decrypt(new FileInputStream(new File(currentPath + Utils.FDR)), new FileOutputStream(new File(fDRTemp)));
            Wizard.code.addRCode("fdr = read.csv(\"" + fDRTemp.replace("\\", "/") + "\")");
            String sDRTemp = Utils.generateTempFile();
            Encryption.decrypt(new FileInputStream(new File(currentPath + Utils.SDR)), new FileOutputStream(new File(sDRTemp)));
            Wizard.code.addRCode("sdr = read.csv(\"" + sDRTemp.replace("\\", "/") + "\")");
            String fDR2Temp = Utils.generateTempFile();
            Encryption.decrypt(new FileInputStream(new File(currentPath + Utils.FDR2)), new FileOutputStream(new File(fDR2Temp)));
            Wizard.code.addRCode("fdr2 = read.csv(\"" + fDR2Temp.replace("\\", "/") + "\")");
            String mauntTemp = Utils.generateTempFile();
            Encryption.decrypt(new FileInputStream(new File(currentPath + Utils.MO_MAUNT)), new FileOutputStream(new File(mauntTemp)));
            Wizard.code.addRCode("mo_maunt = read.csv(\"" + mauntTemp.replace("\\", "/") + "\")");
            String pauntTemp = Utils.generateTempFile();
            Encryption.decrypt(new FileInputStream(new File(currentPath + Utils.MO_PAUNT)), new FileOutputStream(new File(pauntTemp)));
            Wizard.code.addRCode("mo_paunt = read.csv(\"" + pauntTemp.replace("\\", "/") + "\")");
            String m1p1Temp = Utils.generateTempFile();
            Encryption.decrypt(new FileInputStream(new File(currentPath + Utils.M1P1SDR)), new FileOutputStream(new File(m1p1Temp)));
            Wizard.code.addRCode("m1p1sdr = read.csv(\"" + m1p1Temp.replace("\\", "/") + "\")");
            String mp2Temp = Utils.generateTempFile();
            Encryption.decrypt(new FileInputStream(new File(currentPath + Utils.MP2SDR)), new FileOutputStream(new File(mp2Temp)));
            Wizard.code.addRCode("mp2sdr = read.csv(\"" + mp2Temp.replace("\\", "/") + "\")");
            Formula comprisk = Utils.loadFormulaByName(conn, Utils.COMPRISK_DEATH);
            Wizard.code.addRCode(comprisk.value_);
            Formula claus = Utils.loadFormulaByName(conn, Utils.CLAUS_NEW);
            Wizard.code.addRCode(claus.value_);
            Formula run = Utils.loadFormulaByName(conn, Utils.RUN_FORMULA);
            Wizard.RUN_COMMAND = run.value_;
//            Wizard.caller.setRCode(Wizard.code);
//            System.out.println(Wizard.code);
            conn.close();
            fKey = currentPath + Utils.BRCA_KEY;
            fParam = currentPath + Utils.PARAM_DB;
//            String fTemp = Utils.generateTempFile();
//            Encryption.setFileKeyPath(fKey);
//            Encryption.decrypt(new FileInputStream(new File(fParam)), new FileOutputStream(new File(fTemp)));
//            JdbcConnectionSource conn = new JdbcConnectionSource("jdbc:sqlite:" + fTemp);
//            GeneralDao generalDao = new GeneralDao(conn);
//            QueryBuilder<General, Integer> dbGeneral = generalDao.queryBuilder();
//            List<General> generals = generalDao.query(dbGeneral.prepare());
//            for (General general : generals) {
//                Wizard.code.addRCode(general.nama_ + "=" + general.value_);
//            }
//            conn.close();
//            Utils.DeleteFile(fTemp);
//            Properties prop = new Properties();
//            prop.load(new FileInputStream("param.properties"));
//            Enumeration em = prop.keys();
//            while (em.hasMoreElements()) {
//                String str = (String) em.nextElement();
//                //Wizard.s.eval(str + "=" + prop.get(str), false);
//                Wizard.code.addRCode(str + "=" + prop.get(str));
//            }
//            Wizard.code.addRCode("pathLib = '" + currentPath + "/data'");
            //Wizard.s.eval("pathLib = '" + currentPath + "/data'", false);
//            StringBuilder dataCode = Utils.ReadFile("data", "data.jev");
//            Wizard.code.addRCode(dataCode.toString());
//            Wizard.code.addRCode("library(BayesMendel)");
//            Wizard.code.addRCode("BRCApenet.samuel=list(fFX=fFX,fFY=fFY,fMX=fMX,fMY=fMY)");
//            Wizard.code.addRCode("data(death.othercauses)");
//            Wizard.code.addRCode("compriskSurv.matrix=matrix(c(compriskSurv.samuel[,1],compriskSurv.samuel[,2],compriskSurv.samuel[,3],compriskSurv.samuel[,4],\n"
//                    + "compriskSurv.samuel[,5],compriskSurv.samuel[,6],compriskSurv.samuel[,7],compriskSurv.samuel[,8],\n"
//                    + "compriskSurv.samuel[,9],compriskSurv.samuel[,10],compriskSurv.samuel[,11],compriskSurv.samuel[,12],\n"
//                    + "compriskSurv.samuel[,13],compriskSurv.samuel[,14],compriskSurv.samuel[,15],compriskSurv.samuel[,16]),nrow=110,ncol=16)");
//            Wizard.code.addRCode("compriskSurv=compriskSurv.matrix");
//            Wizard.code.addRCode("rownames(compriskSurv)=c(2:111)");
//            Wizard.code.addRCode("colnames(compriskSurv)=c(\"femBRCAnegcomprisk\", \"femBRCA1poscomprisk\",\n"
//                    + "\"femBRCA2poscomprisk\", \"femBRCApluspluscomprisk\", \"femBRCAnegcomprisk1\",\n"
//                    + "\"femBRCA1poscomprisk1\", \"femBRCA2poscomprisk1\", \"femBRCApluspluscomprisk1\",\n"
//                    + "\"maleBRCAnegcomprisk\", \"maleBRCA1poscomprisk\", \"maleBRCA2poscomprisk\",\n"
//                    + "\"maleBRCApluspluscomprisk\", \"maleBRCAnegcomprisk1\", \"maleBRCA1poscomprisk1\",\n"
//                    + "\"maleBRCA2poscomprisk1\", \"maleBRCApluspluscomprisk1\")");
//            Wizard.code.addRCode("colnames(death.othercauses.samuel)=colnames(death.othercauses)");
//            Wizard.code.addRCode("death.othercauses=death.othercauses.samuel");
//
//
//            StringBuilder clauseCode = Utils.ReadFile("data", "claus.jev");
//            Wizard.code.addRCode(clauseCode.toString());
            int aw = 0;
            int af = 0;
            int af1 = 0;
            int af2 = 0;
            int as = 0;
            int amo = 0;
            int amaunt = 0;
            int apaunt = 0;
            int amsdr1 = 0;
            int apsdr1 = 0;
            int amsdr2 = 0;
            int apsdr2 = 0;
            ArrayList firstList = new ArrayList();
            ArrayList secondList = new ArrayList();
            ArrayList secondMateralList = new ArrayList();
            ArrayList secondPateralList = new ArrayList();
            ArrayList thirdList = new ArrayList();
            ArrayList thirdMateralList = new ArrayList();
            ArrayList thirdPateralList = new ArrayList();
//                if (getWizard().famPelican.getProbandID().isEmpty()) {
//                    getWizard().setNextFinishButtonEnabled(true);
//                    getWizard().setBackButtonEnabled(true);
//                    return;
//                }
            PelicanPerson pp = getWizard().famPelican.getPelicanPersonByID(getWizard().famPelican.getProbandID());
            aw = pp.age;
            Iterator it;
//                    firstDegree = getWizard().famPelican.getFamilyFirstDegree(getWizard().famPelican.getPelicanPersonByID(getWizard().famPelican.getProbandID()), progressBar);
//                    secondDegreePateral = getWizard().famPelican.getFamilyPateralSecondDegree(getWizard().famPelican.getPelicanPersonByID(getWizard().famPelican.getProbandID()), progressBar);
//                    secondDegreeMateral = getWizard().famPelican.getFamilyMateralSecondDegree(getWizard().famPelican.getPelicanPersonByID(getWizard().famPelican.getProbandID()), progressBar);
//                    secondDegree = getWizard().famPelican.getFamilySecondDegree(getWizard().famPelican.getPelicanPersonByID(getWizard().famPelican.getProbandID()), progressBar);
//                    thirdDegreePateral = getWizard().famPelican.getFamilyPaternalThirdDegree(getWizard().famPelican.getPelicanPersonByID(getWizard().famPelican.getProbandID()), progressBar);
//                    thirdDegreeMateral = getWizard().famPelican.getFamilyMaternalThirdDegree(getWizard().famPelican.getPelicanPersonByID(getWizard().famPelican.getProbandID()), progressBar);
//                    thirdDegree = getWizard().famPelican.getFamilyThirdDegree(getWizard().famPelican.getPelicanPersonByID(getWizard().famPelican.getProbandID()), progressBar);
            getWizard().famPelican.AutoDetectionDegree(pp);
            firstDegree = getWizard().famPelican.firstDegree;
            secondDegree = getWizard().famPelican.secondDegree;
            secondDegreeMateral = getWizard().famPelican.secondDegreeMaternal;
            secondDegreePateral = getWizard().famPelican.secondDegreePaternal;
            thirdDegree = getWizard().famPelican.thirdDegree;
            thirdDegreeMateral = getWizard().famPelican.thirdDegreeMaternal;
            thirdDegreePateral = getWizard().famPelican.thirdDegreePaternal;
//                    progressBar.setValue(0);
//                    progressBar.setMaximum(100);
//                    progressBar.setString("Print family first degree");
            it = firstDegree.iterator();
            while (it.hasNext()) {
                PelicanPerson tmp = (PelicanPerson) it.next();
                boolean tmpaff = tmp.isAffectedBreast == 1;
//                        claus.first.add(new DefaultMutableTreeNode(new TableRowData(
//                                tmp.id, tmp.name, tmpaff ? "true" : "false",
//                                tmpaff ? "" + tmp.affectedBreast : "", false)));
                if (tmpaff) {
                    firstList.add(tmp.age);
                }
            }
//                    progressBar.setValue(25);
//                    progressBar.setString("Print family second degree");
            it = secondDegree.iterator();
            while (it.hasNext()) {
                PelicanPerson tmp = (PelicanPerson) it.next();
                boolean tmpaff = tmp.isAffectedBreast == 1;
//                        claus.second.add(new DefaultMutableTreeNode(new TableRowData(
//                                tmp.id, tmp.name, tmp.generation+"",
//                                tmpaff ? "" + tmp.affectedBreast : "", false)));
                if (tmpaff) {
                    secondList.add(tmp.age);
                }
            }
//                    progressBar.setValue(50);
//                    progressBar.setString("Print family second degree materal");
            it = secondDegreeMateral.iterator();
            while (it.hasNext()) {
                PelicanPerson tmp = (PelicanPerson) it.next();
                boolean tmpaff = tmp.isAffectedBreast == 1;
//                        claus.secondMateral.add(new DefaultMutableTreeNode(new TableRowData(
//                                tmp.id, tmp.name, tmpaff ? "true" : "false",
//                                tmpaff ? "" + tmp.affectedBreast : "", false)));
                if (tmpaff) {
                    secondMateralList.add(tmp.age);
                }
            }
//                    progressBar.setValue(75);
//                    progressBar.setString("Print family second degree pateral");
            it = secondDegreePateral.iterator();
            while (it.hasNext()) {
                PelicanPerson tmp = (PelicanPerson) it.next();
                boolean tmpaff = tmp.isAffectedBreast == 1;
//                        claus.secondPateral.add(new DefaultMutableTreeNode(new TableRowData(
//                                tmp.id, tmp.name, tmpaff ? "true" : "false",
//                                tmpaff ? "" + tmp.affectedBreast : "", false)));
                if (tmpaff) {
                    secondPateralList.add(tmp.age);
                }
            }
//                    progressBar.setValue(100);
//                    progressBar.setString("Done");
            it = thirdDegreeMateral.iterator();
            while (it.hasNext()) {
                PelicanPerson tmp = (PelicanPerson) it.next();
                boolean tmpaff = tmp.isAffectedBreast == 1;
//                        claus.thirdMateral.add(new DefaultMutableTreeNode(new TableRowData(
//                                tmp.id, tmp.name, tmpaff ? "true" : "false",
//                                tmpaff ? "" + tmp.affectedBreast : "", false)));
                if (tmpaff) {
                    thirdPateralList.add(tmp.age);
                }
            }
//                    progressBar.setValue(100);
//                    progressBar.setString("Done");
            it = thirdDegreePateral.iterator();
            while (it.hasNext()) {
                PelicanPerson tmp = (PelicanPerson) it.next();
                boolean tmpaff = tmp.isAffectedBreast == 1;
//                        claus.thirdPateral.add(new DefaultMutableTreeNode(new TableRowData(
//                                tmp.id, tmp.name, tmpaff ? "true" : "false",
//                                tmpaff ? "" + tmp.affectedBreast : "", false)));
                if (tmpaff) {
                    thirdPateralList.add(tmp.age);
                }
            }
//                    progressBar.setValue(100);
//                    progressBar.setString("Done");
            it = thirdDegree.iterator();
            while (it.hasNext()) {
                PelicanPerson tmp = (PelicanPerson) it.next();
                boolean tmpaff = tmp.isAffectedBreast == 1;
//                        claus.third.add(new DefaultMutableTreeNode(new TableRowData(
//                                tmp.id, tmp.name, tmpaff ? "true" : "false",
//                                tmpaff ? "" + tmp.affectedBreast : "", false)));
                if (tmpaff) {
                    thirdList.add(tmp.age);
                }
            }
//                    progressBar.setValue(100);
//                    progressBar.setString("Done");
            Collections.sort(firstList);
            Collections.sort(secondList);
            Collections.sort(secondMateralList);
            Collections.sort(secondPateralList);
            if (firstList.size() == 1 && (secondList.size() + secondMateralList.size() + secondPateralList.size() == 0)) {
                af = (int) firstList.get(0);
            } else if (firstList.size() == 0 && (secondList.size() + secondMateralList.size() + secondPateralList.size() == 1)) {
                if (secondList.size() == 1) {
                    as = (int) secondList.get(0);
                }
                if (secondMateralList.size() == 1) {
                    as = (int) secondMateralList.get(0);
                }
                if (secondPateralList.size() == 1) {
                    as = (int) secondPateralList.get(0);
                }
            } else if (firstList.size() >= 2) {
                af1 = (int) firstList.get(0);
                af2 = (int) firstList.get(1);
            } else if (pp.hasMother() && pp.mother.isAffectedBreast == 1 && secondMateralList.size() > 0) {
                amo = pp.mother.affectedBreast;
                amaunt = (int) secondMateralList.get(0);
            } else if (pp.hasMother() && pp.mother.isAffectedBreast == 1 && secondPateralList.size() > 0) {
                amo = pp.mother.affectedBreast;
                apaunt = (int) secondPateralList.get(0);
            } else if (secondPateralList.size() == 1 && secondMateralList.size() == 1) {
                amsdr1 = (int) secondMateralList.get(0);
                apsdr1 = (int) secondPateralList.get(0);
            } else if (secondMateralList.size() >= 2) {
                amsdr1 = (int) secondMateralList.get(0);
                amsdr2 = (int) secondMateralList.get(1);
            } else if (secondPateralList.size() >= 2) {
                apsdr1 = (int) secondPateralList.get(0);
                apsdr2 = (int) secondPateralList.get(1);
            }
//                    REXP res = getWizard().s.eval("hsl_claus <- claus(" + aw + "," + af + "," + as + "," +
//                            af1 + "," + af2 + "," + amo + "," + amaunt + "," + apaunt + "," + amsdr1 + "," +
//                            apsdr1 + "," + amsdr2 + "," + apsdr2 + ")",true);
            Wizard.code.addRCode("hsl_claus <- clausNew(" + aw + "," + af + "," + as + ","
                    + af1 + "," + af2 + "," + amo + "," + amaunt + "," + apaunt + "," + amsdr1 + ","
                    + apsdr1 + "," + amsdr2 + "," + apsdr2 + ",Age.By,Age.To)");
//                    Wizard.caller.setRCode(Wizard.code);
            //execute command
//            Wizard.s.source(new File(currentPath + "/data/run.jev"));
            Wizard.code.addRCode("weight=" + getWizard().getWeight());
            Wizard.code.addRCode("height=" + getWizard().getHeight());
            Wizard.code.addRCode("parity=" + getWizard().getParity());
            Wizard.code.addRCode("age.first=" + getWizard().getAgefirst());
            Wizard.code.addRCode("menarche=" + getWizard().getMenarche());
            Wizard.code.addRCode("mens.cycle=" + getWizard().getMens_cycle());
            Wizard.code.addRCode("FP.method=" + getWizard().getFPmethod());
            Wizard.code.addRCode("smooking=" + getWizard().getSmooking());
            Wizard.code.addRCode("horm.t=" + getWizard().getHorm_t());
            Wizard.code.addRCode("menop=" + getWizard().getMenop());
            Wizard.code.addRCode("lactation=" + getWizard().getLactation());
//            Wizard.code.addRCode("pathFam = \"" + Wizard.Samanda_file + "\"");
            Wizard.code.addRCode("counseleeid = " + Wizard.proband);
//            Wizard.code.addRCode("brcafam = read.csv(\"" + Wizard.Samanda_file + "\")");
            Wizard.code.addRCode(Wizard.Samanda_file);
            System.out.println(Wizard.code.toString());
            //execute command
            //String currentPath = directory.getCanonicalPath().replace("\\","/");
            //Wizard.s.source(new File(currentPath + "/data/run.jev"));
//            StringBuilder runCode = Utils.ReadFile("data", "run.jev");
            Wizard.code.addRCode(Wizard.RUN_COMMAND);
//            Wizard.code.addDoubleMatrix(fTemp, matrix);
//            Wizard.code.addRCode("myparams = brcaparams(penetrance = BRCApenet.samuel, allef = c(allef.b1, allef.b2, allef.p53),\n"
//                    + "				age.by = Age.By, age.to = Age.To,\n"
//                    + "marker.prob =marker.prob,\n"
//                    + "				sensitivity1=Sensitivity1,specificity1=Specificity1, sensitivity2=Sensitivity2,specificity2=Specificity2,\n"
//                    + "				comprisk= compriskSurv)\n"
//                    + "data(brca.fam)");
//            Wizard.code.addRCode("BRCA1 <- BRCA2 <- TestOrder <- rep(0,nrow(brca.fam))\n"
//                    + "germline.testing <- data.frame(BRCA1,BRCA2,TestOrder)\n"
//                    + "germline.testing[2,c(\"BRCA1\",\"TestOrder\")] <- c(2,1)");
//            Wizard.code.addRCode("marker.testing <- data.frame(matrix(rep(0,21*5),ncol=5))\n"
//                    + "colnames(marker.testing) <- c(\"ER\",\"CK14\",\"CK5.6\",\"PR\",\"HER2\")\n"
//                    + "brca.fam[1,\"AffectedBreast\"] <- 1 \n"
//                    + "marker.testing[1,\"ER\"] <- 2 \n"
//                    + "counseleeid=4");            
//            Wizard.code.addRCode("hsl=samanda(family = brca.fam, params = myparams, counselee.id=counseleeid,marker.testing=marker.testing, germline.testing=germline.testing)");
//            Wizard.code.addRCode("hslplot <- new(\"BayesMendel\", family = brca.fam, counselee.id=counseleeid)");
//            Wizard.code.addRCode("hsl_pred = samanda2(family = brca.fam, params = myparams,counselee.id=counseleeid,weight=weight,height=height,parity=parity,age.first=age.first,\n"
//                    + "menarche=menarche,mens.cycle=mens.cycle,FP.method=FP.method,smooking=smooking,horm.t=horm.t,age.menop=menop,lactation=lactation)");
            //Wizard.caller.setGraphicsTheme(null);
            //File file = Wizard.code.startPlot();
            //File file = new File("data/plot.png");
            //
//            Wizard.code.addRCode("png(file=\"" +file.getAbsolutePath().replace("\\", "/")+ "\", width = 768, height = 768, units = \"px\") ");
//
//            Wizard.code.addRCode("plot(hslplot)");
//            Wizard.code.addRCode("dev.off()");
            Wizard.caller.setRCode(Wizard.code);
            //Wizard.code.endPlot();
//            System.out.println(Wizard.code.toString());
            //Wizard.caller.runOnly();
//            System.out.println(Wizard.caller.getParser().getNames());
//            System.out.println("List Object");
//                        Wizard.caller.runAndReturnResultOnline("ls()");
//            String[] ls = Wizard.caller.getParser().getAsStringArray("ls()");
//            for (int i = 0; i < ls.length; i++) {
//                System.out.println(ls[i]);                
//            }      
            Wizard.caller.runAndReturnResultOnline("hsl_pred@hasilSamanda");
//            System.out.println(Wizard.caller.getParser().getNames());
            int[] By_Age = Wizard.caller.getParser().getAsIntArray("By_Age");
//            double[] Breast_Ca_Risk = Wizard.caller.getParser().getAsDoubleArray("Breast_Ca_Risk");
//            double[] Total_Risk = Wizard.caller.getParser().getAsDoubleArray("Total_Risk");
            double[] Breast_Ca_Risk = Wizard.caller.getParser().getAsDoubleArray("Genetic_Risk");
            double[] Breast_Ca_Non_Risk = Wizard.caller.getParser().getAsDoubleArray("Non_Genentik_Risk");
            double[] Total_Risk = Wizard.caller.getParser().getAsDoubleArray("Combine_Risk");
            //Wizard.caller.runAndReturnResultOnline("hr@hasilSamanda");
            Wizard.caller.runAndReturnResultOnline("hsl_pred@hr");
            //System.out.println(Wizard.caller.getParser().getNames());
            double[] HR = Wizard.caller.getParser().getAsDoubleArray("hsl_pred@hr");
//            Wizard.caller.runAndReturnResultOnline("hsl_pred@nonGenRisk");
//            double[] nonGenRisk = Wizard.caller.getParser().getAsDoubleArray("hsl_pred@nonGenRisk");
            Wizard.caller.runAndReturnResultOnline("hsl_claus");
            double[] hsl_claus = Wizard.caller.getParser().getAsDoubleArray("hsl_claus");
            //Wizard.s.
//        REXP res = Wizard.s.eval("hsl_pred@hasilSamanda");
//        //REXP elmt;
//        try {
            //RList ret = res.asList();
//            for(int i=0;i < ret.length;i++)
//            {
            //System.out.println(ret.size());
//            }
//            int cols = ret.size();
//            int rows = ret.at(0).length();
//
//            int[] age = ret.at(0).asIntegers();
//            double[] non = ret.at(1).asDoubles();
//            double[] total = ret.at(2).asDoubles();
            for (int i = 0; i < By_Age.length; i++) {
                // System.out.println(age[i] + " " + non[i] + " " + total[i]);
                sr.setTableAndDataset((int) By_Age[i], Breast_Ca_Risk[i], Breast_Ca_Non_Risk[i],
                        Total_Risk[i], hsl_claus[i]);
            }
            int mywidth = sr.getWidth() - 100;
            int myheight = sr.getHeight() - 100;
//            int mywidth = 1024;
//            int myheight = 768;
//            Wizard.s.eval("hsl_pred@hasilSamanda");
//
//            getWizard().s.toPNG(new File("data/plot.png"), mywidth, myheight, "plot(hslplot)");
            //System.out.println(sr.getPlotsize());
            //System.out.println("Plot will be saved to : " + file);
            //sr.setPlot("data/plot.png", sr.getPlotsize().width, sr.getPlotsize().height);
            //sr.setPlot(file);
//            System.out.println("Plot will be saved to : " + Wizard.Samanda_image);
            sr.setPlot(Wizard.Samanda_image);
            sr.setChart();
            ArrayList<String> sT = new ArrayList<String>();
//            sT.add("Non-genetic Risk Probability is " + Utils.forD.format(nonGenRisk[0]));
            sT.add("Non-genetic Relative Risk is " + Utils.forD.format(HR[0])
                    + (pp.isAffectedBreast > 0 ? " (Contralateral)" : ""));
            sr.setSubtitle(sT);
            //sr.setSubtitle(Double.valueOf(Utils.forD.format((HR[0]-1)*100)));
            //sr.setSubtitle(HR[0]); //ini untuk ngeset subtitle hazard relative
            //sr.setSubtitleClaus(Double.valueOf(Utils.forD.format(hsl_claus[hsl_claus.length -1])));
            sr.SetReportPdf();
            Utils.DeleteFile(fTemp);
            Utils.DeleteFile(fFXTemp);
            Utils.DeleteFile(fFYTemp);
            Utils.DeleteFile(fMXTemp);
            Utils.DeleteFile(fMYTemp);
            Utils.DeleteFile(compriskTemp);
            Utils.DeleteFile(deathTemp);
            Utils.DeleteFile(fDRTemp);
            Utils.DeleteFile(sDRTemp);
            Utils.DeleteFile(fDR2Temp);
            Utils.DeleteFile(mauntTemp);
            Utils.DeleteFile(pauntTemp);
            Utils.DeleteFile(m1p1Temp);
            Utils.DeleteFile(mp2Temp);
            dialog.dispose();
        } catch (IOException ex) {
            Logger.getLogger(SamandaResultDesc.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(SamandaResultDesc.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
