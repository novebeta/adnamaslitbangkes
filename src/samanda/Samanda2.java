/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package samanda;

import DB.CustomersDao;
import DB.GenotypeDao;
import DB.ListPedDao;
import DB.PersonDao;
import DB.SpounseDao;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.nexes.wizards.Wizard;
import com.nexes.wizards.WizardPanelDescriptor;
import com.tomtessier.scrollabledesktop.JScrollableDesktopPane;
import java.awt.Dimension;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Properties;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import pedigree.Utils;
import samanda.properties.LanguagesController;

public class Samanda2 {
    private static String versionMessage = "SAMANDA Litbangkes Versi 4.0B";
    private JScrollableDesktopPane parent;
    public Wizard wizard;
    public LanguagesController lc;
//    Samanda2(JScrollableDesktopPane parentPane, String title, LanguagesController l) {
    Samanda2(String title, LanguagesController l) {
        try {
            lc = l;
            //Utils.insertData("test.sam");
            File configFile = new File("config.properties");
            try {
                FileReader reader = new FileReader(configFile);
                Properties props = new Properties();
                props.load(reader);
                Utils.DATA_DIR = props.getProperty("data") + "/";
                Utils.SETTINGS_URL = "jdbc:sqlite:" + Utils.DATA_DIR + "settings.db3";
                System.out.println(Utils.SETTINGS_URL);
                reader.close();
            } catch (FileNotFoundException ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }
            Utils.connectionSource = new JdbcConnectionSource(Utils.SETTINGS_URL);
//            Utils.connectionSource.
//            Utils.customersDao = DaoManager.createDao(Utils.connectionSource, Customers.class);
            Utils.genotypeDao = new GenotypeDao(Utils.connectionSource);
            Utils.spounsesDao = new SpounseDao(Utils.connectionSource);
            Utils.personDao = new PersonDao(Utils.connectionSource, Utils.genotypeDao, Utils.spounsesDao);
            Utils.listpedDao = new ListPedDao(Utils.connectionSource, Utils.personDao);
            Utils.customersDao = new CustomersDao(Utils.connectionSource, Utils.listpedDao);
//            Utils.genotypeDao = DaoManager.createDao(Utils.connectionSource, Genotype.class); //new GenotypeDao(conn);
//            Utils.spounsesDao = DaoManager.createDao(Utils.connectionSource, Spounse.class);//new SpounseDao(conn);
//            Utils.personDao = DaoManager.createDao(Utils.connectionSource, Person.class);//(conn,genotypeDao,spounsesDao);
//            Utils.listpedDao = DaoManager.createDao(Utils.connectionSource, ListPed.class);//new ListPedDao(conn,personDao);
            Wizard.BACK_TEXT = lc.getWord("general.back");
            Wizard.NEXT_TEXT = lc.getWord("general.next");
            Wizard.CANCEL_TEXT = lc.getWord("general.cancel");
            Wizard.FINISH_TEXT = lc.getWord("general.finish");
//            wizard = new Wizard(parentPane);
            wizard = new Wizard();
            wizard.lc = lc;
            wizard.getDialog().setTitle(versionMessage);
            wizard.getDialog().setResizable(true);
//            wizard.getDialog().setClosable(true);
//            wizard.getDialog().setMaximizable(true);
//            wizard.getDialog().setIconifiable(true);
            wizard.getDialog().setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            //wizard.getDialog().setLocationRelativeTo(wizard.getDialog().getContentPane());
            wizard.getDialog().setPreferredSize(new Dimension(800, 600));
            // wizard.getDialog().revalidate();
            //
            WizardPanelDescriptor welcome = null;
            welcome = new WelcomeDesc();
//        try {
//            welcome = new WelcomeDesc();
//        } catch (IOException e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//        }
            wizard.registerWizardPanel(WelcomeDesc.IDENTIFIER, welcome);
            WizardPanelDescriptor agree = new AgreementDesc(lc);
            wizard.registerWizardPanel(AgreementDesc.IDENTIFIER, agree);
            WizardPanelDescriptor rekam = new RekamMedikDesc(lc);
            wizard.registerWizardPanel(RekamMedikDesc.IDENTIFIER, rekam);
            WizardPanelDescriptor sp = new Samanda2PedDesc(lc);
            wizard.registerWizardPanel(Samanda2PedDesc.IDENTIFIER, sp);
            WizardPanelDescriptor sr = new SamandaResultDesc(lc);
            wizard.registerWizardPanel(SamandaResultDesc.IDENTIFIER, sr);
            // WizardPanelDescriptor pedClaus = new PedigreeClausDsc();
            // wizard.registerWizardPanel(AgreementDesc.IDENTIFIER, pedClaus);
            wizard.setCurrentPanel(WelcomeDesc.IDENTIFIER);
            System.out.println("Setting current pannel");
            //wizard.setModal(false);
//            wizard.getDialog().setLocationRelativeTo(null);
            wizard.getDialog().pack();
            wizard.getDialog().setVisible(true);
//            wizard.getDialog().setSelected(true);
        } catch (SQLException | IOException t) {
            System.out.println(t.toString());
        }
    }
}
