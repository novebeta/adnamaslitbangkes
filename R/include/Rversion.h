/* Rversion.h.  Generated automatically. */
#ifndef R_VERSION_H
#define R_VERSION_H

#ifdef __cplusplus
extern "C" {
#endif

#define R_VERSION 134402
#define R_Version(v,p,s) (((v) * 65536) + ((p) * 256) + (s))
#define R_MAJOR  "2"
#define R_MINOR  "13.2"
#define R_STATUS ""
#define R_YEAR   "2011"
#define R_MONTH  "09"
#define R_DAY    "30"
#define R_SVN_REVISION "57111"
#define R_FILEVERSION    2,132,57111,0

#ifdef __cplusplus
}
#endif

#endif /* not R_VERSION_H */
